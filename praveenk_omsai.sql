-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 15, 2021 at 11:58 AM
-- Server version: 5.7.36
-- PHP Version: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `praveenk_omsai`
--

-- --------------------------------------------------------

--
-- Table structure for table `bank_uploads`
--

CREATE TABLE `bank_uploads` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `demo`
--

CREATE TABLE `demo` (
  `id` int(11) NOT NULL,
  `emp` varchar(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2021_02_15_111547_create_menus_table', 1),
(2, '2021_03_01_100700_create_areas_table', 2),
(3, '2021_03_01_100306_create_user_details_table', 3),
(5, '2021_03_02_063819_create_documents_table', 4),
(6, '2021_03_04_110103_create_pvrs_table', 5),
(7, '2021_03_08_084207_create_companies_table', 6),
(8, '2021_03_12_071050_create_company_i_d_s_table', 6),
(9, '2021_03_15_084330_create_products_table', 6),
(10, '2021_03_15_104315_create_banks_table', 6),
(11, '2021_03_15_131341_create_product_short_names_table', 6),
(12, '2021_03_16_062551_create_bank_uploads_table', 7),
(13, '2021_03_18_094725_create_cash_collections_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('aakash@puretechnology.in', '$2y$10$DXObt68.ipLt4PJnMnslz.bFVYb97nrX0n5QokSQqInKB2DnYkhOK', '2021-02-01 01:42:18'),
('aakash@puretechnology.in', '$2y$10$DXObt68.ipLt4PJnMnslz.bFVYb97nrX0n5QokSQqInKB2DnYkhOK', '2021-02-01 01:42:18'),
('aakash@puretechnology.in', '$2y$10$DXObt68.ipLt4PJnMnslz.bFVYb97nrX0n5QokSQqInKB2DnYkhOK', '2021-02-01 01:42:18'),
('aakash@puretechnology.in', '$2y$10$DXObt68.ipLt4PJnMnslz.bFVYb97nrX0n5QokSQqInKB2DnYkhOK', '2021-02-01 01:42:18'),
('aakash@puretechnology.in', '$2y$10$DXObt68.ipLt4PJnMnslz.bFVYb97nrX0n5QokSQqInKB2DnYkhOK', '2021-02-01 01:42:18'),
('aakash@puretechnology.in', '$2y$10$DXObt68.ipLt4PJnMnslz.bFVYb97nrX0n5QokSQqInKB2DnYkhOK', '2021-02-01 01:42:18'),
('aakash@puretechnology.in', '$2y$10$DXObt68.ipLt4PJnMnslz.bFVYb97nrX0n5QokSQqInKB2DnYkhOK', '2021-02-01 01:42:18'),
('aakash@puretechnology.in', '$2y$10$DXObt68.ipLt4PJnMnslz.bFVYb97nrX0n5QokSQqInKB2DnYkhOK', '2021-02-01 01:42:18'),
('aakash@puretechnology.in', '$2y$10$DXObt68.ipLt4PJnMnslz.bFVYb97nrX0n5QokSQqInKB2DnYkhOK', '2021-02-01 01:42:18'),
('aakash@puretechnology.in', '$2y$10$DXObt68.ipLt4PJnMnslz.bFVYb97nrX0n5QokSQqInKB2DnYkhOK', '2021-02-01 01:42:18'),
('aakash@puretechnology.in', '$2y$10$DXObt68.ipLt4PJnMnslz.bFVYb97nrX0n5QokSQqInKB2DnYkhOK', '2021-02-01 01:42:18'),
('aakash@puretechnology.in', '$2y$10$DXObt68.ipLt4PJnMnslz.bFVYb97nrX0n5QokSQqInKB2DnYkhOK', '2021-02-01 01:42:18'),
('aakash@puretechnology.in', '$2y$10$DXObt68.ipLt4PJnMnslz.bFVYb97nrX0n5QokSQqInKB2DnYkhOK', '2021-02-01 01:42:18'),
('aakash@puretechnology.in', '$2y$10$DXObt68.ipLt4PJnMnslz.bFVYb97nrX0n5QokSQqInKB2DnYkhOK', '2021-02-01 01:42:18'),
('aakash@puretechnology.in', '$2y$10$DXObt68.ipLt4PJnMnslz.bFVYb97nrX0n5QokSQqInKB2DnYkhOK', '2021-02-01 01:42:18'),
('aakash@puretechnology.in', '$2y$10$DXObt68.ipLt4PJnMnslz.bFVYb97nrX0n5QokSQqInKB2DnYkhOK', '2021-02-01 01:42:18'),
('aakash@puretechnology.in', '$2y$10$DXObt68.ipLt4PJnMnslz.bFVYb97nrX0n5QokSQqInKB2DnYkhOK', '2021-02-01 01:42:18'),
('aakash@puretechnology.in', '$2y$10$DXObt68.ipLt4PJnMnslz.bFVYb97nrX0n5QokSQqInKB2DnYkhOK', '2021-02-01 01:42:18'),
('aakash@puretechnology.in', '$2y$10$DXObt68.ipLt4PJnMnslz.bFVYb97nrX0n5QokSQqInKB2DnYkhOK', '2021-02-01 01:42:18'),
('pahiralkar@yopmail.com', '$2y$10$WdvyhW1Kq7wGYZ3CPUHEd.UyRVCoImrYGpAb1dZo5blkqJWDegate', '2021-08-12 03:09:35');

-- --------------------------------------------------------

--
-- Table structure for table `ptx_areas`
--

CREATE TABLE `ptx_areas` (
  `id` int(10) UNSIGNED NOT NULL,
  `area_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pincode` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isDeleted` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ptx_areas`
--

INSERT INTO `ptx_areas` (`id`, `area_name`, `pincode`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'Kothrud', '410090, 412012, 401105', '1', '2021-03-03 05:41:37', '2021-04-06 00:07:25'),
(2, 'Hinjewadi Phase I', '412090', '1', '2021-03-03 05:42:33', '2021-04-06 00:07:35'),
(3, 'Hinjewadi Phase I', '401212', '1', NULL, '2021-05-06 08:34:52'),
(4, 'Bhumkar Chowk', '411403,412045,456123', '1', NULL, '2021-05-06 07:22:58'),
(5, 'VAIBHAV GAD APTS FLAT 7 CTS 123/4 SHIVALI NAGAR APTE RD NEAR HOTEL SHREYAS PUNE', '411004', '1', NULL, '2021-05-06 07:23:50'),
(6, 'Shivaji Nagar', '411004', '1', NULL, '2021-05-06 07:29:17'),
(7, 'DHANKAWADI', '411043', '1', NULL, '2021-05-06 08:34:57'),
(8, 'DHAYARI', '411041', '1', NULL, '2021-05-06 08:35:02'),
(9, 'RASTA PETH', '411011', '1', NULL, '2021-05-06 07:29:39'),
(10, 'BHOSRI', '411039', '1', NULL, '2021-05-06 08:35:07'),
(11, 'PIMPRI', '411018', '1', NULL, '2021-05-06 08:35:12'),
(12, 'YERWADA', '411006', '1', NULL, '2021-05-06 08:35:16'),
(13, 'NANA PETH', '411002', '1', NULL, '2021-05-06 07:29:25'),
(14, 'WADGAON BK', '411041', '1', NULL, '2021-05-06 07:27:45'),
(15, 'PADMAVATI', '411009', '1', NULL, '2021-05-06 07:31:57'),
(16, 'WAGHOLI', '412207', '1', NULL, '2021-05-06 08:35:20'),
(17, 'RASTA PETH', '411011', '1', NULL, '2021-05-06 07:29:45'),
(18, 'SADASHIV PETH', '411030', '1', NULL, '2021-05-06 07:29:51'),
(19, 'KOTHRUD', '411004', '1', NULL, '2021-05-06 07:24:17'),
(20, 'TULJAPUR', '413602', '1', NULL, '2021-05-06 08:35:24'),
(21, 'NAVI SANGAVI', '411027', '1', NULL, '2021-05-06 08:35:28'),
(22, 'Shivane', '411023', '1', NULL, '2021-05-06 08:35:32'),
(23, 'CAMP', '411001', '1', NULL, '2021-05-06 08:35:37'),
(24, 'SHIVAJI NAGAR', '411016', '1', NULL, '2021-05-06 08:35:43'),
(25, 'SHIVAPUR', '412210', '1', NULL, '2021-05-06 08:35:51'),
(26, 'KHED SHIVAPUR', '412205', '1', NULL, '2021-05-06 08:36:00'),
(27, 'KHADKI', '411003', '1', NULL, '2021-05-06 08:36:05'),
(28, 'PARVATI', '411009', '1', NULL, '2021-05-06 08:36:10'),
(29, 'Deccan Gymkhana', '411004', '1', NULL, '2021-05-06 07:24:57'),
(30, 'Shivajinagar', '411005', '1', NULL, '2021-05-06 08:37:53'),
(31, 'Ganeshkhind', '411007', '1', NULL, '2021-05-06 08:37:57'),
(32, 'BANER RO', '411008', '1', NULL, '2021-05-06 08:38:01'),
(33, 'Dapodi', '411012', '1', NULL, '2021-05-06 08:38:14'),
(34, 'Hadapsar', '411028', '1', NULL, '2021-05-06 08:38:17'),
(35, 'Hadapsar Gaon', '411013', '1', NULL, '2021-05-06 08:38:20'),
(36, 'Dighi Camp', '411015', '1', NULL, '2021-05-06 08:38:24'),
(37, 'Pimpri Colony', '411017', '1', NULL, '2021-05-06 08:38:27'),
(38, 'Chinchwad', '411019', '1', NULL, '2021-05-06 08:38:31'),
(39, 'Range Hill', '411020', '1', NULL, '2021-05-06 08:36:26'),
(40, 'Khadakwasala', '411024', '1', NULL, '2021-05-06 08:37:12'),
(41, 'Aundh', '411027', '1', NULL, '2021-05-06 08:37:26'),
(42, 'Kothrud', '411029', '1', NULL, '2021-05-06 08:37:42'),
(43, 'Chinchwad Gaon', '411033', '1', NULL, '2021-05-06 08:37:49'),
(44, 'Kasarwadi', '411034', '1', NULL, '2021-05-06 08:39:06'),
(45, 'Akurdi', '411035', '1', NULL, '2021-05-06 08:39:11'),
(46, 'Mundhwa', '411036', '1', NULL, '2021-05-06 08:39:17'),
(47, 'Market Yard / Gultekadi', '411037', '1', NULL, '2021-05-06 08:38:08'),
(48, 'Bhosari Gaon', '411039', '1', NULL, '2021-05-06 08:39:21'),
(49, 'Wanawadi', '411040', '1', NULL, '2021-05-06 08:39:25'),
(50, 'Swargate', '411042', '1', NULL, '2021-05-06 08:39:29'),
(51, 'Pimpri Chinchwad', '411044', '1', NULL, '2021-05-06 08:39:33'),
(52, 'Pashan', '411045', '1', NULL, '2021-05-06 08:39:37'),
(53, 'Katraj', '411046', '1', NULL, '2021-05-06 08:39:41'),
(54, 'Lohgaon', '411047', '1', NULL, '2021-05-06 08:39:44'),
(55, 'N.I.B.M.', '410048', '1', NULL, '2021-05-06 08:39:52'),
(56, 'Navasahyadri', '411052', '1', NULL, '2021-05-06 08:39:48'),
(57, 'BIBVEWADI', '411037', '1', NULL, '2021-05-06 07:26:12'),
(58, 'BHOR', '412205', '1', NULL, '2021-05-06 08:39:56'),
(59, 'SOMWAR PETH', '411011', '1', NULL, '2021-05-06 07:28:36'),
(60, 'GURUWAR PETH', '411042', '1', NULL, '2021-05-06 08:40:37'),
(61, 'GANESH PETH', '411002', '1', NULL, '2021-05-06 08:36:55'),
(62, 'KOREGAON BHIMA', '412216', '1', NULL, '2021-05-06 08:40:40'),
(63, 'BHAWANI PETH', '411042', '1', NULL, '2021-05-06 08:40:43'),
(64, 'NANDGAON', '412108', '1', NULL, '2021-05-06 08:40:47'),
(65, 'DAUND', '413801', '1', NULL, '2021-05-06 08:40:51'),
(66, 'CHARHOLI', '412105', '1', NULL, '2021-05-06 08:40:55'),
(67, 'MANGALWAR PETH', '411011', '1', NULL, '2021-05-06 08:41:03'),
(68, 'MAWAL', '410406', '1', NULL, '2021-05-06 08:41:07'),
(69, 'PHURSNGI', '412308', '1', NULL, '2021-05-06 08:41:12'),
(70, 'CHANDAN NAGAR', '411014', '1', NULL, '2021-05-06 08:40:05'),
(71, 'CHAKAN', '410501', '1', NULL, '2021-05-06 08:40:17'),
(72, 'BHUDHWAR PETH', '411002', '1', NULL, '2021-05-06 08:40:21'),
(73, 'GOKHALE NAGAR', '411016', '1', NULL, '2021-05-06 08:40:25'),
(74, 'PHURSUNGI', '412308', '1', NULL, '2021-05-06 08:40:28'),
(75, 'ERANDAWANE', '411004', '1', NULL, '2021-05-06 08:40:33'),
(76, 'Chikali', '412114', '1', NULL, '2021-05-06 08:41:15'),
(77, 'Pimple Gurav', '411061', '1', NULL, '2021-05-06 08:41:55'),
(78, 'GANJ PETH', '411042', '1', NULL, '2021-05-06 08:42:00'),
(79, 'PUNE STATION', '411001', '1', NULL, '2021-05-06 08:42:03'),
(80, 'MULSHI', '412415', '1', NULL, '2021-05-06 08:42:07'),
(81, 'KONDHWA', '411048', '1', NULL, '2021-05-06 08:40:59'),
(82, 'NASHIK', '422001', '1', NULL, '2021-05-06 08:42:10'),
(83, 'Talawade', '411062', '1', NULL, '2021-05-06 08:42:14'),
(84, 'SHANIPAR Pune-411002', '411002', '1', NULL, '2021-05-06 08:42:17'),
(85, 'THANE 400612.', '400612.', '1', NULL, '2021-05-06 08:42:21'),
(86, 'AT KUDAVE POST PALASPE TALUKA PANVEL RAIGAD', '410221', '1', NULL, '2021-05-06 08:41:51'),
(87, 'DEHU ROAD PUNE-41201', '41201', '1', NULL, '2021-05-06 08:41:29'),
(88, 'LONAVALA', '410401', '1', NULL, '2021-05-06 08:41:33'),
(89, 'SHIRUR PUNE-412207', '412207', '1', NULL, '2021-05-06 08:41:38'),
(90, 'Mumbai', '400001', '1', NULL, '2021-05-06 08:41:47'),
(91, 'OSMANABAD', '413501', '1', NULL, '2021-05-06 08:42:28'),
(92, 'Pune City', '411011, 411030, 411002, 411004', '1', NULL, '2021-05-06 08:43:29'),
(93, 'SATARA ROAD', '411037,411009', '1', NULL, '2021-05-06 08:42:38'),
(94, 'PUNE CITY', '411001,411002,411004,411005,411011, 411016, 411030, 411042', '0', NULL, NULL),
(95, 'SATARA ROAD', '411009,411037, 411043,411046,411048', '0', NULL, NULL),
(96, 'HADAPSAR', '411013,411028,411022, 411036,411040,411060', '0', NULL, NULL),
(97, 'NAGAR ROAD', '411006,411014,411015, 411032, 411047', '0', NULL, NULL),
(98, 'PCMC', '411003,411007,411008,411012,411017,411018,411019,411020,411026, 411027, 411031,411032, 411033,411034, 411039, 411044, 411045, 411062', '0', NULL, NULL),
(99, 'KOTHRUD SINHAGAD ROAD', '411021,411023,410024, 411041,411051,411052,411058', '0', NULL, NULL),
(100, 'MUMBAI', '400001,400002,400003,400004,400005,400006,400007,400008,400009,400010,400011,400012,400013,400014,400015,400016,400017,400018,400019,400020,400021,400025,400026,400027,400028,400029,400030,400031,400032,400033,400034,400035,400037,400049, 400050, 400051', '0', NULL, NULL),
(101, 'OSMANABAD', '413501', '0', NULL, NULL),
(102, 'HEAD OFFICE PUNE', '411011', '0', NULL, NULL),
(103, 'DAUND BARAMATI', '413801, 413102, 413133', '0', NULL, NULL),
(104, 'NASHIK HEAD OFFICE', '422001', '0', NULL, '2021-08-25 06:45:11'),
(105, 'BARAMATI', '412204,413102,413133', '0', NULL, NULL),
(106, 'THANE HEAD OFFICE', '400602', '0', NULL, NULL),
(107, 'SATARA HEAD OFFICE', '415002', '0', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ptx_assign_banks`
--

CREATE TABLE `ptx_assign_banks` (
  `id` int(10) NOT NULL,
  `user_id` int(11) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `is_agg` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `issue_date` date DEFAULT NULL,
  `exp_date` date DEFAULT NULL,
  `id_card_expiry_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isDeleted` tinyint(4) NOT NULL COMMENT '1 = Deleted, 0 = Not Deleted',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surrender_reason` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surrender_date` date DEFAULT NULL,
  `is_surrender` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ptx_assign_banks`
--

INSERT INTO `ptx_assign_banks` (`id`, `user_id`, `bank_id`, `area_id`, `is_agg`, `issue_date`, `exp_date`, `id_card_expiry_date`, `isDeleted`, `created_at`, `updated_at`, `id_number`, `surrender_reason`, `surrender_date`, `is_surrender`) VALUES
(1, 66, 49, 94, 'Yes', '2021-04-01', '2022-03-31', '', 0, NULL, NULL, '210316735000078', NULL, NULL, 'on'),
(2, 58, 7, 96, 'Yes', NULL, NULL, '', 0, NULL, NULL, NULL, '', NULL, ''),
(3, 65, 56, 94, 'Yes', '2021-05-22', '2022-03-31', '', 0, NULL, NULL, '37307', '', NULL, ''),
(4, 37, 49, 94, 'Yes', '2021-04-30', '2022-03-31', '', 0, NULL, NULL, '210516785000095', '', NULL, ''),
(5, 61, 59, 94, 'Yes', '2021-04-29', '2022-03-31', '', 0, NULL, NULL, '36832', '', NULL, ''),
(6, 60, 48, 94, 'Yes', '2021-08-01', '2021-08-31', '', 0, NULL, NULL, '34567', '', NULL, ''),
(7, 136, 49, 98, 'Yes', '2021-04-01', '2022-03-31', '', 0, NULL, NULL, '210316735000073', '', NULL, ''),
(8, 206, 49, 98, 'Yes', '2021-04-01', '2022-03-31', '', 0, NULL, NULL, '210316735000071', '', NULL, ''),
(9, 204, 49, 94, 'Yes', '2421-04-01', '2022-03-31', '', 0, NULL, NULL, '210316735000075', '', NULL, ''),
(10, 139, 49, 97, 'Yes', '2021-04-01', '2022-03-31', '', 0, NULL, NULL, '210316735000076', '', NULL, ''),
(11, 242, 49, 94, 'Yes', '2021-04-01', '2022-03-31', '', 0, NULL, NULL, '210316735000077', '', NULL, ''),
(12, 66, 49, 94, 'Yes', '2021-04-01', '2022-03-31', '', 0, NULL, NULL, '210316735000078', '', NULL, ''),
(13, 63, 49, 94, 'Yes', '2021-07-01', '2022-03-31', '', 0, NULL, NULL, '210316735000079', '', NULL, ''),
(14, 205, 49, 98, 'Yes', '2021-04-01', '2021-08-31', '', 0, NULL, NULL, '210316735000080', '', NULL, ''),
(15, 82, 49, 99, 'Yes', '2021-04-01', '2022-03-31', '', 0, NULL, NULL, '210416735000083', '', NULL, ''),
(16, 77, 49, 94, 'Yes', '2021-04-01', '2022-03-31', '', 0, NULL, NULL, '210416735000084', '', NULL, ''),
(17, 211, 49, 98, 'Yes', '2021-04-01', '2022-03-31', '', 0, NULL, NULL, '210416735000085', '', NULL, ''),
(18, 71, 49, 94, 'Yes', '2021-04-02', '2022-03-31', '', 0, NULL, NULL, '210416735000087', '', NULL, ''),
(19, 141, 49, 98, 'Yes', '2021-04-09', '2022-03-31', '', 0, NULL, NULL, '210416735000089', '', NULL, ''),
(20, 151, 49, 96, 'Yes', '2021-04-23', '2022-03-31', '', 0, NULL, NULL, '210416735000092', '', NULL, ''),
(21, 108, 49, 95, 'Yes', '2021-04-23', '2022-03-31', '', 0, NULL, NULL, '210416735000093', '', NULL, ''),
(22, 79, 49, 97, 'Yes', '2021-05-01', '2022-03-31', '', 0, NULL, NULL, '210416735000094', '', NULL, ''),
(23, 64, 49, 101, 'Yes', '2021-05-03', '2022-03-31', '', 0, NULL, NULL, '210516735000095', '', NULL, ''),
(24, 72, 49, 98, 'Yes', '2021-05-18', '2022-03-31', '', 0, NULL, NULL, '210516735000097', '', NULL, ''),
(25, 61, 49, 94, 'Yes', '2021-05-19', '2022-03-31', '', 0, NULL, NULL, '210516735000099', '', NULL, ''),
(26, 78, 49, 94, 'Yes', '2021-06-11', '2022-03-31', '', 0, NULL, NULL, '210616735000101', '', NULL, ''),
(27, 142, 49, 94, 'Yes', '2021-06-11', '2022-03-31', '', 0, NULL, NULL, '210616735000102', '', NULL, ''),
(28, 81, 49, 95, 'Yes', '2021-06-19', '2022-03-31', '', 0, NULL, NULL, '210616735000103', '', NULL, ''),
(29, 76, 49, 94, 'Yes', '2021-06-21', '2022-03-31', '', 0, NULL, NULL, '210616735000104', '', NULL, ''),
(30, 75, 49, 96, 'Yes', '2021-06-22', '2021-09-30', '', 0, NULL, NULL, '210616735000105', '', NULL, ''),
(31, 83, 49, 94, 'Yes', '2021-06-22', '2021-08-31', '', 0, NULL, NULL, '210616735000106', '', NULL, ''),
(32, 80, 49, 99, 'Yes', '2021-06-22', '2021-08-31', '', 0, NULL, NULL, '210616735000107', '', NULL, ''),
(33, 37, 54, 94, 'Yes', '2020-09-01', '2021-03-31', '', 0, NULL, NULL, '6107', '', NULL, ''),
(34, 135, 54, 94, 'Yes', '2020-09-01', '2021-03-31', '', 0, NULL, NULL, '6447', '', NULL, ''),
(35, 157, 54, 94, 'Yes', '2020-09-01', '2021-03-31', '', 0, NULL, NULL, '6647', '', NULL, ''),
(36, 139, 54, 97, 'Yes', '2020-09-01', '2021-03-31', '', 0, NULL, NULL, '6446', '', NULL, ''),
(37, 164, 54, 98, 'Yes', '2020-09-01', '2021-03-31', '', 0, NULL, NULL, '6585', '', NULL, ''),
(38, 135, 48, 94, 'Yes', '2021-05-05', '2021-10-04', '', 0, NULL, NULL, '53420', '', NULL, ''),
(39, 147, 48, 98, 'Yes', '2021-05-05', '2021-10-04', '', 0, NULL, NULL, '53421', '', NULL, ''),
(40, 143, 48, 98, 'Yes', '2021-05-05', '2021-10-04', '', 0, NULL, NULL, '53422', '', NULL, ''),
(41, 149, 48, 94, 'Yes', '2021-05-20', '2021-10-19', '', 0, NULL, NULL, '53425', '', NULL, ''),
(42, 142, 48, 94, 'Yes', '2021-05-20', '2021-10-19', '', 0, NULL, NULL, '53426', '', NULL, ''),
(43, 156, 48, 94, 'Yes', '2021-05-20', '2021-10-19', '', 0, NULL, NULL, '53427', '', NULL, ''),
(44, 83, 56, 94, 'Yes', '2021-04-29', '2022-03-31', '', 0, NULL, NULL, '36835', '', NULL, ''),
(45, 106, 56, 94, 'Yes', '2021-04-29', '2022-03-31', '', 0, NULL, NULL, '36836', '', NULL, ''),
(46, 120, 56, 98, 'Yes', '2021-04-29', '2022-03-31', '', 0, NULL, NULL, '36837', '', NULL, ''),
(47, 110, 56, 98, 'Yes', '2021-04-29', '2022-03-31', '', 0, NULL, NULL, '36838', '', NULL, ''),
(48, 74, 56, 94, 'Yes', '2021-04-29', '2022-03-31', '', 0, NULL, NULL, '36839', '', NULL, ''),
(49, 81, 56, 95, 'Yes', '2021-04-29', '2022-03-31', '', 0, NULL, NULL, '36840', '', NULL, ''),
(50, 73, 56, 98, 'Yes', '2021-04-29', '2022-03-31', '', 0, NULL, NULL, '36841', '', NULL, ''),
(51, 72, 56, 98, 'Yes', '2021-04-29', '2022-03-31', '', 0, NULL, NULL, '36842', '', NULL, ''),
(52, 238, 56, 99, 'Yes', '2021-05-22', '2022-03-31', '', 0, NULL, NULL, '37304', '', NULL, ''),
(53, 94, 56, 98, 'Yes', '2021-05-22', '2022-03-31', '', 0, NULL, NULL, '37305', '', NULL, ''),
(54, 82, 56, 99, 'Yes', '2021-05-22', '2022-03-31', '', 0, NULL, NULL, '37300', '', NULL, ''),
(55, 116, 59, 97, 'Yes', '2021-04-29', '2022-03-31', '', 0, NULL, NULL, '36824', '', NULL, ''),
(56, 80, 59, 98, 'Yes', '2021-04-29', '2022-03-31', '', 0, NULL, NULL, '36825', '', NULL, ''),
(57, 75, 59, 96, 'Yes', '2021-04-29', '2022-03-31', '', 0, NULL, NULL, '36826', '', NULL, ''),
(58, 96, 59, 94, 'Yes', '2021-04-29', '2022-03-31', '', 0, NULL, NULL, '36827', '', NULL, ''),
(59, 86, 59, 98, 'Yes', '2021-04-29', '2022-03-31', '', 0, NULL, NULL, '36828', '', NULL, ''),
(60, 109, 59, 94, 'Yes', '2021-04-29', '2022-03-31', '', 0, NULL, NULL, '36829', '', NULL, ''),
(61, 108, 59, 99, 'Yes', '2021-04-29', '2022-03-31', '', 0, NULL, NULL, '36830', '', NULL, ''),
(62, 122, 59, 98, 'Yes', '2021-04-29', '2022-03-31', '', 0, NULL, NULL, '36832', '', NULL, ''),
(63, 36, 59, 94, 'Yes', '2021-04-29', '2022-03-22', '', 0, NULL, NULL, '36833', '', NULL, ''),
(64, 66, 59, 94, 'Yes', '2021-04-29', '2022-03-31', '', 0, NULL, NULL, '36834', '', NULL, ''),
(65, 69, 59, 98, 'Yes', '2021-05-22', '2022-03-31', '', 0, NULL, NULL, '37301', '', NULL, ''),
(66, 84, 59, 98, 'Yes', '2021-05-22', '2022-03-31', '', 0, NULL, NULL, '37302', '', NULL, ''),
(67, 71, 59, 94, 'Yes', '2021-05-22', '2022-03-31', '', 0, NULL, NULL, '37303', '', NULL, ''),
(68, 261, 59, 94, 'Yes', '2021-08-19', '2022-03-31', '', 0, NULL, NULL, '38368', '', NULL, ''),
(69, 273, 59, 99, 'Yes', '2021-08-19', '2022-03-31', '', 0, NULL, NULL, '38367', '', NULL, ''),
(70, 105, 59, 98, 'Yes', '2021-08-19', '2022-03-31', '', 0, NULL, NULL, '38369', '', NULL, ''),
(71, 287, 59, 96, 'Yes', '2021-08-19', '2022-03-31', '', 0, NULL, NULL, '38370', '', NULL, ''),
(72, 222, 64, 94, 'Yes', '2021-04-01', '2022-03-31', '', 0, NULL, NULL, '8882', '', NULL, ''),
(73, 219, 64, 94, 'Yes', '2021-04-01', '2022-03-31', '', 0, NULL, NULL, '8884', '', NULL, ''),
(74, 247, 64, 98, 'Yes', '2021-04-01', '2022-03-31', '', 0, NULL, NULL, '8885', '', NULL, ''),
(75, 217, 64, 95, 'Yes', '2021-04-01', '2021-03-31', '', 0, NULL, NULL, '8888', '', NULL, ''),
(76, 221, 64, 98, 'Yes', '2021-04-01', '2022-03-31', '', 0, NULL, NULL, '8912', '', NULL, ''),
(77, 220, 64, 98, 'Yes', '2021-04-01', '2022-03-31', '', 0, NULL, NULL, '8913', '', NULL, ''),
(78, 223, 64, 103, 'Yes', '2021-04-01', '2022-03-31', '', 0, NULL, NULL, '8914', '', NULL, ''),
(79, 216, 64, 94, 'Yes', '2021-04-01', '2021-03-31', '', 0, NULL, NULL, '8915', '', NULL, ''),
(80, 148, 63, 94, 'Yes', '2021-07-01', '2021-12-31', '', 0, NULL, NULL, 'PUNEAJ015', '', NULL, ''),
(81, 37, 63, 94, 'Yes', '2021-07-01', '2021-12-31', '', 0, NULL, NULL, 'PUNEAJ016', '', NULL, ''),
(82, 147, 63, 98, 'Yes', '2021-07-01', '2021-12-31', '', 0, NULL, NULL, 'PUNEAJ017', '', NULL, ''),
(83, 164, 63, 98, 'Yes', '2021-07-01', '2021-12-31', '', 0, NULL, NULL, 'PUNEAJ019', '', NULL, ''),
(84, 171, 63, 94, 'Yes', '2021-07-01', '2021-12-31', '', 0, NULL, NULL, 'PUNEAJ021', '', NULL, ''),
(85, 48, 63, 94, 'Yes', '2021-07-02', '2021-12-31', '', 0, NULL, NULL, 'PUNEAJ023', '', NULL, ''),
(86, 177, 63, 94, 'Yes', '2021-07-01', '2021-12-30', '', 0, NULL, NULL, 'PUNEAJ024', '', NULL, ''),
(87, 175, 63, 98, 'Yes', '2021-07-01', '2021-12-31', '', 0, NULL, NULL, 'PUNEAJ025', '', NULL, ''),
(88, 208, 63, 98, 'Yes', '2021-07-01', '2021-12-31', '', 0, NULL, NULL, 'PUNEAJ026', '', NULL, ''),
(89, 218, 63, 98, 'Yes', '2021-07-01', '2021-12-31', '', 0, NULL, NULL, 'PUNEAJ028', '', NULL, ''),
(90, 157, 63, 94, 'Yes', '2021-07-01', '2021-12-31', '', 0, NULL, NULL, 'PUNEAJ029', '', NULL, ''),
(91, 225, 63, 98, 'Yes', '2021-07-01', '2021-12-31', '', 0, NULL, NULL, 'PUNEAJ030', '', NULL, ''),
(92, 181, 63, 104, 'Yes', '2021-07-01', '2021-12-31', '', 0, NULL, NULL, 'PUNEAJ031', '', NULL, ''),
(93, 37, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '14839', '', NULL, ''),
(94, 168, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '14852', '', NULL, ''),
(95, 86, 61, 98, 'Yes', NULL, NULL, '', 0, NULL, NULL, '14837', '', NULL, ''),
(96, 205, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '28464', '', NULL, ''),
(97, 175, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '30030', '', NULL, ''),
(98, 230, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '30043', '', NULL, ''),
(99, 148, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '25125', '', NULL, ''),
(100, 213, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '25935', '', NULL, ''),
(101, 243, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '14840', '', NULL, ''),
(102, 82, 61, 99, 'Yes', NULL, NULL, '', 0, NULL, NULL, '14846', '', NULL, ''),
(103, 36, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '14850', '', NULL, ''),
(104, 98, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '34014', '', NULL, ''),
(105, 98, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '34012', '', NULL, ''),
(106, 83, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '14853', '', NULL, ''),
(107, 66, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '14838', '', NULL, ''),
(108, 75, 61, 96, 'Yes', NULL, NULL, '', 0, NULL, NULL, '14841', '', NULL, ''),
(109, 174, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '14849', '', NULL, ''),
(110, 163, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '28998', '', NULL, ''),
(111, 169, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '14847', '', NULL, ''),
(112, 171, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '14844', '', NULL, ''),
(113, 87, 61, 98, 'Yes', NULL, NULL, '', 0, NULL, NULL, '34011', '', NULL, ''),
(114, 96, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '33856', '', NULL, ''),
(115, 101, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '33859', '', NULL, ''),
(116, 106, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '33858', '', NULL, ''),
(117, 95, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '33857', '', NULL, ''),
(118, 109, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '34010', '', NULL, ''),
(119, 99, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '33854', '', NULL, ''),
(120, 72, 61, 98, 'Yes', NULL, NULL, '', 0, NULL, NULL, '39351', '', NULL, ''),
(121, 71, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '41921', '', NULL, ''),
(122, 108, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '41916', '', NULL, ''),
(123, 65, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '419120', '', NULL, ''),
(124, 73, 61, 98, 'Yes', NULL, NULL, '', 0, NULL, NULL, '39431', '', NULL, ''),
(125, 81, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '39378', '', NULL, ''),
(126, 105, 61, 98, 'Yes', NULL, NULL, '', 0, NULL, NULL, '39361', '', NULL, ''),
(127, 107, 61, 97, 'Yes', NULL, NULL, '', 0, NULL, NULL, '39356', '', NULL, ''),
(128, 69, 61, 98, 'Yes', NULL, NULL, '', 0, NULL, NULL, '39436', '', NULL, ''),
(129, 115, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '39372', '', NULL, ''),
(130, 80, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '39448', '', NULL, ''),
(131, 93, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '39442', '', NULL, ''),
(132, 79, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '39479', '', NULL, ''),
(133, 92, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '39417', '', NULL, ''),
(134, 94, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '39426', '', NULL, ''),
(135, 122, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '39375', '', NULL, ''),
(136, 236, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '42731', '', NULL, ''),
(137, 119, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '42710', '', NULL, ''),
(138, 293, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '42734', '', NULL, ''),
(139, 216, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '42724', '', NULL, ''),
(140, 120, 61, 98, 'Yes', NULL, NULL, '', 0, NULL, NULL, '42713', '', NULL, ''),
(141, 238, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '42737', '', NULL, ''),
(142, 110, 61, 94, 'Yes', NULL, NULL, '', 0, NULL, NULL, '42707', '', NULL, ''),
(143, 84, 49, 94, 'Yes', '2021-04-01', '2022-03-31', '', 0, NULL, NULL, '210316735000068', '', NULL, ''),
(144, 36, 49, 94, 'Yes', '2021-05-19', '2022-03-31', '', 0, NULL, NULL, '210516735000098', '', NULL, ''),
(145, 37, 50, 94, 'Yes', '2019-06-22', NULL, '', 0, NULL, NULL, '2019286', '', NULL, ''),
(146, 140, 50, 94, 'Yes', '2019-06-22', NULL, '', 0, NULL, NULL, '2019287', '', NULL, ''),
(147, 155, 50, 94, 'Yes', '2019-06-22', NULL, '', 0, NULL, NULL, '2019288', '', NULL, ''),
(148, 150, 50, 94, 'Yes', '2019-06-22', NULL, '', 0, NULL, NULL, '2019289', '', NULL, ''),
(149, 147, 50, 94, 'Yes', '2019-06-22', NULL, '', 0, NULL, NULL, '2019290', '', NULL, ''),
(150, 225, 50, 94, 'Yes', '2019-06-22', NULL, '', 0, NULL, NULL, '2019293', '', NULL, ''),
(151, 141, 50, 94, 'Yes', '2019-06-22', NULL, '', 0, NULL, NULL, '2019294', '', NULL, ''),
(152, 135, 50, 94, 'Yes', '2019-06-22', NULL, '', 0, NULL, NULL, '2019295', '', NULL, ''),
(153, 165, 50, 94, 'Yes', '2019-06-22', NULL, '', 0, NULL, NULL, '2019296', '', NULL, ''),
(154, 138, 50, 94, 'Yes', '2019-06-22', NULL, '', 0, NULL, NULL, '2019298', '', NULL, ''),
(155, 144, 50, 94, 'Yes', '2019-06-22', NULL, '', 0, NULL, NULL, '2019299', '', NULL, ''),
(156, 192, 50, 94, 'Yes', '2019-06-22', NULL, '', 0, NULL, NULL, '2019301', '', NULL, ''),
(157, 176, 50, 94, 'Yes', '2021-07-12', NULL, '', 0, NULL, NULL, '2020001', '', NULL, ''),
(158, 146, 50, 94, 'Yes', '2021-07-12', NULL, '', 0, NULL, NULL, '2020002', '', NULL, ''),
(159, 137, 50, 94, 'Yes', '2021-07-12', NULL, '', 0, NULL, NULL, '2020004', '', NULL, ''),
(160, 36, 66, 94, 'Yes', '2019-07-03', NULL, '', 0, NULL, NULL, '.317', '', NULL, ''),
(161, 77, 66, 94, 'Yes', '2019-07-03', NULL, '', 0, NULL, NULL, '319', '', NULL, ''),
(162, 120, 66, 94, 'Yes', '2021-04-06', NULL, '', 0, NULL, NULL, '3843', '', NULL, ''),
(163, 105, 66, 94, 'Yes', '2020-11-05', NULL, '', 0, NULL, NULL, '2551', '', NULL, ''),
(164, 65, 66, 94, 'Yes', '2019-07-06', NULL, '', 0, NULL, NULL, '352', '', NULL, ''),
(165, 101, 66, 94, 'Yes', '2020-11-19', NULL, '', 0, NULL, NULL, '2731', '', NULL, ''),
(166, 86, 66, 94, 'Yes', '2020-07-06', NULL, '', 0, NULL, NULL, '1890', '', NULL, ''),
(167, 84, 66, 94, 'Yes', '2019-07-03', NULL, '', 0, NULL, NULL, '323', '', NULL, ''),
(168, 292, 66, 94, 'Yes', '2020-12-09', NULL, '', 0, NULL, NULL, '2923', '', NULL, ''),
(169, 98, 66, 94, 'Yes', '2020-10-05', NULL, '', 0, NULL, NULL, '2388', '', NULL, ''),
(170, 89, 66, 94, 'Yes', '2020-08-07', NULL, '', 0, NULL, NULL, '2002', '', NULL, ''),
(171, 239, 66, 94, 'Yes', '2021-06-08', NULL, '', 0, NULL, NULL, '4315', '', NULL, ''),
(172, 92, 66, 94, 'Yes', '2020-08-14', NULL, '', 0, NULL, NULL, '2052', '', NULL, ''),
(173, 110, 66, 94, 'Yes', '2020-12-14', NULL, '', 0, NULL, NULL, '2875', '', NULL, ''),
(174, 274, 66, 94, 'Yes', '2021-07-08', NULL, '', 0, NULL, NULL, '4670', '', NULL, ''),
(175, 256, 66, 94, 'Yes', '2021-07-08', NULL, '', 0, NULL, NULL, '4671', '', NULL, ''),
(176, 104, 66, 94, 'Yes', '2021-01-18', NULL, '', 0, NULL, NULL, '3250', '', NULL, ''),
(177, 35, 66, 94, 'Yes', '2019-04-18', NULL, '', 0, NULL, NULL, '41', '', NULL, ''),
(178, 300, 66, 94, 'Yes', '2021-07-09', NULL, '', 0, NULL, NULL, '4682', '', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `ptx_attendance`
--

CREATE TABLE `ptx_attendance` (
  `attendance_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `leave_category_id` int(11) DEFAULT '0',
  `date_in` varchar(255) DEFAULT NULL,
  `date_out` varchar(255) DEFAULT NULL,
  `in_time` time DEFAULT NULL,
  `out_time` time DEFAULT NULL,
  `punch_by` varchar(225) DEFAULT NULL,
  `in_location` varchar(225) DEFAULT NULL,
  `out_location` varchar(225) DEFAULT NULL,
  `attendance_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'status 0=absent 1=present 3 = onleave',
  `clocking_status` tinyint(1) NOT NULL DEFAULT '0',
  `punchingcode` int(11) NOT NULL DEFAULT '0',
  `emplyee_type` varchar(250) NOT NULL DEFAULT 'Indoor',
  `site_id` int(11) DEFAULT NULL COMMENT 'Outdoor employee site id ',
  `supervisor_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ptx_attendance`
--

INSERT INTO `ptx_attendance` (`attendance_id`, `user_id`, `leave_category_id`, `date_in`, `date_out`, `in_time`, `out_time`, `punch_by`, `in_location`, `out_location`, `attendance_status`, `clocking_status`, `punchingcode`, `emplyee_type`, `site_id`, `supervisor_id`) VALUES
(1, 6688, 0, '2021-08-01', '2021-08-01', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(2, 6688, 0, '2021-08-02', '2021-08-02', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(3, 6688, 0, '2021-08-03', '2021-08-03', '15:02:32', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(4, 6688, 0, '2021-08-04', '2021-08-04', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(5, 6688, 0, '2021-08-05', '2021-08-05', '14:32:37', '15:43:50', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(6, 6688, 0, '2021-08-06', '2021-08-06', '14:25:44', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(7, 6688, 0, '2021-08-07', '2021-08-07', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(8, 6688, 0, '2021-08-08', '2021-08-08', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(9, 6688, 0, '2021-08-09', '2021-08-09', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(10, 6688, 0, '2021-08-10', '2021-08-10', '16:39:58', '17:01:54', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(11, 6688, 0, '2021-08-11', '2021-08-11', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(12, 6688, 0, '2021-08-12', '2021-08-12', '15:11:39', '16:55:26', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(13, 6688, 0, '2021-08-13', '2021-08-13', '14:59:56', '17:10:49', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(14, 6688, 0, '2021-08-14', '2021-08-14', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(15, 6688, 0, '2021-08-15', '2021-08-15', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(16, 6688, 0, '2021-08-16', '2021-08-16', '13:30:37', '15:41:03', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(17, 6688, 0, '2021-08-17', '2021-08-17', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(18, 6688, 0, '2021-08-18', '2021-08-18', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(19, 6688, 0, '2021-08-19', '2021-08-19', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(20, 6688, 0, '2021-08-20', '2021-08-20', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(21, 6688, 0, '2021-08-21', '2021-08-21', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(22, 6688, 0, '2021-08-22', '2021-08-22', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(23, 6688, 0, '2021-08-23', '2021-08-23', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(24, 6688, 0, '2021-08-24', '2021-08-24', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(25, 6688, 0, '2021-08-25', '2021-08-25', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(26, 6688, 0, '2021-08-26', '2021-08-26', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(27, 6688, 0, '2021-08-27', '2021-08-27', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(28, 6688, 0, '2021-08-28', '2021-08-28', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(29, 6688, 0, '2021-08-29', '2021-08-29', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(30, 6688, 0, '2021-08-30', '2021-08-30', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(31, 6688, 0, '2021-08-31', '2021-08-31', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(32, 51067, 0, '2021-08-01', '2021-08-01', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(33, 51067, 0, '2021-08-02', '2021-08-02', '10:07:53', '17:58:14', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(34, 51067, 0, '2021-08-03', '2021-08-03', '10:07:20', '19:24:06', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(35, 51067, 0, '2021-08-04', '2021-08-04', '10:10:02', '19:27:11', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(36, 51067, 0, '2021-08-05', '2021-08-05', '10:04:08', '18:58:12', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(37, 51067, 0, '2021-08-06', '2021-08-06', '10:01:04', '16:59:46', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(38, 51067, 0, '2021-08-07', '2021-08-07', '10:06:22', '18:42:59', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(39, 51067, 0, '2021-08-08', '2021-08-08', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(40, 51067, 0, '2021-08-09', '2021-08-09', '09:59:02', '18:39:47', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(41, 51067, 0, '2021-08-10', '2021-08-10', '10:02:10', '18:53:28', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(42, 51067, 0, '2021-08-11', '2021-08-11', '10:12:02', '18:19:01', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(43, 51067, 0, '2021-08-12', '2021-08-12', '10:04:15', '18:42:35', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(44, 51067, 0, '2021-08-13', '2021-08-13', '10:09:57', '19:00:01', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(45, 51067, 0, '2021-08-14', '2021-08-14', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(46, 51067, 0, '2021-08-15', '2021-08-15', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(47, 51067, 0, '2021-08-16', '2021-08-16', '09:38:48', '18:46:03', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(48, 51067, 0, '2021-08-17', '2021-08-17', '09:24:47', '19:20:25', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(49, 51067, 0, '2021-08-18', '2021-08-18', '09:35:18', '14:17:09', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(50, 51067, 0, '2021-08-19', '2021-08-19', '09:49:55', '18:50:29', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(51, 51067, 0, '2021-08-20', '2021-08-20', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(52, 51067, 0, '2021-08-21', '2021-08-21', '09:47:03', '18:20:46', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(53, 51067, 0, '2021-08-22', '2021-08-22', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(54, 51067, 0, '2021-08-23', '2021-08-23', '10:20:39', '19:44:01', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(55, 51067, 0, '2021-08-24', '2021-08-24', '09:38:55', '17:58:39', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(56, 51067, 0, '2021-08-25', '2021-08-25', '10:01:53', '19:24:47', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(57, 51067, 0, '2021-08-26', '2021-08-26', '09:30:52', '19:18:33', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(58, 51067, 0, '2021-08-27', '2021-08-27', '10:28:17', '18:52:17', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(59, 51067, 0, '2021-08-28', '2021-08-28', '09:57:49', '19:01:40', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(60, 51067, 0, '2021-08-29', '2021-08-29', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(61, 51067, 0, '2021-08-30', '2021-08-30', '09:52:26', '19:24:19', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(62, 51067, 0, '2021-08-31', '2021-08-31', '10:28:42', '19:34:33', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(63, 51252, 0, '2021-08-01', '2021-08-01', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(64, 51252, 0, '2021-08-02', '2021-08-02', '10:19:39', '19:12:44', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(65, 51252, 0, '2021-08-03', '2021-08-03', '11:00:36', '19:39:34', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(66, 51252, 0, '2021-08-04', '2021-08-04', '11:37:01', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(67, 51252, 0, '2021-08-05', '2021-08-05', '20:13:19', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(68, 51252, 0, '2021-08-06', '2021-08-06', '10:33:45', '19:05:35', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(69, 51252, 0, '2021-08-07', '2021-08-07', '11:35:04', '15:21:54', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(70, 51252, 0, '2021-08-08', '2021-08-08', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(71, 51252, 0, '2021-08-09', '2021-08-09', '12:40:07', '19:38:30', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(72, 51252, 0, '2021-08-10', '2021-08-10', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(73, 51252, 0, '2021-08-11', '2021-08-11', '10:21:57', '19:50:34', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(74, 51252, 0, '2021-08-12', '2021-08-12', '10:05:54', '19:43:47', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(75, 51252, 0, '2021-08-13', '2021-08-13', '10:12:52', '14:14:17', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(76, 51252, 0, '2021-08-14', '2021-08-14', '10:10:12', '13:58:07', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(77, 51252, 0, '2021-08-15', '2021-08-15', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(78, 51252, 0, '2021-08-16', '2021-08-16', '10:37:25', '19:39:03', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(79, 51252, 0, '2021-08-17', '2021-08-17', '10:22:32', '19:31:17', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(80, 51252, 0, '2021-08-18', '2021-08-18', '10:35:06', '20:29:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(81, 51252, 0, '2021-08-19', '2021-08-19', '10:31:44', '19:01:18', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(82, 51252, 0, '2021-08-20', '2021-08-20', '10:48:44', '19:57:13', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(83, 51252, 0, '2021-08-21', '2021-08-21', '10:38:02', '19:19:28', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(84, 51252, 0, '2021-08-22', '2021-08-22', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(85, 51252, 0, '2021-08-23', '2021-08-23', '10:23:48', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(86, 51252, 0, '2021-08-24', '2021-08-24', '10:08:01', '20:22:40', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(87, 51252, 0, '2021-08-25', '2021-08-25', '10:35:24', '20:32:22', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(88, 51252, 0, '2021-08-26', '2021-08-26', '11:45:03', '19:53:25', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(89, 51252, 0, '2021-08-27', '2021-08-27', '11:05:31', '19:57:26', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(90, 51252, 0, '2021-08-28', '2021-08-28', '10:41:11', '20:17:27', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(91, 51252, 0, '2021-08-29', '2021-08-29', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(92, 51252, 0, '2021-08-30', '2021-08-30', '10:47:32', '20:22:53', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(93, 51252, 0, '2021-08-31', '2021-08-31', '12:31:25', '21:24:38', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(94, 51633, 0, '2021-08-01', '2021-08-01', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(95, 51633, 0, '2021-08-02', '2021-08-02', '12:08:49', '17:23:26', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(96, 51633, 0, '2021-08-03', '2021-08-03', '10:32:14', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(97, 51633, 0, '2021-08-04', '2021-08-04', '10:57:25', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(98, 51633, 0, '2021-08-05', '2021-08-05', '10:22:24', '19:09:21', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(99, 51633, 0, '2021-08-06', '2021-08-06', '10:41:32', '18:50:53', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(100, 51633, 0, '2021-08-07', '2021-08-07', '10:42:20', '19:14:12', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(101, 51633, 0, '2021-08-08', '2021-08-08', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(102, 51633, 0, '2021-08-09', '2021-08-09', '11:22:02', '18:59:23', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(103, 51633, 0, '2021-08-10', '2021-08-10', '10:34:44', '19:33:17', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(104, 51633, 0, '2021-08-11', '2021-08-11', '10:03:37', '19:01:26', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(105, 51633, 0, '2021-08-12', '2021-08-12', '10:26:34', '18:56:27', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(106, 51633, 0, '2021-08-13', '2021-08-13', '10:43:58', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(107, 51633, 0, '2021-08-14', '2021-08-14', '10:41:12', '16:30:36', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(108, 51633, 0, '2021-08-15', '2021-08-15', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(109, 51633, 0, '2021-08-16', '2021-08-16', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(110, 51633, 0, '2021-08-17', '2021-08-17', '10:38:31', '19:09:31', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(111, 51633, 0, '2021-08-18', '2021-08-18', '10:43:13', '19:35:11', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(112, 51633, 0, '2021-08-19', '2021-08-19', '10:56:49', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(113, 51633, 0, '2021-08-20', '2021-08-20', '10:48:48', '19:11:06', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(114, 51633, 0, '2021-08-21', '2021-08-21', '10:41:24', '18:52:06', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(115, 51633, 0, '2021-08-22', '2021-08-22', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(116, 51633, 0, '2021-08-23', '2021-08-23', '10:45:06', '18:54:04', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(117, 51633, 0, '2021-08-24', '2021-08-24', '11:09:32', '19:41:47', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(118, 51633, 0, '2021-08-25', '2021-08-25', '10:38:52', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(119, 51633, 0, '2021-08-26', '2021-08-26', '09:14:56', '19:38:09', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(120, 51633, 0, '2021-08-27', '2021-08-27', '10:42:47', '19:38:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(121, 51633, 0, '2021-08-28', '2021-08-28', '10:49:27', '19:24:40', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(122, 51633, 0, '2021-08-29', '2021-08-29', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(123, 51633, 0, '2021-08-30', '2021-08-30', '10:32:47', '19:16:56', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(124, 51633, 0, '2021-08-31', '2021-08-31', '12:12:49', '21:18:43', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(125, 51658, 0, '2021-08-01', '2021-08-01', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(126, 51658, 0, '2021-08-02', '2021-08-02', '09:52:38', '18:28:28', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(127, 51658, 0, '2021-08-03', '2021-08-03', '10:06:20', '18:31:05', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(128, 51658, 0, '2021-08-04', '2021-08-04', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(129, 51658, 0, '2021-08-05', '2021-08-05', '09:59:16', '19:12:31', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(130, 51658, 0, '2021-08-06', '2021-08-06', '11:51:05', '18:47:19', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(131, 51658, 0, '2021-08-07', '2021-08-07', '12:21:58', '19:18:33', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(132, 51658, 0, '2021-08-08', '2021-08-08', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(133, 51658, 0, '2021-08-09', '2021-08-09', '12:05:25', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(134, 51658, 0, '2021-08-10', '2021-08-10', '12:04:19', '18:41:08', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(135, 51658, 0, '2021-08-11', '2021-08-11', '12:29:21', '19:10:49', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(136, 51658, 0, '2021-08-12', '2021-08-12', '09:37:01', '19:10:33', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(137, 51658, 0, '2021-08-13', '2021-08-13', '12:33:38', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(138, 51658, 0, '2021-08-14', '2021-08-14', '12:39:40', '19:29:02', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(139, 51658, 0, '2021-08-15', '2021-08-15', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(140, 51658, 0, '2021-08-16', '2021-08-16', '09:33:27', '19:22:32', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(141, 51658, 0, '2021-08-17', '2021-08-17', '09:32:31', '19:28:12', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(142, 51658, 0, '2021-08-18', '2021-08-18', '12:04:24', '19:31:13', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(143, 51658, 0, '2021-08-19', '2021-08-19', '12:33:30', '18:49:09', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(144, 51658, 0, '2021-08-20', '2021-08-20', '12:19:59', '19:09:28', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(145, 51658, 0, '2021-08-21', '2021-08-21', '12:06:38', '18:54:21', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(146, 51658, 0, '2021-08-22', '2021-08-22', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(147, 51658, 0, '2021-08-23', '2021-08-23', '12:01:52', '19:21:36', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(148, 51658, 0, '2021-08-24', '2021-08-24', '12:21:59', '19:20:47', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(149, 51658, 0, '2021-08-25', '2021-08-25', '12:30:05', '19:24:53', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(150, 51658, 0, '2021-08-26', '2021-08-26', '12:17:19', '19:41:28', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(151, 51658, 0, '2021-08-27', '2021-08-27', '12:40:02', '19:38:06', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(152, 51658, 0, '2021-08-28', '2021-08-28', '13:04:28', '19:32:49', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(153, 51658, 0, '2021-08-29', '2021-08-29', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(154, 51658, 0, '2021-08-30', '2021-08-30', '09:21:32', '19:25:44', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(155, 51658, 0, '2021-08-31', '2021-08-31', '12:35:42', '21:25:11', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(156, 51681, 0, '2021-08-01', '2021-08-01', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(157, 51681, 0, '2021-08-02', '2021-08-02', '16:15:01', '18:28:02', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(158, 51681, 0, '2021-08-03', '2021-08-03', '11:35:29', '19:25:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(159, 51681, 0, '2021-08-04', '2021-08-04', '10:57:43', '18:50:28', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(160, 51681, 0, '2021-08-05', '2021-08-05', '19:11:23', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(161, 51681, 0, '2021-08-06', '2021-08-06', '11:20:56', '18:50:49', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(162, 51681, 0, '2021-08-07', '2021-08-07', '10:56:31', '19:17:02', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(163, 51681, 0, '2021-08-08', '2021-08-08', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(164, 51681, 0, '2021-08-09', '2021-08-09', '17:32:54', '18:53:47', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(165, 51681, 0, '2021-08-10', '2021-08-10', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(166, 51681, 0, '2021-08-11', '2021-08-11', '19:05:07', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(167, 51681, 0, '2021-08-12', '2021-08-12', '10:46:52', '15:47:32', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(168, 51681, 0, '2021-08-13', '2021-08-13', '12:11:32', '19:07:04', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(169, 51681, 0, '2021-08-14', '2021-08-14', '15:09:20', '19:28:15', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(170, 51681, 0, '2021-08-15', '2021-08-15', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(171, 51681, 0, '2021-08-16', '2021-08-16', '10:48:28', '19:18:13', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(172, 51681, 0, '2021-08-17', '2021-08-17', '10:46:32', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(173, 51681, 0, '2021-08-18', '2021-08-18', '10:55:35', '19:29:46', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(174, 51681, 0, '2021-08-19', '2021-08-19', '10:37:09', '18:48:49', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(175, 51681, 0, '2021-08-20', '2021-08-20', '12:45:46', '16:12:05', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(176, 51681, 0, '2021-08-21', '2021-08-21', '11:08:09', '18:51:59', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(177, 51681, 0, '2021-08-22', '2021-08-22', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(178, 51681, 0, '2021-08-23', '2021-08-23', '11:03:58', '19:52:41', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(179, 51681, 0, '2021-08-24', '2021-08-24', '11:04:07', '19:23:19', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(180, 51681, 0, '2021-08-25', '2021-08-25', '14:43:42', '19:27:11', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(181, 51681, 0, '2021-08-26', '2021-08-26', '19:43:26', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(182, 51681, 0, '2021-08-27', '2021-08-27', '10:28:21', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(183, 51681, 0, '2021-08-28', '2021-08-28', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(184, 51681, 0, '2021-08-29', '2021-08-29', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(185, 51681, 0, '2021-08-30', '2021-08-30', '10:58:09', '19:24:26', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(186, 51681, 0, '2021-08-31', '2021-08-31', '10:26:35', '20:32:34', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(187, 51808, 0, '2021-08-01', '2021-08-01', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(188, 51808, 0, '2021-08-02', '2021-08-02', '17:53:54', '18:00:27', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(189, 51808, 0, '2021-08-03', '2021-08-03', '10:14:50', '18:22:24', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(190, 51808, 0, '2021-08-04', '2021-08-04', '09:56:59', '18:16:46', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(191, 51808, 0, '2021-08-05', '2021-08-05', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(192, 51808, 0, '2021-08-06', '2021-08-06', '09:49:49', '18:41:55', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(193, 51808, 0, '2021-08-07', '2021-08-07', '09:58:26', '14:42:35', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(194, 51808, 0, '2021-08-08', '2021-08-08', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(195, 51808, 0, '2021-08-09', '2021-08-09', '09:57:56', '18:33:21', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(196, 51808, 0, '2021-08-10', '2021-08-10', '09:48:15', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(197, 51808, 0, '2021-08-11', '2021-08-11', '09:52:31', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(198, 51808, 0, '2021-08-12', '2021-08-12', '09:39:53', '18:46:17', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(199, 51808, 0, '2021-08-13', '2021-08-13', '09:38:27', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(200, 51808, 0, '2021-08-14', '2021-08-14', '09:52:48', '18:34:11', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(201, 51808, 0, '2021-08-15', '2021-08-15', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(202, 51808, 0, '2021-08-16', '2021-08-16', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(203, 51808, 0, '2021-08-17', '2021-08-17', '10:05:50', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(204, 51808, 0, '2021-08-18', '2021-08-18', '09:39:13', '12:01:42', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(205, 51808, 0, '2021-08-19', '2021-08-19', '09:53:50', '18:31:06', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(206, 51808, 0, '2021-08-20', '2021-08-20', '09:39:19', '14:33:39', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(207, 51808, 0, '2021-08-21', '2021-08-21', '10:33:01', '18:32:46', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(208, 51808, 0, '2021-08-22', '2021-08-22', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(209, 51808, 0, '2021-08-23', '2021-08-23', '09:40:58', '19:17:24', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(210, 51808, 0, '2021-08-24', '2021-08-24', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(211, 51808, 0, '2021-08-25', '2021-08-25', '19:14:58', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(212, 51808, 0, '2021-08-26', '2021-08-26', '09:49:01', '19:24:50', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(213, 51808, 0, '2021-08-27', '2021-08-27', '09:49:28', '16:56:27', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(214, 51808, 0, '2021-08-28', '2021-08-28', '09:46:10', '18:28:46', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(215, 51808, 0, '2021-08-29', '2021-08-29', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(216, 51808, 0, '2021-08-30', '2021-08-30', '09:39:53', '19:14:46', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(217, 51808, 0, '2021-08-31', '2021-08-31', '09:35:20', '16:17:42', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(218, 51961, 0, '2021-08-01', '2021-08-01', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(219, 51961, 0, '2021-08-02', '2021-08-02', '10:15:13', '18:07:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(220, 51961, 0, '2021-08-03', '2021-08-03', '10:23:02', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(221, 51961, 0, '2021-08-04', '2021-08-04', '10:18:02', '19:12:44', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(222, 51961, 0, '2021-08-05', '2021-08-05', '10:12:55', '18:31:04', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(223, 51961, 0, '2021-08-06', '2021-08-06', '10:02:18', '19:04:14', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(224, 51961, 0, '2021-08-07', '2021-08-07', '17:47:35', '18:36:50', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(225, 51961, 0, '2021-08-08', '2021-08-08', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(226, 51961, 0, '2021-08-09', '2021-08-09', '10:05:53', '18:55:50', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(227, 51961, 0, '2021-08-10', '2021-08-10', '09:51:10', '18:45:29', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(228, 51961, 0, '2021-08-11', '2021-08-11', '09:56:33', '18:58:29', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(229, 51961, 0, '2021-08-12', '2021-08-12', '09:56:56', '18:56:18', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(230, 51961, 0, '2021-08-13', '2021-08-13', '09:57:30', '19:03:58', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(231, 51961, 0, '2021-08-14', '2021-08-14', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(232, 51961, 0, '2021-08-15', '2021-08-15', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(233, 51961, 0, '2021-08-16', '2021-08-16', '10:00:07', '19:09:50', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(234, 51961, 0, '2021-08-17', '2021-08-17', '10:18:16', '19:43:21', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(235, 51961, 0, '2021-08-18', '2021-08-18', '11:52:06', '19:37:54', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(236, 51961, 0, '2021-08-19', '2021-08-19', '12:20:54', '18:44:33', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(237, 51961, 0, '2021-08-20', '2021-08-20', '09:59:18', '19:27:39', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(238, 51961, 0, '2021-08-21', '2021-08-21', '10:15:01', '17:32:59', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(239, 51961, 0, '2021-08-22', '2021-08-22', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(240, 51961, 0, '2021-08-23', '2021-08-23', '12:06:14', '19:36:43', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(241, 51961, 0, '2021-08-24', '2021-08-24', '10:23:29', '19:32:45', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(242, 51961, 0, '2021-08-25', '2021-08-25', '10:04:50', '19:25:28', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(243, 51961, 0, '2021-08-26', '2021-08-26', '10:10:48', '19:24:08', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(244, 51961, 0, '2021-08-27', '2021-08-27', '10:12:01', '19:16:19', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(245, 51961, 0, '2021-08-28', '2021-08-28', '09:53:25', '19:05:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(246, 51961, 0, '2021-08-29', '2021-08-29', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(247, 51961, 0, '2021-08-30', '2021-08-30', '10:07:20', '20:19:12', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(248, 51961, 0, '2021-08-31', '2021-08-31', '10:09:25', '20:49:10', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(249, 52058, 0, '2021-08-01', '2021-08-01', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(250, 52058, 0, '2021-08-02', '2021-08-02', '09:15:51', '18:28:16', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(251, 52058, 0, '2021-08-03', '2021-08-03', '09:37:43', '18:30:50', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(252, 52058, 0, '2021-08-04', '2021-08-04', '09:42:42', '18:50:20', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(253, 52058, 0, '2021-08-05', '2021-08-05', '19:11:22', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(254, 52058, 0, '2021-08-06', '2021-08-06', '09:03:56', '18:47:28', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(255, 52058, 0, '2021-08-07', '2021-08-07', '09:11:17', '19:16:55', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(256, 52058, 0, '2021-08-08', '2021-08-08', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(257, 52058, 0, '2021-08-09', '2021-08-09', '09:03:17', '18:48:52', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(258, 52058, 0, '2021-08-10', '2021-08-10', '09:23:34', '18:40:57', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(259, 52058, 0, '2021-08-11', '2021-08-11', '09:26:15', '19:08:26', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(260, 52058, 0, '2021-08-12', '2021-08-12', '09:25:16', '19:10:27', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(261, 52058, 0, '2021-08-13', '2021-08-13', '09:04:00', '19:05:39', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(262, 52058, 0, '2021-08-14', '2021-08-14', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(263, 52058, 0, '2021-08-15', '2021-08-15', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(264, 52058, 0, '2021-08-16', '2021-08-16', '09:14:59', '19:17:54', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(265, 52058, 0, '2021-08-17', '2021-08-17', '09:15:00', '19:28:03', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(266, 52058, 0, '2021-08-18', '2021-08-18', '09:05:47', '19:30:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(267, 52058, 0, '2021-08-19', '2021-08-19', '09:08:51', '18:48:46', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(268, 52058, 0, '2021-08-20', '2021-08-20', '09:10:06', '19:09:17', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(269, 52058, 0, '2021-08-21', '2021-08-21', '09:14:32', '18:54:05', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(270, 52058, 0, '2021-08-22', '2021-08-22', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(271, 52058, 0, '2021-08-23', '2021-08-23', '09:19:26', '19:20:27', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(272, 52058, 0, '2021-08-24', '2021-08-24', '09:00:11', '19:20:39', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(273, 52058, 0, '2021-08-25', '2021-08-25', '09:06:35', '19:23:23', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(274, 52058, 0, '2021-08-26', '2021-08-26', '08:57:43', '19:39:35', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(275, 52058, 0, '2021-08-27', '2021-08-27', '08:59:38', '19:37:53', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(276, 52058, 0, '2021-08-28', '2021-08-28', '09:11:43', '19:32:31', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(277, 52058, 0, '2021-08-29', '2021-08-29', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(278, 52058, 0, '2021-08-30', '2021-08-30', '08:59:54', '19:24:35', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(279, 52058, 0, '2021-08-31', '2021-08-31', '09:01:52', '21:24:58', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(280, 52089, 0, '2021-08-01', '2021-08-01', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(281, 52089, 0, '2021-08-02', '2021-08-02', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(282, 52089, 0, '2021-08-03', '2021-08-03', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(283, 52089, 0, '2021-08-04', '2021-08-04', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(284, 52089, 0, '2021-08-05', '2021-08-05', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(285, 52089, 0, '2021-08-06', '2021-08-06', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(286, 52089, 0, '2021-08-07', '2021-08-07', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(287, 52089, 0, '2021-08-08', '2021-08-08', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(288, 52089, 0, '2021-08-09', '2021-08-09', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(289, 52089, 0, '2021-08-10', '2021-08-10', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(290, 52089, 0, '2021-08-11', '2021-08-11', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(291, 52089, 0, '2021-08-12', '2021-08-12', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(292, 52089, 0, '2021-08-13', '2021-08-13', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(293, 52089, 0, '2021-08-14', '2021-08-14', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(294, 52089, 0, '2021-08-15', '2021-08-15', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(295, 52089, 0, '2021-08-16', '2021-08-16', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(296, 52089, 0, '2021-08-17', '2021-08-17', '19:34:23', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(297, 52089, 0, '2021-08-18', '2021-08-18', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(298, 52089, 0, '2021-08-19', '2021-08-19', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(299, 52089, 0, '2021-08-20', '2021-08-20', '16:20:13', '18:37:19', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(300, 52089, 0, '2021-08-21', '2021-08-21', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(301, 52089, 0, '2021-08-22', '2021-08-22', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(302, 52089, 0, '2021-08-23', '2021-08-23', '16:53:55', '17:42:39', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(303, 52089, 0, '2021-08-24', '2021-08-24', '14:08:38', '16:06:15', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(304, 52089, 0, '2021-08-25', '2021-08-25', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(305, 52089, 0, '2021-08-26', '2021-08-26', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(306, 52089, 0, '2021-08-27', '2021-08-27', '15:54:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(307, 52089, 0, '2021-08-28', '2021-08-28', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(308, 52089, 0, '2021-08-29', '2021-08-29', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(309, 52089, 0, '2021-08-30', '2021-08-30', '16:15:15', '18:17:33', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(310, 52089, 0, '2021-08-31', '2021-08-31', '14:58:56', '18:00:01', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(311, 52090, 0, '2021-08-01', '2021-08-01', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(312, 52090, 0, '2021-08-02', '2021-08-02', '10:11:57', '17:57:04', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(313, 52090, 0, '2021-08-03', '2021-08-03', '09:54:54', '19:24:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(314, 52090, 0, '2021-08-04', '2021-08-04', '10:09:57', '19:13:14', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(315, 52090, 0, '2021-08-05', '2021-08-05', '10:17:01', '18:45:44', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(316, 52090, 0, '2021-08-06', '2021-08-06', '10:02:36', '18:51:09', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(317, 52090, 0, '2021-08-07', '2021-08-07', '10:04:11', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(318, 52090, 0, '2021-08-08', '2021-08-08', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(319, 52090, 0, '2021-08-09', '2021-08-09', '12:28:46', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(320, 52090, 0, '2021-08-10', '2021-08-10', '10:05:06', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(321, 52090, 0, '2021-08-11', '2021-08-11', '10:07:52', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(322, 52090, 0, '2021-08-12', '2021-08-12', '10:06:36', '18:44:11', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(323, 52090, 0, '2021-08-13', '2021-08-13', '10:16:20', '19:03:53', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(324, 52090, 0, '2021-08-14', '2021-08-14', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(325, 52090, 0, '2021-08-15', '2021-08-15', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(326, 52090, 0, '2021-08-16', '2021-08-16', '09:58:53', '18:46:14', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(327, 52090, 0, '2021-08-17', '2021-08-17', '10:02:17', '19:26:44', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(328, 52090, 0, '2021-08-18', '2021-08-18', '10:07:18', '19:11:07', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(329, 52090, 0, '2021-08-19', '2021-08-19', '10:02:38', '18:50:10', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(330, 52090, 0, '2021-08-20', '2021-08-20', '09:54:42', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(331, 52090, 0, '2021-08-21', '2021-08-21', '09:59:40', '18:25:01', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(332, 52090, 0, '2021-08-22', '2021-08-22', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(333, 52090, 0, '2021-08-23', '2021-08-23', '10:08:18', '18:49:58', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(334, 52090, 0, '2021-08-24', '2021-08-24', '10:03:08', '19:08:07', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(335, 52090, 0, '2021-08-25', '2021-08-25', '10:11:57', '19:24:40', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(336, 52090, 0, '2021-08-26', '2021-08-26', '10:08:26', '19:32:11', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(337, 52090, 0, '2021-08-27', '2021-08-27', '10:16:37', '18:58:24', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(338, 52090, 0, '2021-08-28', '2021-08-28', '09:51:29', '18:51:23', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(339, 52090, 0, '2021-08-29', '2021-08-29', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(340, 52090, 0, '2021-08-30', '2021-08-30', '10:07:14', '19:12:03', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(341, 52090, 0, '2021-08-31', '2021-08-31', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(342, 52101, 0, '2021-08-01', '2021-08-01', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(343, 52101, 0, '2021-08-02', '2021-08-02', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(344, 52101, 0, '2021-08-03', '2021-08-03', '11:30:31', '18:17:31', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(345, 52101, 0, '2021-08-04', '2021-08-04', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(346, 52101, 0, '2021-08-05', '2021-08-05', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(347, 52101, 0, '2021-08-06', '2021-08-06', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(348, 52101, 0, '2021-08-07', '2021-08-07', '10:21:49', '19:12:17', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(349, 52101, 0, '2021-08-08', '2021-08-08', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(350, 52101, 0, '2021-08-09', '2021-08-09', '11:06:14', '19:38:11', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(351, 52101, 0, '2021-08-10', '2021-08-10', '10:37:07', '20:02:41', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(352, 52101, 0, '2021-08-11', '2021-08-11', '10:21:41', '19:12:47', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(353, 52101, 0, '2021-08-12', '2021-08-12', '11:52:56', '20:06:22', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(354, 52101, 0, '2021-08-13', '2021-08-13', '10:20:51', '20:02:34', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(355, 52101, 0, '2021-08-14', '2021-08-14', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(356, 52101, 0, '2021-08-15', '2021-08-15', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(357, 52101, 0, '2021-08-16', '2021-08-16', '10:38:06', '20:11:03', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(358, 52101, 0, '2021-08-17', '2021-08-17', '10:45:04', '19:44:25', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(359, 52101, 0, '2021-08-18', '2021-08-18', '09:40:52', '19:37:33', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(360, 52101, 0, '2021-08-19', '2021-08-19', '09:50:37', '21:04:38', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(361, 52101, 0, '2021-08-20', '2021-08-20', '10:20:16', '19:59:34', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(362, 52101, 0, '2021-08-21', '2021-08-21', '10:27:58', '20:42:06', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(363, 52101, 0, '2021-08-22', '2021-08-22', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(364, 52101, 0, '2021-08-23', '2021-08-23', '10:29:47', '19:58:22', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(365, 52101, 0, '2021-08-24', '2021-08-24', '11:55:55', '20:23:22', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(366, 52101, 0, '2021-08-25', '2021-08-25', '10:41:05', '19:37:14', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(367, 52101, 0, '2021-08-26', '2021-08-26', '10:14:58', '19:50:17', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(368, 52101, 0, '2021-08-27', '2021-08-27', '10:39:25', '20:19:42', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(369, 52101, 0, '2021-08-28', '2021-08-28', '10:43:18', '20:17:04', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(370, 52101, 0, '2021-08-29', '2021-08-29', '15:36:59', '16:17:16', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(371, 52101, 0, '2021-08-30', '2021-08-30', '10:25:54', '20:23:39', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(372, 52101, 0, '2021-08-31', '2021-08-31', '10:58:22', '20:47:38', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(373, 52171, 0, '2021-08-01', '2021-08-01', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(374, 52171, 0, '2021-08-02', '2021-08-02', '09:27:36', '18:07:15', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(375, 52171, 0, '2021-08-03', '2021-08-03', '09:27:33', '18:19:32', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(376, 52171, 0, '2021-08-04', '2021-08-04', '09:32:41', '19:12:52', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(377, 52171, 0, '2021-08-05', '2021-08-05', '09:32:19', '18:58:25', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(378, 52171, 0, '2021-08-06', '2021-08-06', '09:19:23', '19:04:33', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(379, 52171, 0, '2021-08-07', '2021-08-07', '09:37:13', '19:12:43', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(380, 52171, 0, '2021-08-08', '2021-08-08', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(381, 52171, 0, '2021-08-09', '2021-08-09', '09:34:58', '19:38:44', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(382, 52171, 0, '2021-08-10', '2021-08-10', '09:47:13', '18:51:45', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(383, 52171, 0, '2021-08-11', '2021-08-11', '09:58:39', '19:00:14', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(384, 52171, 0, '2021-08-12', '2021-08-12', '09:52:26', '20:05:50', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(385, 52171, 0, '2021-08-13', '2021-08-13', '09:38:56', '19:00:06', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(386, 52171, 0, '2021-08-14', '2021-08-14', '10:00:21', '18:49:41', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(387, 52171, 0, '2021-08-15', '2021-08-15', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(388, 52171, 0, '2021-08-16', '2021-08-16', '09:33:31', '20:12:29', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(389, 52171, 0, '2021-08-17', '2021-08-17', '09:42:39', '19:47:54', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(390, 52171, 0, '2021-08-18', '2021-08-18', '10:07:22', '19:37:38', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(391, 52171, 0, '2021-08-19', '2021-08-19', '09:24:53', '21:05:42', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(392, 52171, 0, '2021-08-20', '2021-08-20', '10:04:46', '15:36:54', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(393, 52171, 0, '2021-08-21', '2021-08-21', '09:33:35', '19:09:31', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(394, 52171, 0, '2021-08-22', '2021-08-22', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(395, 52171, 0, '2021-08-23', '2021-08-23', '09:40:53', '19:13:52', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(396, 52171, 0, '2021-08-24', '2021-08-24', '09:51:39', '20:22:25', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(397, 52171, 0, '2021-08-25', '2021-08-25', '09:56:18', '19:37:46', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(398, 52171, 0, '2021-08-26', '2021-08-26', '09:48:23', '19:50:26', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(399, 52171, 0, '2021-08-27', '2021-08-27', '09:42:18', '20:17:58', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(400, 52171, 0, '2021-08-28', '2021-08-28', '09:50:32', '20:14:17', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(401, 52171, 0, '2021-08-29', '2021-08-29', '15:40:58', '16:15:58', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(402, 52171, 0, '2021-08-30', '2021-08-30', '09:37:32', '20:22:42', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(403, 52171, 0, '2021-08-31', '2021-08-31', '10:02:09', '20:24:15', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(404, 52173, 0, '2021-08-01', '2021-08-01', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(405, 52173, 0, '2021-08-02', '2021-08-02', '09:57:50', '17:56:51', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(406, 52173, 0, '2021-08-03', '2021-08-03', '10:01:30', '18:18:59', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(407, 52173, 0, '2021-08-04', '2021-08-04', '10:13:02', '19:13:07', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(408, 52173, 0, '2021-08-05', '2021-08-05', '10:12:21', '18:45:53', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(409, 52173, 0, '2021-08-06', '2021-08-06', '10:12:12', '19:01:33', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(410, 52173, 0, '2021-08-07', '2021-08-07', '10:23:47', '18:23:33', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(411, 52173, 0, '2021-08-08', '2021-08-08', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(412, 52173, 0, '2021-08-09', '2021-08-09', '09:51:39', '18:38:08', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(413, 52173, 0, '2021-08-10', '2021-08-10', '10:28:37', '18:42:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(414, 52173, 0, '2021-08-11', '2021-08-11', '09:50:03', '18:38:35', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(415, 52173, 0, '2021-08-12', '2021-08-12', '10:00:24', '19:53:45', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(416, 52173, 0, '2021-08-13', '2021-08-13', '10:10:56', '18:57:39', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(417, 52173, 0, '2021-08-14', '2021-08-14', '10:19:13', '18:13:42', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(418, 52173, 0, '2021-08-15', '2021-08-15', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(419, 52173, 0, '2021-08-16', '2021-08-16', '09:57:19', '19:11:40', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(420, 52173, 0, '2021-08-17', '2021-08-17', '10:03:45', '19:20:05', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(421, 52173, 0, '2021-08-18', '2021-08-18', '09:52:27', '19:35:05', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(422, 52173, 0, '2021-08-19', '2021-08-19', '10:06:30', '19:46:43', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(423, 52173, 0, '2021-08-20', '2021-08-20', '10:08:14', '19:15:37', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(424, 52173, 0, '2021-08-21', '2021-08-21', '10:13:25', '18:22:03', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(425, 52173, 0, '2021-08-22', '2021-08-22', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(426, 52173, 0, '2021-08-23', '2021-08-23', '10:09:54', '19:47:40', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(427, 52173, 0, '2021-08-24', '2021-08-24', '09:48:28', '19:27:56', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(428, 52173, 0, '2021-08-25', '2021-08-25', '10:04:26', '19:20:34', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(429, 52173, 0, '2021-08-26', '2021-08-26', '10:01:48', '19:19:01', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL);
INSERT INTO `ptx_attendance` (`attendance_id`, `user_id`, `leave_category_id`, `date_in`, `date_out`, `in_time`, `out_time`, `punch_by`, `in_location`, `out_location`, `attendance_status`, `clocking_status`, `punchingcode`, `emplyee_type`, `site_id`, `supervisor_id`) VALUES
(430, 52173, 0, '2021-08-27', '2021-08-27', '10:00:25', '18:03:19', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(431, 52173, 0, '2021-08-28', '2021-08-28', '10:11:02', '18:49:06', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(432, 52173, 0, '2021-08-29', '2021-08-29', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(433, 52173, 0, '2021-08-30', '2021-08-30', '09:51:16', '19:45:05', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(434, 52173, 0, '2021-08-31', '2021-08-31', '10:05:35', '20:09:12', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(435, 52177, 0, '2021-08-02', '2021-08-02', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(436, 52177, 0, '2021-08-03', '2021-08-03', '18:12:09', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(437, 52177, 0, '2021-08-04', '2021-08-04', '09:22:40', '18:18:16', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(438, 52177, 0, '2021-08-05', '2021-08-05', '12:41:58', '17:23:12', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(439, 52177, 0, '2021-08-06', '2021-08-06', '09:49:13', '16:15:48', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(440, 52177, 0, '2021-08-07', '2021-08-07', '09:23:38', '18:39:57', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(441, 52177, 0, '2021-08-08', '2021-08-08', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(442, 52177, 0, '2021-08-09', '2021-08-09', '09:08:04', '18:52:32', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(443, 52177, 0, '2021-08-10', '2021-08-10', '09:21:22', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(444, 52177, 0, '2021-08-11', '2021-08-11', '09:25:26', '18:40:53', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(445, 52177, 0, '2021-08-12', '2021-08-12', '09:18:18', '18:41:56', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(446, 52177, 0, '2021-08-13', '2021-08-13', '09:40:19', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(447, 52177, 0, '2021-08-14', '2021-08-14', '09:28:42', '18:08:18', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(448, 52177, 0, '2021-08-15', '2021-08-15', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(449, 52177, 0, '2021-08-16', '2021-08-16', '09:30:39', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(450, 52177, 0, '2021-08-17', '2021-08-17', '09:17:49', '19:19:49', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(451, 52177, 0, '2021-08-18', '2021-08-18', '09:16:40', '18:11:32', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(452, 52177, 0, '2021-08-19', '2021-08-19', '09:33:53', '18:31:09', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(453, 52177, 0, '2021-08-20', '2021-08-20', '09:30:45', '18:48:23', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(454, 52177, 0, '2021-08-21', '2021-08-21', '09:34:07', '18:43:01', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(455, 52177, 0, '2021-08-22', '2021-08-22', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(456, 52177, 0, '2021-08-23', '2021-08-23', '09:10:16', '18:46:37', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(457, 52177, 0, '2021-08-24', '2021-08-24', '09:25:52', '18:42:01', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(458, 52177, 0, '2021-08-25', '2021-08-25', '09:17:34', '19:04:44', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(459, 52177, 0, '2021-08-26', '2021-08-26', '09:38:14', '18:36:42', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(460, 52177, 0, '2021-08-27', '2021-08-27', '09:38:16', '18:31:42', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(461, 52177, 0, '2021-08-28', '2021-08-28', '09:27:24', '19:05:48', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(462, 52177, 0, '2021-08-29', '2021-08-29', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(463, 52177, 0, '2021-08-30', '2021-08-30', '09:25:27', '12:31:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(464, 52177, 0, '2021-08-31', '2021-08-31', '09:40:27', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(465, 52178, 0, '2021-08-01', '2021-08-01', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(466, 52178, 0, '2021-08-02', '2021-08-02', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(467, 52178, 0, '2021-08-03', '2021-08-03', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(468, 52178, 0, '2021-08-04', '2021-08-04', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(469, 52178, 0, '2021-08-05', '2021-08-05', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(470, 52178, 0, '2021-08-06', '2021-08-06', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(471, 52178, 0, '2021-08-07', '2021-08-07', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(472, 52178, 0, '2021-08-08', '2021-08-08', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(473, 52178, 0, '2021-08-09', '2021-08-09', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(474, 52178, 0, '2021-08-10', '2021-08-10', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(475, 52178, 0, '2021-08-11', '2021-08-11', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(476, 52178, 0, '2021-08-12', '2021-08-12', '19:00:51', '20:05:35', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(477, 52178, 0, '2021-08-13', '2021-08-13', '09:39:10', '19:00:04', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(478, 52178, 0, '2021-08-14', '2021-08-14', '10:00:12', '13:25:05', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(479, 52178, 0, '2021-08-15', '2021-08-15', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(480, 52178, 0, '2021-08-16', '2021-08-16', '11:20:34', '20:13:08', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(481, 52178, 0, '2021-08-17', '2021-08-17', '10:02:12', '19:42:22', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(482, 52178, 0, '2021-08-18', '2021-08-18', '10:07:27', '19:37:45', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(483, 52178, 0, '2021-08-19', '2021-08-19', '09:24:57', '21:04:48', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(484, 52178, 0, '2021-08-20', '2021-08-20', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(485, 52178, 0, '2021-08-21', '2021-08-21', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(486, 52178, 0, '2021-08-22', '2021-08-22', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(487, 52178, 0, '2021-08-23', '2021-08-23', '09:23:05', '19:14:09', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(488, 52178, 0, '2021-08-24', '2021-08-24', '09:51:44', '19:24:27', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(489, 52178, 0, '2021-08-25', '2021-08-25', '09:56:14', '19:23:37', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(490, 52178, 0, '2021-08-26', '2021-08-26', '09:48:38', '19:50:21', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(491, 52178, 0, '2021-08-27', '2021-08-27', '09:42:26', '20:17:18', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(492, 52178, 0, '2021-08-28', '2021-08-28', '09:51:32', '20:14:03', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(493, 52178, 0, '2021-08-29', '2021-08-29', '15:41:06', '16:17:12', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(494, 52178, 0, '2021-08-30', '2021-08-30', '10:00:16', '20:21:55', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(495, 52178, 0, '2021-08-31', '2021-08-31', '10:02:13', '20:24:32', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(496, 52184, 0, '2021-08-18', '2021-08-18', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(497, 52184, 0, '2021-08-19', '2021-08-19', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(498, 52184, 0, '2021-08-20', '2021-08-20', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(499, 52184, 0, '2021-08-21', '2021-08-21', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(500, 52184, 0, '2021-08-22', '2021-08-22', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(501, 52184, 0, '2021-08-23', '2021-08-23', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(502, 52184, 0, '2021-08-24', '2021-08-24', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(503, 52184, 0, '2021-08-25', '2021-08-25', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(504, 52184, 0, '2021-08-26', '2021-08-26', '19:27:35', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(505, 52184, 0, '2021-08-27', '2021-08-27', '09:50:57', '18:56:40', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(506, 52184, 0, '2021-08-28', '2021-08-28', '09:45:08', '19:04:45', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(507, 52184, 0, '2021-08-29', '2021-08-29', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(508, 52184, 0, '2021-08-30', '2021-08-30', '09:45:49', '19:59:33', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(509, 52184, 0, '2021-08-31', '2021-08-31', '10:10:26', '20:23:54', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(510, 52190, 0, '2021-08-01', '2021-08-01', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(511, 52190, 0, '2021-08-02', '2021-08-02', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(512, 52190, 0, '2021-08-03', '2021-08-03', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(513, 52190, 0, '2021-08-04', '2021-08-04', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(514, 52190, 0, '2021-08-05', '2021-08-05', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(515, 52190, 0, '2021-08-06', '2021-08-06', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(516, 52190, 0, '2021-08-07', '2021-08-07', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(517, 52190, 0, '2021-08-08', '2021-08-08', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(518, 52190, 0, '2021-08-09', '2021-08-09', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(519, 52190, 0, '2021-08-10', '2021-08-10', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(520, 52190, 0, '2021-08-11', '2021-08-11', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(521, 52190, 0, '2021-08-12', '2021-08-12', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(522, 52190, 0, '2021-08-13', '2021-08-13', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(523, 52190, 0, '2021-08-14', '2021-08-14', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(524, 52190, 0, '2021-08-15', '2021-08-15', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(525, 52190, 0, '2021-08-16', '2021-08-16', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(526, 52190, 0, '2021-08-17', '2021-08-17', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(527, 52190, 0, '2021-08-18', '2021-08-18', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(528, 52190, 0, '2021-08-19', '2021-08-19', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(529, 52190, 0, '2021-08-20', '2021-08-20', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(530, 52190, 0, '2021-08-21', '2021-08-21', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(531, 52190, 0, '2021-08-22', '2021-08-22', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(532, 52190, 0, '2021-08-23', '2021-08-23', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(533, 52190, 0, '2021-08-24', '2021-08-24', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(534, 52190, 0, '2021-08-25', '2021-08-25', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(535, 52190, 0, '2021-08-26', '2021-08-26', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(536, 52190, 0, '2021-08-27', '2021-08-27', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(537, 52190, 0, '2021-08-28', '2021-08-28', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(538, 52190, 0, '2021-08-29', '2021-08-29', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(539, 52190, 0, '2021-08-30', '2021-08-30', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(540, 52190, 0, '2021-08-31', '2021-08-31', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(541, 52200, 0, '2021-08-01', '2021-08-01', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(542, 52200, 0, '2021-08-02', '2021-08-02', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(543, 52200, 0, '2021-08-03', '2021-08-03', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(544, 52200, 0, '2021-08-04', '2021-08-04', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(545, 52200, 0, '2021-08-05', '2021-08-05', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(546, 52200, 0, '2021-08-06', '2021-08-06', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(547, 52200, 0, '2021-08-07', '2021-08-07', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(548, 52200, 0, '2021-08-08', '2021-08-08', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(549, 52200, 0, '2021-08-09', '2021-08-09', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(550, 52200, 0, '2021-08-10', '2021-08-10', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(551, 52200, 0, '2021-08-11', '2021-08-11', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(552, 52200, 0, '2021-08-12', '2021-08-12', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(553, 52200, 0, '2021-08-13', '2021-08-13', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(554, 52200, 0, '2021-08-14', '2021-08-14', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(555, 52200, 0, '2021-08-15', '2021-08-15', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(556, 52200, 0, '2021-08-16', '2021-08-16', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(557, 52200, 0, '2021-08-17', '2021-08-17', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(558, 52200, 0, '2021-08-18', '2021-08-18', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(559, 52200, 0, '2021-08-19', '2021-08-19', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(560, 52200, 0, '2021-08-20', '2021-08-20', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(561, 52200, 0, '2021-08-21', '2021-08-21', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(562, 52200, 0, '2021-08-22', '2021-08-22', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(563, 52200, 0, '2021-08-23', '2021-08-23', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(564, 52200, 0, '2021-08-24', '2021-08-24', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(565, 52200, 0, '2021-08-25', '2021-08-25', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(566, 52200, 0, '2021-08-26', '2021-08-26', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(567, 52200, 0, '2021-08-27', '2021-08-27', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(568, 52200, 0, '2021-08-28', '2021-08-28', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(569, 52200, 0, '2021-08-29', '2021-08-29', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(570, 52200, 0, '2021-08-30', '2021-08-30', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL),
(571, 52200, 0, '2021-08-31', '2021-08-31', '00:00:00', '00:00:00', NULL, NULL, NULL, 1, 0, 0, 'Indoor', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ptx_banks`
--

CREATE TABLE `ptx_banks` (
  `id` int(11) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `team_leader` varchar(255) NOT NULL,
  `comp_under_work` varchar(255) NOT NULL,
  `bank_agree` varchar(100) NOT NULL,
  `agree_date` date NOT NULL,
  `agree_date_to` date DEFAULT NULL,
  `agreement` varchar(255) NOT NULL,
  `bank_payout` varchar(100) NOT NULL,
  `repo_kit` varchar(255) NOT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT '0' COMMENT '1 = Deleted, 0 = Not Deleted',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ptx_banks`
--

INSERT INTO `ptx_banks` (`id`, `bank_name`, `team_leader`, `comp_under_work`, `bank_agree`, `agree_date`, `agree_date_to`, `agreement`, `bank_payout`, `repo_kit`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'SBI Bank', '1', 'Om Sai Collection', 'Yes', '2008-11-19', NULL, 'Yes', 'Yes', 'repository kit', 1, '2021-04-03 12:40:24', '2021-06-23 04:21:53'),
(2, 'Kotak Bank', '1', 'Om Sai Collection', 'No', '2009-01-03', NULL, 'Yes', 'Yes', 'repository kit', 1, '2021-04-03 12:57:48', '2021-06-23 04:22:19'),
(3, 'HDFC BANK LTD', 'New', 'Company', 'Yes', '2021-03-02', NULL, 'YES', 'Yes', 'YES', 1, '2021-06-23 07:09:48', '2021-08-19 05:39:35'),
(4, 'INDUSIND BANK LTD', 'New', 'OM SAI  KRUPA ASSOCIATS', 'Yes', '2019-01-02', NULL, 'YES', 'Yes', 'NO', 1, '2021-06-23 07:54:26', '2021-08-19 05:40:03'),
(5, 'INDUSIND BANK LTD', '37', 'OM SAI  KRUPA ASSOCIATED', 'Yes', '2019-01-02', NULL, 'YES', 'Yes', 'NO', 1, '2021-06-23 07:54:49', '2021-06-23 02:55:38'),
(6, 'YES BANK', 'New', 'OM SAIKRUPA ASSOCIATS', 'Yes', '2014-12-09', NULL, 'YES', 'Yes', 'YES', 1, '2021-06-23 07:59:18', '2021-08-19 05:41:01'),
(7, 'IDFC BANK', 'New', 'OM SAI  KRUPA ASSOCIATS', 'Yes', '2018-06-08', NULL, 'YES', 'Yes', 'NO', 1, '2021-06-23 08:09:06', '2021-08-19 05:39:42'),
(8, 'FULLERTON INDIA CREDIT COMPANY LIMITED', 'New', 'Om Saii Credit Collection Servicess Pvt Ltd', 'Yes', '2020-11-29', NULL, 'YES', 'Yes', 'NO', 1, '2021-06-23 08:37:08', '2021-08-19 05:39:01'),
(9, 'PUNJAB  NATIONAL BANKK', 'New', 'Om Saii Credit Collection Servicess Pvt Ltd', 'Yes', '2020-12-09', NULL, 'YES', 'Yes', 'NO', 1, '2021-06-23 08:42:16', '2021-08-19 05:40:37'),
(10, 'AXIS BANK', 'New', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2018-04-01', '2021-07-24', 'YES', 'Yes', 'YES', 1, '2021-06-23 08:45:42', '2021-08-19 05:38:00'),
(11, 'TOYOTA FINANCE', 'New', 'Om Saii Credit Collection Servicess Pvt Ltd', 'Yes', '2021-04-24', NULL, 'YES', 'Yes', 'YES', 1, '2021-06-23 08:47:53', '2021-08-19 05:40:45'),
(12, 'DAIMLER FINANCE', 'New', 'Company', 'Yes', '2020-11-04', '2021-07-31', 'bank_agreement/agreement_210729054646.pdf', 'Yes', 'YES', 1, '2021-06-23 08:49:20', '2021-08-19 05:38:31'),
(13, 'VOLKSWAGEN FINANCE', 'New', 'Om Saii Credit Collection Servicess Pvt Ltd', 'Yes', '2019-05-06', NULL, 'YES', 'Yes', 'YES', 1, '2021-06-23 08:56:36', '2021-08-19 05:40:52'),
(14, 'DAIMLER FINANCE', 'New', 'OM SAI  KRUPA ASSOCIATS', 'Yes', '2020-11-04', NULL, 'YES', 'Yes', 'YES', 1, '2021-06-23 09:31:56', '2021-08-19 05:38:35'),
(15, 'DAIMLER FINANCE', 'New', 'OM SAIKRUPA ASSOCIATES', 'Yes', '2020-11-04', NULL, 'YES', 'Yes', 'YES', 1, '2021-06-23 09:43:25', '2021-08-19 05:38:40'),
(16, 'AXIS BANK', 'New', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2018-04-01', '2021-07-31', 'YES', 'Yes', 'YES', 1, '2021-06-23 09:52:29', '2021-08-19 05:38:04'),
(17, 'DAIMLER FINANCE', 'New', 'OM SAIKRUPA ASSOCIATES', 'Yes', '2020-11-04', NULL, 'YES', 'Yes', 'YES', 1, '2021-06-23 09:53:50', '2021-08-19 05:38:45'),
(18, 'DAIMLER FINANCE', 'New', 'OM SAIKRUPA ASSOCIATES', 'Yes', '2020-11-04', NULL, 'YES', 'Yes', 'YES', 1, '2021-06-23 09:54:18', '2021-08-19 05:38:50'),
(19, 'DAIMLER FINANCE', '37', 'OM SAIKRUPA ASSOCIATES', 'Yes', '2020-11-04', NULL, 'YES', 'Yes', 'YES', 1, '2021-06-23 09:55:48', '2021-08-19 05:38:56'),
(20, 'AXIS BANK', 'New', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2020-04-01', NULL, 'YES', 'Yes', 'YES', 1, '2021-06-23 09:57:18', '2021-08-19 05:38:07'),
(21, 'INDUSIND BANK LTD', '37', 'OM SAIKRUPA ASSOCIATES', 'Yes', '2019-01-02', NULL, 'YES', 'Yes', 'NO', 1, '2021-06-23 09:57:56', '2021-08-19 05:40:07'),
(22, 'FULLERTON INDIA CREDIT COMPANY LIMITED', '36', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2020-11-28', NULL, 'YES', 'Yes', 'NO', 1, '2021-06-23 09:58:21', '2021-08-19 05:39:10'),
(23, 'HDFC BANK', 'New', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2021-03-02', '2024-03-02', 'bank_agreement/agreement_210819095050.pdf', 'Yes', 'YES', 1, '2021-06-23 09:59:40', '2021-08-19 05:39:25'),
(24, 'YES BANK', '36', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2014-12-09', NULL, 'YES', 'Yes', 'YES', 1, '2021-06-23 09:59:52', '2021-06-23 06:18:34'),
(25, 'PUNJAB  NATIONAL BANK', 'New', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2020-12-09', NULL, 'YES', 'Yes', 'NO', 1, '2021-06-23 10:00:31', '2021-08-19 05:40:25'),
(26, 'TOYOTA FINANCE', '37', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2021-04-24', NULL, 'YES', 'Yes', 'YES', 1, '2021-06-23 10:01:33', '2021-08-19 05:40:48'),
(27, 'VOLKSWAGEN FINANCE', '37', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2019-05-06', NULL, 'YES', 'Yes', 'YES', 1, '2021-06-23 10:02:36', '2021-08-19 05:40:58'),
(28, 'IDFC BANK', 'New', 'OM SAIKRUPA ASSOCIATES', 'Yes', '2018-06-08', NULL, 'YES', 'Yes', 'NO', 1, '2021-06-23 10:04:42', '2021-08-19 05:39:47'),
(29, 'KOTAK MAHINDRA PRIME', 'New', 'OM SAIKRUPA ASSOCIATES', 'Yes', '2020-03-01', NULL, 'YES', 'Yes', 'NO', 1, '2021-06-23 10:06:07', '2021-08-19 05:40:10'),
(30, 'PAYTM', 'New', 'OM SAIKRUPA ASSOCIATES', 'Yes', '2020-07-15', NULL, 'YES', 'Yes', 'NO', 1, '2021-06-23 10:07:10', '2021-08-19 05:40:18'),
(31, 'PAYTM', '36', 'OM SAIKRUPA ASSOCIATES', 'Yes', '2020-07-16', NULL, 'YES', 'Yes', 'YES', 1, '2021-06-23 10:08:39', '2021-08-19 05:40:21'),
(32, 'HDFC BANK', '36', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2021-03-02', NULL, 'YES', 'Yes', 'YES', 1, '2021-06-23 10:10:32', '2021-08-19 05:39:28'),
(33, 'KOTAK MAHINDRA PRIME', '37', 'OM SAIKRUPA ASSOCIATES', 'Yes', '2020-03-01', NULL, 'YES', 'Yes', 'YES', 1, '2021-06-23 10:12:36', '2021-08-19 05:40:14'),
(34, 'IDFC BANK', '36', 'OM SAIKRUPA ASSOCIATES', 'Yes', '2018-07-08', NULL, 'YES', 'Yes', 'NO', 1, '2021-06-23 10:13:42', '2021-08-19 05:39:56'),
(35, 'PUNJAB  NATIONAL BANK', 'New', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2020-12-09', NULL, 'YES', 'Yes', 'NO', 1, '2021-06-23 10:15:59', '2021-08-19 05:40:30'),
(36, 'PUNJAB  NATIONAL BANK', '36', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2020-12-09', NULL, 'YES', 'Yes', 'NO', 1, '2021-06-23 10:18:16', '2021-08-19 05:40:34'),
(37, 'FULLERTON INDIA CREDIT COMPANY LIMITED', '36', 'OM SAIKRUPA ASSOCIATES', 'Yes', '2020-11-29', NULL, 'YES', 'Yes', 'NO', 1, '2021-06-23 10:19:56', '2021-08-19 05:39:20'),
(38, 'TATA CAPITAL', '36', 'OM SAIKRUPA ASSOCIATES', 'Yes', '2020-11-29', NULL, 'YES', 'No', 'NO', 1, '2021-06-23 10:22:16', '2021-08-19 05:40:42'),
(39, 'BMW FINANCE', 'New', 'OM SAIKRUPA ASSOCIATES', 'Yes', '2020-09-02', NULL, 'YES', 'Yes', 'NO', 1, '2021-06-23 10:25:14', '2021-08-19 05:38:22'),
(40, 'BMW FINANCE', 'New', 'OM SAIKRUPA ASSOCIATES', 'Yes', '2020-09-02', NULL, 'YES', 'Yes', 'NO', 1, '2021-06-23 10:26:23', '2021-08-19 05:38:26'),
(41, 'BAJAJ FINANCE', 'New', 'OM SAIKRUPA ASSOCIATES', 'Yes', '2020-01-27', NULL, '', 'Yes', 'NO', 1, '2021-06-23 10:27:54', '2021-08-19 05:38:17'),
(42, 'YES BANK', '36', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2014-12-09', '2022-12-11', 'YES', 'Yes', 'YES', 1, '2021-06-23 11:05:14', '2021-08-19 05:41:04'),
(43, 'IDFC BANK', '37', 'OM SAIKRUPA ASSOCIATES', 'Yes', '2018-07-08', NULL, 'YES', 'Yes', 'NO', 1, '2021-06-23 11:15:22', '2021-08-19 05:39:59'),
(44, 'IDBI BANK', 'New', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2021-02-11', '2024-02-10', 'bank_agreement/agreement_210712124706.pdf', 'Yes', 'NO', 1, '2021-06-23 11:20:57', '2021-08-19 05:39:38'),
(45, 'HDFC BANK', '37', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2021-03-02', NULL, 'YES', 'Yes', 'YES', 1, '2021-06-23 11:23:51', '2021-08-19 05:39:32'),
(46, 'AXIS BANK', 'New', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2021-04-01', '2024-03-31', 'bank_agreement/agreement_210712113110.pdf', 'Yes', 'YES', 1, '2021-07-12 11:31:10', '2021-08-19 05:38:10'),
(47, 'Testing Bank', 'New', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2021-07-10', '2021-07-30', 'bank_agreement/agreement_210729012031.pdf', 'Yes', 'test.com', 1, '2021-07-29 01:20:31', '2021-07-28 20:21:36'),
(48, 'AXIS BANK', '37', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2021-03-15', '2024-03-14', 'bank_agreement/agreement_210819125611.pdf', 'Yes', 'YES', 0, '2021-08-19 12:56:11', '2021-10-19 05:40:41'),
(49, 'HDFC BANK', '36', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2021-04-01', '2024-04-04', 'bank_agreement/agreement_210820091344.pdf', 'Yes', 'YES', 0, '2021-08-20 09:13:44', '2021-10-18 07:48:38'),
(50, 'TOYATA', '37', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2021-04-21', '2024-04-26', 'bank_agreement/agreement_210820092156.pdf', 'Yes', 'YES', 0, '2021-08-20 09:21:56', '2021-10-18 06:52:00'),
(51, 'PNB HOUSING  FINANCE', '36', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2020-12-10', '2023-12-07', 'bank_agreement/agreement_210821061345.pdf', 'Yes', 'NO', 0, '2021-08-21 06:13:45', '2021-10-18 06:49:57'),
(52, 'CAPITAL FLOAT', '36', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2021-08-12', '2024-08-08', 'bank_agreement/agreement_210821064114.pdf', 'Yes', 'YES', 0, '2021-08-21 06:41:14', '2021-10-16 07:09:05'),
(53, 'IDBI BANK', '36', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2021-02-11', '2026-02-05', 'bank_agreement/agreement_210821065440.pdf', 'Yes', 'YES', 0, '2021-08-21 06:54:40', '2021-10-18 01:13:47'),
(54, 'VOLKSWAGEN FINANCE', '37', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2019-05-08', '2022-05-05', 'bank_agreement/agreement_210821070433.pdf', 'Yes', 'NO', 0, '2021-08-21 07:04:33', '2021-10-18 06:53:51'),
(55, 'YES BANK', '36', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2021-08-13', '2024-08-09', 'bank_agreement/agreement_210821071253.pdf', 'Yes', 'NO', 0, '2021-08-21 07:12:53', '2021-10-18 06:58:58'),
(56, 'FULLERTON INDIA HOME FINANCE COMPANY LTD', '36', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2020-11-25', '2023-11-23', 'bank_agreement/agreement_210824112430.pdf', 'No', 'NO', 0, '2021-08-24 11:24:30', '2021-10-16 07:10:06'),
(57, 'CREDIT-WISE CAPITAL PVT LTD', '36', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2020-12-14', '2023-12-07', 'bank_agreement/agreement_210824112719.pdf', 'Yes', 'NO', 0, '2021-08-24 11:27:19', '2021-08-24 11:27:19'),
(58, 'GADI WEB PVT LTD', '37', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2021-07-23', '2024-07-11', 'bank_agreement/agreement_210824112952.pdf', 'Yes', 'NO', 0, '2021-08-24 11:29:52', '2021-08-24 11:29:52'),
(59, 'FULLERTON INDIA SERVICE LTD', '36', 'OM SAI  KRUPA ASSOCIATS', 'Yes', '2020-07-27', '2023-07-20', 'bank_agreement/agreement_210824121146.pdf', 'Yes', 'NO', 0, '2021-08-24 12:11:46', '2021-10-16 07:13:00'),
(60, 'HDFC Bank LTD', '36', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2021-03-04', '2024-03-04', 'bank_agreement/agreement_210825110356.pdf', 'Yes', 'Yes', 0, '2021-08-25 11:03:56', '2021-10-13 02:56:56'),
(61, 'IDFC BANK', '36', 'OM SAI  KRUPA ASSOCIATS', 'Yes', '2019-09-05', '2021-09-07', 'bank_agreement/agreement_210825013501.pdf', 'Yes', 'NO', 0, '2021-08-25 13:35:01', '2021-10-18 05:52:55'),
(62, 'BMW FINANCE', '37', 'OM SAI  KRUPA ASSOCIATS', 'Yes', '2020-09-22', '2024-09-10', 'bank_agreement/agreement_210825013900.pdf', 'Yes', 'NO', 0, '2021-08-25 13:39:00', '2021-10-19 05:08:29'),
(63, 'INDUSLND BANK', '37', 'OM SAI  KRUPA ASSOCIATS', 'Yes', '2019-01-02', '2021-12-28', 'bank_agreement/agreement_210825014050.pdf', 'Yes', 'NO', 0, '2021-08-25 13:40:50', '2021-10-18 06:10:46'),
(64, 'KOTAK MAHINDRA PRIME', '37', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2019-04-16', '2022-04-07', 'bank_agreement/agreement_210826045148.pdf', 'Yes', 'YES', 0, '2021-08-26 04:51:48', '2021-10-18 06:43:00'),
(65, 'BAJAJ FINANCE', '36', 'OM SAI  KRUPA ASSOCIATS', 'Yes', '2019-10-16', '2022-10-06', 'bank_agreement/agreement_210828122209.pdf', 'Yes', 'YES', 0, '2021-08-28 12:22:09', '2021-10-16 07:13:41'),
(66, 'PAYTM FINANCE', '36', 'OM SAI  KRUPA ASSOCIATS', 'Yes', '2020-07-31', '2023-07-27', 'bank_agreement/agreement_210828124035.pdf', 'Yes', 'NO', 0, '2021-08-28 12:40:35', '2021-10-18 06:47:33'),
(67, 'LIC HOUSING FINANCE LTD{LIC HFL}', '36', 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 'Yes', '2021-08-20', '2022-08-20', 'bank_agreement/agreement_210911071754.pdf', 'Yes', 'YES', 0, '2021-09-11 07:17:54', '2021-10-18 06:45:47');

-- --------------------------------------------------------

--
-- Table structure for table `ptx_branches`
--

CREATE TABLE `ptx_branches` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) UNSIGNED NOT NULL,
  `branch_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `office_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1 = Deleted, 0 = Not Deleted',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ptx_branches`
--

INSERT INTO `ptx_branches` (`id`, `company_id`, `branch_name`, `office_no`, `address`, `pin`, `area`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 'Nigadi Branch', '209', 'Pune, Maharashtra', '411013', 'Nigadi, Pune', 1, NULL, '2021-04-03 07:05:23'),
(2, 2, 'Head Office', '209', 'Kanak Apartment Shiral Chowk Rasta Peth Pune.', '411011', 'Rasta Peth', 0, NULL, NULL),
(3, 2, 'PCMC', 'Pride Plaza', 'Chinchwad Chowk PCMC', '411033', 'PCMC', 0, NULL, NULL),
(4, 2, 'THANE', '06 MANAV MANDIR BUILDING FIRST FLOOR', 'RAM MARUTI ROAD NEAR P N GADGIL JWELLER NAVPADA THANE', '400602', 'THANE', 0, NULL, NULL),
(7, 5, 'THANE', 'THANE', 'THANE', '400032', 'THANE', 1, NULL, NULL),
(8, 6, 'Demo', 'demo', 'demo', '411011', 'demo', 1, NULL, NULL),
(9, 7, 'Head Office', '209', 'ANAK APARTMENT SHIRAL SHETH CHOWK RASTA PETH PUNE', '411011', 'Rasta Peth', 0, NULL, NULL),
(10, 2, 'NASHIK', 'Flat no 302', 'Flat no 302 shashi Anand  Apartment behind aspecia hotel Indira Nagar Nashik 422009', '422009', 'Indira Nagar', 0, NULL, NULL),
(11, 2, 'SATARA', '210 SHAMIWAR PETH', '210 SHAMIWAR PETH KALIKA BHAVAN  DEVI CHOWK RAJPATH SATARA', '415002', 'SATARA', 0, NULL, '2021-08-25 07:12:18');

-- --------------------------------------------------------

--
-- Table structure for table `ptx_bucket`
--

CREATE TABLE `ptx_bucket` (
  `id` int(11) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `bucket` varchar(255) NOT NULL,
  `bucket_attach` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ptx_bucket`
--

INSERT INTO `ptx_bucket` (`id`, `bank_id`, `bucket`, `bucket_attach`, `created_at`) VALUES
(5, 72, 'PL', 'bank_agreement/attach_211013041729.pdf', '2021-10-13 04:17:29'),
(6, 73, 'PL', 'bank_agreement/attach_211013041831.pdf', '2021-10-13 04:18:31'),
(7, 73, 'AUTO', 'bank_agreement/attach_211013041831.pdf', '2021-10-13 04:18:31'),
(8, 65, '3 WHL', 'bank_agreement/attach_211016081538.pdf', '2021-10-16 08:15:38'),
(9, 62, 'AUTO', 'bank_agreement/attach_211016113714.pdf', '2021-10-16 11:37:14'),
(10, 52, 'PL W OFF', 'bank_agreement/attach_211016120554.pdf', '2021-10-16 12:05:54'),
(11, 59, 'PL,0,1,2,4', 'bank_agreement/attach_211016121300.pdf', '2021-10-16 12:13:00'),
(12, 49, 'AUTO,CTG, BKT 2,3,4,W OFF', 'bank_agreement/attach_211018061031.pdf', '2021-10-18 06:10:31'),
(13, 53, 'AUTO,PL BKT 0 TO W OFF', 'bank_agreement/attach_211018061347.pdf', '2021-10-18 06:13:47'),
(14, 61, 'X CELL,BKT 0,1,3,5', 'bank_agreement/attach_211018105255.pdf', '2021-10-18 10:52:55'),
(15, 61, 'BIL PL BKT 1,2,4', 'bank_agreement/attach_211018105255.pdf', '2021-10-18 10:52:55'),
(16, 61, 'CREDIT CARD,BKT 1,2,3,4', 'bank_agreement/attach_211018105255.pdf', '2021-10-18 10:52:55'),
(17, 63, 'BL,BKT 1 & 2', 'bank_agreement/attach_211018111046.pdf', '2021-10-18 11:10:46'),
(18, 63, 'BL,LACR,FLIP BKT 3,4,5,6,7,8,9 CHR-OFF', 'bank_agreement/attach_211018111046.pdf', '2021-10-18 11:10:46'),
(19, 64, 'AUTO, BKT -1,3,4,5', 'bank_agreement/attach_211018114300.pdf', '2021-10-18 11:43:00'),
(20, 67, 'HL,BKT -3 TO W OFF', 'bank_agreement/attach_211018114547.pdf', '2021-10-18 11:45:47'),
(21, 66, 'BL,BKT - 0 To W-OFF', 'bank_agreement/attach_211018114733.pdf', '2021-10-18 11:47:33'),
(22, 51, 'HL, BKT-2,3,NPA', 'bank_agreement/attach_211018114957.pdf', '2021-10-18 11:49:57'),
(23, 50, 'AUTO,BKT-1,2,NPA,W OFF', 'bank_agreement/attach_211018115200.PDF', '2021-10-18 11:52:00'),
(24, 54, 'AUTO, BKT-1,2,3,4 NPA', 'bank_agreement/attach_211018115351.pdf', '2021-10-18 11:53:51'),
(25, 55, 'CV , BKT 0 To W-OFF', 'bank_agreement/attach_211018115826.pdf', '2021-10-18 11:58:26'),
(26, 55, 'HL, BKT 0 TO W-OFF', 'bank_agreement/attach_211018115826.pdf', '2021-10-18 11:58:26'),
(27, 55, 'HL', 'bank_agreement/attach_211018115826.pdf', '2021-10-18 11:58:26'),
(28, 48, 'AUTO-Proposed Repo, Stock Yard & Valuation Payout structure', 'bank_agreement/attach_211019104042.pdf', '2021-10-19 10:40:42'),
(29, 48, 'AUTO-Writeoff Payout Recovery Secured Products', 'bank_agreement/attach_211019104042.pdf', '2021-10-19 10:40:42'),
(30, 48, 'TW', 'bank_agreement/attach_211019104042.pdf', '2021-10-19 10:40:42');

-- --------------------------------------------------------

--
-- Table structure for table `ptx_cash_collections`
--

CREATE TABLE `ptx_cash_collections` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bank_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `loan_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amt_due` double(10,2) DEFAULT NULL,
  `product_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telecaller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `field_executive_id` int(11) DEFAULT NULL,
  `collect_amt` decimal(10,2) NOT NULL,
  `receipt_date` date NOT NULL,
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ptx_cash_collections`
--

INSERT INTO `ptx_cash_collections` (`id`, `bank_id`, `loan_no`, `amt_due`, `product_id`, `telecaller`, `agent`, `field_executive_id`, `collect_amt`, `receipt_date`, `remark`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, '50', '321', 5000.00, '16', '37', '33', 39, 900.00, '2021-09-01', '1 st EMI', 0, '2021-09-02 06:12:40', '2021-09-02 01:14:10'),
(2, '50', '321', 4000.00, '16', '37', '36', 39, 1100.00, '2021-09-02', '2nd EMI', 0, '2021-09-02 06:16:34', '2021-09-02 06:16:34'),
(3, '50', '1231321', 500.00, '16', '37', '36', 39, 4000.00, '2021-09-02', '1st EMI', 0, '2021-09-02 06:17:32', '2021-09-02 06:17:32');

-- --------------------------------------------------------

--
-- Table structure for table `ptx_cash_collection_details`
--

CREATE TABLE `ptx_cash_collection_details` (
  `id` int(11) NOT NULL,
  `bank_id` varchar(255) NOT NULL,
  `account_no` varchar(255) NOT NULL,
  `product_id` varchar(255) NOT NULL,
  `team_leader` varchar(255) NOT NULL,
  `amount_due` varchar(255) NOT NULL,
  `telecaller` varchar(255) NOT NULL,
  `agent` varchar(255) NOT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ptx_companies`
--

CREATE TABLE `ptx_companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1 = Deleted, 0 = Not Deleted',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ptx_companies`
--

INSERT INTO `ptx_companies` (`id`, `company_name`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'Om Sai Collection', 1, '2021-04-03 12:04:44', '2021-06-22 09:08:01'),
(2, 'OM SAI CREDIT COLLECTION SERVICES PVT. LTD.', 0, '2021-04-05 05:47:20', '2021-08-24 06:35:32'),
(5, 'OM SAIKRUPA ASSOCIATES', 1, '2021-04-19 16:22:06', '2021-08-23 02:50:23'),
(6, 'Demo 3', 1, '2021-06-29 12:17:12', '2021-06-29 07:17:29'),
(7, 'OM SAI  KRUPA ASSOCIATS', 0, '2021-08-24 11:53:01', '2021-08-24 11:53:01');

-- --------------------------------------------------------

--
-- Table structure for table `ptx_companyids`
--

CREATE TABLE `ptx_companyids` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `omsai_id_card` varchar(100) NOT NULL,
  `temporary_id` varchar(100) NOT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT '0' COMMENT '1 = Deleted, 0 = Not Deleted',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ptx_companyids`
--

INSERT INTO `ptx_companyids` (`id`, `user_id`, `omsai_id_card`, `temporary_id`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 137, 'Yes', 'No', 0, '2021-05-18 15:58:10', '2021-05-18 15:58:10'),
(2, 37, 'Yes', 'No', 0, '2021-06-23 07:30:20', '2021-06-23 07:30:20'),
(3, 61, 'Yes', 'Yes', 0, '2021-06-23 08:04:40', '2021-06-23 08:04:40'),
(4, 58, 'No', 'Yes', 0, '2021-06-28 11:01:26', '2021-06-28 11:01:26'),
(5, 32, 'Yes', 'Yes', 0, '2021-07-01 05:31:38', '2021-07-01 05:31:38'),
(6, 36, 'Yes', 'Yes', 0, '2021-07-07 13:56:47', '2021-07-07 13:56:47'),
(7, 1, 'Yes', 'No', 0, '2021-08-25 13:45:43', '2021-08-25 08:45:59'),
(8, 324, 'Yes', 'Yes', 0, '2021-10-02 10:14:56', '2021-10-02 10:14:56'),
(9, 251, 'Yes', 'Yes', 0, '2021-10-02 10:16:12', '2021-10-02 10:16:12'),
(10, 307, 'Yes', 'Yes', 0, '2021-10-02 10:23:04', '2021-10-02 10:23:04'),
(11, 313, 'Yes', 'Yes', 0, '2021-10-02 10:31:54', '2021-10-02 10:31:54'),
(12, 303, 'Yes', 'Yes', 0, '2021-10-02 10:44:37', '2021-10-02 10:44:37'),
(13, 308, 'Yes', 'Yes', 0, '2021-10-02 11:00:36', '2021-10-02 11:00:36'),
(14, 253, 'Yes', 'Yes', 0, '2021-10-02 11:09:10', '2021-10-02 11:09:10'),
(15, 286, 'Yes', 'Yes', 0, '2021-10-05 07:37:08', '2021-10-05 07:37:08'),
(16, 129, 'Yes', 'Yes', 0, '2021-10-07 08:16:07', '2021-10-07 08:16:07'),
(17, 314, 'Yes', 'Yes', 0, '2021-10-26 09:55:04', '2021-10-26 09:55:04'),
(18, 317, 'Yes', 'Yes', 0, '2021-10-26 10:08:57', '2021-10-26 10:08:57');

-- --------------------------------------------------------

--
-- Table structure for table `ptx_departments`
--

CREATE TABLE `ptx_departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `department_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_head_id` int(11) DEFAULT NULL COMMENT 'department_head_id == user_id',
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 = Deleted, 0 = Not Deleted',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ptx_departments`
--

INSERT INTO `ptx_departments` (`id`, `department_name`, `department_head_id`, `isDeleted`, `created_at`, `updated_at`) VALUES
(15, 'Admin', NULL, 0, '2021-02-24 09:44:27', '2021-07-30 07:19:47'),
(19, 'User', NULL, 0, '2021-03-15 11:05:31', '2021-03-15 11:05:31'),
(20, 'Operations', NULL, 0, '2021-04-05 05:52:21', '2021-04-05 05:52:21'),
(21, 'BACK OFFICE', NULL, 0, '2021-04-05 06:21:37', '2021-04-05 06:21:37'),
(22, 'ACCOUNTS', NULL, 0, '2021-04-05 06:22:46', '2021-04-05 06:22:46'),
(23, 'PROCESS', NULL, 0, '2021-04-05 06:29:28', '2021-10-20 04:53:32'),
(24, 'Support', NULL, 0, '2021-04-09 07:32:28', '2021-04-09 07:32:28'),
(26, 'Repo', NULL, 0, '2021-04-19 11:40:57', '2021-04-19 11:40:57');

-- --------------------------------------------------------

--
-- Table structure for table `ptx_designations`
--

CREATE TABLE `ptx_designations` (
  `id` int(10) UNSIGNED NOT NULL,
  `dept_id` int(10) UNSIGNED NOT NULL,
  `designation_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 = Deleted, 0 = Not Deleted',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ptx_designations`
--

INSERT INTO `ptx_designations` (`id`, `dept_id`, `designation_name`, `isDeleted`, `created_at`, `updated_at`) VALUES
(19, 15, 'Admin', 0, '2021-02-24 09:44:27', '2021-02-24 09:44:27'),
(24, 19, 'User', 0, '2021-03-15 11:05:31', '2021-03-15 11:05:31'),
(25, 15, 'Agent', 0, '2021-03-16 10:38:03', '2021-03-16 10:38:03'),
(26, 20, 'HR', 0, '2021-04-05 05:52:21', '2021-04-05 05:52:21'),
(27, 21, 'CASHIER', 0, '2021-04-05 06:21:37', '2021-04-05 06:21:37'),
(28, 22, 'SENIOR MANAGER', 0, '2021-04-05 06:22:46', '2021-04-05 06:22:46'),
(29, 22, 'ACCOUNT MANAGER', 0, '2021-04-05 06:23:04', '2021-04-05 06:23:04'),
(30, 22, 'ACCOUNT EXECUTIVE', 0, '2021-04-05 06:23:59', '2021-04-05 06:23:59'),
(31, 21, 'MANAGER', 0, '2021-04-05 06:25:35', '2021-04-05 06:25:35'),
(32, 21, 'CASHIER INCHARGE', 0, '2021-04-05 06:25:58', '2021-04-05 06:25:58'),
(33, 21, 'AUDIT INCHARGE', 0, '2021-04-05 06:28:30', '2021-04-05 06:28:30'),
(34, 21, 'AUDIT EXECUTIVE', 0, '2021-04-05 06:28:51', '2021-04-05 06:28:51'),
(35, 23, 'TELECALLER', 0, '2021-04-05 06:29:28', '2021-04-05 06:29:28'),
(36, 23, 'FIELD EXECUTIVE', 0, '2021-04-05 06:29:44', '2021-04-05 06:29:44'),
(37, 20, 'GENERAL MANAGER', 0, '2021-04-05 06:30:25', '2021-04-05 06:30:25'),
(38, 23, 'PORTFOLIO MANAGER', 0, '2021-04-05 06:33:45', '2021-04-05 06:33:45'),
(39, 15, 'ADMIN INCHARGE', 0, '2021-04-05 06:34:49', '2021-04-05 06:34:49'),
(40, 15, 'ADMIN EXECUTIVE', 0, '2021-04-05 06:35:50', '2021-04-05 06:35:50'),
(41, 20, 'MANAGING DIRECTOR', 0, '2021-04-05 06:36:51', '2021-04-05 06:36:51'),
(42, 20, 'CHAIRMAN', 0, '2021-04-05 06:37:01', '2021-04-05 06:37:01'),
(43, 15, 'OFFICE ASSIATANT', 0, '2021-04-05 06:37:57', '2021-04-05 06:37:57'),
(44, 21, 'PROCESS MANAGER', 0, '2021-04-05 06:40:16', '2021-04-05 06:40:16'),
(45, 24, 'Driver', 0, '2021-04-09 07:32:28', '2021-04-09 07:32:28'),
(46, 24, 'Security', 0, '2021-04-09 07:32:47', '2021-04-09 07:32:47'),
(48, 26, 'Repo Manager', 0, '2021-04-19 11:40:57', '2021-04-19 11:40:57'),
(50, 23, 'SENIOR AREA MANAGER', 0, '2021-04-19 11:43:54', '2021-04-19 11:43:54'),
(52, 23, 'AREA MANAGER', 0, '2021-10-20 09:49:41', '2021-10-20 09:49:41'),
(53, 23, 'SENIOR AREA MANAGER', 0, '2021-10-20 09:51:20', '2021-10-20 09:51:20'),
(54, 23, 'SENIOR FIELD EXECUTIVE', 0, '2021-10-20 09:51:54', '2021-10-20 09:51:54');

-- --------------------------------------------------------

--
-- Table structure for table `ptx_documents`
--

CREATE TABLE `ptx_documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `pan_card_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pan_card_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aadhar_card_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aadhar_card_front_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aadhar_card_back_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `education` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bnk_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bnk_acc_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ifsc_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bnk_pass_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `edu_proof_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rent_agmnt_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rotary_agmnt_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `elect_bill_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `voter_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `driving_licence` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resume` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `interview_check` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vacc_receipt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isDelete` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '1 = Deleted, 0 = Not Deleted	',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ptx_documents`
--

INSERT INTO `ptx_documents` (`id`, `user_id`, `pan_card_number`, `pan_card_img`, `aadhar_card_number`, `aadhar_card_front_img`, `aadhar_card_back_img`, `dob`, `education`, `bnk_name`, `bnk_acc_number`, `ifsc_code`, `bnk_pass_img`, `edu_proof_img`, `rent_agmnt_img`, `rotary_agmnt_img`, `elect_bill_img`, `voter_id`, `driving_licence`, `photo`, `resume`, `interview_check`, `vacc_receipt`, `isDelete`, `created_at`, `updated_at`) VALUES
(1, 1, 'ABCDE1234F', 'pan_card/pan_210603095654.pdf', '000011112222', 'aadhar_card/aadhar_front_210603095654.pdf', 'aadhar_card/aadhar_back_210603095654.pdf', '1993-12-13', 'Post Graduate', 'HDFC', '011401533', 'HDFC001212AC', 'bank_passbook/bnk_passbook_210603095654.pdf', 'education_proof/edu_proof_210603095654.pdf', 'rent_rotary/rent_210603095654.pdf', 'rent_rotary/rotary_210603095655.pdf', 'electricity_bill/electricity_210603095655.pdf', 'voter_id/voterid_210603095655.pdf', 'driving_licence/drive_210603095655.pdf', 'photo/photo_210603095655.pdf', 'resume/resume_210603095655.pdf', 'interview_check/interview_check_210603095655.pdf', '', '0', '2021-06-03 09:56:55', '2021-06-03 09:56:55'),
(2, 49, 'ARDPB0138J', 'pan_card/pan_210603113519.pdf', '402256047903', 'aadhar_card/aadhar_front_210603113519.pdf', 'aadhar_card/aadhar_back_210603113519.pdf', '1984-08-30', 'HSC', 'STATE  BANK OF INDIA', '32506385546', 'SBIN0001110', 'bank_passbook/aadhar_210604055744.pdf', 'education_proof/edu_proof_210603113519.pdf', 'rent_rotary/rent_210603113519.pdf', 'rent_rotary/rotary_210603113519.pdf', 'electricity_bill/electricity_210603113519.pdf', 'voter_id/voterid_210603113519.pdf', 'driving_licence/drive_210603113519.pdf', 'photo/photo_210603113519.pdf', 'resume/resume_210603113519.pdf', 'interview_check/interview_check_210603113519.pdf', '', '0', '2021-06-03 11:35:19', '2021-06-04 00:57:44'),
(3, 48, 'AJAPM6167B', 'pan_card/pan_210603120358.pdf', '945090635182', 'aadhar_card/aadhar_front_210603120358.pdf', 'aadhar_card/aadhar_back_210603120358.pdf', '1964-09-27', 'SSC', 'COSMOS BANK', '0270501016667', 'COSB0000027', 'bank_passbook/bnk_passbook_210603120358.pdf', 'education_proof/edu_proof_210603120358.pdf', NULL, NULL, 'electricity_bill/electricity_210603120358.pdf', 'voter_id/voterid_210603120358.pdf', 'driving_licence/drive_210603120358.pdf', 'photo/photo_210603120358.pdf', 'resume/resume_210603120358.pdf', 'interview_check/interview_check_210603120358.pdf', '', '0', '2021-06-03 12:03:58', '2021-06-03 12:03:58'),
(4, 32, 'AHZPH6577K', 'pan_card/pan_210603121242.pdf', '896042691019', 'aadhar_card/aadhar_front_210603121242.pdf', 'aadhar_card/aadhar_back_210603121242.pdf', '1984-06-19', 'Post Graduate', 'ICICI', '196701505963', 'ICIC0001967', 'bank_passbook/bnk_passbook_210603121242.pdf', 'education_proof/edu_proof_210603121242.pdf', 'rent_rotary/rent_210603121242.pdf', 'rent_rotary/rotary_210603121242.pdf', 'electricity_bill/electricity_210603121242.jfif', 'voter_id/voterid_210603121242.pdf', 'driving_licence/drive_210603121242.pdf', 'photo/photo_210603121242.jfif', 'resume/resume_210603121242.pdf', 'interview_check/interview_check_210603121242.pdf', '', '0', '2021-06-03 12:12:42', '2021-06-15 02:45:33'),
(5, 142, 'AVWPB8195Q', 'pan_card/pan_210603125034.pdf', '322895044741', 'aadhar_card/aadhar_front_210603125034.pdf', 'aadhar_card/aadhar_back_210603125034.pdf', '1991-10-04', 'SSC', 'SYNIDICATE BANK', '53312180004285', 'SYNB0005331', 'bank_passbook/bnk_passbook_210603125034.pdf', 'education_proof/edu_proof_210603125034.pdf', 'rent_rotary/rent_210915010458.pdf', 'rent_rotary/rotary_210915010458.pdf', 'electricity_bill/electricity_210603125034.pdf', 'voter_id/voterid_210603125034.pdf', 'driving_licence/drive_210603125034.pdf', 'photo/photo_210603125034.pdf', 'resume/resume_210603125034.pdf', 'interview_check/interview_check_210603125034.pdf', '', '0', '2021-06-03 12:50:34', '2021-09-15 08:04:58'),
(6, 41, 'ANYPK4813E', 'pan_card/pan_210603011104.pdf', '618080591043', 'aadhar_card/aadhar_front_210603011104.pdf', 'aadhar_card/aadhar_back_210603011104.pdf', '1973-10-06', 'HSC', 'HDFC BANK', '50100229576031', 'HDFC0000633', 'bank_passbook/bnk_passbook_210603011104.pdf', 'education_proof/edu_proof_210603011104.pdf', NULL, NULL, 'electricity_bill/electricity_210603011104.pdf', 'voter_id/voterid_210603011104.pdf', 'driving_licence/drive_210603011104.pdf', 'photo/photo_210603011104.pdf', 'resume/resume_210603011104.pdf', 'interview_check/interview_check_210603011104.pdf', '', '0', '2021-06-03 13:11:04', '2021-06-03 13:11:04'),
(7, 139, 'ALQPG2996L', 'pan_card/pan_210604063219.pdf', '923103987781', 'aadhar_card/aadhar_front_210604063219.pdf', 'aadhar_card/aadhar_back_210604063219.pdf', '1981-09-24', 'SSC', 'ICIC BANK', '000501580220', 'ICIC0000005', 'bank_passbook/bnk_passbook_210604063219.pdf', 'education_proof/edu_proof_210604063219.pdf', 'rent_rotary/rent_210915125106.pdf', 'rent_rotary/rotary_210915125106.pdf', 'electricity_bill/electricity_210604063219.pdf', 'voter_id/voterid_210915122546.pdf', 'driving_licence/drive_210604063219.pdf', 'photo/photo_210604063219.pdf', 'resume/resume_210604063219.pdf', 'interview_check/interview_check_210604063219.pdf', '', '0', '2021-06-04 06:32:19', '2021-09-15 07:51:06'),
(8, 135, 'AJXPD7734L', 'pan_card/pan_210604073626.pdf', '851571601337', 'aadhar_card/aadhar_front_210604073626.pdf', 'aadhar_card/aadhar_back_210604073626.pdf', '1983-05-17', 'HSC', 'HDFC BANK', '50100332123092', 'HDFC0000633', 'bank_passbook/bnk_passbook_210604073626.pdf', 'education_proof/edu_proof_210604073626.pdf', NULL, NULL, 'electricity_bill/electricity_210604073626.pdf', NULL, 'driving_licence/drive_210604073626.pdf', 'photo/photo_210604073626.pdf', 'resume/resume_210604073626.pdf', 'interview_check/interview_check_210604073626.pdf', '', '0', '2021-06-04 07:36:26', '2021-06-04 07:36:26'),
(9, 45, 'AFKPT6785N', 'pan_card/pan_210604110028.pdf', '955692135682', 'aadhar_card/aadhar_front_210604110028.pdf', 'aadhar_card/aadhar_back_210604110028.pdf', '1979-07-18', 'Any Graduate', 'INDIAN BANK', '6956092174', 'IDIB000N012', 'bank_passbook/bnk_passbook_210604110028.pdf', 'education_proof/edu_proof_210604110028.pdf', NULL, NULL, 'electricity_bill/electricity_210604110028.pdf', NULL, 'driving_licence/drive_210604110028.pdf', 'photo/photo_210604110028.pdf', 'resume/resume_210604110028.pdf', 'interview_check/interview_check_210604110028.pdf', '', '0', '2021-06-04 11:00:28', '2021-06-04 11:00:28'),
(10, 116, 'BKID0000518', 'pan_card/pan_210604113543.pdf', '694757284747', 'aadhar_card/aadhar_front_210604113543.pdf', 'aadhar_card/aadhar_back_210604113543.pdf', '1970-04-15', 'Any Graduate', 'BANK OF INDIA', '51810110009838', 'BKID0000518', NULL, 'education_proof/edu_proof_210604113543.pdf', NULL, NULL, 'electricity_bill/electricity_210604113543.pdf', NULL, NULL, 'photo/photo_210604113543.pdf', 'resume/resume_210604113543.pdf', 'interview_check/interview_check_210604113543.pdf', '', '0', '2021-06-04 11:35:43', '2021-06-04 11:35:43'),
(11, 36, 'ABKPY0975F', 'pan_card/pan_210604114825.pdf', '243869007727', 'aadhar_card/aadhar_front_210604114825.pdf', 'aadhar_card/aadhar_back_210604114825.pdf', '1981-06-30', 'HSC', 'INDIAN BANK', '6306734283', 'IDIB000N012', 'bank_passbook/bnk_passbook_210604114825.pdf', 'education_proof/edu_proof_210604114825.pdf', NULL, NULL, 'electricity_bill/electricity_210604114825.pdf', 'voter_id/voterid_210604114825.pdf', NULL, 'photo/photo_210604114825.pdf', 'resume/resume_210604114825.pdf', 'interview_check/interview_check_210604114825.pdf', '', '0', '2021-06-04 11:48:25', '2021-06-04 11:48:25'),
(12, 61, 'ANYPB4853F', 'pan_card/pan_210604120701.pdf', '530561834788', 'aadhar_card/aadhar_front_210604120701.pdf', 'aadhar_card/aadhar_back_210604120701.pdf', '1987-10-20', 'SSC', 'HDFC BANK', '50100332123270', 'HDFC0000633', 'bank_passbook/bnk_passbook_210604120701.pdf', 'education_proof/edu_proof_210604120701.pdf', NULL, NULL, 'electricity_bill/electricity_210604120701.pdf', NULL, 'driving_licence/drive_210604120701.pdf', 'photo/photo_210604120701.pdf', 'resume/resume_210604120701.pdf', 'interview_check/interview_check_210604120701.pdf', '', '0', '2021-06-04 12:07:01', '2021-06-04 12:07:01'),
(13, 136, 'AMVPD3913C', 'pan_card/pan_210604122343.pdf', '596987393244', 'aadhar_card/aadhar_front_210604122343.pdf', 'aadhar_card/aadhar_back_210604122343.pdf', '1979-10-06', 'HSC', 'HDFC BANK', '50100332122929', 'HDFC0000633', 'bank_passbook/bnk_passbook_210604122343.pdf', 'education_proof/edu_proof_210604122343.pdf', NULL, NULL, 'electricity_bill/electricity_210604122343.pdf', NULL, 'driving_licence/drive_210604122343.pdf', 'photo/photo_210604122343.pdf', 'resume/resume_210604122343.pdf', 'interview_check/interview_check_210604122343.pdf', '', '0', '2021-06-04 12:23:43', '2021-06-04 12:23:43'),
(14, 140, 'AIPPD9597C', 'pan_card/pan_210604125111.pdf', '372764269593', 'aadhar_card/aadhar_front_210604125111.pdf', 'aadhar_card/aadhar_back_210604125111.pdf', '1974-05-19', 'SSC', 'HDFC BANK', '50100332123116', 'HDFC0000633', 'bank_passbook/bnk_passbook_210604125111.pdf', 'education_proof/edu_proof_210604125111.pdf', NULL, NULL, 'electricity_bill/electricity_210604125111.pdf', 'voter_id/voterid_210915010215.pdf', 'driving_licence/drive_210604125111.pdf', 'photo/photo_210604125111.pdf', 'resume/resume_210604125111.pdf', 'interview_check/interview_check_210604125111.pdf', '', '0', '2021-06-04 12:51:11', '2021-09-15 08:02:15'),
(15, 230, 'ANJPS3258A', 'pan_card/pan_210604012430.pdf', '224927077321', 'aadhar_card/aadhar_front_210604012430.pdf', 'aadhar_card/aadhar_back_210604012430.pdf', '1977-12-03', 'Any Graduate', 'KALYAN JANTA SAHKARI BANK', '214010100000440', 'KJSB0000214', 'bank_passbook/bnk_passbook_210604012430.pdf', 'education_proof/edu_proof_210604012430.pdf', NULL, NULL, 'electricity_bill/electricity_210604012430.pdf', NULL, 'driving_licence/drive_210604012430.pdf', 'photo/photo_210604012430.pdf', 'resume/resume_210604012430.pdf', 'interview_check/interview_check_210604012430.pdf', '', '0', '2021-06-04 13:24:30', '2021-06-04 13:24:30'),
(16, 35, 'AGGPB8444H', 'pan_card/pan_210604021435.pdf', '526565579816', 'aadhar_card/aadhar_front_210604021435.pdf', 'aadhar_card/aadhar_back_210604021435.pdf', '1974-07-10', 'HSC', 'HDFC', '04271000020713', 'HDFC0000427', NULL, 'education_proof/edu_proof_210604021435.pdf', NULL, NULL, 'electricity_bill/electricity_210604021435.pdf', NULL, 'driving_licence/drive_210604021435.pdf', 'photo/photo_210604021435.pdf', 'resume/resume_210604021435.pdf', 'interview_check/interview_check_210604021435.pdf', '', '0', '2021-06-04 14:14:35', '2021-06-04 14:14:35'),
(17, 109, 'AEGPH2908Q', 'pan_card/pan_210604022843.pdf', '565671957086', 'aadhar_card/aadhar_front_210604022843.pdf', 'aadhar_card/aadhar_back_210604022843.pdf', '1985-11-20', 'Any Graduate', 'CORPORATION BANK', '520101200245666', 'CORP0000785', NULL, 'education_proof/edu_proof_210604022843.pdf', NULL, NULL, 'electricity_bill/electricity_210604022843.pdf', NULL, 'driving_licence/drive_210604022843.pdf', 'photo/photo_210604022843.pdf', 'resume/resume_210604022843.pdf', 'interview_check/interview_check_210604022843.pdf', '', '0', '2021-06-04 14:28:43', '2021-06-04 14:28:43'),
(18, 122, 'BRJPN7334F', 'pan_card/pan_210604025029.pdf', '598728611746', 'aadhar_card/aadhar_front_210604025029.pdf', 'aadhar_card/aadhar_back_210604025029.pdf', '2000-02-00', 'HSC', 'KOTAK BANK', '4312608428', 'KKBK0000725', NULL, 'education_proof/edu_proof_210604025029.pdf', NULL, NULL, 'electricity_bill/electricity_210604025029.pdf', 'voter_id/voterid_211004095313.pdf', 'driving_licence/drive_210604025029.pdf', 'photo/photo_210604025029.pdf', 'resume/resume_210604025029.pdf', 'interview_check/interview_check_210604025029.pdf', '', '0', '2021-06-04 14:50:29', '2021-10-04 04:53:13'),
(19, 120, 'BBEPK8670C', 'pan_card/pan_210604030657.pdf', '397426088015', 'aadhar_card/aadhar_front_210604030657.pdf', 'aadhar_card/aadhar_back_210604030657.pdf', '1987-03-15', 'SSC', 'STATE BANK OF INDIA', '30656740048', 'SBIN0000575', NULL, 'education_proof/edu_proof_210604030657.pdf', NULL, NULL, 'electricity_bill/electricity_210604030657.pdf', NULL, 'driving_licence/drive_210604030657.pdf', 'photo/photo_210604030657.pdf', 'resume/resume_210604030657.pdf', 'interview_check/interview_check_210604030657.pdf', '', '0', '2021-06-04 15:06:57', '2021-06-04 15:06:57'),
(20, 131, 'ACGPE8818G', 'pan_card/pan_210604034633.pdf', '429763980173', 'aadhar_card/aadhar_front_210604034633.pdf', 'aadhar_card/aadhar_back_210604034633.pdf', '1996-06-27', 'SSC', 'UNION BANK', '589502010025609', 'UBIN0558958', NULL, 'education_proof/edu_proof_210604034633.pdf', NULL, NULL, 'electricity_bill/electricity_210604034633.pdf', NULL, NULL, 'photo/photo_210604034633.pdf', 'resume/resume_210604034633.pdf', 'interview_check/interview_check_210604034633.pdf', '', '0', '2021-06-04 15:46:33', '2021-06-04 15:46:33'),
(21, 221, 'APXPD4100Q', 'pan_card/pan_210604035443.pdf', '481626236738', 'aadhar_card/aadhar_front_210604035443.pdf', 'aadhar_card/aadhar_back_210604035443.pdf', '1984-02-19', 'Any Graduate', 'KOTAK BANK', '5913427340', 'KKBK0001774', 'bank_passbook/bnk_passbook_210604035443.pdf', 'education_proof/edu_proof_210604035443.pdf', NULL, NULL, 'electricity_bill/electricity_210604035443.pdf', 'voter_id/voterid_210604035443.pdf', NULL, 'photo/photo_210604035443.pdf', 'resume/resume_210604035443.pdf', 'interview_check/interview_check_210604035443.pdf', '', '0', '2021-06-04 15:54:43', '2021-06-04 15:54:43'),
(22, 66, 'BAMPP1297F', 'pan_card/pan_210605085101.pdf', '407051242687', 'aadhar_card/aadhar_front_210605085101.pdf', 'aadhar_card/aadhar_back_210605085101.pdf', '1980-11-19', 'HSC', 'HDFC BANK', '50100320681491', 'HDFC0000633', 'bank_passbook/bnk_passbook_210605085101.pdf', 'education_proof/edu_proof_210605085101.pdf', NULL, NULL, 'electricity_bill/electricity_210605085101.pdf', 'voter_id/voterid_210605085101.pdf', NULL, 'photo/photo_210605085101.pdf', 'resume/resume_210605085101.pdf', 'interview_check/interview_check_210605085101.pdf', '', '0', '2021-06-05 08:51:01', '2021-06-05 08:51:01'),
(23, 141, 'AHAPA3065G', 'pan_card/pan_210605085511.pdf', '758388359273', 'aadhar_card/aadhar_front_210605085511.pdf', 'aadhar_card/aadhar_back_210605085511.pdf', '1972-09-27', 'HSC', 'HDFC BANK', '50100332122981', 'HDFC0000633', 'bank_passbook/bnk_passbook_210605085511.pdf', 'education_proof/edu_proof_210605085511.pdf', NULL, NULL, 'electricity_bill/electricity_210605085511.pdf', 'voter_id/voterid_210605085511.pdf', NULL, 'photo/photo_210605085511.pdf', 'resume/resume_210605085511.pdf', 'interview_check/interview_check_210605085511.pdf', '', '0', '2021-06-05 08:55:11', '2021-06-05 08:55:11'),
(24, 138, 'ARZPD9804B', 'pan_card/pan_210605085936.pdf', '418353165642', 'aadhar_card/aadhar_front_210605085936.pdf', 'aadhar_card/aadhar_back_210605085936.pdf', '1983-08-23', 'HSC', 'HDFC BANK', '50100332123079', 'HDFC0000633', 'bank_passbook/bnk_passbook_210605085936.pdf', 'education_proof/edu_proof_210605085936.pdf', 'rent_rotary/rent_210915122451.pdf', NULL, 'electricity_bill/electricity_210605085936.pdf', 'voter_id/voterid_210915122451.pdf', 'driving_licence/drive_210605085936.pdf', 'photo/photo_210605085936.pdf', 'resume/resume_210605085936.pdf', 'interview_check/interview_check_210605085936.pdf', '', '0', '2021-06-05 08:59:36', '2021-09-15 07:24:51'),
(25, 212, 'BRUPA9437K', 'pan_card/pan_210605094939.pdf', '454790900233', 'aadhar_card/aadhar_front_210605094939.pdf', 'aadhar_card/aadhar_back_210605094939.pdf', '1988-12-07', 'HSC', 'BANK OF MAHARSHTRA', '20146767609', 'MAHB0000853', NULL, 'education_proof/edu_proof_210605094939.pdf', NULL, NULL, 'electricity_bill/electricity_210605094939.pdf', NULL, NULL, NULL, 'resume/resume_210605094939.pdf', 'interview_check/interview_check_210605094939.pdf', '', '0', '2021-06-05 09:49:39', '2021-06-05 09:49:39'),
(26, 97, 'AYZPP9553G', 'pan_card/pan_210605101354.pdf', '203805875708', 'aadhar_card/aadhar_front_210605101354.pdf', 'aadhar_card/aadhar_back_210605101354.pdf', '1980-12-01', 'SSC', 'BANK OF MAHARSHTRA', '68013436797', 'MAHB0000322', NULL, 'education_proof/edu_proof_210605101354.pdf', NULL, NULL, NULL, NULL, 'driving_licence/drive_210605101354.pdf', NULL, 'resume/resume_210605101354.pdf', NULL, '', '0', '2021-06-05 10:13:54', '2021-06-05 10:13:54'),
(27, 211, 'BQBPT2739P', 'pan_card/pan_210605102721.pdf', '646101496291', 'aadhar_card/aadhar_front_210605102721.pdf', 'aadhar_card/aadhar_back_210605102721.pdf', '2000-10-25', 'SSC', 'STATE BANK OF INDIA', '38870126135', 'SBIN0011647', NULL, 'education_proof/edu_proof_210605102721.pdf', NULL, NULL, 'electricity_bill/electricity_210605102721.pdf', NULL, 'driving_licence/drive_210605102721.pdf', NULL, 'resume/resume_210605102721.pdf', NULL, '', '0', '2021-06-05 10:27:21', '2021-06-05 10:27:21'),
(28, 50, 'EVMPK3245Q', 'pan_card/pan_210605103150.pdf', '382990688143', 'aadhar_card/aadhar_front_210605103150.pdf', 'aadhar_card/aadhar_back_210605103150.pdf', '1996-07-21', 'SSC', 'HDFC BANK', '50100333658641', 'HDFC0000633', 'bank_passbook/bnk_passbook_210605103150.pdf', 'education_proof/edu_proof_210605103150.pdf', NULL, NULL, 'electricity_bill/electricity_210605103150.pdf', NULL, NULL, 'photo/photo_210605103150.pdf', 'resume/resume_210605103150.pdf', 'interview_check/interview_check_210605103150.pdf', '', '0', '2021-06-05 10:31:50', '2021-06-05 10:31:50'),
(29, 82, 'BAVPK2845R', 'pan_card/pan_210605105852.pdf', '245569740481', 'aadhar_card/aadhar_front_210605105852.pdf', 'aadhar_card/aadhar_back_210605105852.pdf', '1981-02-07', 'SSC', 'HDFC BANK', '50100332123307', 'HDFC0000633', 'bank_passbook/bnk_passbook_210605105852.pdf', 'education_proof/edu_proof_210605105852.pdf', NULL, NULL, NULL, NULL, 'driving_licence/drive_210605105852.pdf', 'photo/photo_210605105852.jpg', 'resume/resume_210605105852.pdf', 'interview_check/interview_check_210605105852.pdf', '', '0', '2021-06-05 10:58:52', '2021-06-05 10:58:52'),
(30, 38, 'BWDPK2518K', 'pan_card/pan_210605112706.pdf', '708544010833', 'aadhar_card/aadhar_front_210605112706.pdf', 'aadhar_card/aadhar_back_210605112706.pdf', '1979-12-01', 'HSC', 'HDFC', '50100320681870', 'HDFC0000633', NULL, 'education_proof/edu_proof_210605112706.pdf', NULL, NULL, NULL, NULL, NULL, 'photo/photo_210605112706.pdf', 'resume/resume_210605112706.pdf', 'interview_check/interview_check_210605112706.pdf', '', '0', '2021-06-05 11:27:06', '2021-06-05 11:27:06'),
(31, 134, 'BYJPD2403C', 'pan_card/pan_210605112925.pdf', '936701988687', 'aadhar_card/aadhar_front_210605112925.pdf', 'aadhar_card/aadhar_back_210605112925.pdf', '1989-06-03', 'HSC', 'BANK OF MAHARSHTRA', '80050445116', 'MAHG0004145', NULL, 'education_proof/edu_proof_210605112925.pdf', 'rent_rotary/rent_210605112925.pdf', 'rent_rotary/rotary_210605112925.pdf', NULL, NULL, NULL, 'photo/photo_210605112925.pdf', 'resume/resume_210605112925.pdf', NULL, '', '0', '2021-06-05 11:29:25', '2021-06-05 11:29:25'),
(32, 119, 'CLAPR2739G', 'pan_card/pan_210605113430.pdf', '963725433327', 'aadhar_card/aadhar_front_210605113430.pdf', 'aadhar_card/aadhar_back_210605113430.pdf', '1997-11-05', 'HSC', 'HDFC', '50100407193886', 'HDFC0000633', NULL, 'education_proof/edu_proof_210605113430.pdf', NULL, NULL, 'electricity_bill/electricity_210605113430.pdf', NULL, NULL, 'photo/photo_210605113430.pdf', 'resume/resume_210605113430.pdf', NULL, '', '0', '2021-06-05 11:34:30', '2021-06-05 11:34:30'),
(33, 146, 'AMSPK7674G', 'pan_card/pan_210605113447.pdf', '247804601623', 'aadhar_card/aadhar_front_210605113447.pdf', 'aadhar_card/aadhar_back_210605113447.pdf', '1983-12-01', 'HSC', 'HDFC BANK', '50100331764577', 'HDFC0000633', 'bank_passbook/bnk_passbook_210605113447.pdf', 'education_proof/edu_proof_210605113447.pdf', NULL, NULL, 'electricity_bill/electricity_210605113447.pdf', 'voter_id/voterid_210915011111.pdf', 'driving_licence/drive_210605113447.pdf', 'photo/photo_210605113447.pdf', 'resume/resume_210605113447.pdf', 'interview_check/interview_check_210605113447.pdf', '', '0', '2021-06-05 11:34:47', '2021-09-15 08:11:11'),
(34, 213, 'EEIPS0845R', 'pan_card/pan_210605114237.pdf', '509668704496', 'aadhar_card/aadhar_front_210605114237.pdf', 'aadhar_card/aadhar_back_210605114237.pdf', '1991-01-27', 'HSC', 'CANARA BANK', '260108000924', 'CNRB0000260', 'bank_passbook/bnk_passbook_210605114237.pdf', 'education_proof/edu_proof_210605114237.pdf', NULL, NULL, 'electricity_bill/electricity_210605114237.pdf', NULL, 'driving_licence/drive_210605114237.pdf', NULL, 'resume/resume_210605114237.pdf', 'interview_check/interview_check_210605114237.pdf', '', '0', '2021-06-05 11:42:37', '2021-06-05 11:42:37'),
(35, 37, 'AJWPM0729K', 'pan_card/pan_210605115541.pdf', '954380206694', 'aadhar_card/aadhar_front_210605115541.pdf', 'aadhar_card/aadhar_back_210605115541.pdf', '1976-09-26', 'HSC', 'HDFC', '50100316327505', 'HDFC0000633', NULL, 'education_proof/edu_proof_210605115541.pdf', NULL, NULL, 'electricity_bill/electricity_210605115541.pdf', NULL, 'driving_licence/drive_210605115541.pdf', 'photo/photo_210605115541.pdf', 'resume/resume_210605115541.pdf', 'interview_check/interview_check_210605115541.pdf', '', '0', '2021-06-05 11:55:41', '2021-06-05 11:55:41'),
(36, 152, 'ACTPH4357E', 'pan_card/pan_210605120754.pdf', '796582363017', 'aadhar_card/aadhar_front_210605120754.pdf', 'aadhar_card/aadhar_back_210605120754.pdf', '1978-03-13', 'SSC', 'HDFC BANK', '50100332122892', 'HDFC0000633', 'bank_passbook/bnk_passbook_210605120754.pdf', 'education_proof/edu_proof_210605120754.pdf', NULL, NULL, 'electricity_bill/electricity_210605120754.pdf', NULL, 'driving_licence/drive_210605120754.pdf', 'photo/photo_210605120754.pdf', 'resume/resume_210605120754.pdf', 'interview_check/interview_check_210605120754.pdf', '', '0', '2021-06-05 12:07:54', '2021-06-05 12:07:54'),
(37, 235, 'ALRPP4061G', 'pan_card/pan_210605121418.pdf', '780974590330', 'aadhar_card/aadhar_front_210605121418.pdf', 'aadhar_card/aadhar_back_210605121418.pdf', '1975-09-15', 'Any Graduate', 'HDFC', '07941050003433', 'HDFC0000794', 'bank_passbook/bnk_passbook_210605121418.pdf', 'education_proof/edu_proof_210605121418.pdf', NULL, NULL, NULL, NULL, 'driving_licence/drive_210605121418.pdf', NULL, 'resume/resume_210605121418.pdf', NULL, '', '0', '2021-06-05 12:14:18', '2021-06-05 12:14:18'),
(38, 234, 'BHFPK3494M', 'pan_card/pan_210605123721.pdf', '9439340233788', 'aadhar_card/aadhar_front_210605123721.pdf', 'aadhar_card/aadhar_back_210605123721.pdf', '1982-07-17', 'Any Graduate', 'HDFC', '50100411762672', 'HDFC0003841', 'bank_passbook/bnk_passbook_210605123721.pdf', 'education_proof/edu_proof_210605123721.pdf', NULL, NULL, NULL, NULL, 'driving_licence/drive_210605123721.pdf', 'photo/photo_210605123721.pdf', 'resume/resume_210605123721.pdf', NULL, '', '0', '2021-06-05 12:37:21', '2021-06-05 12:37:21'),
(39, 236, 'AHEPB3809M', 'pan_card/pan_210605123814.pdf', '668381984851', 'aadhar_card/aadhar_front_210605123814.pdf', 'aadhar_card/aadhar_back_210605123814.pdf', '1979-02-09', 'HSC', 'HDFC', '50100334103079', 'HDFC0000103', 'bank_passbook/bnk_passbook_210605123814.pdf', 'education_proof/edu_proof_210605123814.pdf', NULL, NULL, 'electricity_bill/electricity_210605123814.pdf', NULL, NULL, 'photo/photo_210605123814.pdf', 'resume/resume_210605123814.pdf', 'interview_check/interview_check_210605123814.pdf', '', '0', '2021-06-05 12:38:14', '2021-06-05 12:38:14'),
(40, 216, 'AHSPN4627K', 'pan_card/pan_210605124412.pdf', '528781256866', 'aadhar_card/aadhar_front_210605124412.pdf', 'aadhar_card/aadhar_back_210605124412.pdf', '1984-08-23', 'SSC', 'HDFC BANK', '50100389046372', 'HDFC0000633', 'bank_passbook/bnk_passbook_210605124412.pdf', 'education_proof/edu_proof_210605124412.pdf', NULL, NULL, 'electricity_bill/electricity_210605124412.pdf', NULL, NULL, 'photo/photo_210605124412.pdf', 'resume/resume_210605124412.pdf', 'interview_check/interview_check_210605124412.pdf', '', '0', '2021-06-05 12:44:12', '2021-06-05 12:44:12'),
(41, 59, 'EFFPB7371B', 'pan_card/pan_210605124823.pdf', '299469990804', 'aadhar_card/aadhar_front_210605124823.pdf', 'aadhar_card/aadhar_back_210605124823.pdf', '1996-11-05', 'Any Graduate', 'HDFC', '50100405393773', 'HDFC0000633', 'bank_passbook/bnk_passbook_210605124823.pdf', 'education_proof/edu_proof_210605124823.pdf', NULL, NULL, NULL, NULL, NULL, 'photo/photo_210605124823.pdf', 'resume/resume_210605124823.pdf', 'interview_check/interview_check_210605124823.pdf', '', '0', '2021-06-05 12:48:23', '2021-06-05 12:48:23'),
(42, 225, 'AZXPP3414F', 'pan_card/pan_210605010049.pdf', '954669033809', 'aadhar_card/aadhar_front_210605010049.pdf', 'aadhar_card/aadhar_back_210605010049.pdf', '1989-08-25', 'SSC', 'PANJAB NATIONAL BANK', '2901000400008850', 'PUNB0290100', NULL, 'education_proof/edu_proof_210605010049.pdf', NULL, NULL, 'electricity_bill/electricity_210605010049.pdf', NULL, 'driving_licence/drive_210605010049.pdf', 'photo/photo_210605010049.pdf', 'resume/resume_210605010049.pdf', 'interview_check/interview_check_210605010049.pdf', '', '0', '2021-06-05 13:00:49', '2021-06-05 13:00:49'),
(43, 69, 'DCUPK9782A', 'pan_card/pan_210605011520.pdf', '411167986464', 'aadhar_card/aadhar_front_210605011520.pdf', 'aadhar_card/aadhar_back_210605011520.pdf', '1971-02-14', 'HSC', 'HDFC', '50100320680920', 'HDFC0000633', NULL, 'education_proof/edu_proof_210605011520.pdf', NULL, NULL, NULL, NULL, 'driving_licence/drive_210605011520.pdf', 'photo/photo_210605011520.pdf', 'resume/resume_210605011520.pdf', 'interview_check/interview_check_210605011520.pdf', '', '0', '2021-06-05 13:15:20', '2021-06-05 13:15:20'),
(44, 147, 'AQOPD7982J', 'pan_card/pan_210605012156.pdf', '610120612708', 'aadhar_card/aadhar_front_210605012156.pdf', 'aadhar_card/aadhar_back_210605012156.pdf', '1970-12-01', 'SSC', 'PUNJAB NATIONAL BANK', '2901000100415182', 'PUNB0290100', 'bank_passbook/bnk_passbook_210605012156.pdf', 'education_proof/edu_proof_210605012156.pdf', NULL, NULL, 'electricity_bill/electricity_210605012156.pdf', NULL, 'driving_licence/drive_210605012156.pdf', 'photo/photo_210605012156.pdf', 'resume/resume_210605012156.pdf', 'interview_check/interview_check_210605012156.pdf', '', '0', '2021-06-05 13:21:56', '2021-06-05 13:21:56'),
(45, 79, 'ALLPH8943H', 'pan_card/pan_210605012840.pdf', '630438415596', 'aadhar_card/aadhar_front_210605012840.pdf', 'aadhar_card/aadhar_back_210605012840.pdf', '1990-10-09', 'HSC', 'HDFC', '50100332122916', 'HDFC0000103', NULL, 'education_proof/edu_proof_210605012840.pdf', NULL, NULL, NULL, NULL, NULL, NULL, 'resume/resume_210605012840.pdf', 'interview_check/interview_check_210605012840.pdf', '', '0', '2021-06-05 13:28:40', '2021-06-05 13:28:40'),
(46, 237, 'AULPJ0875F', 'pan_card/pan_210605013704.pdf', '217281347377', 'aadhar_card/aadhar_front_210605013704.pdf', 'aadhar_card/aadhar_back_210605013704.pdf', '1985-02-02', 'Any Graduate', 'SHAMRAO VITHAL CO OPRATIVE BANK', '91001010756', 'SVBL0000001', NULL, 'education_proof/edu_proof_210605013704.pdf', NULL, NULL, 'electricity_bill/electricity_210605013704.pdf', NULL, NULL, 'photo/photo_210605013704.pdf', 'resume/resume_210605013704.pdf', 'interview_check/interview_check_210605013704.pdf', '', '0', '2021-06-05 13:37:04', '2021-06-05 13:37:04'),
(47, 94, 'AXXPJ5704C', 'pan_card/pan_210605014724.pdf', '897092704774', 'aadhar_card/aadhar_front_210605014724.pdf', 'aadhar_card/aadhar_back_210605014724.pdf', '1994-03-20', 'SSC', 'HDFC BANK', '50100376864945', 'HDFC0009268', 'bank_passbook/bnk_passbook_210605014724.pdf', 'education_proof/edu_proof_210605014724.pdf', NULL, NULL, 'electricity_bill/electricity_210605014724.pdf', NULL, 'driving_licence/drive_210605014724.pdf', 'photo/photo_210605014724.pdf', 'resume/resume_210605014724.pdf', 'interview_check/interview_check_210605014724.pdf', '', '0', '2021-06-05 13:47:24', '2021-06-05 13:47:24'),
(48, 33, 'ACWPL7766N', 'pan_card/pan_210606060440.jpg', '556206561177', 'aadhar_card/aadhar_front_210606060440.jpg', 'aadhar_card/aadhar_back_210606060440.jpg', '1985-05-20', 'Any Graduate', 'HDFC', '50100407192755', 'HDFC0000103', NULL, 'education_proof/edu_proof_210606060440.jpg', NULL, NULL, 'electricity_bill/electricity_210606060440.jpg', NULL, 'driving_licence/drive_210606060440.jpg', NULL, 'resume/resume_210606060440.jpg', NULL, '', '0', '2021-06-06 06:04:40', '2021-06-06 06:04:40'),
(49, 81, 'HBMPK4109Q', 'pan_card/pan_210606061945.pdf', '591053735145', 'aadhar_card/aadhar_front_210606061945.pdf', 'aadhar_card/aadhar_back_210606061945.pdf', '1996-04-23', 'HSC', 'HDFC', '50100332123260', 'HDFC0000103', NULL, 'education_proof/edu_proof_210606061945.pdf', NULL, NULL, NULL, NULL, 'driving_licence/drive_210606061945.pdf', 'photo/photo_210606061945.jpg', 'resume/resume_210606061945.pdf', 'interview_check/interview_check_210606061945.pdf', '', '0', '2021-06-06 06:19:45', '2021-06-06 06:19:45'),
(50, 92, 'CEQPK7585A', 'pan_card/pan_210606063327.pdf', '544632313449+', 'aadhar_card/aadhar_front_210606063327.pdf', 'aadhar_card/aadhar_back_210606063327.pdf', '1990-09-02', 'HSC', 'HDFC', '50100380853193', 'HDFC0009606', NULL, 'education_proof/edu_proof_210606063327.pdf', NULL, NULL, 'electricity_bill/electricity_210606063327.pdf', NULL, 'driving_licence/drive_210606063327.pdf', NULL, 'resume/resume_210606063327.pdf', 'interview_check/interview_check_210606063327.pdf', '', '0', '2021-06-06 06:33:27', '2021-06-06 06:33:27'),
(51, 70, 'BNAPB0058B', 'pan_card/pan_210606064318.jpg', '593672017363', 'aadhar_card/aadhar_front_210606064318.jpg', 'aadhar_card/aadhar_back_210606064318.jpg', '1989-11-17', 'HSC', 'HDFC', '50100389046830', 'HDFC0009606', NULL, 'education_proof/edu_proof_210606064318.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-06-06 06:43:18', '2021-06-06 06:43:18'),
(52, 107, 'ADCPL2421R', 'pan_card/pan_210606064512.pdf', '731998207064', 'aadhar_card/aadhar_front_210606064512.pdf', 'aadhar_card/aadhar_back_210606064512.pdf', '1984-03-23', 'HSC', 'HDFC', '50100394622189', 'HDFC0009526', NULL, 'education_proof/edu_proof_210606064512.pdf', NULL, NULL, NULL, NULL, 'driving_licence/drive_210606064512.pdf', 'photo/photo_210606064512.jpg', 'resume/resume_210606064512.pdf', 'interview_check/interview_check_210606064512.pdf', '', '0', '2021-06-06 06:45:12', '2021-06-06 06:45:12'),
(53, 105, 'AWNPG8968L', 'pan_card/pan_210606065940.pdf', '568619978751', 'aadhar_card/aadhar_front_210606065940.pdf', 'aadhar_card/aadhar_back_210606065940.pdf', '1984-10-25', 'Any Graduate', 'UNION BANK', '705702010008846', 'UBIN0570575', NULL, 'education_proof/edu_proof_210606065940.pdf', NULL, NULL, 'electricity_bill/electricity_210606065940.pdf', NULL, 'driving_licence/drive_210606065940.pdf', 'photo/photo_210606065940.jpg', 'resume/resume_210606065940.pdf', 'interview_check/interview_check_210606065940.pdf', '', '0', '2021-06-06 06:59:40', '2021-06-06 06:59:40'),
(54, 100, 'CTNPM3888A', 'pan_card/pan_210606071839.pdf', '742954986584', 'aadhar_card/aadhar_front_210606071839.pdf', 'aadhar_card/aadhar_back_210606071839.pdf', '1989-11-16', 'HSC', 'BANK OF MAHARASTRA', '60347263606', 'MAHB0000158', NULL, 'education_proof/edu_proof_210606071839.pdf', NULL, NULL, 'electricity_bill/electricity_210606071839.pdf', NULL, 'driving_licence/drive_210606071839.pdf', 'photo/photo_210606071839.jpg', 'resume/resume_210606071839.pdf', NULL, '', '0', '2021-06-06 07:18:39', '2021-06-06 07:18:39'),
(55, 115, 'DQJPA7547G', 'pan_card/pan_210606072536.pdf', '287942673252', 'aadhar_card/aadhar_front_210606072536.pdf', 'aadhar_card/aadhar_back_210606072536.pdf', '2001-10-08', 'Any Graduate', 'HDFC', '50100405393810', 'HDFC0009526', NULL, NULL, NULL, NULL, 'electricity_bill/electricity_210606072536.pdf', NULL, NULL, 'photo/photo_210606072536.jpg', 'resume/resume_210606072536.pdf', 'interview_check/interview_check_210606072536.pdf', '', '0', '2021-06-06 07:25:36', '2021-06-26 02:40:04'),
(56, 161, 'ADNPH6047C', 'pan_card/pan_210606075654.pdf', '373605100889', 'aadhar_card/aadhar_front_210606075654.pdf', 'aadhar_card/aadhar_back_210606075654.pdf', '1988-03-14', 'HSC', 'HDFC', '50100320681093', 'HDFC0009526', NULL, 'education_proof/edu_proof_210606075654.pdf', NULL, NULL, 'electricity_bill/electricity_210606075654.pdf', 'voter_id/voterid_210606075654.pdf', 'driving_licence/drive_210606075654.pdf', 'photo/photo_210606075654.jpg', 'resume/resume_210606075654.pdf', 'interview_check/interview_check_210606075654.pdf', '', '0', '2021-06-06 07:56:54', '2021-06-06 07:56:54'),
(57, 169, 'AVWPJ2443B', 'pan_card/pan_210606082601.pdf', '279495271407', 'aadhar_card/aadhar_front_210606082601.pdf', 'aadhar_card/aadhar_back_210606082601.pdf', '1994-08-20', 'SSC', 'HDFC', '50100268860474', 'HDFC0000007', NULL, 'education_proof/edu_proof_210606082601.pdf', NULL, NULL, 'electricity_bill/electricity_210606082601.pdf', NULL, NULL, 'photo/photo_210606082601.pdf', 'resume/resume_210606082601.pdf', 'interview_check/interview_check_210606082601.pdf', '', '0', '2021-06-06 08:26:01', '2021-06-06 08:26:01'),
(58, 43, 'CRGPB0466C', 'pan_card/pan_210606103631.pdf', '556053835308', 'aadhar_card/aadhar_front_210606103631.jpeg', 'aadhar_card/aadhar_back_210606103631.pdf', '1993-05-13', 'Any Graduate', 'HDFC', '50100331764703', 'HDFC0000103', 'bank_passbook/bnk_passbook_210606103631.pdf', 'education_proof/edu_proof_210606103631.pdf', NULL, NULL, 'electricity_bill/electricity_210606103631.pdf', 'voter_id/voterid_210606103631.pdf', NULL, NULL, 'resume/resume_210606103631.pdf', 'interview_check/interview_check_210606103631.pdf', '', '0', '2021-06-06 10:36:31', '2021-06-06 10:36:31'),
(59, 137, 'FFSPS8815F', 'pan_card/pan_210606104637.pdf', '802020445776', 'aadhar_card/aadhar_front_210606104637.pdf', 'aadhar_card/aadhar_back_210606104637.pdf', '1961-12-09', 'SSC', 'HDFC', '50100333658473', 'HDFC0000633', NULL, 'education_proof/edu_proof_210606104637.pdf', 'rent_rotary/rent_210915112741.pdf', NULL, 'electricity_bill/electricity_210606104637.pdf', 'voter_id/voterid_210606104637.pdf', NULL, NULL, 'resume/resume_210606104637.pdf', 'interview_check/interview_check_210606104637.pdf', '', '0', '2021-06-06 10:46:37', '2021-09-15 06:27:41'),
(60, 78, 'BRVPC5972J', 'pan_card/pan_210606105356.pdf', '697731076336', 'aadhar_card/aadhar_front_210606105356.pdf', 'aadhar_card/aadhar_back_210606105356.pdf', '1991-03-06', 'Any Graduate', 'HDFC', '50100332123053', 'HDFC0000633', NULL, 'education_proof/edu_proof_210606105356.pdf', NULL, NULL, 'electricity_bill/electricity_210606105356.pdf', NULL, NULL, NULL, 'resume/resume_210606105356.pdf', 'interview_check/interview_check_210606105356.pdf', '', '0', '2021-06-06 10:53:56', '2021-06-06 10:53:56'),
(61, 68, 'APCPT5483M', 'pan_card/pan_210606110124.pdf', '943609574095', 'aadhar_card/aadhar_front_210606110124.pdf', 'aadhar_card/aadhar_back_210606110124.pdf', '1993-01-14', 'HSC', 'HDFC', '50100313922392', 'HDFC0009167', NULL, 'education_proof/edu_proof_210606110124.pdf', NULL, NULL, NULL, NULL, NULL, NULL, 'resume/resume_210606110124.pdf', 'interview_check/interview_check_210606110124.pdf', '', '0', '2021-06-06 11:01:24', '2021-06-06 11:01:24'),
(62, 170, 'CBOPK2878P', 'pan_card/pan_210606111402.pdf', '86810460628', 'aadhar_card/aadhar_front_210606111402.pdf', 'aadhar_card/aadhar_back_210606111402.pdf', '1985-06-01', 'HSC', 'HDFC', '50100316327202', 'HDFC0000633', NULL, 'education_proof/edu_proof_210606111402.pdf', NULL, NULL, 'electricity_bill/electricity_210606111402.pdf', 'voter_id/voterid_210606111402.pdf', NULL, NULL, 'resume/resume_210606111402.pdf', 'interview_check/interview_check_210606111402.pdf', '', '0', '2021-06-06 11:14:02', '2021-06-06 11:14:02'),
(64, 165, 'AAVPZ8114B', 'pan_card/pan_210606112857.pdf', '219531411551', 'aadhar_card/aadhar_front_210606112857.pdf', 'aadhar_card/aadhar_back_210606112857.pdf', '1978-04-11', 'HSC', 'IDBI', '451104000038298', 'IBKL0000451', NULL, 'education_proof/edu_proof_210606112857.pdf', NULL, NULL, 'electricity_bill/electricity_210606112857.pdf', NULL, NULL, NULL, 'resume/resume_210606112857.pdf', NULL, '', '0', '2021-06-06 11:28:57', '2021-07-20 05:01:28'),
(65, 143, 'AHKPN1578M', 'pan_card/pan_210606114221.pdf', '731655035461', 'aadhar_card/aadhar_front_210606114221.pdf', 'aadhar_card/aadhar_back_210606114221.pdf', '1986-06-23', 'SSC', 'HDFC0000633', '50100320681308', 'HDFC0000633', NULL, 'education_proof/edu_proof_210606114221.pdf', NULL, NULL, 'electricity_bill/electricity_210606114221.pdf', 'voter_id/voterid_210915010622.pdf', 'driving_licence/drive_210606114221.pdf', NULL, 'resume/resume_210606114221.pdf', 'interview_check/interview_check_210606114221.pdf', '', '0', '2021-06-06 11:42:21', '2021-09-15 08:06:22'),
(66, 238, 'FHCPK7891F', 'pan_card/pan_210606120204.pdf', '947755701889', 'aadhar_card/aadhar_front_210606120204.pdf', 'aadhar_card/aadhar_back_210606120204.pdf', '1987-08-27', 'SSC', 'IDBI', '616104000231695', 'IBKL0000616', NULL, 'education_proof/edu_proof_210606120204.pdf', NULL, NULL, 'electricity_bill/electricity_210606120204.pdf', NULL, 'driving_licence/drive_210606120204.pdf', NULL, 'resume/resume_210606120204.pdf', 'interview_check/interview_check_210606120204.pdf', '', '0', '2021-06-06 12:02:04', '2021-06-06 12:02:04'),
(67, 80, 'EJKPM2776Q', 'pan_card/pan_210606121036.pdf', '362196959507', 'aadhar_card/aadhar_front_210606121036.pdf', 'aadhar_card/aadhar_back_210606121036.pdf', '1998-08-28', 'HSC', 'HDFC', '50100347240640', 'HDFC0004187', NULL, 'education_proof/edu_proof_210606121036.pdf', NULL, NULL, NULL, 'voter_id/voterid_210606121036.pdf', NULL, 'photo/photo_210606121036.pdf', 'resume/resume_210606121036.pdf', 'interview_check/interview_check_210606121036.pdf', '', '0', '2021-06-06 12:10:36', '2021-06-06 12:10:36'),
(68, 63, 'BDBPK4840H', 'pan_card/pan_210606122150.pdf', '480366937482', 'aadhar_card/aadhar_front_210606122150.pdf', 'aadhar_card/aadhar_back_210606122150.pdf', '1982-03-26', 'HSC', 'HDFC', '50100328004809', 'HDFC0000633', NULL, 'education_proof/edu_proof_210606122150.pdf', 'rent_rotary/rent_210915010807.pdf', NULL, 'electricity_bill/electricity_210606122150.pdf', 'voter_id/voterid_210606122150.pdf', NULL, NULL, 'resume/resume_210606122150.pdf', 'interview_check/interview_check_210606122150.pdf', '', '0', '2021-06-06 12:21:50', '2021-09-15 08:08:07'),
(69, 164, 'DDVPK5326Q', 'pan_card/pan_210606122152.pdf', '214481207318', 'aadhar_card/aadhar_front_210606122152.pdf', 'aadhar_card/aadhar_back_210606122152.pdf', '1991-04-05', 'HSC', 'HDFC', '50100328004006', 'HDFC0000633', NULL, 'education_proof/edu_proof_210606122152.pdf', NULL, NULL, 'electricity_bill/electricity_210606122152.pdf', NULL, 'driving_licence/drive_210606122152.pdf', NULL, 'resume/resume_210606122152.pdf', 'interview_check/interview_check_210606122152.pdf', '', '0', '2021-06-06 12:21:52', '2021-06-06 12:21:52'),
(70, 42, 'AOHPK0720N', 'pan_card/pan_210607060028.pdf', '249386723112', 'aadhar_card/aadhar_front_210607060028.pdf', 'aadhar_card/aadhar_back_210607060028.pdf', '1974-07-10', 'Any Graduate', 'HDFC BANK', '50100320681002', 'HDFC0000633', 'bank_passbook/bnk_passbook_210607060028.pdf', 'education_proof/edu_proof_210607060028.pdf', NULL, NULL, 'electricity_bill/electricity_210607060028.pdf', 'voter_id/voterid_210607060028.pdf', 'driving_licence/drive_210607060028.pdf', 'photo/photo_210607060028.pdf', NULL, 'interview_check/interview_check_210607060028.pdf', '', '0', '2021-06-07 06:00:28', '2021-06-07 06:00:28'),
(71, 75, 'BZMPP6267J', 'pan_card/pan_210607060517.pdf', '517028615585', 'aadhar_card/aadhar_front_210607060517.pdf', 'aadhar_card/aadhar_back_210607060517.pdf', '1986-07-04', 'HSC', 'HDFC BANK', '50100320681577', 'HDFC0000633', 'bank_passbook/bnk_passbook_210607060517.jpg', 'education_proof/edu_proof_210607060517.pdf', 'rent_rotary/rent_210607060517.pdf', 'rent_rotary/rotary_210607060517.pdf', 'electricity_bill/electricity_210607060517.pdf', 'voter_id/voterid_210916053106.pdf', 'driving_licence/drive_210607060517.pdf', 'photo/photo_210607060517.pdf', 'resume/resume_210607060517.pdf', 'interview_check/interview_check_210607060517.pdf', '', '0', '2021-06-07 06:05:17', '2021-09-16 00:31:06'),
(72, 144, 'BVSPP9953R', 'pan_card/pan_210607062157.pdf', '321022652458', 'aadhar_card/aadhar_front_210607062157.pdf', 'aadhar_card/aadhar_back_210607062157.pdf', '1989-01-28', 'HSC', 'HDFC', '50100316327471', 'HDFC0000633', NULL, 'education_proof/edu_proof_210607062157.pdf', 'rent_rotary/rent_210915010858.pdf', NULL, 'electricity_bill/electricity_210607062157.pdf', NULL, NULL, NULL, 'resume/resume_210607062157.pdf', NULL, '', '0', '2021-06-07 06:21:57', '2021-09-15 08:08:58'),
(73, 64, 'BPBPM5038R', 'pan_card/pan_210607071647.pdf', '363894531986', 'aadhar_card/aadhar_front_210607071647.pdf', 'aadhar_card/aadhar_back_210607071647.pdf', '1989-05-19', 'Any Graduate', 'HDFC', '50100001090739', 'HDFC0002502', NULL, 'education_proof/edu_proof_210607071647.pdf', NULL, NULL, 'electricity_bill/electricity_210607071647.pdf', 'voter_id/voterid_210607071647.pdf', NULL, NULL, NULL, NULL, '', '0', '2021-06-07 07:16:47', '2021-09-15 08:10:01'),
(74, 145, 'BDDPS8285A', 'pan_card/pan_210607074912.pdf', '786115561315', 'aadhar_card/aadhar_front_210607074912.pdf', 'aadhar_card/aadhar_back_210607074912.pdf', '1980-11-06', 'HSC', 'HDFC', '50100018759004', 'HDFC0000633', NULL, 'education_proof/edu_proof_210607074912.pdf', NULL, NULL, 'electricity_bill/electricity_210607074912.pdf', 'voter_id/voterid_210607074912.pdf', 'driving_licence/drive_210607074912.pdf', 'photo/photo_210607074912.pdf', 'resume/resume_210607074912.pdf', 'interview_check/interview_check_210607074912.pdf', '', '0', '2021-06-07 07:49:12', '2021-06-07 07:49:12'),
(75, 151, 'CHZPS1184B', 'pan_card/pan_210607080405.pdf', '597409999798', 'aadhar_card/aadhar_front_210607080405.pdf', 'aadhar_card/aadhar_back_210607080405.pdf', '1971-06-15', 'Any Graduate', 'Punjab National Bank', '2901000400008680', 'PUNB0290100', NULL, NULL, NULL, NULL, 'electricity_bill/electricity_210607080405.pdf', 'voter_id/voterid_210607080405.pdf', NULL, NULL, 'resume/resume_210607080405.pdf', NULL, '', '0', '2021-06-07 08:04:05', '2021-06-07 08:04:05'),
(77, 56, 'AZCPS4022R', 'pan_card/pan_210607083635.pdf', '276875130031', 'aadhar_card/aadhar_front_210607083635.pdf', 'aadhar_card/aadhar_back_210607083635.pdf', '1975-07-23', 'HSC', 'indian bank', '828447459', 'IDIB000N012', 'bank_passbook/bnk_passbook_210607083635.jpeg', 'education_proof/edu_proof_210607083635.pdf', NULL, NULL, 'electricity_bill/electricity_210607083635.pdf', NULL, NULL, 'photo/photo_210607083635.pdf', 'resume/resume_210607083635.pdf', 'interview_check/interview_check_210607083635.pdf', '', '0', '2021-06-07 08:36:35', '2021-06-07 08:36:35'),
(78, 148, 'ALZPB3489L', 'pan_card/pan_210607084231.pdf', '897897603873', 'aadhar_card/aadhar_front_210607084231.pdf', 'aadhar_card/aadhar_back_210607084231.pdf', '1979-03-06', 'HSC', 'HDFC', '50100320681666', 'HDFC0000633', NULL, 'education_proof/edu_proof_210607084231.pdf', NULL, NULL, 'electricity_bill/electricity_210607084231.pdf', NULL, NULL, 'photo/photo_210607084231.pdf', 'resume/resume_210607084231.pdf', NULL, '', '0', '2021-06-07 08:42:31', '2021-06-07 08:42:31'),
(79, 65, 'CIUPS5179J', 'pan_card/pan_210607094450.pdf', '634891428754', 'aadhar_card/aadhar_front_210607094450.pdf', 'aadhar_card/aadhar_back_210607094450.pdf', '1985-04-04', 'Any Graduate', 'HDFC', '50100320681551', 'HDFC0000633', 'bank_passbook/aadhar_210916054628.pdf', 'education_proof/edu_proof_210607094450.pdf', 'rent_rotary/rent_210916054629.pdf', NULL, 'electricity_bill/electricity_210607094450.pdf', 'voter_id/voterid_210915011152.pdf', 'driving_licence/drive_210607094450.pdf', 'photo/photo_210916054629.pdf', 'resume/resume_210916054901.pdf', 'interview_check/interview_check_210916054901.pdf', '', '0', '2021-06-07 09:44:50', '2021-09-16 00:49:58'),
(80, 166, 'AVKPR6202Q', 'pan_card/pan_210607102321.pdf', '422313795270', 'aadhar_card/aadhar_front_210607102321.pdf', 'aadhar_card/aadhar_back_210607102321.pdf', '1985-01-19', 'HSC', 'HDFC', '50100316327191', 'HDFC0000633', NULL, 'education_proof/edu_proof_210607102321.pdf', NULL, NULL, 'electricity_bill/electricity_210607102321.pdf', NULL, 'driving_licence/drive_210607102321.pdf', 'photo/photo_210607102321.pdf', 'resume/resume_210607102321.pdf', 'interview_check/interview_check_210607102321.pdf', '', '0', '2021-06-07 10:23:21', '2021-06-07 10:23:21'),
(81, 239, 'EKVPS8289E', 'pan_card/pan_210607120409.pdf', '215472229217', 'aadhar_card/aadhar_front_210607120409.pdf', 'aadhar_card/aadhar_back_210607120409.pdf', '1994-06-05', 'HSC', 'HDFC BANK', '50100395344396', 'HDFC0009606', 'bank_passbook/bnk_passbook_210607120409.pdf', 'education_proof/edu_proof_210607120409.pdf', 'rent_rotary/rent_210923081424.pdf', NULL, 'electricity_bill/electricity_210607120409.pdf', 'voter_id/voterid_210607120409.pdf', 'driving_licence/drive_210607120409.pdf', 'photo/photo_210607120409.jpg', 'resume/resume_210607120409.pdf', 'interview_check/interview_check_210607120409.pdf', 'vacc_receipt/vacc_receipt_211012070409.pdf', '0', '2021-06-07 12:04:09', '2021-10-12 02:04:09'),
(82, 51, 'FAHPR3903R', 'pan_card/pan_210607124202.pdf', '664764460114', 'aadhar_card/aadhar_front_210607124202.pdf', 'aadhar_card/aadhar_back_210607124202.pdf', '2000-06-01', 'HSC', 'HDFC', '50100370387730', 'HDFC0009526', 'bank_passbook/bnk_passbook_210607124202.pdf', NULL, NULL, NULL, 'electricity_bill/electricity_210607124202.pdf', NULL, NULL, 'photo/photo_210607124202.jpg', 'resume/resume_210607124202.pdf', 'interview_check/interview_check_210607124202.pdf', '', '0', '2021-06-07 12:42:02', '2021-06-07 12:42:02'),
(83, 150, 'ATTPM9823H', 'pan_card/pan_210607010928.pdf', '806777794413', 'aadhar_card/aadhar_front_210607010928.pdf', 'aadhar_card/aadhar_back_210607010928.pdf', '1979-05-16', 'SSC', 'HDFC', '50100332123244', 'HDFC0000633', NULL, 'education_proof/edu_proof_210607010928.pdf', NULL, NULL, 'electricity_bill/electricity_210607010928.pdf', NULL, 'driving_licence/drive_210607010928.pdf', NULL, 'resume/resume_210607010928.pdf', NULL, '', '0', '2021-06-07 13:09:28', '2021-06-07 13:09:28'),
(84, 206, 'CBAPK0879A', 'pan_card/pan_210607012310.pdf', '962758449842', 'aadhar_card/aadhar_front_210607012310.pdf', 'aadhar_card/aadhar_back_210607012310.pdf', '1992-12-29', 'Any Graduate', 'HDFC', '50100332123359', 'HDFC0000633', NULL, 'education_proof/edu_proof_210607012310.pdf', NULL, NULL, 'electricity_bill/electricity_210607012310.pdf', NULL, 'driving_licence/drive_210607012310.pdf', NULL, 'resume/resume_210607012310.pdf', NULL, '', '0', '2021-06-07 13:23:10', '2021-06-07 13:23:10'),
(85, 153, 'ASEPP0228K', 'pan_card/pan_210607014443.pdf', '357103262309', 'aadhar_card/aadhar_front_210607014443.pdf', 'aadhar_card/aadhar_back_210607014443.pdf', '1980-12-16', 'HSC', 'HDFC', '1851050119579', 'HDFC0000185', NULL, 'education_proof/edu_proof_210607014443.pdf', NULL, NULL, 'electricity_bill/electricity_210607014443.pdf', NULL, 'driving_licence/drive_210607014443.pdf', NULL, NULL, NULL, '', '0', '2021-06-07 13:44:43', '2021-06-07 13:44:43'),
(86, 203, 'DNLPS7599B', 'pan_card/pan_210607015904.pdf', '818112882873', 'aadhar_card/aadhar_front_210607015904.pdf', 'aadhar_card/aadhar_back_210607015904.pdf', '1992-09-11', 'HSC', 'HDFC', '50100331764399', 'HDFC0004884', NULL, 'education_proof/edu_proof_210607015904.pdf', NULL, NULL, 'electricity_bill/electricity_210607015904.pdf', 'voter_id/voterid_210607015904.pdf', NULL, NULL, NULL, NULL, '', '0', '2021-06-07 13:59:04', '2021-06-07 13:59:04'),
(87, 156, 'DYMPP5408N', 'pan_card/pan_210607021243.pdf', '791960838490', 'aadhar_card/aadhar_front_210607021243.pdf', 'aadhar_card/aadhar_back_210607021243.pdf', '1996-12-09', 'SSC', 'HDFC', '50100333658486', 'HDFC0000633', NULL, 'education_proof/edu_proof_210607021243.pdf', NULL, NULL, 'electricity_bill/electricity_210607021243.pdf', NULL, 'driving_licence/drive_210607021243.pdf', 'photo/photo_210607021243.pdf', 'resume/resume_210607021243.pdf', NULL, '', '0', '2021-06-07 14:12:43', '2021-06-07 14:12:43'),
(88, 155, 'AHVPT9123H', 'pan_card/pan_210607022031.pdf', '742304202765', 'aadhar_card/aadhar_front_210607022031.pdf', 'aadhar_card/aadhar_back_210607022031.pdf', '2017-10-25', 'HSC', 'HDFC', '50100320066429', 'HDFC0000437', NULL, 'education_proof/edu_proof_210607022031.pdf', NULL, NULL, 'electricity_bill/electricity_210607022031.pdf', 'voter_id/voterid_210607022031.pdf', NULL, NULL, 'resume/resume_210607022031.pdf', NULL, '', '0', '2021-06-07 14:20:31', '2021-06-07 14:20:31'),
(89, 71, 'CEIPK1053B', 'pan_card/pan_210607023006.pdf', '903824916830', 'aadhar_card/aadhar_front_210607023006.pdf', 'aadhar_card/aadhar_back_210607023006.pdf', '1993-07-30', 'HSC', 'HDFC', '50100316327468', 'HDFC0000633', NULL, 'education_proof/edu_proof_210607023006.pdf', NULL, NULL, 'electricity_bill/electricity_210607023006.pdf', NULL, 'driving_licence/drive_210607023006.pdf', 'photo/photo_210607023006.pdf', 'resume/resume_210607023006.pdf', NULL, '', '0', '2021-06-07 14:30:06', '2021-06-07 14:30:06'),
(90, 160, 'DUTPS4136P', 'pan_card/pan_210608080121.pdf', '447299097956', 'aadhar_card/aadhar_front_210608080121.pdf', 'aadhar_card/aadhar_back_210608080121.pdf', '1991-08-07', 'SSC', 'HDFC', '50100320681653', 'HDFC0000633', NULL, 'education_proof/edu_proof_210608080121.pdf', 'rent_rotary/rent_210608080121.pdf', NULL, 'electricity_bill/electricity_210608080121.pdf', NULL, 'driving_licence/drive_210608080121.pdf', NULL, 'resume/resume_210608080121.pdf', NULL, '', '0', '2021-06-08 08:01:21', '2021-06-08 08:01:21'),
(91, 172, 'APCPT7705A', 'pan_card/pan_210608095830.pdf', '562018109647', 'aadhar_card/aadhar_front_210608095830.pdf', 'aadhar_card/aadhar_back_210608095830.pdf', '1991-06-15', 'HSC', 'PNB', '481626236738', 'PUNB0290100', NULL, NULL, NULL, NULL, 'electricity_bill/electricity_210608095830.pdf', 'voter_id/voterid_210608095830.pdf', NULL, NULL, 'resume/resume_210608095830.pdf', NULL, '', '0', '2021-06-08 09:58:30', '2021-06-08 09:58:30');
INSERT INTO `ptx_documents` (`id`, `user_id`, `pan_card_number`, `pan_card_img`, `aadhar_card_number`, `aadhar_card_front_img`, `aadhar_card_back_img`, `dob`, `education`, `bnk_name`, `bnk_acc_number`, `ifsc_code`, `bnk_pass_img`, `edu_proof_img`, `rent_agmnt_img`, `rotary_agmnt_img`, `elect_bill_img`, `voter_id`, `driving_licence`, `photo`, `resume`, `interview_check`, `vacc_receipt`, `isDelete`, `created_at`, `updated_at`) VALUES
(92, 72, 'GAGPS6719Q', 'pan_card/pan_210608101535.pdf', '598201181723', 'aadhar_card/aadhar_front_210608101535.pdf', 'aadhar_card/aadhar_back_210608101535.pdf', '1995-06-12', 'HSC', 'HDFC', '50100332123310', 'HDFC0000633', NULL, 'education_proof/edu_proof_210608101535.pdf', NULL, NULL, 'electricity_bill/electricity_210608101535.pdf', NULL, 'driving_licence/drive_210608101535.pdf', NULL, 'resume/resume_210608101535.pdf', NULL, '', '0', '2021-06-08 10:15:35', '2021-06-08 10:15:35'),
(93, 204, 'BHCPJ1148N', 'pan_card/pan_210608020446.pdf', '768666282117', 'aadhar_card/aadhar_front_210608020446.pdf', 'aadhar_card/aadhar_back_210608020446.pdf', '1996-06-16', 'Any Graduate', 'HDFC', '50100320681449', 'HDFC0000633', NULL, 'education_proof/edu_proof_210608020446.pdf', NULL, NULL, 'electricity_bill/electricity_210608020446.pdf', 'voter_id/voterid_210608020446.pdf', NULL, NULL, 'resume/resume_210608020446.pdf', NULL, '', '0', '2021-06-08 14:04:46', '2021-06-08 14:04:46'),
(94, 162, 'ANDPD1056H', 'pan_card/pan_210608021417.pdf', '798566782770', 'aadhar_card/aadhar_front_210608021417.pdf', 'aadhar_card/aadhar_back_210608021417.pdf', '1982-01-06', 'SSC', 'HDFC', '50100332123333', 'HDFC0000633', NULL, 'education_proof/edu_proof_210608021417.pdf', NULL, NULL, 'electricity_bill/electricity_210608021417.pdf', NULL, 'driving_licence/drive_210608021417.pdf', NULL, 'resume/resume_210608021417.pdf', NULL, '', '0', '2021-06-08 14:14:17', '2021-06-08 14:14:17'),
(95, 83, 'BEBPG2345J', 'pan_card/pan_210608022300.pdf', '440242033048', 'aadhar_card/aadhar_front_210608022300.pdf', 'aadhar_card/aadhar_back_210608022300.pdf', '1994-01-21', 'HSC', 'HDFC', '50100211848159', 'HDFC0003649', NULL, 'education_proof/edu_proof_210608022300.pdf', NULL, NULL, 'electricity_bill/electricity_210608022300.pdf', 'voter_id/voterid_210608022300.pdf', NULL, NULL, 'resume/resume_210608022300.pdf', NULL, '', '0', '2021-06-08 14:23:00', '2021-06-08 14:23:00'),
(96, 205, 'CUBPK4950Q', 'pan_card/pan_210608023103.pdf', '316327389528', 'aadhar_card/aadhar_front_210608023103.pdf', 'aadhar_card/aadhar_back_210608023103.pdf', '1983-05-11', 'HSC', 'HDFC', '50100320681630', 'HDFC0000633', NULL, 'education_proof/edu_proof_210608023103.pdf', NULL, NULL, 'electricity_bill/electricity_210608023103.pdf', NULL, 'driving_licence/drive_210608023103.pdf', NULL, 'resume/resume_210608023103.pdf', NULL, '', '0', '2021-06-08 14:31:03', '2021-06-08 14:31:03'),
(97, 99, 'ASUPT8885E', 'pan_card/pan_210609074106.pdf', '506077651608', 'aadhar_card/aadhar_front_210609074106.pdf', 'aadhar_card/aadhar_back_210609074106.pdf', '1986-11-20', 'HSC', 'HDFC', '50100377932046', 'HDFC0004887', NULL, 'education_proof/edu_proof_210609074106.pdf', NULL, NULL, 'electricity_bill/electricity_210609074106.pdf', NULL, NULL, 'photo/photo_210609074106.jpeg', 'resume/resume_210609074106.pdf', 'interview_check/interview_check_210609074106.pdf', '', '0', '2021-06-09 07:41:06', '2021-06-09 07:41:06'),
(98, 62, 'CIOPB7196N', 'pan_card/pan_210609081303.pdf', '439243699453', 'aadhar_card/aadhar_front_210609081303.pdf', 'aadhar_card/aadhar_back_210609081303.pdf', '1992-09-21', 'HSC', 'HDFC', '50100328003845', 'HDFC0004887', NULL, 'education_proof/edu_proof_210609081303.pdf', NULL, NULL, NULL, NULL, NULL, 'photo/photo_210609081303.jpg', 'resume/resume_210609081303.pdf', 'interview_check/interview_check_210609081303.pdf', '', '0', '2021-06-09 08:13:03', '2021-06-16 07:30:49'),
(99, 101, 'AMCPG1590P', 'pan_card/pan_210609093109.pdf', '259328095910', 'aadhar_card/aadhar_front_210609093109.pdf', 'aadhar_card/aadhar_back_210609093109.pdf', '1982-01-20', 'HSC', 'HDFC', '50100389046359', 'HDFC0009526', NULL, 'education_proof/edu_proof_210609093109.pdf', NULL, NULL, 'electricity_bill/electricity_210609093109.pdf', 'voter_id/voterid_210609093109.pdf', 'driving_licence/drive_210609093109.jpg', 'photo/photo_210609093109.jpg', 'resume/resume_210609093109.pdf', NULL, '', '0', '2021-06-09 09:31:09', '2021-06-09 09:31:09'),
(100, 218, 'BLPPS7775D', 'pan_card/pan_210609110550.jpeg', '964903645317', 'aadhar_card/aadhar_front_210609110550.jpeg', 'aadhar_card/aadhar_back_210609110550.jpeg', '1983-05-31', 'HSC', 'HDFC', '50100389046409', 'HDFC0009526', NULL, 'education_proof/edu_proof_210609110550.pdf', NULL, NULL, 'electricity_bill/electricity_210609110550.pdf', NULL, 'driving_licence/drive_210609110550.pdf', 'photo/photo_210609110550.jpg', 'resume/resume_210609110550.pdf', NULL, '', '0', '2021-06-09 11:05:50', '2021-06-09 11:05:50'),
(101, 219, 'AKHPP1060E', 'pan_card/pan_210609112655.pdf', '600570231599', 'aadhar_card/aadhar_front_210609112655.pdf', 'aadhar_card/aadhar_back_210609112655.pdf', '1979-07-07', 'SSC', 'HDFC', '50100391586968', 'HDFC0000825', NULL, 'education_proof/edu_proof_210609112655.pdf', NULL, NULL, NULL, NULL, 'driving_licence/drive_210609112655.pdf', 'photo/photo_210609112655.jpg', 'resume/resume_210609112655.pdf', 'interview_check/interview_check_210609112655.pdf', '', '0', '2021-06-09 11:26:55', '2021-06-09 11:26:55'),
(102, 241, 'BZSPK7026G', 'pan_card/pan_210609115701.pdf', '550519604987', 'aadhar_card/aadhar_front_210609115701.jpg', 'aadhar_card/aadhar_back_210609115701.jpg', '1990-12-16', 'Any Graduate', 'IDBI', '0062104000223676', 'IBKL0000062', 'bank_passbook/bnk_passbook_210609115701.jpeg', 'education_proof/edu_proof_210609115701.pdf', NULL, NULL, 'electricity_bill/electricity_210609115701.pdf', NULL, NULL, 'photo/photo_210609115701.jpg', 'resume/resume_210609115701.pdf', 'interview_check/interview_check_210609115701.pdf', '', '0', '2021-06-09 11:57:01', '2021-06-09 11:57:01'),
(103, 123, 'EQMPR3091A', 'pan_card/pan_210609120235.pdf', '492072498996', 'aadhar_card/aadhar_front_210609120235.pdf', 'aadhar_card/aadhar_back_210609120235.pdf', '1998-02-09', 'Any Graduate', 'HDFC', '50100376044041', 'HDFC0000633', NULL, 'education_proof/edu_proof_210609120235.pdf', NULL, NULL, 'electricity_bill/electricity_210609120235.pdf', 'voter_id/voterid_210609120235.pdf', NULL, 'photo/photo_210609120235.jpeg', 'resume/resume_210609120235.pdf', 'interview_check/interview_check_210609120235.pdf', '', '0', '2021-06-09 12:02:35', '2021-06-09 12:02:35'),
(104, 124, 'CXIPA2091Q', 'pan_card/pan_210609122505.pdf', '392427640686', 'aadhar_card/aadhar_front_210609122505.pdf', 'aadhar_card/aadhar_back_210609122505.pdf', '1999-04-05', 'HSC', 'HDFC', '50100394624245', 'HDFC0009526', NULL, 'education_proof/edu_proof_210609122505.pdf', NULL, NULL, 'electricity_bill/electricity_210609122505.pdf', NULL, NULL, 'photo/photo_210609122505.jpg', 'resume/resume_210609122505.pdf', 'interview_check/interview_check_210609122505.pdf', '', '0', '2021-06-09 12:25:05', '2021-06-09 12:25:05'),
(105, 106, 'AHNPT4863E', 'pan_card/pan_210610060511.pdf', '606396855352', 'aadhar_card/aadhar_front_210610060511.pdf', 'aadhar_card/aadhar_back_210610060511.pdf', '1984-11-03', 'Any Graduate', 'HDFC', '50100405395499', 'HDFC0009526', 'bank_passbook/bnk_passbook_210610060511.jpeg', 'education_proof/edu_proof_210610060511.pdf', NULL, NULL, 'electricity_bill/electricity_210610060511.pdf', 'voter_id/voterid_210916055636.pdf', 'driving_licence/drive_210916055636.pdf', 'photo/photo_210610060511.jpg', 'resume/resume_210610060511.pdf', 'interview_check/interview_check_210610060511.pdf', '', '0', '2021-06-10 06:05:11', '2021-09-16 00:56:36'),
(106, 174, 'DZXPM1649N', 'pan_card/pan_210610072940.pdf', '246180123393', 'aadhar_card/aadhar_front_210610072940.pdf', 'aadhar_card/aadhar_back_210610072940.pdf', '1998-07-31', 'SSC', 'UNION BANK OF INDIA', '583002010021711', 'UBIN0558958', NULL, 'education_proof/edu_proof_210610072940.pdf', NULL, NULL, 'electricity_bill/electricity_210610072940.pdf', NULL, NULL, NULL, 'resume/resume_210610072940.pdf', NULL, '', '0', '2021-06-10 07:29:40', '2021-06-10 07:29:40'),
(107, 126, 'FVAPP2551K', 'pan_card/pan_210610074246.pdf', '698043656482', 'aadhar_card/aadhar_front_210610074246.pdf', 'aadhar_card/aadhar_back_210610074246.pdf', '1999-09-18', 'HSC', 'DSB BANK', '881037385095', 'DBSS0IN0811', 'bank_passbook/bnk_passbook_210610074246.jpeg', 'education_proof/edu_proof_210610074246.pdf', NULL, NULL, 'electricity_bill/electricity_210610074246.pdf', NULL, NULL, 'photo/photo_210610074246.jpg', 'resume/resume_210610074246.pdf', 'interview_check/interview_check_210610074246.pdf', '', '0', '2021-06-10 07:42:46', '2021-06-10 07:42:46'),
(108, 242, 'AFVPU5915N', 'pan_card/pan_210610080648.pdf', '891463970830', 'aadhar_card/aadhar_front_210610080648.pdf', 'aadhar_card/aadhar_back_210610080648.pdf', '1985-05-02', 'Any Graduate', 'HDFC', '50100328003094', 'HDFC0000633', NULL, 'education_proof/edu_proof_210610080648.pdf', NULL, NULL, 'electricity_bill/electricity_210610080648.pdf', 'voter_id/voterid_210610080648.pdf', NULL, 'photo/photo_210610080648.pdf', 'resume/resume_210610080648.pdf', NULL, '', '0', '2021-06-10 08:06:48', '2021-06-10 08:06:48'),
(109, 201, 'JZNPS0709F', 'pan_card/pan_210610081851.pdf', '990655637443', NULL, 'aadhar_card/aadhar_back_210610081851.pdf', '1978-01-20', 'SSC', 'TJSB', '103110100004392', 'TJSB0000103', NULL, 'education_proof/edu_proof_210610081851.pdf', 'rent_rotary/rent_210610081851.pdf', 'rent_rotary/rotary_210610081851.pdf', 'electricity_bill/electricity_210610081851.pdf', NULL, NULL, NULL, 'resume/resume_210610081851.pdf', NULL, '', '0', '2021-06-10 08:18:51', '2021-06-10 08:18:51'),
(110, 176, 'AZUPK6391F', 'pan_card/pan_210610083516.pdf', '662462874393', 'aadhar_card/aadhar_front_210610083516.pdf', 'aadhar_card/aadhar_back_210610083516.pdf', '1981-08-02', 'SSC', 'HDFC', '50100320680930', 'HDFC0000633', NULL, 'education_proof/edu_proof_210610083516.pdf', NULL, NULL, 'electricity_bill/electricity_210610083516.pdf', NULL, NULL, NULL, 'resume/resume_210610083516.pdf', NULL, '', '0', '2021-06-10 08:35:16', '2021-06-10 08:35:16'),
(111, 108, 'BMZPN1031N', 'pan_card/pan_210610084334.pdf', '23865441', 'aadhar_card/aadhar_front_210610084334.pdf', 'aadhar_card/aadhar_back_210610084334.pdf', '1992-09-30', 'Any Graduate', 'HDFC', '51810310000843', 'HDFC0000633', NULL, 'education_proof/edu_proof_210610084334.pdf', NULL, NULL, 'electricity_bill/electricity_210610084334.pdf', NULL, 'driving_licence/drive_210610084334.pdf', 'photo/photo_210610084334.jpg', 'resume/resume_210610084334.pdf', 'interview_check/interview_check_210610084334.pdf', '', '0', '2021-06-10 08:43:34', '2021-06-10 08:43:34'),
(112, 110, 'CUAPG6681F', 'pan_card/pan_210626063637.jpg', '298129564666', 'aadhar_card/aadhar_front_210626063637.jpg', 'aadhar_card/aadhar_back_210626063637.jpg', '1996-01-29', 'Any Graduate', 'HDFC', '50100389046856', 'HDFC0009606', NULL, 'education_proof/pan_210626063637.jpg', 'rent_rotary/rent_211007073521.pdf', 'rent_rotary/rotary_211007073521.pdf', 'electricity_bill/electricity_210626063637.jpg', 'voter_id/voterid_210626063637.jpg', 'driving_licence/drive_210626063637.jpg', 'photo/photo_210626064035.jpeg', 'resume/resume_210626063854.jpg', 'interview_check/interview_check_210626063854.jpg', 'vacc_receipt/vacc_receipt_211007073759.pdf', '0', '2021-06-10 08:55:01', '2021-10-07 02:37:59'),
(113, 129, 'GLIPD3708R', 'pan_card/pan_210610104938.pdf', '489173185800', 'aadhar_card/aadhar_front_210610104938.pdf', 'aadhar_card/aadhar_back_210610104938.pdf', '2002-02-24', 'HSC', 'HDFC', '50100389046092', 'HDFC0009606', NULL, 'education_proof/edu_proof_210610104938.pdf', 'rent_rotary/rent_211007082513.pdf', NULL, 'electricity_bill/electricity_210610104938.pdf', NULL, NULL, NULL, 'resume/resume_210610104938.pdf', NULL, '', '0', '2021-06-10 10:49:38', '2021-10-07 03:25:13'),
(114, 114, 'HDIPK4446Q', 'pan_card/pan_210610122257.jpeg', '690193020564', 'aadhar_card/aadhar_front_210610122257.pdf', 'aadhar_card/aadhar_back_210610122257.pdf', '1995-02-17', 'SSC', 'UCB', '6870110046661', 'UCBA0000687', NULL, 'education_proof/edu_proof_210610122257.pdf', NULL, NULL, 'electricity_bill/electricity_210610122257.pdf', NULL, NULL, 'photo/photo_210610122257.pdf', 'resume/resume_210610122257.pdf', NULL, '', '0', '2021-06-10 12:22:57', '2021-06-10 12:22:57'),
(115, 154, 'GYNPS8501R', 'pan_card/pan_210611062529.pdf', '757573202617', 'aadhar_card/aadhar_front_210611062529.pdf', 'aadhar_card/aadhar_back_210611062529.pdf', '1997-06-22', 'Any Graduate', 'HDFC', '50100316327494', 'HDFC0000633', NULL, NULL, NULL, NULL, 'electricity_bill/electricity_210617031645.pdf', NULL, 'driving_licence/drive_210617031802.pdf', 'photo/photo_210611062529.pdf', 'resume/resume_210611062529.pdf', NULL, '', '0', '2021-06-11 06:25:29', '2021-06-17 10:18:02'),
(116, 243, 'BECPL9655M', 'pan_card/pan_210617121746.pdf', '205108426457', 'aadhar_card/aadhar_front_210617121746.pdf', 'aadhar_card/aadhar_back_210617121746.pdf', '1999-08-14', 'HSC', 'HDFC', '50100320681028', 'HDFC0000633', NULL, 'education_proof/edu_proof_210617121746.pdf', NULL, NULL, 'electricity_bill/electricity_210617121746.pdf', NULL, NULL, NULL, 'resume/resume_210617121746.pdf', NULL, '', '0', '2021-06-17 12:17:46', '2021-06-17 12:17:46'),
(117, 132, 'BCYPL1662G', 'pan_card/pan_210623080842.pdf', '939681830000', 'aadhar_card/aadhar_front_210623080842.pdf', 'aadhar_card/aadhar_back_210623080842.pdf', '2001-06-22', 'SSC', 'BANK OF BARODA', '9080100014862', 'BARB0WADPOO', NULL, 'education_proof/edu_proof_210623080842.pdf', NULL, NULL, 'electricity_bill/electricity_210623080842.pdf', NULL, NULL, 'photo/photo_210623080842.pdf', 'resume/resume_210623080842.pdf', NULL, '', '0', '2021-06-23 08:08:42', '2021-06-23 08:08:42'),
(118, 240, 'AVVPD3794L', 'pan_card/pan_210623083121.pdf', '401621717292', 'aadhar_card/aadhar_front_210623083121.pdf', 'aadhar_card/aadhar_back_210623083121.pdf', '1988-11-04', 'Any Graduate', 'BANK OF MAHARASHTRA', '60331297259', 'MAHB0000041', 'bank_passbook/bnk_passbook_210623083121.pdf', 'education_proof/edu_proof_210623083121.pdf', NULL, NULL, 'electricity_bill/electricity_210623083121.pdf', NULL, 'driving_licence/drive_210623083121.pdf', 'photo/photo_210623083121.pdf', 'resume/resume_210623083121.pdf', 'interview_check/interview_check_210623083121.pdf', '', '0', '2021-06-23 08:31:21', '2021-06-23 08:31:21'),
(119, 84, 'CANPP2799C', 'pan_card/pan_210623121227.jpg', '420399463486', 'aadhar_card/aadhar_front_210626092202.pdf', 'aadhar_card/aadhar_back_210623121227.jpg', '1993-05-13', 'HSC', 'HDFC', '50100332123257', 'HDFC0000633', NULL, 'education_proof/edu_proof_210623121227.pdf', NULL, NULL, 'electricity_bill/electricity_210623121227.jpg', 'voter_id/voterid_210623121227.jpg', NULL, NULL, 'resume/resume_210626092202.pdf', NULL, '', '0', '2021-06-23 12:12:27', '2021-06-26 04:22:02'),
(120, 39, 'AKAPR1647N', 'pan_card/pan_210623122135.pdf', '663769091914', 'aadhar_card/aadhar_front_210623122135.pdf', 'aadhar_card/aadhar_back_210623122135.pdf', '1967-10-22', 'HSC', 'HDFC BANK', '01031140041465', 'HDFC0009658', NULL, 'education_proof/edu_proof_210623122135.pdf', NULL, NULL, 'electricity_bill/electricity_210623122135.pdf', NULL, 'driving_licence/drive_210623122135.pdf', 'photo/photo_210623122135.pdf', 'resume/resume_210623122135.pdf', 'interview_check/interview_check_210623122135.pdf', '', '0', '2021-06-23 12:21:35', '2021-06-23 12:21:35'),
(121, 175, 'GVXPK2196D', 'pan_card/pan_210623123900.pdf', '775311183705', 'aadhar_card/aadhar_front_210623123900.pdf', 'aadhar_card/aadhar_back_210623123900.pdf', '1999-11-27', 'HSC', 'HDFC', '50100316327455', 'HDFC0000633', 'bank_passbook/bnk_passbook_210623123900.pdf', 'education_proof/edu_proof_210623123900.pdf', NULL, NULL, 'electricity_bill/electricity_210623123900.pdf', 'voter_id/voterid_210623123900.pdf', NULL, 'photo/photo_210623123900.pdf', 'resume/resume_210623123900.pdf', NULL, '', '0', '2021-06-23 12:39:00', '2021-06-23 12:39:00'),
(122, 93, 'AYZPJ8743J', 'pan_card/pan_210623124822.pdf', '952142661628', 'aadhar_card/aadhar_front_210623124822.pdf', 'aadhar_card/aadhar_back_210623124822.pdf', '1992-06-09', 'HSC', 'HDFC BANK', '50100382382672', 'HDFC0000794', NULL, 'education_proof/edu_proof_210623124822.pdf', NULL, NULL, 'electricity_bill/electricity_210623124822.pdf', 'voter_id/voterid_210703011928.pdf', 'driving_licence/drive_210703011928.pdf', 'photo/photo_210623124822.pdf', 'resume/resume_210623124822.pdf', 'interview_check/interview_check_210623124822.pdf', '', '0', '2021-06-23 12:48:22', '2021-07-03 08:19:28'),
(123, 189, 'ABBPO9830A', 'pan_card/pan_210623124854.pdf', '719477591817', 'aadhar_card/aadhar_front_210623124854.pdf', 'aadhar_card/aadhar_back_210623124854.pdf', '1986-01-31', 'Any Graduate', 'STATE BANK OF INDIA', '20485804487', 'SBIN0008784', NULL, 'education_proof/edu_proof_210623124854.pdf', NULL, NULL, NULL, NULL, 'driving_licence/drive_210623124854.pdf', 'photo/photo_210623124854.pdf', 'resume/resume_210623124854.pdf', 'interview_check/interview_check_210623124854.pdf', '', '0', '2021-06-23 12:48:54', '2021-06-23 12:48:54'),
(124, 182, 'DASPA6129K', 'pan_card/pan_210623010142.pdf', '932607913515', 'aadhar_card/aadhar_front_210623010142.pdf', 'aadhar_card/aadhar_back_210623010142.pdf', '1997-03-23', 'SSC', 'BANK OF INDIA', '050510110014460', 'BKID0000505', NULL, 'education_proof/edu_proof_210623010142.pdf', NULL, NULL, 'electricity_bill/electricity_210623010142.pdf', NULL, 'driving_licence/drive_210623010142.pdf', 'photo/photo_210623010142.pdf', 'resume/resume_210623010142.pdf', 'interview_check/interview_check_210623010142.pdf', '', '0', '2021-06-23 13:01:42', '2021-06-23 13:01:42'),
(125, 181, 'CMYPP7079L', 'pan_card/pan_210623010304.pdf', '223454259386', 'aadhar_card/aadhar_front_210623010304.pdf', 'aadhar_card/aadhar_back_210623010304.pdf', '1988-09-17', 'SSC', 'HDFC BANK', '50100426447072', 'HDFC0002802', NULL, 'education_proof/edu_proof_210623010304.pdf', NULL, NULL, 'electricity_bill/electricity_210623010304.pdf', NULL, 'driving_licence/drive_210623010304.pdf', 'photo/photo_210623010304.pdf', 'resume/resume_210623010304.pdf', 'interview_check/interview_check_210623010304.pdf', '', '0', '2021-06-23 13:03:04', '2021-06-23 13:03:04'),
(126, 198, 'DYROK0019R', 'pan_card/pan_210624091643.pdf', '363564323566', 'aadhar_card/aadhar_front_210624091643.pdf', 'aadhar_card/aadhar_back_210624091643.pdf', '1986-01-02', 'Any Graduate', 'ICIC BANK', '345001502434', 'ICIC0003450', NULL, 'education_proof/edu_proof_210624091643.pdf', NULL, NULL, 'electricity_bill/electricity_210624091643.pdf', 'voter_id/voterid_210624091643.pdf', NULL, 'photo/photo_210624091643.pdf', 'resume/resume_210624091643.pdf', 'interview_check/interview_check_210624091643.pdf', '', '0', '2021-06-24 09:16:43', '2021-06-24 09:16:43'),
(127, 199, 'ATVPA0503A', 'pan_card/pan_210624092326.pdf', '905938629831', 'aadhar_card/aadhar_front_210624092326.pdf', 'aadhar_card/aadhar_back_210624092326.pdf', '1975-09-24', 'SSC', 'ICIC BANK', '345001502599', 'ICIC0003450', NULL, 'education_proof/edu_proof_210624092326.pdf', NULL, NULL, 'electricity_bill/electricity_210624092326.pdf', 'voter_id/voterid_210624092326.pdf', NULL, 'photo/photo_210624092326.pdf', 'resume/resume_210624092326.pdf', 'interview_check/interview_check_210624092326.pdf', '', '0', '2021-06-24 09:23:26', '2021-06-24 09:23:26'),
(128, 192, 'AOZPB4158F', 'pan_card/pan_210624093114.pdf', '490667557028', 'aadhar_card/aadhar_front_210624093114.jpg', 'aadhar_card/aadhar_back_210624093114.pdf', '1983-11-05', 'SSC', 'ICIC BANK', '120601506826', 'ICIC0001206', NULL, 'education_proof/edu_proof_210624093114.pdf', 'rent_rotary/rent_210624093114.pdf', NULL, 'electricity_bill/electricity_210624093114.pdf', NULL, NULL, 'photo/photo_210624093114.jpg', 'resume/resume_210624093114.pdf', 'interview_check/interview_check_210624093114.pdf', '', '0', '2021-06-24 09:31:14', '2021-06-24 09:31:14'),
(129, 96, 'IGKPS1974M', 'pan_card/pan_210624120327.pdf', '936096076675', 'aadhar_card/aadhar_front_210624120327.pdf', 'aadhar_card/aadhar_back_210624120327.pdf', '1999-04-25', 'HSC', 'HDFC', '50100389046398', 'HDFC0009606', NULL, 'education_proof/pan_210624120327.pdf', NULL, NULL, 'electricity_bill/electricity_210624120327.pdf', NULL, 'driving_licence/drive_210624120327.pdf', NULL, 'resume/resume_210624120327.pdf', 'interview_check/interview_check_210624120327.pdf', '', '0', '2021-06-24 12:01:58', '2021-06-24 07:03:27'),
(130, 95, 'BTKPS9980B', 'pan_card/pan_210904075306.jpg', '684658320047', 'aadhar_card/aadhar_front_210904075306.jpg', 'aadhar_card/aadhar_back_210904075306.jpg', '1981-04-08', 'HSC', 'HDFC', '50100376866250', 'HDFC0009526', NULL, 'education_proof/edu_proof_210624123447.pdf', NULL, NULL, NULL, 'voter_id/voterid_210904074101.jpg', 'driving_licence/drive_210624123447.pdf', 'photo/photo_210904074330.jpg', 'resume/resume_210624123447.pdf', NULL, '', '0', '2021-06-24 12:34:47', '2021-09-04 02:53:06'),
(131, 191, 'DBMPB6200H', NULL, '412478543568', NULL, NULL, '1978-10-11', 'HSC', 'ICICI', '345001502418', 'ICIC0003450', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-06-24 13:01:55', '2021-06-24 13:01:55'),
(132, 232, 'BQRPB2308E', 'pan_card/pan_210625064051.pdf', '592772883879', 'aadhar_card/aadhar_front_210625064051.pdf', 'aadhar_card/aadhar_back_210625064051.pdf', '1994-06-22', 'HSC', 'HDFC', '50100320681831', 'HDFC0000633', NULL, 'education_proof/edu_proof_210625064051.pdf', NULL, NULL, NULL, 'voter_id/voterid_210625064051.pdf', NULL, NULL, NULL, NULL, '', '0', '2021-06-25 06:40:51', '2021-06-25 06:40:51'),
(133, 85, 'AMRPB3998B', 'pan_card/pan_210625064843.pdf', '989044711004', 'aadhar_card/aadhar_front_210625064843.pdf', 'aadhar_card/aadhar_back_210625064843.pdf', '1981-06-04', 'Any Graduate', 'HDFC', '50100099810139', 'HDFC0001790', NULL, 'education_proof/edu_proof_210625064843.pdf', NULL, NULL, NULL, NULL, 'driving_licence/drive_210625064843.pdf', NULL, 'resume/resume_210625064843.pdf', NULL, '', '0', '2021-06-25 06:48:43', '2021-06-25 06:48:43'),
(134, 86, 'AKKPJ7547F', 'pan_card/pan_210625093538.pdf', '512829117535', 'aadhar_card/aadhar_front_210625093218.pdf', 'aadhar_card/aadhar_back_210625093218.pdf', '1990-03-09', 'Any Graduate', 'HDFC', '50100320681541', 'HDFC0000633', NULL, 'education_proof/pan_210625093218.pdf', NULL, NULL, 'electricity_bill/electricity_210625093218.pdf', NULL, NULL, NULL, NULL, NULL, '', '0', '2021-06-25 09:30:00', '2021-06-25 04:35:38'),
(135, 87, 'AUWPP8245Q', 'pan_card/pan_210625095304.pdf', '520670035615', 'aadhar_card/aadhar_front_210625095304.pdf', 'aadhar_card/aadhar_back_210625095304.pdf', '1983-01-12', 'HSC', 'HDFC', '50100328003832', 'HDFC0004887', NULL, 'education_proof/edu_proof_210625095304.pdf', NULL, NULL, 'electricity_bill/electricity_210625095304.pdf', NULL, 'driving_licence/drive_210625095304.pdf', NULL, NULL, NULL, '', '0', '2021-06-25 09:53:04', '2021-06-25 09:53:04'),
(136, 244, 'EHSPS6265L', 'pan_card/pan_210625101303.pdf', '675881012393', 'aadhar_card/aadhar_front_210625101303.pdf', NULL, '1986-10-02', 'HSC', 'HDFC', '50100342371398', 'HDFC0004887', NULL, 'education_proof/edu_proof_210625101303.pdf', NULL, NULL, 'electricity_bill/electricity_210625101303.pdf', NULL, NULL, NULL, 'resume/resume_210625101303.pdf', NULL, '', '0', '2021-06-25 10:13:03', '2021-06-25 10:13:03'),
(137, 89, 'KFJPS7216J', 'pan_card/pan_210625102622.pdf', '613641250314', 'aadhar_card/aadhar_front_210625102622.pdf', NULL, '1994-06-01', 'Any Graduate', 'HDFC', '50100328003822', 'HDFC0000633', NULL, 'education_proof/edu_proof_210625102622.pdf', 'rent_rotary/rent_210916062205.pdf', NULL, NULL, 'voter_id/voterid_210916062205.pdf', 'driving_licence/drive_210625102622.pdf', NULL, 'resume/resume_210625102622.pdf', NULL, '', '0', '2021-06-25 10:26:22', '2021-09-16 01:22:05'),
(138, 149, 'BWKPS6534A', 'pan_card/pan_210625121943.pdf', '448390964918', 'aadhar_card/aadhar_front_210625121943.pdf', 'aadhar_card/aadhar_back_210625121943.pdf', '1982-10-19', 'HSC', 'HDFC', '50100371438814', 'HDFC0009606', NULL, 'education_proof/edu_proof_210625121943.pdf', NULL, NULL, NULL, NULL, 'driving_licence/drive_210625121943.pdf', NULL, 'resume/resume_210625121943.pdf', NULL, '', '0', '2021-06-25 12:19:43', '2021-06-25 12:19:43'),
(139, 157, 'EPQPS7779M', 'pan_card/pan_210625122758.pdf', '485893470480', 'aadhar_card/aadhar_front_210625122758.pdf', NULL, '1978-01-21', 'SSC', 'HDFC', '50100332123221', 'HDFC0000633', NULL, NULL, NULL, NULL, 'electricity_bill/electricity_210625122758.pdf', NULL, NULL, NULL, 'resume/resume_210625122758.pdf', NULL, '', '0', '2021-06-25 12:27:58', '2021-06-25 12:27:58'),
(140, 73, 'AHDTP4674P', 'pan_card/pan_210625123944.pdf', '763070024239', 'aadhar_card/aadhar_front_210625123944.pdf', NULL, '1987-01-01', 'HSC', 'HDFC', '50100332123346', 'HDFC0000633', NULL, 'education_proof/edu_proof_210625123944.pdf', NULL, NULL, 'electricity_bill/electricity_210625123944.pdf', NULL, 'driving_licence/drive_210625123944.pdf', NULL, NULL, NULL, '', '0', '2021-06-25 12:39:44', '2021-06-25 12:39:44'),
(141, 74, 'CULPG1317B', NULL, '204136666704', 'aadhar_card/aadhar_front_210625124727.pdf', NULL, '1993-06-07', 'HSC', 'HDFC', '50100407192729', 'HDFC0000794', NULL, 'education_proof/edu_proof_210625124727.pdf', NULL, NULL, 'electricity_bill/electricity_210625124727.pdf', NULL, NULL, NULL, NULL, NULL, '', '0', '2021-06-25 12:47:27', '2021-06-25 12:47:27'),
(142, 168, 'AWTPC4173F', 'pan_card/pan_210625011017.pdf', '847410391354', 'aadhar_card/aadhar_front_210625011017.pdf', 'aadhar_card/aadhar_back_210625011017.pdf', '1988-11-23', 'SSC', 'Punjab National Bank', '2901000400008800', 'PUNB0290100', NULL, 'education_proof/edu_proof_210625011017.pdf', NULL, NULL, NULL, 'voter_id/voterid_210625011017.pdf', NULL, NULL, NULL, NULL, '', '0', '2021-06-25 13:10:17', '2021-06-25 13:10:17'),
(143, 77, 'BJOPD0691Q', NULL, '885132618661', 'aadhar_card/aadhar_front_210625012001.pdf', 'aadhar_card/aadhar_back_210625012001.pdf', '1994-03-11', 'HSC', 'HDFC', '50100342371385', 'HDFC0004887', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-06-25 13:13:44', '2021-06-25 08:20:01'),
(144, 208, 'AZWPG3813G', 'pan_card/pan_210626085501.jpg', '481073330332', 'aadhar_card/aadhar_front_210626085501.jpg', 'aadhar_card/aadhar_back_210626085501.jpg', '1974-07-17', 'SSC', 'HDFC BANK', '50100332122903', 'HDFC0000633', NULL, NULL, NULL, NULL, NULL, 'voter_id/voterid_210626085501.jpg', NULL, NULL, NULL, 'interview_check/interview_check_210626085501.jpg', '', '0', '2021-06-26 05:59:55', '2021-06-26 03:55:01'),
(145, 177, 'AGKPL1504E', NULL, '754462203534', NULL, NULL, '1974-09-19', 'SSC', 'HDFC BANK', '50100336270779', 'HDFC0000185', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-06-26 06:01:45', '2021-06-26 06:01:45'),
(146, 210, 'DEDPG1586R', 'pan_card/pan_210626103148.jpg', '975276886576', 'aadhar_card/aadhar_front_210626103148.jpg', 'aadhar_card/aadhar_back_210626103148.jpg', '1999-02-19', 'SSC', 'HDFC BANK', '50100328003819', 'HDFC0000633', NULL, 'education_proof/pan_210626103148.jpg', NULL, NULL, 'electricity_bill/electricity_210626103148.jpg', NULL, NULL, NULL, NULL, 'interview_check/interview_check_210626103148.jpg', '', '0', '2021-06-26 06:03:32', '2021-06-26 05:31:48'),
(147, 179, 'AEBPO0019C', 'pan_card/pan_210626100324.jpg', '504131288662', 'aadhar_card/aadhar_front_210626100324.jpg', 'aadhar_card/aadhar_back_210626100324.jpg', '1995-08-26', 'HSC', 'HDFC BANK', '50100370387753', 'HDFC0009526', NULL, 'education_proof/pan_210626100324.jpg', NULL, NULL, NULL, NULL, 'driving_licence/drive_210626100324.jpg', NULL, 'resume/resume_210626100324.jpg', 'interview_check/interview_check_210626100324.jpg', '', '0', '2021-06-26 06:08:24', '2021-06-26 05:03:24'),
(148, 98, 'BSKPJ0070L', 'pan_card/pan_210626061127.jpg', '866699568009', 'aadhar_card/aadhar_front_210626061127.jpg', 'aadhar_card/aadhar_back_210626061127.jpg', '2000-06-11', 'HSC', 'HDFC BANK', '50100389046079', 'HDFC0009606', NULL, 'education_proof/edu_proof_210626061127.jpg', NULL, NULL, 'electricity_bill/electricity_210626061127.jpg', NULL, 'driving_licence/drive_210626061127.jpg', NULL, NULL, NULL, '', '0', '2021-06-26 06:11:27', '2021-06-26 06:11:27'),
(149, 231, 'DJEPB9630K', 'pan_card/pan_210626091359.pdf', '808909501344', 'aadhar_card/aadhar_front_210626091359.jpg', 'aadhar_card/aadhar_back_210626091359.jpg', '1999-01-00', 'HSC', 'HDFC BANK', '50100376923657', 'HDFC0009526', NULL, 'education_proof/pan_210626091359.pdf', NULL, NULL, 'electricity_bill/electricity_210626091359.pdf', 'voter_id/voterid_210626091359.pdf', NULL, 'photo/photo_210626091359.jpg', 'resume/resume_210626091359.pdf', 'interview_check/interview_check_210626091359.jpg', '', '0', '2021-06-26 06:15:11', '2021-06-26 04:13:59'),
(150, 127, 'DTRPB1990P', 'pan_card/pan_210626062303.jpg', '672134342764', 'aadhar_card/aadhar_front_210626062303.jpg', 'aadhar_card/aadhar_back_210626062303.jpg', '1997-03-16', 'HSC', 'HDFC BANK', '50100389046231', 'HDFC0009606', NULL, NULL, NULL, NULL, 'electricity_bill/electricity_210626062303.jpg', NULL, NULL, NULL, NULL, 'interview_check/interview_check_210626062303.jpg', '', '0', '2021-06-26 06:23:03', '2021-06-26 06:23:03'),
(151, 183, 'BDSPV8919F', 'pan_card/pan_210626081352.jpg', '750111376712', 'aadhar_card/aadhar_front_210626081352.jpg', 'aadhar_card/aadhar_back_210626081352.jpg', '1993-07-03', 'SSC', 'HDFC BANK', '50100407192630', 'HDFC0000794', NULL, 'education_proof/pan_210626081352.jpg', NULL, NULL, 'electricity_bill/electricity_210626081352.jpg', NULL, 'driving_licence/drive_210626090418.pdf', 'photo/photo_210626081352.jpg', NULL, 'interview_check/interview_check_210626081352.jpg', '', '0', '2021-06-26 06:52:49', '2021-06-26 04:04:18'),
(153, 223, 'ASTPP5240J', 'pan_card/pan_210626072509.pdf', '629207916969', 'aadhar_card/aadhar_front_210626072509.pdf', 'aadhar_card/aadhar_back_210626072509.pdf', '1979-10-27', 'SSC', 'BANK OF MAHARASHTRA', '60291471117', 'MAHB0001012', NULL, NULL, NULL, NULL, 'electricity_bill/electricity_210626072509.pdf', NULL, 'driving_licence/drive_210626072509.pdf', 'photo/photo_210626072509.pdf', 'resume/resume_210626072509.pdf', NULL, '', '0', '2021-06-26 07:25:09', '2021-06-26 07:25:09'),
(154, 171, 'EMAPB4702E', 'pan_card/pan_210626074430.pdf', '885724411543', 'aadhar_card/aadhar_front_210626074430.pdf', NULL, '1996-09-18', 'Any Graduate', 'HDFC', '50100320681601', 'HDFC0000633', NULL, 'education_proof/edu_proof_210626074430.pdf', NULL, NULL, 'electricity_bill/electricity_210626074430.pdf', 'voter_id/voterid_210626074430.pdf', NULL, NULL, 'resume/resume_210626074430.pdf', 'interview_check/interview_check_210626074430.pdf', '', '0', '2021-06-26 07:44:30', '2021-06-26 07:44:30'),
(155, 76, 'CPXPR4658R', 'pan_card/pan_210626095210.pdf', '677423492852', 'aadhar_card/aadhar_front_210626095210.pdf', 'aadhar_card/aadhar_back_210626095210.pdf', '1994-10-27', 'Any Graduate', 'HDFC', '50100320681525', 'HDFC0000633', NULL, NULL, NULL, NULL, 'electricity_bill/electricity_210626095210.pdf', 'voter_id/voterid_210626095210.pdf', NULL, NULL, 'resume/resume_210626095210.pdf', NULL, '', '0', '2021-06-26 09:52:10', '2021-06-26 09:52:10'),
(156, 104, 'DEFPP3463A', 'pan_card/pan_210626112317.pdf', '602617557913', 'aadhar_card/aadhar_front_210626112317.pdf', 'aadhar_card/aadhar_back_210626112317.pdf', '1992-01-28', 'Any Graduate', 'HDFC', '50100394624156', 'HDFC0009526', NULL, 'education_proof/edu_proof_210626112317.pdf', NULL, NULL, 'electricity_bill/electricity_210626112317.pdf', NULL, 'driving_licence/drive_210626112317.pdf', 'photo/photo_210626112317.pdf', 'resume/resume_210626112317.pdf', NULL, '', '0', '2021-06-26 11:23:17', '2021-06-26 11:23:17'),
(157, 250, 'AGWPH7504E', NULL, '697837904501', NULL, NULL, '1990-03-04', 'Any Graduate', 'HDFC BANK', '50100395345070', 'HDFC0009606', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-07-29 11:43:25', '2021-08-19 09:00:19'),
(159, 254, 'BKWPB1872L', 'pan_card/pan_210923084022.pdf', '419255228710', 'aadhar_card/aadhar_front_210923084022.pdf', 'aadhar_card/aadhar_back_210923084022.pdf', '1985-06-16', 'Any Graduate', 'HDFC', '50100412097250', 'HDFC0009526', 'bank_passbook/aadhar_210923084022.pdf', 'education_proof/pan_210923084022.pdf', 'rent_rotary/rent_210923084022.pdf', NULL, 'electricity_bill/electricity_210923084022.pdf', 'voter_id/voterid_210923084022.pdf', 'driving_licence/drive_210923084022.pdf', 'photo/photo_210923084022.pdf', 'resume/resume_210923084022.pdf', 'interview_check/interview_check_210923084022.pdf', '', '0', '2021-08-03 10:20:11', '2021-09-23 03:40:22'),
(160, 277, 'DOQPK1408K', 'pan_card/pan_210819113106.pdf', '734567695665', 'aadhar_card/aadhar_front_210819113106.pdf', 'aadhar_card/aadhar_back_210819113106.pdf', '1995-04-25', 'Any Graduate', 'JANSEVA BANK', '19021001050', 'JANA0000019', 'bank_passbook/bnk_passbook_210819113106.pdf', 'education_proof/edu_proof_210819113106.pdf', NULL, NULL, NULL, 'voter_id/voterid_210819113106.pdf', 'driving_licence/drive_210819113106.pdf', NULL, 'resume/resume_210819113106.pdf', 'interview_check/interview_check_210819113106.pdf', '', '0', '2021-08-19 11:31:06', '2021-08-19 11:31:06'),
(161, 282, 'ELLPK8535C', 'pan_card/pan_210819124755.pdf', '363486307647', 'aadhar_card/aadhar_front_210819124755.pdf', 'aadhar_card/aadhar_back_210819124755.pdf', '1997-07-28', 'Any Graduate', 'HDFC BANK', '50100441541872', 'HDFC0004187', NULL, 'education_proof/edu_proof_210819124755.pdf', NULL, NULL, NULL, 'voter_id/voterid_210819124755.pdf', NULL, NULL, 'resume/resume_210819124755.pdf', 'interview_check/interview_check_210819124755.pdf', '', '0', '2021-08-19 12:47:55', '2021-08-19 12:47:55'),
(162, 280, 'MPWPS4332A', 'pan_card/pan_210819125500.pdf', '560135537410', 'aadhar_card/aadhar_front_210819125500.pdf', 'aadhar_card/aadhar_back_210819125500.pdf', '1992-09-10', 'SSC', 'HDFC BANK', '50100441541945', 'HDFC0004187', NULL, 'education_proof/edu_proof_210819125500.pdf', NULL, NULL, 'electricity_bill/electricity_210819125500.pdf', 'voter_id/voterid_210819125500.pdf', 'driving_licence/drive_210819125500.pdf', NULL, NULL, 'interview_check/interview_check_210819125500.pdf', '', '0', '2021-08-19 12:55:00', '2021-08-19 12:55:00'),
(163, 273, 'APVPK3925J', 'pan_card/pan_210819010936.pdf', '226978741017', 'aadhar_card/aadhar_front_210819010936.pdf', 'aadhar_card/aadhar_back_210819010936.pdf', '1979-04-23', 'SSC', 'SBI', '87931065102', 'SBIN0011490', NULL, 'education_proof/edu_proof_210819010936.pdf', NULL, NULL, NULL, 'voter_id/voterid_210819010936.pdf', 'driving_licence/drive_210819010936.pdf', NULL, 'resume/resume_210819010936.pdf', 'interview_check/interview_check_210819010936.pdf', '', '0', '2021-08-19 13:09:36', '2021-08-19 13:09:36'),
(164, 265, 'DWBPB4143K', 'pan_card/pan_210819011929.pdf', '808123783591', 'aadhar_card/aadhar_front_210819011929.pdf', 'aadhar_card/aadhar_back_210819011929.pdf', '1998-12-10', 'Any Graduate', 'HDFC BANK', '50100441541770', 'HDFC0004187', 'bank_passbook/bnk_passbook_210819011929.pdf', 'education_proof/edu_proof_210819011929.pdf', NULL, NULL, 'electricity_bill/electricity_210819011929.pdf', NULL, 'driving_licence/drive_210819011929.pdf', NULL, 'resume/resume_210819011929.pdf', 'interview_check/interview_check_210819011929.pdf', '', '0', '2021-08-19 13:19:29', '2021-08-19 13:19:29'),
(165, 272, 'HWYPK7516N', 'pan_card/pan_210819012147.pdf', '920899579279', 'aadhar_card/aadhar_front_210819012147.pdf', 'aadhar_card/aadhar_back_210819012147.pdf', '1979-07-11', 'SSC', 'BANK OF BARODA', '12490100017224', 'BARBODIGHIX', 'bank_passbook/bnk_passbook_210819012147.pdf', NULL, NULL, NULL, 'electricity_bill/electricity_210819012147.pdf', NULL, NULL, NULL, 'resume/resume_210819012147.pdf', 'interview_check/interview_check_210819012147.pdf', '', '0', '2021-08-19 13:21:47', '2021-08-19 13:21:47'),
(166, 262, 'AEIPH0429Q', 'pan_card/pan_210819014308.pdf', '437983007447', 'aadhar_card/aadhar_front_210819014308.pdf', 'aadhar_card/aadhar_back_210819014308.pdf', '1974-07-02', 'Any Graduate', 'IDBI BANK', '0034104000340571', 'IBKL0000034', NULL, 'education_proof/edu_proof_210819014308.pdf', NULL, NULL, NULL, 'voter_id/voterid_210819014308.pdf', 'driving_licence/drive_210819014308.pdf', NULL, 'resume/resume_210819014308.pdf', 'interview_check/interview_check_210819014308.pdf', '', '0', '2021-08-19 13:43:08', '2021-08-19 13:43:08'),
(167, 285, 'DVXPS9839A', 'pan_card/pan_210819014540.pdf', '542641350711', 'aadhar_card/aadhar_front_210819014540.pdf', 'aadhar_card/aadhar_back_210819014540.pdf', '1992-12-06', 'SSC', 'HDFC BANK', '50100441541804', 'HDFC0004187', NULL, 'education_proof/edu_proof_210819014540.pdf', NULL, NULL, 'electricity_bill/electricity_210819014540.pdf', NULL, NULL, NULL, 'resume/resume_210819014540.pdf', 'interview_check/interview_check_210819014540.pdf', '', '0', '2021-08-19 13:45:40', '2021-08-19 13:45:40'),
(168, 274, 'GHPPM1131Q', 'pan_card/pan_210819015232.pdf', '833529509792', 'aadhar_card/aadhar_front_210819015232.pdf', 'aadhar_card/aadhar_back_210819015232.pdf', '2001-08-07', 'HSC', 'HDFC BANK', '50100399918567', 'HDFC0009606', NULL, 'education_proof/edu_proof_210819015232.pdf', NULL, NULL, NULL, NULL, NULL, NULL, 'resume/resume_210819015232.pdf', 'interview_check/interview_check_210819015232.pdf', '', '0', '2021-08-19 13:52:32', '2021-08-19 13:52:32'),
(169, 268, 'AQKPV3141K', 'pan_card/pan_210819020830.pdf', '588276575448', 'aadhar_card/aadhar_front_210819020830.pdf', 'aadhar_card/aadhar_back_210819020830.pdf', '1990-03-14', 'Any Graduate', 'INDUSIND BANK', '168483837001', 'INDB0001477', 'bank_passbook/bnk_passbook_210819020830.pdf', 'education_proof/pan_211005073500.pdf', 'rent_rotary/rent_211005073500.pdf', 'rent_rotary/rotary_211005073500.pdf', 'electricity_bill/electricity_210819020830.pdf', 'voter_id/voterid_211005073500.pdf', 'driving_licence/drive_210819020830.pdf', NULL, 'resume/resume_210819020830.pdf', 'interview_check/interview_check_211005073500.pdf', '', '0', '2021-08-19 14:08:30', '2021-10-05 02:35:00'),
(170, 261, 'ARLPG2630E', 'pan_card/pan_210819022208.pdf', '248680512629', 'aadhar_card/aadhar_front_210819022208.pdf', 'aadhar_card/aadhar_back_210819022208.pdf', '1997-07-26', 'HSC', 'ICICI BANK', '196701515344', 'ICC0001967', NULL, 'education_proof/edu_proof_210819022208.pdf', 'rent_rotary/rent_210915023548.pdf', NULL, 'electricity_bill/electricity_210819022208.pdf', 'voter_id/voterid_210819022208.pdf', 'driving_licence/drive_210819022208.pdf', 'photo/photo_210915023548.pdf', 'resume/resume_210819022208.pdf', 'interview_check/interview_check_210819022208.pdf', '', '0', '2021-08-19 14:22:08', '2021-09-15 09:35:48'),
(171, 260, 'AGWPH7504E', 'pan_card/pan_210819024857.pdf', '697837904501', 'aadhar_card/aadhar_front_210819024857.pdf', 'aadhar_card/aadhar_back_210819024857.pdf', '1990-03-04', 'Any Graduate', 'KOTAK BANK', '6645613339', 'KKBK0001782', 'bank_passbook/bnk_passbook_210819024857.pdf', 'education_proof/edu_proof_210819024857.pdf', NULL, NULL, 'electricity_bill/electricity_210819024857.pdf', NULL, 'driving_licence/drive_210819024857.pdf', NULL, 'resume/resume_210819024857.pdf', 'interview_check/interview_check_210819024857.pdf', '', '0', '2021-08-19 14:48:57', '2021-08-19 14:48:57'),
(172, 286, 'FENPP6866K', 'pan_card/pan_210819025714.pdf', '743282587837', 'aadhar_card/aadhar_front_210819025714.pdf', 'aadhar_card/aadhar_back_210819025714.pdf', '2021-02-15', 'Any Graduate', 'ICICI', '645001522912', 'ICIC0006450', 'bank_passbook/bnk_passbook_210819025714.pdf', 'education_proof/edu_proof_210819025714.pdf', 'rent_rotary/rent_211005074736.pdf', 'rent_rotary/rotary_211005074736.pdf', 'electricity_bill/electricity_210819025714.pdf', NULL, NULL, NULL, 'resume/resume_210819025714.pdf', 'interview_check/interview_check_211005074736.pdf', '', '0', '2021-08-19 14:57:14', '2021-10-05 02:47:36'),
(173, 159, 'BZIPM8276J', NULL, '725814514629', NULL, NULL, '1994-10-31', 'Any Graduate', 'HDFC', '50100320680982', 'HDFC0000633', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-08-19 15:03:05', '2021-08-19 15:03:05'),
(174, 271, 'DONPK4873P', 'pan_card/pan_210819031032.pdf', '944427001335', 'aadhar_card/aadhar_front_210819031032.pdf', 'aadhar_card/aadhar_back_210819031032.pdf', '1982-04-05', 'Any Graduate', 'SBI', '001633707872', 'SBIN00001546', NULL, NULL, NULL, NULL, 'electricity_bill/electricity_210819031032.pdf', 'voter_id/voterid_210819031032.pdf', 'driving_licence/drive_210819031032.pdf', NULL, 'resume/resume_210819031032.pdf', 'interview_check/interview_check_210819031032.pdf', '', '0', '2021-08-19 15:10:32', '2021-08-19 15:10:32'),
(175, 194, 'DCIPM6821G', NULL, '296428046229', NULL, NULL, '1996-06-17', 'SSC', 'NKGSB BANK', '034130500000345', 'NKGS0000034', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-08-20 12:41:51', '2021-08-20 12:41:51'),
(176, 276, 'IXRPK2992H', 'pan_card/pan_210820125055.pdf', '337602161024', 'aadhar_card/aadhar_front_210820125055.pdf', NULL, '2001-08-10', 'Any Graduate', 'INDUSLND BANK', '157821847813', 'INDB0000881', 'bank_passbook/bnk_passbook_210820125055.pdf', 'education_proof/edu_proof_210820125055.pdf', NULL, NULL, 'electricity_bill/electricity_210820125055.pdf', NULL, NULL, NULL, 'resume/resume_210820125055.pdf', 'interview_check/interview_check_210820125055.pdf', '', '0', '2021-08-20 12:50:55', '2021-08-20 12:50:55'),
(177, 163, 'BIZPB5350E', 'pan_card/pan_210823072856.pdf', '894406751410', 'aadhar_card/aadhar_front_210823072856.pdf', 'aadhar_card/aadhar_back_210823072856.pdf', '1986-01-29', 'SSC', 'COSMOS BANK', '01605010147439', 'COSB0000016', 'bank_passbook/aadhar_210823072856.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-08-20 12:59:11', '2021-08-23 02:28:56'),
(178, 112, 'BXWPB5847C', NULL, '765089221863', NULL, NULL, '1990-08-23', 'HSC', 'MAHARASHTRA BANK', '60195788294', 'MAHB0001615', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-08-20 13:07:36', '2021-08-20 13:07:36'),
(179, 267, 'CHZPM9233M', 'pan_card/pan_210820011029.pdf', '903550763706', 'aadhar_card/aadhar_front_210820011029.pdf', NULL, '1996-11-24', 'Any Graduate', 'HDFC BANK', '50100441541780', 'HDFC0004187', 'bank_passbook/bnk_passbook_210820011029.pdf', 'education_proof/edu_proof_210820011029.pdf', NULL, NULL, 'electricity_bill/electricity_210820011029.pdf', 'voter_id/voterid_210820011029.pdf', 'driving_licence/drive_210820011029.pdf', NULL, 'resume/resume_210820011029.pdf', 'interview_check/interview_check_210820011029.pdf', '', '0', '2021-08-20 13:10:29', '2021-08-20 13:10:29'),
(180, 222, 'BQJPG6276L', 'pan_card/pan_210821054050.pdf', '689584047147', 'aadhar_card/aadhar_front_210821054050.pdf', NULL, '1977-11-15', 'SSC', 'CANARA BANK', '418101017665', 'CNRB0000418', 'bank_passbook/aadhar_210821054050.pdf', 'education_proof/pan_210821054050.pdf', NULL, NULL, 'electricity_bill/electricity_210821054050.pdf', NULL, 'driving_licence/drive_210821054050.pdf', NULL, 'resume/resume_210821054050.pdf', 'interview_check/interview_check_210821054050.pdf', '', '0', '2021-08-20 13:26:27', '2021-08-21 00:40:50'),
(181, 279, 'AAJPZ2804J', 'pan_card/pan_210821055441.pdf', '395546835768', 'aadhar_card/aadhar_front_210821055441.pdf', NULL, '1980-08-03', 'Any Graduate', 'AXIS BANK', '911010012534937', 'UTIB0000215', NULL, 'education_proof/edu_proof_210821055441.pdf', NULL, NULL, NULL, NULL, NULL, NULL, 'resume/resume_210821055441.pdf', 'interview_check/interview_check_210821055441.pdf', '', '0', '2021-08-21 05:54:41', '2021-08-21 05:54:41'),
(182, 281, 'BMOPK7814F', 'pan_card/pan_210821060231.pdf', '638611743129', 'aadhar_card/aadhar_front_210821060231.pdf', NULL, '1986-10-01', 'Any Graduate', 'KOTAK BANK', '2614067349', 'KKBK0002038', 'bank_passbook/bnk_passbook_210821060231.pdf', 'education_proof/edu_proof_210821060231.pdf', NULL, NULL, NULL, NULL, 'driving_licence/drive_210821060231.pdf', NULL, 'resume/resume_210821060231.pdf', 'interview_check/interview_check_210821060231.pdf', '', '0', '2021-08-21 06:02:31', '2021-08-21 06:02:31'),
(183, 215, 'JLIPK1476A', NULL, '404958288393', NULL, NULL, '1998-05-07', 'HSC', 'HDFC', '50100342371333', 'HDFC0000633', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-08-21 13:32:31', '2021-08-21 13:32:31'),
(184, 288, 'AKYPS3950K', NULL, '257348223062', NULL, NULL, '1957-10-23', 'SSC', 'SOUTH INDIAN BANK', '366053000001458', 'SIBL0000366', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-08-25 12:36:54', '2021-08-25 12:36:54'),
(185, 292, 'FVAPP2551K', NULL, '698043656482', NULL, NULL, '2020-04-16', 'SSC', 'DBS BANK LTD', '881037385095', 'DBSS0IN0811', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-08-25 12:58:17', '2021-08-25 12:58:17'),
(186, 293, 'CCPPN1816A', NULL, '470393679444', NULL, NULL, '1993-12-24', 'Any Graduate', 'HDFC', '50100429050202', 'HDFC0004887', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-08-25 13:22:20', '2021-08-25 13:22:20'),
(187, 297, 'CRNPS1267H', 'pan_card/pan_210901011757.pdf', '980178246584', 'aadhar_card/aadhar_front_210901011757.pdf', 'aadhar_card/aadhar_back_210901011757.pdf', '1984-07-18', 'HSC', 'HDFC', '50100389047072', 'HDFC0009606', 'bank_passbook/aadhar_210901011757.pdf', 'education_proof/pan_210901011757.pdf', NULL, NULL, NULL, 'voter_id/voterid_210901011757.pdf', 'driving_licence/drive_210901011757.pdf', NULL, 'resume/resume_210901011757.pdf', 'interview_check/interview_check_210901011757.pdf', '', '0', '2021-08-25 13:39:54', '2021-09-01 08:17:57'),
(188, 298, 'CSUPR6515B', 'pan_card/pan_210825063143.pdf', '760016899970', 'aadhar_card/aadhar_front_210825063143.pdf', 'aadhar_card/aadhar_back_210825063143.pdf', '1998-07-09', 'Any Graduate', 'HDFC', '50100292052082', 'HDFC0004328', 'bank_passbook/bnk_passbook_210825063143.pdf', 'education_proof/edu_proof_210825063143.pdf', NULL, NULL, 'electricity_bill/electricity_210825063143.pdf', NULL, NULL, NULL, 'resume/resume_210825063143.pdf', NULL, '', '0', '2021-08-25 18:31:43', '2021-08-25 18:31:43'),
(189, 259, 'CBLPS3863B', NULL, '370784777687', 'aadhar_card/aadhar_front_210915023832.pdf', 'aadhar_card/aadhar_back_210915023832.pdf', '1987-12-25', 'HSC', 'AXIS BANK', '919010049062326', 'UTIB002754', NULL, 'education_proof/pan_210915023832.pdf', NULL, NULL, NULL, NULL, NULL, 'photo/photo_210915023832.pdf', 'resume/resume_210915023832.pdf', 'interview_check/interview_check_210915023832.pdf', '', '0', '2021-08-27 11:23:00', '2021-09-15 09:38:32'),
(190, 278, 'CNVPB8461N', NULL, '325682019566', NULL, NULL, '1997-09-05', 'Any Graduate', 'SBI', '40121515160', 'SBIN0000575', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-08-27 11:49:11', '2021-08-27 11:49:11'),
(191, 300, 'MVGPS3922J', 'pan_card/pan_210830111348.pdf', '528315043576', 'aadhar_card/aadhar_front_210830111348.pdf', 'aadhar_card/aadhar_back_210830111348.pdf', '2001-11-04', 'HSC', 'UNION BANK', '410702010011655', 'UBIN0541079', 'bank_passbook/bnk_passbook_210830111348.pdf', 'education_proof/edu_proof_210830111348.pdf', NULL, NULL, 'electricity_bill/electricity_210830111348.pdf', NULL, 'driving_licence/drive_210830111348.pdf', NULL, 'resume/resume_210830111348.pdf', 'interview_check/interview_check_210830111348.pdf', '', '0', '2021-08-30 11:13:48', '2021-08-30 11:13:48'),
(192, 253, 'AQOPM7462Q', 'pan_card/pan_210901110507.pdf', '804680138906', 'aadhar_card/aadhar_front_210901110507.pdf', NULL, '1980-01-15', 'Any Graduate', 'INDUSIND BANK', '159834772897', 'INDB0000002', NULL, 'education_proof/edu_proof_210901110507.pdf', NULL, NULL, NULL, NULL, NULL, NULL, 'resume/resume_210901110507.pdf', 'interview_check/interview_check_210901110507.pdf', '', '0', '2021-09-01 11:05:07', '2021-09-01 11:05:07'),
(200, 193, 'BXXPP8078A', NULL, '962793173470', NULL, NULL, '1992-05-07', 'HSC', 'BANK OF MAHARASHTRA', '60149894900', 'MAHB0000630', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-09-07 12:08:24', '2021-09-07 12:08:24'),
(201, 195, 'BWBPP4093G', NULL, '941959817424', NULL, NULL, '1992-11-07', 'SSC', 'STATE BANK OF INDIA', '38521546003', 'SBIN0021840', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-09-07 12:11:11', '2021-09-07 12:11:11'),
(202, 197, 'BPHPY6995J', NULL, '381121455318', NULL, NULL, '1997-11-17', 'Any Graduate', 'BANK OF MAHARASHTRA', '60224514084', 'MAHB0000359', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-09-07 12:13:02', '2021-09-07 12:13:02'),
(203, 200, 'EPTPD8572J', NULL, '800035635153', NULL, NULL, '2000-06-05', 'SSC', 'BANK OF MAHARASHTRA', '60378268508', 'MAHB0000769', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-09-07 12:15:25', '2021-09-07 12:15:25'),
(204, 158, 'ELYPK1218M', NULL, '883429164231', NULL, NULL, '1995-06-06', 'HSC', 'HDFC', '50100320681117', 'HDFC0000633', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-09-07 12:45:23', '2021-09-07 12:45:23'),
(205, 311, 'DPBPK6253F', 'pan_card/pan_210908105642.pdf', '366014084524', 'aadhar_card/aadhar_front_210908105642.pdf', NULL, '1995-04-14', 'Any Graduate', 'STATE BANK OF INDIA', '20238901880', 'SBIN0001399', 'bank_passbook/aadhar_210908105642.pdf', 'education_proof/pan_210908105642.pdf', 'rent_rotary/rent_210908105642.pdf', 'rent_rotary/rotary_210908105642.pdf', NULL, 'voter_id/voterid_210908105642.pdf', 'driving_licence/drive_210908105642.pdf', 'photo/photo_210908105642.pdf', 'resume/resume_210908105642.pdf', 'interview_check/interview_check_210908105642.pdf', '', '0', '2021-09-08 05:05:58', '2021-09-08 05:56:42');
INSERT INTO `ptx_documents` (`id`, `user_id`, `pan_card_number`, `pan_card_img`, `aadhar_card_number`, `aadhar_card_front_img`, `aadhar_card_back_img`, `dob`, `education`, `bnk_name`, `bnk_acc_number`, `ifsc_code`, `bnk_pass_img`, `edu_proof_img`, `rent_agmnt_img`, `rotary_agmnt_img`, `elect_bill_img`, `voter_id`, `driving_licence`, `photo`, `resume`, `interview_check`, `vacc_receipt`, `isDelete`, `created_at`, `updated_at`) VALUES
(206, 312, 'GBEPS8499Q', 'pan_card/pan_211002091011.pdf', '355603391310', 'aadhar_card/aadhar_front_211002091011.pdf', 'aadhar_card/aadhar_back_211002091011.pdf', '1995-03-29', 'Any Graduate', 'IDFC', '10047173623', 'IDFB0041358', NULL, 'education_proof/pan_211002091011.pdf', 'rent_rotary/rent_211002091011.pdf', NULL, 'electricity_bill/electricity_211002091011.pdf', 'voter_id/voterid_211002091011.pdf', 'driving_licence/drive_211002091011.pdf', 'photo/photo_211002091011.pdf', 'resume/resume_211002091011.pdf', 'interview_check/interview_check_211002091011.pdf', '', '0', '2021-09-08 06:52:40', '2021-10-02 04:10:11'),
(207, 202, 'AQJPB2959G', NULL, '918471203556', NULL, NULL, '1976-01-05', 'SSC', 'TJSB Bank', '53110100005470', 'TJSB0000053', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-09-09 06:20:02', '2021-09-09 06:20:02'),
(208, 186, 'BNGPJ0392C', NULL, '917454462504', NULL, NULL, '1997-04-10', 'SSC', 'BANK OF INDIA', '52010110012619', 'BKID0000520', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-09-09 06:44:59', '2021-09-09 06:44:59'),
(209, 117, 'EAFPB5731Q', NULL, '958408095412', NULL, NULL, '1999-08-21', 'Any Graduate', 'HDFC BANK', '50100407194008', 'HDFC0000633', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-09-09 06:54:54', '2021-09-09 06:54:54'),
(210, 190, 'AEFPH2753C', NULL, '896373059796', NULL, NULL, '1984-01-15', 'HSC', 'HDFC BANK', '50100415224200', 'HDFC0000633', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-09-09 07:00:51', '2021-09-09 07:00:51'),
(211, 304, 'AJSPN8836A', NULL, '350857758770', NULL, NULL, '1985-11-23', 'HSC', 'BANK OF MAHARASHTRA', '20042336512', 'MAHB0000244', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-09-09 07:01:58', '2021-09-09 07:01:58'),
(212, 121, 'DMKPM2533K', NULL, '509175462898', NULL, NULL, '1997-09-02', 'HSC', 'HDFC BANK', '50100319127494', 'HDFC0000633', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-09-09 07:02:59', '2021-09-09 07:02:59'),
(213, 303, 'BEKPP1904F', 'pan_card/pan_211002105859.pdf', '661690883735', 'aadhar_card/aadhar_front_211002105859.pdf', 'aadhar_card/aadhar_back_211002105859.pdf', '1989-12-05', 'Any Graduate', 'HDFC BANK', '01041040007789', 'HDFC0003038', 'bank_passbook/aadhar_211002105859.pdf', 'education_proof/pan_211002105859.pdf', NULL, NULL, 'electricity_bill/electricity_211002105859.pdf', 'voter_id/voterid_211002105859.pdf', 'driving_licence/drive_211002105859.pdf', 'photo/photo_211002105859.jpg', 'resume/resume_211002105859.pdf', 'interview_check/interview_check_211002105859.pdf', 'vacc_receipt/vacc_receipt_211002105859.pdf', '0', '2021-09-09 07:09:40', '2021-10-02 05:58:59'),
(214, 88, 'EBFPB0729F', NULL, '500270166069', NULL, NULL, '1999-08-26', 'SSC', 'KOTAK BANK', '9814250380', 'KKBK0001770', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-09-09 07:12:50', '2021-09-09 07:12:50'),
(215, 308, 'BXFPK6086D', NULL, '536138162450', NULL, NULL, '1977-04-21', 'HSC', 'COSMOS BANK LTD', '0040501030029', 'COSB0000004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-09-09 07:14:45', '2021-09-09 07:14:45'),
(216, 306, 'BCJPC3413K', NULL, '590362655091', NULL, NULL, '1996-05-08', 'Any Graduate', 'CENTRAL BANK OF INDIA', '3134480562', 'CBIN0280655', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-09-09 07:19:59', '2021-09-09 07:19:59'),
(217, 310, 'ELVPP7707R', NULL, '767586053393', NULL, NULL, '1991-01-29', 'Any Graduate', 'BANK OF BARODA', '04480100013471', 'BARB0POOCTY', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-09-09 07:31:55', '2021-10-02 04:19:41'),
(218, 309, 'AJVPT7051Q', 'pan_card/pan_210915022007.pdf', '588748004177', 'aadhar_card/aadhar_front_210915022007.pdf', 'aadhar_card/aadhar_back_210915022007.pdf', '1991-07-19', 'HSC', 'KOTAK BANK', '6512181244', 'KKBK0000723', 'bank_passbook/aadhar_210915022007.pdf', 'education_proof/pan_210915022007.pdf', NULL, NULL, 'electricity_bill/electricity_210915022007.pdf', 'voter_id/voterid_210915022007.pdf', 'driving_licence/drive_210915022007.pdf', NULL, 'resume/resume_210915022007.pdf', NULL, '', '0', '2021-09-09 07:34:05', '2021-09-15 09:20:07'),
(219, 287, 'KKVPS0337B', NULL, '335907687891', NULL, NULL, '1997-03-07', 'Any Graduate', 'BANK OF INDIA', '62410110009542', 'BKID0000624', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-09-09 07:35:09', '2021-09-23 03:24:32'),
(220, 296, 'AEJPB4785N', NULL, '658514325095', NULL, NULL, '1971-01-12', 'SSC', 'SARASWAT BANK', '202209100000365', 'SRCB0000202', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-09-09 07:40:29', '2021-09-09 07:40:29'),
(221, 307, 'GQZPS1524Q', 'pan_card/pan_211002103106.pdf', '477797398539', 'aadhar_card/aadhar_front_211002103106.pdf', 'aadhar_card/aadhar_back_211002103106.pdf', '1993-07-17', 'HSC', 'HDFC BANK', '50100441541767', 'HDFC0004187', 'bank_passbook/aadhar_211002103106.pdf', 'education_proof/pan_211002103106.pdf', NULL, NULL, NULL, NULL, NULL, NULL, 'resume/resume_211002103106.pdf', 'interview_check/interview_check_211002103106.pdf', '', '0', '2021-09-09 07:41:08', '2021-10-02 05:31:06'),
(222, 295, 'AOXPB4122R', NULL, '429471673060', NULL, NULL, '1981-05-19', 'SSC', 'HDFC BANK', '50100282763461', 'HDFC0000633', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-09-09 07:43:40', '2021-09-09 07:43:40'),
(223, 294, 'BVDPM6122Q', NULL, '308696717718', NULL, NULL, '1988-08-31', 'Any Graduate', 'SBI', '31256328586', 'SBIN0005126', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-09-09 07:45:45', '2021-09-09 07:45:45'),
(224, 305, 'AKFPA5139H', 'pan_card/pan_211002084516.pdf', '841404601255', 'aadhar_card/aadhar_front_211002084516.pdf', 'aadhar_card/aadhar_back_211002084516.pdf', '1977-01-08', 'Any Graduate', 'HDFC BANK', '50100450524187', 'HDFC0000633', NULL, 'education_proof/pan_211002084516.pdf', 'rent_rotary/rent_211002084516.pdf', NULL, 'electricity_bill/electricity_211002084516.pdf', 'voter_id/voterid_211002084516.pdf', NULL, NULL, 'resume/resume_211002084516.pdf', 'interview_check/interview_check_211002084516.pdf', '', '0', '2021-09-09 07:47:49', '2021-10-02 03:45:16'),
(225, 266, 'BIYPH2016J', NULL, '918303074688', NULL, NULL, '1987-05-11', 'SSC', 'KOTAK BANK', '7346040592', 'KKBK0001752', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-09-09 07:51:01', '2021-09-09 07:51:01'),
(226, 102, 'AFCPJ0793G', NULL, '787253088871', NULL, NULL, '1968-09-05', 'SSC', 'HDFC BANK', '50100409744289', 'HDFC0001578', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-09-09 08:04:38', '2021-09-09 08:04:38'),
(227, 130, 'MGDPS5291Q', NULL, '666745270522', NULL, NULL, '2000-11-14', 'HSC', 'HDFC', '50100394624120', 'HDFC0009526', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-09-09 12:15:33', '2021-09-15 09:51:33'),
(228, 316, 'BCGPR3295B', NULL, '287530860412', NULL, NULL, '1991-07-19', 'HSC', 'BHSGINI NIVEDITA  CO OP SOCI BANK', '003002300044000', 'BNSB0000002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-09-14 12:13:15', '2021-09-14 12:13:15'),
(229, 313, 'GFJPP9740G', 'pan_card/pan_210915030532.pdf', '391458036962', 'aadhar_card/aadhar_front_210915030532.pdf', 'aadhar_card/aadhar_back_210915030532.pdf', '2001-06-21', 'Any Graduate', 'HDFC', '50100441541895', 'HDFC0004187', 'bank_passbook/bnk_passbook_210915030532.pdf', 'education_proof/edu_proof_210915030532.pdf', NULL, NULL, 'electricity_bill/electricity_210915030532.pdf', NULL, NULL, 'photo/photo_210915030532.pdf', 'resume/resume_210915030532.pdf', NULL, '', '0', '2021-09-15 15:05:32', '2021-09-15 15:05:32'),
(230, 317, 'AMVPB9790R', NULL, '243015898358', NULL, NULL, '1982-03-01', 'Any Graduate', 'Punjab National Bank', '1783000400070734', 'PUNB178300', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '2021-09-20 06:40:43', '2021-09-20 06:40:43'),
(231, 220, 'GFJPP9740G', 'pan_card/pan_210923082109.pdf', '391458036962', 'aadhar_card/aadhar_front_210923082109.pdf', 'aadhar_card/aadhar_back_210923082109.pdf', '2021-06-21', 'Any Graduate', 'HDFC', '50100441541895', 'HDFC0004187', 'bank_passbook/bnk_passbook_210923082109.pdf', 'education_proof/edu_proof_210923082109.pdf', NULL, NULL, 'electricity_bill/electricity_210923082109.pdf', NULL, NULL, NULL, 'resume/resume_210923082109.pdf', 'interview_check/interview_check_210923082109.pdf', NULL, '0', '2021-09-23 08:21:09', '2021-09-23 08:21:09'),
(232, 263, 'GKYPP9502G', NULL, '214206961438', NULL, NULL, '2002-07-05', 'HSC', 'COSMOS', '01505010120340', 'COSB0000015', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '2021-09-23 08:48:28', '2021-10-02 04:02:02'),
(233, 251, 'FSDPB7441A', 'pan_card/pan_211002102229.pdf', '902506605748', 'aadhar_card/aadhar_front_210923090038.pdf', 'aadhar_card/aadhar_back_210923090038.pdf', '1999-09-09', 'HSC', 'HDFC', '50100395345219', 'HDFC0009606', 'bank_passbook/aadhar_211002102229.pdf', 'education_proof/edu_proof_210923090038.pdf', 'rent_rotary/rent_211002102229.pdf', 'rent_rotary/rotary_211002102229.pdf', 'electricity_bill/electricity_211002102229.pdf', NULL, NULL, NULL, 'resume/resume_211002102229.pdf', 'interview_check/interview_check_211002102229.pdf', NULL, '0', '2021-09-23 09:00:38', '2021-10-02 05:22:29'),
(234, 318, 'HIJPS3161J', NULL, '719463291650', NULL, NULL, '1996-06-09', 'SSC', 'BANK OF MAHARASHTRA', '60104054203', 'MAHB0001716', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '2021-09-24 10:11:27', '2021-09-24 10:11:27'),
(235, 319, 'FDOPS6745E', NULL, '737160677625', NULL, NULL, '1995-11-19', 'HSC', 'ICICI', '239001507577', 'ICIC0002399', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '2021-09-24 10:55:23', '2021-09-24 10:55:23'),
(236, 314, 'NZTPS5488D', 'pan_card/pan_211002093004.pdf', '387092972499', 'aadhar_card/aadhar_front_211002093004.pdf', 'aadhar_card/aadhar_back_211002093004.pdf', '1998-08-25', 'Any Graduate', 'HDFC', '50100459451147', 'HDFC0005529', 'bank_passbook/bnk_passbook_211002093004.pdf', 'education_proof/edu_proof_211002093004.pdf', 'rent_rotary/rent_211002093004.pdf', 'rent_rotary/rotary_211002093004.pdf', 'electricity_bill/electricity_211002093004.pdf', 'voter_id/voterid_211002093004.pdf', 'driving_licence/drive_211002093004.pdf', 'photo/photo_211002093004.pdf', 'resume/resume_211002093004.pdf', 'interview_check/interview_check_211002093004.pdf', NULL, '0', '2021-10-02 09:30:04', '2021-10-02 09:30:04'),
(237, 324, 'AMAMPG3879R', 'pan_card/pan_211002101409.pdf', '928805976618', 'aadhar_card/aadhar_front_211002101409.pdf', 'aadhar_card/aadhar_back_211002101409.pdf', '1981-12-04', 'SSC', 'SVC CO OP BANK', '107503130007636', 'SVCB0000075', 'bank_passbook/bnk_passbook_211002101409.pdf', 'education_proof/edu_proof_211002101409.pdf', 'rent_rotary/rent_211002103648.pdf', 'rent_rotary/rotary_211002103648.pdf', NULL, 'voter_id/voterid_211002101409.pdf', 'driving_licence/drive_211002103648.pdf', 'photo/photo_211002101409.pdf', 'resume/resume_211002101409.pdf', NULL, NULL, '0', '2021-10-02 10:14:09', '2021-10-02 05:36:48'),
(238, 321, 'IOCPK5424N', NULL, '988936085300', NULL, NULL, '2000-04-11', 'Any Graduate', 'BANK OF MAHARASHTRA', '60376404769', 'MAHB0000145', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '2021-10-08 10:44:29', '2021-10-08 10:44:29'),
(239, 322, 'ARSPM0648M', NULL, '245335449600', NULL, NULL, '1982-06-08', 'Any Graduate', 'AXIS BANK', '0370100722087', 'UTIB0000037', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '2021-10-08 12:15:09', '2021-10-08 12:15:09'),
(240, 133, 'OPDPS0909Q', NULL, '480687392591', 'aadhar_card/aadhar_front_211008122512.pdf', 'aadhar_card/aadhar_back_211008122512.pdf', '2003-06-13', 'HSC', 'HDFC', '50100441541793', 'HDFC0004187', 'bank_passbook/bnk_passbook_211008122512.pdf', 'education_proof/edu_proof_211008122512.pdf', NULL, NULL, 'electricity_bill/electricity_211008122512.pdf', NULL, NULL, 'photo/photo_211008122512.pdf', 'resume/resume_211008122512.pdf', 'interview_check/interview_check_211008122512.pdf', NULL, '0', '2021-10-08 12:25:12', '2021-10-08 12:25:12'),
(241, 247, 'ALHPG1802A', NULL, '261263658669', NULL, NULL, '1881-06-24', 'HSC', 'UNION BANK', '520291023975586', 'UBIN0910210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '2021-10-08 13:12:35', '2021-10-08 13:12:35'),
(242, 325, 'BQXPB1431L', NULL, '446820304499', NULL, NULL, '1989-09-01', 'SSC', 'HDFC', '50100320065975', 'HDFC0000437', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '2021-10-09 12:00:44', '2021-10-09 12:00:44'),
(243, 323, 'BRSPN2236', 'pan_card/pan_211026100758.pdf', '671078944529', 'aadhar_card/aadhar_front_211026100758.pdf', 'aadhar_card/aadhar_back_211026100758.pdf', '2000-04-27', 'Any Graduate', 'HDFC', '50100459435895', 'HDFC0005529', NULL, 'education_proof/edu_proof_211026100758.pdf', 'rent_rotary/rent_211026100758.pdf', 'rent_rotary/rotary_211026100758.pdf', 'electricity_bill/electricity_211026100758.pdf', NULL, NULL, NULL, 'resume/resume_211026100758.pdf', 'interview_check/interview_check_211026100758.pdf', NULL, '0', '2021-10-26 10:07:58', '2021-10-26 10:07:58'),
(244, 320, 'FMRPR4964N', NULL, '995984733990', NULL, NULL, '1999-05-18', 'HSC', 'HDFC', '50100456563764', 'HDFC0009606', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '2021-10-26 11:05:09', '2021-10-26 11:05:09'),
(245, 326, 'FMVPB0709P', NULL, '899516839129', NULL, NULL, '2003-01-01', 'HSC', 'HDFC', '50100461009011', 'HDFC0001445', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '2021-10-26 11:32:20', '2021-10-26 11:32:20'),
(246, 326, 'FMVPB0709P', NULL, '899516839129', NULL, NULL, '2003-01-01', 'HSC', 'HDFC', '50100461009011', 'HDFC0001445', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '2021-10-26 12:02:30', '2021-10-26 12:02:30');

-- --------------------------------------------------------

--
-- Table structure for table `ptx_dras`
--

CREATE TABLE `ptx_dras` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `dra` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dra_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dra_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_of_conduct` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trainer_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receipt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1 = Deleted, 0 = Not Deleted',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ptx_dras`
--

INSERT INTO `ptx_dras` (`id`, `user_id`, `dra`, `dra_date`, `dra_no`, `code_of_conduct`, `trainer_name`, `receipt`, `remark`, `isDeleted`, `created_at`, `updated_at`) VALUES
(2, 33, 'Training', '', '', '', '', NULL, NULL, 0, '2021-04-07 03:29:39', '2021-04-07 03:30:50'),
(3, 137, 'Completed', '2011-5-20', '845088257', 'Yes', '', 'dra_receipt/dra_210624094814.pdf', NULL, 0, '2021-05-18 10:56:11', '2021-06-24 04:48:14'),
(4, 61, 'Completed', '2011-10-03', '845093733', 'Yes', '', 'dra_receipt/dra_210625062756.pdf', NULL, 0, '2021-05-18 11:01:43', '2021-06-25 01:27:56'),
(6, 32, 'Untouched', '', '', '', '', NULL, NULL, 0, '2021-05-19 05:27:50', '2021-06-24 00:42:57'),
(7, 35, 'Completed', '2010-3-10', '845046304', 'Yes', '', 'dra_receipt/dra_210624092452.pdf', NULL, 0, '2021-05-27 12:31:02', '2021-06-24 04:24:52'),
(8, 191, 'Completed', '2012-6-30', '857105754', 'Yes', '', 'dra_receipt/dra_210624123352.pdf', NULL, 0, '2021-05-28 05:07:24', '2021-06-24 07:33:52'),
(9, 192, 'Completed', '2019-8-03', '801372840', 'Yes', '', 'dra_receipt/dra_210624010510.pdf', NULL, 0, '2021-05-28 05:09:55', '2021-06-24 08:05:10'),
(10, 193, 'Completed', '2019-8-22', '801372849', 'Yes', '', 'dra_receipt/dra_210625065349.pdf', NULL, 0, '2021-05-28 05:11:24', '2021-06-25 01:53:49'),
(11, 194, 'Completed', '2018-7-27', '801202033', 'Yes', '', 'dra_receipt/dra_210625073157.pdf', NULL, 0, '2021-05-28 05:13:43', '2021-06-25 02:31:57'),
(12, 196, 'Completed', '2012-1-05', '845104942', 'Yes', '', 'dra_receipt/dra_210819123137.pdf', NULL, 0, '2021-05-28 05:14:29', '2021-08-19 07:31:37'),
(13, 198, 'Completed', '2016-6-21', '801132593', 'Yes', '', 'dra_receipt/dra_210625061824.pdf', NULL, 0, '2021-05-28 05:15:09', '2021-06-25 01:18:24'),
(14, 199, 'Completed', '2010-11-30', '845067877', 'Yes', '', 'dra_receipt/dra_210624100247.pdf', NULL, 0, '2021-05-28 05:16:00', '2021-06-24 05:02:47'),
(15, 135, 'Completed', '2009-10-06', '845035852', 'Yes', '', 'dra_receipt/dra_210624010550.pdf', NULL, 0, '2021-05-28 05:17:08', '2021-06-24 08:05:50'),
(16, 138, 'Completed', '2011-2-8', '845072829', 'Yes', '', 'dra_receipt/dra_210624010841.pdf', NULL, 0, '2021-05-28 07:27:32', '2021-06-24 08:08:41'),
(17, 139, 'Completed', '2010-3-10', '845046094', 'Yes', '', 'dra_receipt/dra_210624125820.pdf', NULL, 0, '2021-05-28 07:29:01', '2021-06-24 07:58:20'),
(18, 140, 'Completed', '2010-3-29', '845048689', 'Yes', '', 'dra_receipt/dra_210624095434.pdf', NULL, 0, '2021-05-28 07:43:28', '2021-06-24 04:54:34'),
(19, 141, 'Completed', '2010-3-10', '845046093', 'Yes', '', 'dra_receipt/dra_210624095338.pdf', NULL, 0, '2021-05-28 07:44:42', '2021-06-24 04:53:38'),
(20, 142, 'Completed', '2016-12-02', '801135931', 'Yes', '', 'dra_receipt/dra_210625065224.pdf', NULL, 0, '2021-05-28 07:45:40', '2021-06-25 01:52:24'),
(21, 143, 'Completed', '2010-3-29', '845048683', 'Yes', '', 'dra_receipt/dra_210625061947.pdf', NULL, 0, '2021-05-28 07:50:14', '2021-06-25 01:19:47'),
(22, 63, 'Completed', '2019-3-20', '801300207', 'Yes', '', 'dra_receipt/dra_210624010147.pdf', NULL, 0, '2021-05-28 07:51:08', '2021-06-24 08:01:47'),
(23, 144, 'Completed', '2012-8-28', '8484046740', 'Yes', '', 'dra_receipt/dra_210625063647.pdf', NULL, 0, '2021-05-28 07:53:06', '2021-06-25 01:36:47'),
(24, 64, 'Completed', '2012-8-03', '845106783', 'Yes', '', 'dra_receipt/dra_210625063907.pdf', NULL, 0, '2021-05-28 07:54:45', '2021-06-25 01:39:07'),
(25, 145, 'Completed', '2009-10-06', '845035851', 'Yes', '', 'dra_receipt/dra_210624125156.pdf', NULL, 0, '2021-05-28 07:57:37', '2021-06-24 07:51:56'),
(26, 146, 'Completed', '2009-10-06', '845035850', 'Yes', '', 'dra_receipt/dra_210624010917.pdf', NULL, 0, '2021-05-28 07:58:28', '2021-06-24 08:09:17'),
(27, 147, 'Completed', '2009-10-06', '845035844', 'Yes', '', 'dra_receipt/dra_210624095012.pdf', NULL, 0, '2021-05-28 07:59:24', '2021-06-24 04:50:12'),
(28, 148, 'Completed', '2009-10-06', '845035841', 'Yes', '', 'dra_receipt/dra_210624123812.pdf', NULL, 0, '2021-05-28 08:00:30', '2021-06-24 07:38:12'),
(29, 66, 'Completed', '2016-3-11', '801106844', 'Yes', '', 'dra_receipt/dra_210624125259.pdf', NULL, 0, '2021-05-28 08:01:26', '2021-06-24 07:52:59'),
(30, 69, 'Completed', '2013-7-31', '845117189', 'Yes', '', 'dra_receipt/dra_210624095111.pdf', NULL, 0, '2021-05-29 06:27:59', '2021-06-24 04:51:11'),
(31, 150, 'Completed', '2010-3-29', '845117189', 'Yes', '', 'dra_receipt/dra_210624124009.pdf', NULL, 0, '2021-05-29 06:28:49', '2021-06-24 07:40:09'),
(32, 151, 'Completed', '2010-3-29', '845048686', 'Yes', '', 'dra_receipt/dra_210624095252.pdf', NULL, 0, '2021-05-29 06:29:53', '2021-06-24 04:52:52'),
(33, 152, 'Completed', '2016-7-25', '801122223', 'Yes', '', 'dra_receipt/dra_210624122500.pdf', NULL, 0, '2021-05-29 06:30:37', '2021-06-24 07:25:00'),
(34, 203, 'Completed', '2016-3-11', '801106849', 'Yes', '', 'dra_receipt/dra_210625065602.pdf', NULL, 0, '2021-05-29 06:31:16', '2021-06-25 01:56:02'),
(35, 68, 'Completed', '2016-12-02', '801135933', 'Yes', '', 'dra_receipt/dra_210625070057.pdf', NULL, 0, '2021-05-29 06:31:52', '2021-06-25 02:00:57'),
(36, 71, 'Completed', '2016-7-25', '801122222', 'Yes', '', 'dra_receipt/dra_210625071408.pdf', NULL, 0, '2021-05-29 06:32:39', '2021-06-25 02:14:08'),
(37, 159, 'Completed', '2019-4-16', '801316760', 'Yes', '', 'dra_receipt/dra_210624123556.pdf', NULL, 0, '2021-05-29 06:33:22', '2021-06-24 07:35:56'),
(38, 160, 'Completed', '2010-10-07', '801163678', 'Yes', '', 'dra_receipt/dra_210625065133.pdf', NULL, 0, '2021-05-29 06:34:02', '2021-06-25 01:51:33'),
(39, 72, 'Completed', '2018-7-27', '801194988', 'Yes', '', 'dra_receipt/dra_210625072919.pdf', NULL, 0, '2021-05-29 06:36:43', '2021-06-25 02:29:19'),
(40, 204, 'Completed', '2016-12-02', '801135932', 'Yes', '', 'dra_receipt/dra_210625073005.pdf', NULL, 0, '2021-05-29 06:37:52', '2021-06-25 02:30:05'),
(41, 162, 'Completed', '2018-9-27', '801214456', 'Yes', '', 'dra_receipt/dra_210624125900.pdf', NULL, 0, '2021-05-29 06:38:40', '2021-06-24 07:59:00'),
(42, 164, 'Completed', '2018-7-25', '801122049', 'Yes', '', 'dra_receipt/dra_210625065032.pdf', NULL, 0, '2021-05-29 06:39:41', '2021-06-25 01:50:32'),
(43, 75, 'Completed', '2018-7-27', '801194986', 'Yes', '', 'dra_receipt/dra_210625062323.pdf', NULL, 0, '2021-05-29 06:40:18', '2021-06-25 01:23:23'),
(44, 166, 'Completed', '2017-10-07', '801163679', 'Yes', '', 'dra_receipt/dra_210624011542.pdf', NULL, 0, '2021-05-29 06:41:10', '2021-06-24 08:15:42'),
(45, 205, 'Completed', '2018-9-27', '801214447', 'Yes', '', 'dra_receipt/dra_210624010423.pdf', NULL, 0, '2021-05-29 06:42:46', '2021-06-24 08:04:23'),
(46, 77, 'Completed', '2019-2-01', '801262794', 'Yes', '', 'dra_receipt/dra_210625072100.pdf', NULL, 0, '2021-05-29 06:44:33', '2021-06-25 02:21:00'),
(47, 170, 'Completed', '2019-11-13', '801386437', 'Yes', '', 'dra_receipt/dra_210625060703.pdf', NULL, 0, '2021-05-29 06:45:08', '2021-06-25 01:07:03'),
(48, 79, 'Completed', '2018-7-27', '801194987', 'Yes', '', 'dra_receipt/dra_210625064940.pdf', NULL, 0, '2021-05-29 06:45:42', '2021-06-25 01:49:40'),
(49, 82, 'Completed', '2019-2-01', '801242795', 'Yes', '', 'dra_receipt/dra_210624125359.pdf', NULL, 0, '2021-05-29 06:48:52', '2021-06-24 07:53:59'),
(50, 83, 'Completed', '2020-3-20', '801300212', 'Yes', '', 'dra_receipt/dra_210625071233.pdf', NULL, 0, '2021-05-29 06:49:31', '2021-06-25 02:12:33'),
(51, 171, 'Completed', '2019-4-16', '801316759', 'Yes', '', 'dra_receipt/dra_210624094546.pdf', NULL, 0, '2021-05-29 06:50:13', '2021-06-24 04:45:46'),
(52, 85, 'Completed', '2013-4-06', '845114131', 'Yes', '', 'dra_receipt/dra_210624125656.pdf', NULL, 0, '2021-05-29 06:52:57', '2021-06-24 07:56:56'),
(53, 176, 'Completed', '2019-4-16', '801317797', 'Yes', '', 'dra_receipt/dra_210819082218.pdf', NULL, 0, '2021-05-29 06:53:43', '2021-08-19 03:22:18'),
(54, 89, 'Completed', '2019-2-01', '801262634', 'Yes', '', 'dra_receipt/dra_210625072519.pdf', NULL, 0, '2021-05-29 06:54:19', '2021-06-25 02:25:19'),
(55, 216, 'Completed', '2010-11-30', '845067855', 'Yes', '', 'dra_receipt/dra_210624011334.pdf', NULL, 0, '2021-05-29 06:54:52', '2021-06-24 08:13:34'),
(56, 108, 'Completed', '2020-3-18', '801462285', 'Yes', '', 'dra_receipt/dra_210625065653.pdf', NULL, 0, '2021-05-29 06:55:29', '2021-06-25 01:56:53'),
(57, 109, 'Completed', '2020-11-27', '801548556', 'Yes', '', 'dra_receipt/dra_210625061752.pdf', NULL, 0, '2021-05-29 06:56:13', '2021-06-25 01:17:52'),
(58, 230, 'Completed', '2010-3-10', '845046305', 'Yes', '', 'dra_receipt/dra_210624093242.pdf', NULL, 0, '2021-05-29 06:58:19', '2021-06-24 04:32:42'),
(59, 116, 'Completed', '2019-2-01', '801262629', 'Yes', '', 'dra_receipt/dra_210625080406.pdf', NULL, 0, '2021-05-29 06:59:29', '2021-06-25 03:04:06'),
(60, 188, 'Completed', '2020-2-12', '801445789', 'Yes', NULL, NULL, NULL, 0, '2021-05-29 07:02:11', '2021-05-29 07:02:11'),
(61, 41, 'Completed', '2011-12-03', '845093742', 'Yes', '', 'dra_receipt/dra_210624093006.pdf', NULL, 0, '2021-05-29 07:04:34', '2021-06-24 04:30:06'),
(62, 210, 'Completed', '2021-4-07', '801576513', 'Yes', '', 'dra_receipt/dra_210625074701.pdf', NULL, 0, '2021-05-29 07:10:43', '2021-06-25 02:47:01'),
(63, 215, 'Trained and Passed(Certificate Pending)', '', '', '', '', NULL, NULL, 0, '2021-05-29 07:11:12', '2021-09-18 05:51:07'),
(64, 100, 'Trained and Failed', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-05-29 07:11:28', '2021-05-29 07:11:28'),
(65, 165, 'Completed', '2021-1-23', '801576516', 'Yes', '', 'dra_receipt/dra_210624122615.pdf', NULL, 0, '2021-05-29 11:23:59', '2021-06-24 07:26:15'),
(66, 87, 'Completed', '2021-1-22', '801576507', 'Yes', '', 'dra_receipt/dra_210624010338.pdf', NULL, 0, '2021-05-29 11:25:12', '2021-06-24 08:03:38'),
(67, 91, 'Completed', '2021-1-23', '801576526', 'Yes', '', 'dra_receipt/dra_210625062244.pdf', NULL, 0, '2021-05-29 11:27:53', '2021-06-25 01:22:44'),
(68, 167, 'Completed', '2021-1-23', '801576510', 'Yes', '', 'dra_receipt/dra_210625063558.pdf', NULL, 0, '2021-05-29 11:28:54', '2021-06-25 01:35:58'),
(69, 217, 'Completed', '2021-1-23', '801576511', 'Yes', '', 'dra_receipt/dra_210625064315.pdf', NULL, 0, '2021-05-29 11:29:38', '2021-06-25 01:43:15'),
(70, 92, 'Completed', '2021-1-23', '801576519', 'Yes', '', 'dra_receipt/dra_210625064410.pdf', NULL, 0, '2021-05-29 11:31:22', '2021-06-25 01:44:10'),
(71, 93, 'Completed', '2021-1-23', '801576515', 'Yes', '', 'dra_receipt/dra_210625065315.pdf', NULL, 0, '2021-05-29 11:33:08', '2021-06-25 01:53:15'),
(72, 94, 'Completed', '2021-1-23', '801576508', 'Yes', '', 'dra_receipt/dra_210625072331.pdf', NULL, 0, '2021-05-29 11:36:14', '2021-06-25 02:23:31'),
(73, 156, 'Completed', '2021-1-23', '801576521', 'Yes', '', 'dra_receipt/dra_210625073410.pdf', NULL, 0, '2021-05-29 11:37:20', '2021-06-25 02:34:10'),
(74, 154, 'Completed', '2021-1-23', '801576506', 'Yes', '', 'dra_receipt/dra_210625073502.pdf', NULL, 0, '2021-05-29 11:37:58', '2021-06-25 02:35:02'),
(75, 231, 'Completed', '2021-1-23', '801576523', 'Yes', '', 'dra_receipt/dra_210625074457.pdf', NULL, 0, '2021-05-29 11:50:17', '2021-06-25 02:44:57'),
(76, 98, 'Completed', '2021-1-23', '801576512', 'Yes', '', 'dra_receipt/dra_210625075801.pdf', NULL, 0, '2021-05-29 11:50:58', '2021-06-25 02:58:01'),
(77, 211, 'Completed', '2021-1-23', '801576524', 'Yes', '', 'dra_receipt/dra_210625075909.pdf', NULL, 0, '2021-05-29 11:52:06', '2021-06-25 02:59:09'),
(78, 213, 'Trained and Passed(Certificate Pending)', '', '', '', '', NULL, NULL, 0, '2021-05-29 11:53:01', '2021-09-18 05:52:28'),
(79, 232, 'Trained and Failed', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-05-29 11:58:27', '2021-05-29 11:58:27'),
(80, 80, 'Trained and Passed(Certificate Pending)', '', '', '', '', NULL, NULL, 0, '2021-05-29 11:59:20', '2021-09-18 03:55:54'),
(81, 65, 'Completed', '2020-11-27', '801548508', 'Yes', '', 'dra_receipt/dra_211006065005.pdf', NULL, 0, '2021-05-29 12:02:13', '2021-10-06 01:50:05'),
(82, 214, 'Completed', '2020-11-27', '801548515', 'Yes', '', 'dra_receipt/dra_211018091251.pdf', NULL, 0, '2021-05-29 12:33:42', '2021-10-18 04:12:51'),
(83, 86, 'Completed', '2020-11-26', '801548506', 'Yes', '', 'dra_receipt/dra_211018091458.pdf', NULL, 0, '2021-05-29 12:34:37', '2021-10-18 04:14:58'),
(84, 78, 'Completed', '2020-11-23', '801548513', 'Yes', '', 'dra_receipt/dra_211006062259.pdf', NULL, 0, '2021-05-29 12:36:34', '2021-10-06 01:22:59'),
(85, 206, 'Completed', '2020-11-27', '801548505', 'Yes', '', 'dra_receipt/dra_211018091426.pdf', NULL, 0, '2021-05-29 12:37:28', '2021-10-18 04:14:26'),
(86, 76, 'Completed', '2020-11-23', '801548512', 'Yes', '', 'dra_receipt/dra_211006064904.pdf', NULL, 0, '2021-05-29 12:38:08', '2021-10-06 01:49:04'),
(87, 233, 'Completed', '2020-11-27', '801548510', 'Yes', '', 'dra_receipt/dra_211018091531.pdf', NULL, 0, '2021-05-29 12:41:38', '2021-10-18 04:15:31'),
(90, 1, 'Completed', '2021-6-20', '1234567', 'Yes', '', 'dra_receipt/dra_210624092205.jpg', NULL, 0, '2021-06-24 03:04:27', '2021-06-24 04:22:05'),
(91, 36, 'Completed', '2011-10-03', '845093745', 'Yes', '', 'dra_receipt/dra_210824070940.pdf', NULL, 0, '2021-06-24 04:27:01', '2021-08-24 02:09:40'),
(92, 37, 'Completed', '2009-10-06', '845035845', 'Yes', '', 'dra_receipt/dra_210819114530.pdf', NULL, 0, '2021-06-24 04:28:53', '2021-08-19 06:45:30'),
(93, 60, 'Untouched', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-06-24 04:40:39', '2021-06-24 04:40:39'),
(94, 202, 'Completed', '2021-4-19', '801646436', 'Yes', '', 'dra_receipt/dra_210624112906.pdf', NULL, 0, '2021-06-24 05:39:27', '2021-06-24 06:29:06'),
(95, 207, 'Completed', '2015-7-14', '801021013', 'Yes', '', 'dra_receipt/dra_210819081555.pdf', NULL, 0, '2021-06-24 07:09:41', '2021-08-19 03:15:55'),
(96, 222, 'Completed', '2021-4-19', '801646439', 'Yes', '', 'dra_receipt/dra_210819114852.pdf', NULL, 0, '2021-06-24 07:16:09', '2021-08-19 06:48:52'),
(97, 62, 'Completed', '2011-10-03', '845118971', 'Yes', '', 'dra_receipt/dra_210819115013.pdf', NULL, 0, '2021-06-24 07:17:46', '2021-08-19 06:50:13'),
(98, 201, 'Completed', '2021-4-19', '801646432', 'Yes', '', 'dra_receipt/dra_210819115057.pdf', NULL, 0, '2021-06-24 07:19:54', '2021-08-19 06:50:57'),
(99, 157, 'Completed', '2021-4-07', '801635871', 'Yes', '', 'dra_receipt/dra_210819115157.pdf', NULL, 0, '2021-06-24 07:22:41', '2021-08-19 06:51:57'),
(100, 219, 'Completed', '2021-4-19', '801646430', 'Yes', '', 'dra_receipt/dra_210824070217.pdf', NULL, 0, '2021-06-24 07:43:29', '2021-08-24 02:02:17'),
(101, 136, 'Completed', '2012-8-28', '845107787', 'Yes', '', 'dra_receipt/dra_210824070408.pdf', NULL, 0, '2021-06-24 07:50:05', '2021-08-24 02:04:08'),
(102, 95, 'Completed', '2021-4-07', '801635869', 'Yes', '', 'dra_receipt/dra_210824070800.pdf', NULL, 0, '2021-06-24 07:56:13', '2021-08-24 02:08:00'),
(103, 101, 'Completed', '2021-4-07', '801635874', 'Yes', '', 'dra_receipt/dra_210824071427.pdf', NULL, 0, '2021-06-24 08:00:51', '2021-08-24 02:14:27'),
(104, 218, 'Completed', '2021-4-19', '801646437', 'Yes', '', 'dra_receipt/dra_210824071910.pdf', NULL, 0, '2021-06-24 08:07:55', '2021-08-24 02:19:10'),
(105, 221, 'Completed', '2021-4-19', '801646438', 'Yes', '', 'dra_receipt/dra_210824072134.pdf', NULL, 0, '2021-06-24 08:11:21', '2021-08-24 02:21:34'),
(106, 107, 'Completed', '2021-4-07', '801635873', 'Yes', '', 'dra_receipt/dra_210824072232.pdf', NULL, 0, '2021-06-24 08:12:44', '2021-08-24 02:22:32'),
(107, 106, 'Completed', '2021-4-07', '801635859', 'Yes', '', 'dra_receipt/dra_210824072558.pdf', NULL, 0, '2021-06-24 08:14:54', '2021-08-24 02:25:58'),
(108, 242, 'Completed', '2021-1-23', '801576517', 'Yes', '', 'dra_receipt/dra_210819082407.pdf', NULL, 0, '2021-06-25 01:02:42', '2021-08-19 03:24:07'),
(109, 99, 'Completed', '2021-4-07', '801635868', 'Yes', '', 'dra_receipt/dra_210824092501.pdf', NULL, 0, '2021-06-25 01:25:26', '2021-08-24 04:25:01'),
(110, 73, 'Completed', '2021-4-19', '801646441', 'Yes', '', 'dra_receipt/dra_210819110729.pdf', NULL, 0, '2021-06-25 01:27:12', '2021-08-19 06:07:29'),
(111, 220, 'Completed', '2021-4-19', '801646440', 'Yes', '', 'dra_receipt/dra_210825104145.pdf', NULL, 0, '2021-06-25 01:29:52', '2021-08-25 05:41:45'),
(112, 181, 'Completed', '2021-4-07', '801635862', 'Yes', '', 'dra_receipt/dra_210825104416.pdf', NULL, 0, '2021-06-25 01:34:59', '2021-08-25 05:44:16'),
(113, 243, 'Completed', '2021-4-07', '801635866', 'Yes', '', 'dra_receipt/dra_210825104614.pdf', NULL, 0, '2021-06-25 01:38:27', '2021-08-25 05:46:14'),
(114, 113, 'Completed', '2021-4-07', '801635863', 'Yes', '', 'dra_receipt/dra_210825104829.pdf', NULL, 0, '2021-06-25 01:47:25', '2021-08-25 05:48:29'),
(115, 195, 'Completed', '2021-4-19', '801646434', 'Yes', '', 'dra_receipt/dra_210825105158.pdf', NULL, 0, '2021-06-25 02:00:14', '2021-08-25 05:51:58'),
(116, 90, 'Completed', '2021-4-07', '801635860', 'Yes', '', 'dra_receipt/dra_210625071626.pdf', NULL, 0, '2021-06-25 02:02:59', '2021-06-25 02:16:26'),
(117, 84, 'Completed', '2020-11-27', '801548516', 'Yes', '', 'dra_receipt/dra_210625071531.pdf', NULL, 0, '2021-06-25 02:04:22', '2021-06-25 02:15:31'),
(118, 74, 'Completed', '2021-4-07', '801635861', 'Yes', '', 'dra_receipt/dra_210625071449.pdf', NULL, 0, '2021-06-25 02:05:40', '2021-06-25 02:14:49'),
(119, 169, 'Completed', '2018-9-27', '801214446', 'Yes', '', 'dra_receipt/dra_210825110933.pdf', NULL, 0, '2021-06-25 02:28:44', '2021-08-25 06:09:33'),
(120, 197, 'Completed', '2021-4-19', '801646431', 'Yes', '', 'dra_receipt/dra_210825111645.pdf', NULL, 0, '2021-06-25 02:38:13', '2021-08-25 06:16:45'),
(121, 96, 'Completed', '2021-4-07', '801635865', 'Yes', '', 'dra_receipt/dra_210625075315.pdf', NULL, 0, '2021-06-25 02:51:10', '2021-06-25 02:53:15'),
(122, 88, 'Completed', '2021-4-07', '801635864', 'Yes', '', 'dra_receipt/dra_210825111923.pdf', NULL, 0, '2021-06-25 02:55:18', '2021-08-25 06:19:23'),
(123, 200, 'Completed', '2021-4-19', '801646435', 'Yes', '', 'dra_receipt/dra_210825112025.pdf', NULL, 0, '2021-06-25 02:57:19', '2021-08-25 06:20:25'),
(124, 175, 'Completed', '2021-4-07', '801635872', 'Yes', '', 'dra_receipt/dra_210825112151.pdf', NULL, 0, '2021-06-25 03:01:09', '2021-08-25 06:21:51'),
(125, 225, 'Completed', '2018-9-27', '801214457', 'Yes', '', 'dra_receipt/dra_210825112342.pdf', NULL, 0, '2021-06-25 03:03:11', '2021-08-25 06:23:42'),
(126, 103, 'Trained and Failed', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-06-25 04:26:34', '2021-06-25 04:26:34'),
(127, 149, 'Completed', '2015-11-02', '801026961', 'Yes', '', 'dra_receipt/dra_210824071605.pdf', NULL, 0, '2021-06-26 00:55:42', '2021-08-24 02:16:05'),
(128, 81, 'Completed', '2021-3-05', '801548504', 'Yes', '', 'dra_receipt/dra_210825111206.pdf', NULL, 0, '2021-06-26 06:48:24', '2021-08-25 06:12:06'),
(129, 259, 'Trained and Passed(Certificate Pending)', '', '', '', '', NULL, NULL, 0, '2021-08-19 02:47:42', '2021-09-18 03:55:18'),
(130, 123, 'Trained and Passed(Certificate Pending)', '', '', '', '', NULL, NULL, 0, '2021-08-19 02:48:07', '2021-09-18 03:53:24'),
(131, 115, 'Trained and Passed(Certificate Pending)', '', '', '', '', NULL, NULL, 0, '2021-08-19 02:48:39', '2021-09-18 05:48:25'),
(132, 122, 'Trained and Passed(Certificate Pending)', '', '', '', '', NULL, NULL, 0, '2021-08-19 02:49:35', '2021-09-18 05:53:19'),
(133, 105, 'Trained and Passed(Certificate Pending)', '', '', '', '', NULL, NULL, 0, '2021-08-19 02:50:17', '2021-09-18 03:50:30'),
(134, 110, 'Trained and Passed(Certificate Pending)', '', '', '', '', NULL, NULL, 0, '2021-08-19 02:50:55', '2021-09-18 05:59:16'),
(135, 238, 'Trained and Passed(Certificate Pending)', '', '', '', '', NULL, NULL, 0, '2021-08-19 02:51:47', '2021-09-18 03:51:02'),
(136, 244, 'Trained and Passed(Certificate Pending)', '', '', '', '', NULL, NULL, 0, '2021-08-19 02:53:39', '2021-09-18 03:52:13'),
(137, 118, 'Trained and Failed', '', '', '', '', NULL, NULL, 0, '2021-08-19 02:54:00', '2021-09-18 06:00:40'),
(138, 119, 'Trained and Passed(Certificate Pending)', '', '', '', '', NULL, NULL, 0, '2021-08-19 02:54:42', '2021-09-18 03:53:54'),
(139, 104, 'Trained and Passed(Certificate Pending)', '', '', '', '', NULL, NULL, 0, '2021-08-19 02:55:09', '2021-09-18 03:53:01'),
(140, 120, 'Trained and Passed(Certificate Pending)', '', '', '', '', NULL, NULL, 0, '2021-08-19 02:55:34', '2021-09-18 05:46:46'),
(141, 126, 'Trained and Passed(Certificate Pending)', '', '', '', '', NULL, NULL, 0, '2021-08-19 02:55:54', '2021-09-18 05:43:25'),
(142, 127, 'Trained and Passed(Certificate Pending)', '', '', '', '', NULL, NULL, 0, '2021-08-19 02:56:21', '2021-09-18 05:50:34'),
(143, 129, 'Trained and Failed', '', '', '', '', NULL, NULL, 0, '2021-08-19 02:56:58', '2021-09-18 06:00:14'),
(144, 130, 'Trained and Passed(Certificate Pending)', '', '', '', '', NULL, NULL, 0, '2021-08-19 02:57:18', '2021-09-18 05:52:52'),
(145, 131, 'Trained and Passed(Certificate Pending)', '', '', '', '', NULL, NULL, 0, '2021-08-19 02:57:46', '2021-09-18 03:54:37'),
(146, 132, 'Trained and Passed(Certificate Pending)', '', '', '', '', NULL, NULL, 0, '2021-08-19 02:58:14', '2021-09-18 05:44:04'),
(147, 134, 'Trained and Passed(Certificate Pending)', '', '', '', '', NULL, NULL, 0, '2021-08-19 02:58:43', '2021-09-18 05:54:00'),
(148, 168, 'Trained and Passed(Certificate Pending)', '', '', '', '', NULL, NULL, 0, '2021-08-19 02:59:37', '2021-09-18 05:47:46'),
(149, 97, 'Trained and Passed(Certificate Pending)', '', '', '', '', NULL, NULL, 0, '2021-08-19 03:00:35', '2021-09-18 05:44:27'),
(150, 177, 'Trained and Passed(Certificate Pending)', '', '', '', '', NULL, NULL, 0, '2021-08-19 03:01:15', '2021-09-18 05:45:03'),
(151, 180, 'Trained and Passed(Certificate Pending)', '', '', '', '', 'dra_receipt/dra_210909124218.pdf', NULL, 0, '2021-08-19 03:01:38', '2021-09-18 03:50:08'),
(152, 212, 'Trained and Passed(Certificate Pending)', '', '', '', '', NULL, NULL, 0, '2021-08-19 03:02:00', '2021-09-18 04:20:06'),
(153, 67, 'Completed', '2015-7-24', '801017762', 'Yes', '', 'dra_receipt/dra_210824070326.pdf', NULL, 0, '2021-08-19 03:19:23', '2021-08-24 02:03:26'),
(154, 294, 'Completed', '2020-1-14', '801435325', 'Yes', NULL, NULL, NULL, 0, '2021-08-25 08:43:06', '2021-08-25 08:43:06'),
(155, 58, 'Untouched', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-08-25 08:49:28', '2021-08-25 08:49:28'),
(156, 295, 'Completed', '2019-8-22', '801373892', 'Yes', NULL, NULL, NULL, 0, '2021-08-26 00:05:57', '2021-08-26 00:05:57'),
(157, 296, 'Completed', '2019-9-19', '801386633', 'Yes', NULL, NULL, NULL, 0, '2021-08-26 00:12:52', '2021-08-26 00:12:52'),
(158, 287, 'Completed', '2020-3-18', '801462325', 'Yes', '', 'dra_receipt/dra_210917071501.pdf', NULL, 0, '2021-09-16 08:10:59', '2021-09-17 02:15:01'),
(159, 253, 'Completed', '2014-11-01', '845120370', 'Yes', NULL, NULL, NULL, 0, '2021-09-17 02:23:31', '2021-09-17 02:23:31'),
(160, 312, 'Completed', '2016-3-11', '801106842', 'Yes', NULL, NULL, NULL, 0, '2021-09-17 07:28:08', '2021-09-17 07:28:08'),
(161, 236, 'Completed', '2011-10-03', '845093730', 'Yes', '', 'dra_receipt/dra_210917123341.pdf', NULL, 0, '2021-09-17 07:32:41', '2021-09-17 07:33:41'),
(162, 293, 'Trained and Passed(Certificate Pending)', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-09-18 06:11:11', '2021-09-18 06:11:11'),
(163, 179, 'Training', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-10-22 02:33:03', '2021-10-22 02:33:03'),
(164, 190, 'Training', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-10-22 02:33:49', '2021-10-22 02:33:49'),
(165, 208, 'Training', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-10-22 02:34:17', '2021-10-22 02:34:17'),
(166, 247, 'Training', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-10-22 02:34:52', '2021-10-22 02:34:52'),
(167, 174, 'Training', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-10-22 02:35:23', '2021-10-22 02:35:23'),
(168, 234, 'Training', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-10-22 02:35:52', '2021-10-22 02:35:52'),
(169, 163, 'Training', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-10-22 02:36:23', '2021-10-22 02:36:23'),
(170, 324, 'Training', '', '', '', 'TRADE KART INDIA ENTERPRISES', NULL, NULL, 0, '2021-10-22 02:37:06', '2021-10-22 02:39:18'),
(171, 124, 'Training', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-10-22 02:40:23', '2021-10-22 02:40:23'),
(172, 254, 'Training', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-10-22 02:40:55', '2021-10-22 02:40:55'),
(173, 240, 'Training', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-10-22 02:41:28', '2021-10-22 02:41:28'),
(174, 251, 'Training', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-10-22 02:41:50', '2021-10-22 02:41:50'),
(175, 261, 'Training', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-10-22 02:42:16', '2021-10-22 02:42:16'),
(176, 276, 'Training', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-10-22 02:43:13', '2021-10-22 02:43:13'),
(177, 272, 'Training', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-10-22 02:45:09', '2021-10-22 02:45:09'),
(178, 322, 'Training', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-10-22 02:46:24', '2021-10-22 02:46:24'),
(179, 239, 'Training', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-10-22 02:47:07', '2021-10-22 02:47:07'),
(180, 277, 'Training', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-10-22 02:47:46', '2021-10-22 02:47:46'),
(181, 309, 'Training', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-10-22 02:48:34', '2021-10-22 02:48:34'),
(182, 133, 'Training', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-10-22 02:49:18', '2021-10-22 02:49:18'),
(183, 280, 'Training', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-10-22 02:49:51', '2021-10-22 02:49:51'),
(184, 318, 'Training', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-10-22 02:50:44', '2021-10-22 02:50:44'),
(185, 313, 'Training', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-10-22 02:51:06', '2021-10-22 02:51:06'),
(186, 297, 'Training', '--', NULL, NULL, NULL, NULL, NULL, 0, '2021-10-22 02:52:10', '2021-10-22 02:52:10');

-- --------------------------------------------------------

--
-- Table structure for table `ptx_employee_salary`
--

CREATE TABLE `ptx_employee_salary` (
  `id` int(10) NOT NULL,
  `employee_id` int(10) DEFAULT NULL,
  `company_id` int(10) DEFAULT NULL COMMENT 'Employee company ID',
  `key` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'salary type name',
  `value` double(10,2) DEFAULT NULL COMMENT 'Amount of that salary type',
  `type` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'This column contains the ',
  `salary_template_id` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deduct_from` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deduct_to` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ptx_employee_salary`
--

INSERT INTO `ptx_employee_salary` (`id`, `employee_id`, `company_id`, `key`, `value`, `type`, `salary_template_id`, `deduct_from`, `deduct_to`, `created_at`, `updated_at`) VALUES
(1, 52090, NULL, 'PER DAY', 200.00, NULL, NULL, NULL, NULL, '2021-09-17 07:53:04', '2021-09-17 07:53:04'),
(2, 51273, NULL, 'PER DAY', 200.00, NULL, NULL, NULL, NULL, '2021-09-17 07:53:04', '2021-09-17 07:53:04'),
(3, 51961, NULL, 'PER DAY', 200.00, NULL, NULL, NULL, NULL, '2021-09-17 07:53:04', '2021-09-17 07:53:04'),
(4, 51658, NULL, 'PER DAY', 200.00, NULL, NULL, NULL, NULL, '2021-09-17 07:53:04', '2021-09-17 07:53:04'),
(5, 51067, NULL, 'PER DAY', 200.00, NULL, NULL, NULL, NULL, '2021-09-17 07:53:04', '2021-09-17 07:53:04'),
(6, 51808, NULL, 'PER DAY', 200.00, NULL, NULL, NULL, NULL, '2021-09-17 07:53:04', '2021-09-17 07:53:04'),
(7, 52090, NULL, 'INCENTIVE', 100.00, NULL, NULL, NULL, NULL, '2021-09-17 07:53:04', '2021-09-17 07:53:04'),
(8, 51273, NULL, 'INCENTIVE', 100.00, NULL, NULL, NULL, NULL, '2021-09-17 07:53:04', '2021-09-17 07:53:04'),
(9, 51961, NULL, 'INCENTIVE', 100.00, NULL, NULL, NULL, NULL, '2021-09-17 07:53:04', '2021-09-17 07:53:04'),
(10, 51658, NULL, 'INCENTIVE', 100.00, NULL, NULL, NULL, NULL, '2021-09-17 07:53:04', '2021-09-17 07:53:04'),
(11, 51067, NULL, 'INCENTIVE', 100.00, NULL, NULL, NULL, NULL, '2021-09-17 07:53:04', '2021-09-17 07:53:04'),
(12, 51808, NULL, 'INCENTIVE', 100.00, NULL, NULL, NULL, NULL, '2021-09-17 07:53:04', '2021-09-17 07:53:04'),
(13, 52101, NULL, 'PER DAY', 500.00, NULL, NULL, NULL, NULL, '2021-09-21 13:56:18', '2021-09-22 02:03:40'),
(14, 51048, NULL, 'PER DAY', 1500.00, NULL, NULL, NULL, NULL, '2021-09-21 13:57:29', '2021-09-21 13:57:29'),
(15, 51681, NULL, 'PER DAY', 500.00, NULL, NULL, NULL, NULL, '2021-09-21 13:58:19', '2021-09-21 13:58:19'),
(16, 51963, NULL, 'PER DAY', 270.00, NULL, NULL, NULL, NULL, '2021-09-21 13:58:40', '2021-09-21 13:58:40'),
(17, 51962, NULL, 'PER DAY', 270.00, NULL, NULL, NULL, NULL, '2021-09-21 13:58:59', '2021-09-21 13:58:59'),
(18, 51964, NULL, 'PER DAY', 270.00, NULL, NULL, NULL, NULL, '2021-09-21 13:58:59', '2021-09-21 13:58:59'),
(19, 52137, NULL, 'PER DAY', 270.00, NULL, NULL, NULL, NULL, '2021-09-21 13:58:59', '2021-09-21 13:58:59'),
(20, 51633, NULL, 'PER DAY', 500.00, NULL, NULL, NULL, NULL, '2021-09-21 14:00:17', '2021-09-21 09:00:30'),
(21, 51252, NULL, 'PER DAY', 500.00, NULL, NULL, NULL, NULL, '2021-09-21 14:00:46', '2021-09-21 14:00:46'),
(22, 52058, NULL, 'PER DAY', 500.00, NULL, NULL, NULL, NULL, '2021-09-21 14:01:19', '2021-09-21 14:01:19'),
(23, 52101, NULL, 'SALARY', 30000.00, NULL, NULL, NULL, NULL, '2021-09-26 15:55:11', '2021-09-29 02:42:24'),
(24, 52072, NULL, 'SALARY', 15000.00, NULL, NULL, NULL, NULL, '2021-09-26 15:55:11', '2021-09-26 15:55:11'),
(25, 51589, NULL, 'SALARY', 15000.00, NULL, NULL, NULL, NULL, '2021-09-26 15:55:11', '2021-09-26 15:55:11'),
(26, 52101, NULL, 'TDS', 1.00, NULL, NULL, NULL, NULL, '2021-09-26 16:37:41', '2021-10-29 01:49:42'),
(27, 52072, NULL, 'TDS', 1.00, NULL, NULL, NULL, NULL, '2021-09-26 16:37:41', '2021-09-26 16:37:41'),
(28, 51048, NULL, 'TDS', 1.00, NULL, NULL, NULL, NULL, '2021-09-26 16:37:41', '2021-09-26 16:37:41'),
(29, 51589, NULL, 'TDS', 1.00, NULL, NULL, NULL, NULL, '2021-09-26 16:37:41', '2021-09-26 16:37:41'),
(30, 52073, NULL, 'TDS', 1.00, NULL, NULL, NULL, NULL, '2021-09-26 16:37:41', '2021-09-26 16:37:41'),
(31, 52101, NULL, 'PENALTY', 0.00, NULL, NULL, NULL, NULL, '2021-09-26 16:38:00', '2021-09-27 05:24:52'),
(32, 52072, NULL, 'PENALTY', 100.00, NULL, NULL, NULL, NULL, '2021-09-26 16:38:00', '2021-09-26 16:38:00'),
(33, 51589, NULL, 'PENALTY', 100.00, NULL, NULL, NULL, NULL, '2021-09-26 16:38:00', '2021-09-26 16:38:00'),
(34, 52101, NULL, 'ADVANCE', 500.00, NULL, NULL, NULL, NULL, '2021-09-26 16:38:22', '2021-09-26 16:38:22'),
(35, 52072, NULL, 'ADVANCE', 500.00, NULL, NULL, NULL, NULL, '2021-09-26 16:38:22', '2021-09-26 16:38:22'),
(36, 51589, NULL, 'ADVANCE', 500.00, NULL, NULL, NULL, NULL, '2021-09-26 16:38:22', '2021-09-26 16:38:22'),
(37, 51252, NULL, 'SALARY', 16000.00, NULL, NULL, NULL, NULL, '2021-09-28 12:12:17', '2021-09-29 00:32:20'),
(38, 51681, NULL, 'SALARY', 15000.00, NULL, NULL, NULL, NULL, '2021-09-29 05:28:39', '2021-09-29 05:28:39'),
(39, 51963, NULL, 'SALARY', 8000.00, NULL, NULL, NULL, NULL, '2021-09-29 05:29:00', '2021-09-29 05:29:00'),
(40, 51962, NULL, 'SALARY', 8000.00, NULL, NULL, NULL, NULL, '2021-09-29 05:29:00', '2021-09-29 05:29:00'),
(41, 51964, NULL, 'SALARY', 8000.00, NULL, NULL, NULL, NULL, '2021-09-29 05:29:00', '2021-09-29 05:29:00'),
(42, 52090, NULL, 'SALARY', 18000.00, NULL, NULL, NULL, NULL, '2021-09-29 05:29:30', '2021-10-22 05:49:23'),
(43, 51067, NULL, 'SALARY', 18000.00, NULL, NULL, NULL, NULL, '2021-09-29 05:29:30', '2021-09-29 05:29:30'),
(44, 51961, NULL, 'SALARY', 25000.00, NULL, NULL, NULL, NULL, '2021-09-29 05:29:45', '2021-09-29 05:29:45'),
(45, 51658, NULL, 'SALARY', 15000.00, NULL, NULL, NULL, NULL, '2021-09-29 05:30:01', '2021-09-29 05:30:01'),
(46, 51808, NULL, 'SALARY', 12000.00, NULL, NULL, NULL, NULL, '2021-09-29 05:31:30', '2021-09-29 05:31:30'),
(47, 51633, NULL, 'SALARY', 18000.00, NULL, NULL, NULL, NULL, '2021-09-29 05:32:37', '2021-09-29 05:32:37'),
(48, 52058, NULL, 'SALARY', 15000.00, NULL, NULL, NULL, NULL, '2021-09-29 05:32:58', '2021-09-29 05:32:58'),
(49, 52069, NULL, 'SALARY', 10000.00, NULL, NULL, NULL, NULL, '2021-09-29 05:33:18', '2021-09-29 05:33:18'),
(50, 52089, NULL, 'SALARY', 12000.00, NULL, NULL, NULL, NULL, '2021-09-29 05:33:32', '2021-09-29 05:33:32'),
(51, 52101, NULL, 'INCENTIVE', 100.00, NULL, NULL, NULL, NULL, '2021-10-29 06:49:28', '2021-10-29 06:49:28'),
(52, 52072, NULL, 'INCENTIVE', 100.00, NULL, NULL, NULL, NULL, '2021-10-29 06:49:28', '2021-10-29 06:49:28');

-- --------------------------------------------------------

--
-- Table structure for table `ptx_failed_jobs`
--

CREATE TABLE `ptx_failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ptx_import_data_files`
--

CREATE TABLE `ptx_import_data_files` (
  `id` int(11) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `telecaller_id` int(11) DEFAULT NULL,
  `field_executive_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `pincode` varchar(6) DEFAULT NULL,
  `allocation_date` varchar(255) NOT NULL,
  `loan_no` varchar(255) DEFAULT NULL,
  `applicant_id` varchar(255) DEFAULT NULL,
  `product` varchar(255) DEFAULT NULL,
  `model` varchar(255) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `multiple_ac` varchar(255) NOT NULL,
  `sai_per` varchar(255) NOT NULL,
  `exe_per` varchar(255) NOT NULL,
  `cycle` varchar(255) NOT NULL,
  `bucket` varchar(255) NOT NULL,
  `pos` varchar(255) NOT NULL,
  `emi` varchar(255) NOT NULL,
  `penal_amount` varchar(255) NOT NULL,
  `clm` varchar(255) NOT NULL,
  `tcl` varchar(255) NOT NULL,
  `exe` varchar(255) NOT NULL,
  `last_month` varchar(255) NOT NULL,
  `omsai_status` varchar(255) NOT NULL,
  `expected_amt` varchar(255) NOT NULL,
  `ptp` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `resolved` varchar(255) NOT NULL,
  `exe_telecalling_remark` varchar(255) NOT NULL,
  `exe_visit_remark` varchar(255) NOT NULL,
  `tcl_ptp` varchar(255) NOT NULL,
  `tcl_status` varchar(255) NOT NULL,
  `tcl_remark` varchar(255) NOT NULL,
  `working_industry` varchar(255) NOT NULL,
  `ndustry_desc` varchar(255) NOT NULL,
  `paid_file` varchar(255) NOT NULL,
  `additional_no` varchar(255) NOT NULL,
  `tracing_feedback` varchar(255) NOT NULL,
  `res_no` varchar(255) NOT NULL,
  `off_no` varchar(255) NOT NULL,
  `mailing_no` varchar(255) NOT NULL,
  `rec_no` varchar(255) NOT NULL,
  `deposite_date` varchar(255) NOT NULL,
  `amt` varchar(255) NOT NULL,
  `penal` varchar(255) NOT NULL,
  `total` varchar(255) NOT NULL,
  `mode` varchar(255) NOT NULL,
  `month_1_status` varchar(255) NOT NULL,
  `month_2_status` varchar(255) NOT NULL,
  `month_3_status` varchar(255) NOT NULL,
  `month_4_status` varchar(255) NOT NULL,
  `month_5_status` varchar(255) NOT NULL,
  `last_pd_last` varchar(255) NOT NULL,
  `allocation_area` varchar(255) NOT NULL,
  `res_add` varchar(255) NOT NULL,
  `off_Add` varchar(255) NOT NULL,
  `property_add` varchar(255) NOT NULL,
  `disb_date` varchar(255) NOT NULL,
  `emi_start_date` varchar(255) NOT NULL,
  `emi_end_date` varchar(255) NOT NULL,
  `tenure` varchar(255) NOT NULL,
  `disb_amt` varchar(255) NOT NULL,
  `ref` varchar(255) NOT NULL,
  `ref_2` varchar(255) NOT NULL,
  `father_name` varchar(255) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `cibil` varchar(255) NOT NULL,
  `cibil_feedback` varchar(255) NOT NULL,
  `npa_stageid` varchar(255) NOT NULL,
  `bounce_reason` varchar(255) NOT NULL,
  `opening_dpd` varchar(255) NOT NULL,
  `current_dpd` varchar(255) NOT NULL,
  `last_month_agency` varchar(255) NOT NULL,
  `reg_no` varchar(255) NOT NULL,
  `engine_num` varchar(255) NOT NULL,
  `chasisnum` varchar(255) NOT NULL,
  `total_emi_due` varchar(255) NOT NULL,
  `cbc` varchar(255) NOT NULL,
  `lpp` varchar(255) NOT NULL,
  `total_due` varchar(255) NOT NULL,
  `no_of_bounces` varchar(255) NOT NULL,
  `edi` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `latitude_longitude` varchar(255) NOT NULL,
  `covid_flag` varchar(255) NOT NULL,
  `trails` varchar(255) NOT NULL,
  `psr_cat` varchar(255) NOT NULL,
  `priority` varchar(255) NOT NULL,
  `mannual_bkt` varchar(255) NOT NULL,
  `dpd` varchar(255) NOT NULL,
  `dpd_group` varchar(255) NOT NULL,
  `book_flag` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `reference_one_mobile_no` varchar(20) DEFAULT NULL,
  `reference_two_mobile_no` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ptx_import_data_files`
--

INSERT INTO `ptx_import_data_files` (`id`, `bank_id`, `agent_id`, `telecaller_id`, `field_executive_id`, `product_id`, `pincode`, `allocation_date`, `loan_no`, `applicant_id`, `product`, `model`, `customer_name`, `multiple_ac`, `sai_per`, `exe_per`, `cycle`, `bucket`, `pos`, `emi`, `penal_amount`, `clm`, `tcl`, `exe`, `last_month`, `omsai_status`, `expected_amt`, `ptp`, `status`, `resolved`, `exe_telecalling_remark`, `exe_visit_remark`, `tcl_ptp`, `tcl_status`, `tcl_remark`, `working_industry`, `ndustry_desc`, `paid_file`, `additional_no`, `tracing_feedback`, `res_no`, `off_no`, `mailing_no`, `rec_no`, `deposite_date`, `amt`, `penal`, `total`, `mode`, `month_1_status`, `month_2_status`, `month_3_status`, `month_4_status`, `month_5_status`, `last_pd_last`, `allocation_area`, `res_add`, `off_Add`, `property_add`, `disb_date`, `emi_start_date`, `emi_end_date`, `tenure`, `disb_amt`, `ref`, `ref_2`, `father_name`, `dob`, `cibil`, `cibil_feedback`, `npa_stageid`, `bounce_reason`, `opening_dpd`, `current_dpd`, `last_month_agency`, `reg_no`, `engine_num`, `chasisnum`, `total_emi_due`, `cbc`, `lpp`, `total_due`, `no_of_bounces`, `edi`, `category`, `latitude_longitude`, `covid_flag`, `trails`, `psr_cat`, `priority`, `mannual_bkt`, `dpd`, `dpd_group`, `book_flag`, `created_at`, `reference_one_mobile_no`, `reference_two_mobile_no`) VALUES
(1, 52, 41, 199, 200, 18, '411.54', '02-03-2021', '1231321', '1', '1254', 'test', 'Naveen Kh', 'tstff', 'the sai tech', 'exe per', 'new cycle', 'bucket', 'new pos', 'Emi vlaue', '4587', 'clm', 'tcl', 'exe', 'july', 'success', '78946', 'ptp', 'TRUE', 'resolved value', 'remark', 'visit remarl', 'rcl vlauw', 'staus tcl', 'tcl remarl', 'no', 'development', 'truq', 'sdfc', 'gfdsg', 'fsdfdsvs', '78465', 'gsdgsd', '8764', '646545', '54654', '1321', '3154545644', '21321', '1321446', '', '', '', '', '', '', 'baner', 'pune', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '7879465', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-14 10:49:40', '', ''),
(2, 52, 41, 199, 200, 18, '411.54', '02-03-2021', '321', '1', '1254', 'test', 'Naveen K', 'tstff', 'the sai tech', 'exe per', 'new cycle', 'bucket', 'new pos', 'Emi vlaue', '4587', 'clm', 'tcl', 'exe', 'july', 'success', '78946', 'ptp', 'TRUE', 'resolved value', 'remark', 'visit remarl', 'rcl vlauw', 'staus tcl', 'tcl remarl', 'no', 'development', 'truq', 'sdfc', 'gfdsg', 'fsdfdsvs', '78465', 'gsdgsd', '8764', '646545', '54654', '1321', '3154545644', '21321', '1321446', '', '', '', '', '', '', 'baner', 'pune', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '7879465', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-14 10:49:40', '', ''),
(3, 52, 41, 199, 200, 18, '411.54', '02-03-2021', '1231321', '1', '1254', 'test', 'Naveen Kh', 'tstff', 'the sai tech', 'exe per', 'new cycle', 'bucket', 'new pos', 'Emi vlaue', '4587', 'clm', 'tcl', 'exe', 'july', 'success', '78946', 'ptp', 'TRUE', 'resolved value', 'remark', 'visit remarl', 'rcl vlauw', 'staus tcl', 'tcl remarl', 'no', 'development', 'truq', 'sdfc', 'gfdsg', 'fsdfdsvs', '78465', 'gsdgsd', '8764', '646545', '54654', '1321', '3154545644', '21321', '1321446', '', '', '', '', '', '', 'baner', 'pune', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '7879465', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-14 10:49:40', '', ''),
(4, 56, 41, 199, 200, 27, '411.54', '02-03-2021', '1231321', '1', '1254', 'test', 'Naveen Kh', 'tstff', 'the sai tech', 'exe per', 'new cycle', 'bucket', 'new pos', 'Emi vlaue', '4587', 'clm', 'tcl', 'exe', 'july', 'success', '78946', 'ptp', 'TRUE', 'resolved value', 'remark', 'visit remarl', 'rcl vlauw', 'staus tcl', 'tcl remarl', 'no', 'development', 'truq', 'sdfc', 'gfdsg', 'fsdfdsvs', '78465', 'gsdgsd', '8764', '646545', '54654', '1321', '3154545644', '21321', '1321446', '', '', '', '', '', '', 'baner', 'pune', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '7879465', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-17 14:52:25', '', ''),
(5, 56, 41, 199, 200, 27, '411.54', '02-03-2021', '321', '1', '1254', 'test', 'Naveen K', 'tstff', 'the sai tech', 'exe per', 'new cycle', 'bucket', 'new pos', 'Emi vlaue', '4587', 'clm', 'tcl', 'exe', 'july', 'success', '78946', 'ptp', 'TRUE', 'resolved value', 'remark', 'visit remarl', 'rcl vlauw', 'staus tcl', 'tcl remarl', 'no', 'development', 'truq', 'sdfc', 'gfdsg', 'fsdfdsvs', '78465', 'gsdgsd', '8764', '646545', '54654', '1321', '3154545644', '21321', '1321446', '', '', '', '', '', '', 'baner', 'pune', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '7879465', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-17 14:52:25', '', ''),
(6, 56, 41, 199, 200, 27, '411.54', '02-03-2021', '1231321', '1', '1254', 'test', 'Naveen Kh', 'tstff', 'the sai tech', 'exe per', 'new cycle', 'bucket', 'new pos', 'Emi vlaue', '4587', 'clm', 'tcl', 'exe', 'july', 'success', '78946', 'ptp', 'TRUE', 'resolved value', 'remark', 'visit remarl', 'rcl vlauw', 'staus tcl', 'tcl remarl', 'no', 'development', 'truq', 'sdfc', 'gfdsg', 'fsdfdsvs', '78465', 'gsdgsd', '8764', '646545', '54654', '1321', '3154545644', '21321', '1321446', '', '', '', '', '', '', 'baner', 'pune', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '7879465', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-17 14:52:25', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ptx_import_data_files_temp`
--

CREATE TABLE `ptx_import_data_files_temp` (
  `id` int(11) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `hash` varchar(50) NOT NULL,
  `final_flag` tinyint(1) NOT NULL,
  `pincode` varchar(6) DEFAULT NULL,
  `allocation_date` varchar(255) NOT NULL,
  `loan_no` varchar(255) NOT NULL,
  `applicant_id` varchar(255) DEFAULT NULL,
  `product` varchar(255) DEFAULT NULL,
  `model` varchar(255) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `multiple_ac` varchar(255) NOT NULL,
  `sai_per` varchar(255) NOT NULL,
  `exe_per` varchar(255) NOT NULL,
  `cycle` varchar(255) NOT NULL,
  `bucket` varchar(255) NOT NULL,
  `pos` varchar(255) NOT NULL,
  `emi` varchar(255) NOT NULL,
  `penal_amount` varchar(255) NOT NULL,
  `clm` varchar(255) NOT NULL,
  `tcl` varchar(255) NOT NULL,
  `exe` varchar(255) NOT NULL,
  `last_month` varchar(255) NOT NULL,
  `omsai_status` varchar(255) NOT NULL,
  `expected_amt` varchar(255) NOT NULL,
  `ptp` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `resolved` varchar(255) NOT NULL,
  `exe_telecalling_remark` varchar(255) NOT NULL,
  `exe_visit_remark` varchar(255) NOT NULL,
  `tcl_ptp` varchar(255) NOT NULL,
  `tcl_status` varchar(255) NOT NULL,
  `tcl_remark` varchar(255) NOT NULL,
  `working_industry` varchar(255) NOT NULL,
  `ndustry_desc` varchar(255) NOT NULL,
  `paid_file` varchar(255) NOT NULL,
  `additional_no` varchar(255) NOT NULL,
  `tracing_feedback` varchar(255) NOT NULL,
  `res_no` varchar(255) NOT NULL,
  `off_no` varchar(255) NOT NULL,
  `mailing_no` varchar(255) NOT NULL,
  `rec_no` varchar(255) NOT NULL,
  `deposite_date` varchar(255) NOT NULL,
  `amt` varchar(255) NOT NULL,
  `penal` varchar(255) NOT NULL,
  `total` varchar(255) NOT NULL,
  `mode` varchar(255) NOT NULL,
  `month_1_status` varchar(255) NOT NULL,
  `month_2_status` varchar(255) NOT NULL,
  `month_3_status` varchar(255) NOT NULL,
  `month_4_status` varchar(255) NOT NULL,
  `month_5_status` varchar(255) NOT NULL,
  `last_pd_last` varchar(255) NOT NULL,
  `allocation_area` varchar(255) NOT NULL,
  `res_add` varchar(255) NOT NULL,
  `off_Add` varchar(255) NOT NULL,
  `property_add` varchar(255) NOT NULL,
  `disb_date` varchar(255) NOT NULL,
  `emi_start_date` varchar(255) NOT NULL,
  `emi_end_date` varchar(255) NOT NULL,
  `tenure` varchar(255) NOT NULL,
  `disb_amt` varchar(255) NOT NULL,
  `ref` varchar(255) NOT NULL,
  `ref_2` varchar(255) NOT NULL,
  `father_name` varchar(255) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `cibil` varchar(255) NOT NULL,
  `cibil_feedback` varchar(255) NOT NULL,
  `npa_stageid` varchar(255) NOT NULL,
  `bounce_reason` varchar(255) NOT NULL,
  `opening_dpd` varchar(255) NOT NULL,
  `current_dpd` varchar(255) NOT NULL,
  `last_month_agency` varchar(255) NOT NULL,
  `reg_no` varchar(255) NOT NULL,
  `engine_num` varchar(255) NOT NULL,
  `chasisnum` varchar(255) NOT NULL,
  `total_emi_due` varchar(255) NOT NULL,
  `cbc` varchar(255) NOT NULL,
  `lpp` varchar(255) NOT NULL,
  `total_due` varchar(255) NOT NULL,
  `no_of_bounces` varchar(255) NOT NULL,
  `edi` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `latitude_longitude` varchar(255) NOT NULL,
  `covid_flag` varchar(255) NOT NULL,
  `trails` varchar(255) NOT NULL,
  `psr_cat` varchar(255) NOT NULL,
  `priority` varchar(255) NOT NULL,
  `mannual_bkt` varchar(255) NOT NULL,
  `dpd` varchar(255) NOT NULL,
  `dpd_group` varchar(255) NOT NULL,
  `book_flag` varchar(255) NOT NULL,
  `reference_one_mobile_no` varchar(20) DEFAULT NULL,
  `reference_two_mobile_no` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ptx_import_data_files_temp`
--

INSERT INTO `ptx_import_data_files_temp` (`id`, `bank_id`, `product_id`, `hash`, `final_flag`, `pincode`, `allocation_date`, `loan_no`, `applicant_id`, `product`, `model`, `customer_name`, `multiple_ac`, `sai_per`, `exe_per`, `cycle`, `bucket`, `pos`, `emi`, `penal_amount`, `clm`, `tcl`, `exe`, `last_month`, `omsai_status`, `expected_amt`, `ptp`, `status`, `resolved`, `exe_telecalling_remark`, `exe_visit_remark`, `tcl_ptp`, `tcl_status`, `tcl_remark`, `working_industry`, `ndustry_desc`, `paid_file`, `additional_no`, `tracing_feedback`, `res_no`, `off_no`, `mailing_no`, `rec_no`, `deposite_date`, `amt`, `penal`, `total`, `mode`, `month_1_status`, `month_2_status`, `month_3_status`, `month_4_status`, `month_5_status`, `last_pd_last`, `allocation_area`, `res_add`, `off_Add`, `property_add`, `disb_date`, `emi_start_date`, `emi_end_date`, `tenure`, `disb_amt`, `ref`, `ref_2`, `father_name`, `dob`, `cibil`, `cibil_feedback`, `npa_stageid`, `bounce_reason`, `opening_dpd`, `current_dpd`, `last_month_agency`, `reg_no`, `engine_num`, `chasisnum`, `total_emi_due`, `cbc`, `lpp`, `total_due`, `no_of_bounces`, `edi`, `category`, `latitude_longitude`, `covid_flag`, `trails`, `psr_cat`, `priority`, `mannual_bkt`, `dpd`, `dpd_group`, `book_flag`, `reference_one_mobile_no`, `reference_two_mobile_no`) VALUES
(1, 19, 16, '617053cf7a1c89d8dc1cf935f9e4c56b', 1, '200', '02-03-2021', '1231321', '1', '1254', 'test', 'Naveen Kh', 'tstff', 'the sai tech', 'exe per', 'new cycle', 'bucket', 'new pos', 'Emi vlaue', '4587', 'clm', 'tcl', 'exe', 'july', 'success', '78946', 'ptp', 'TRUE', 'resolved value', 'remark', 'visit remarl', 'rcl vlauw', 'staus tcl', 'tcl remarl', 'no', 'development', 'truq', 'sdfc', 'gfdsg', 'fsdfdsvs', '78465', 'gsdgsd', '8764', '646545', '54654', '1321', '3154545644', '21321', '1321446', '', '', '', '', '', '', 'baner 200', 'pune', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '7879465', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2, 19, 16, '617053cf7a1c89d8dc1cf935f9e4c56b', 1, '2000', '02-03-2021', '321', '1', '1254', 'test', 'Naveen K', 'tstff', 'the sai tech', 'exe per', 'new cycle', 'bucket', 'new pos', 'Emi vlaue', '4587', 'clm', 'tcl', 'exe', 'july', 'success', '78946', 'ptp', 'TRUE', 'resolved value', 'remark', 'visit remarl', 'rcl vlauw', 'staus tcl', 'tcl remarl', 'no', 'development', 'truq', 'sdfc', 'gfdsg', 'fsdfdsvs', '78465', 'gsdgsd', '8764', '646545', '54654', '1321', '3154545644', '21321', '1321446', '', '', '', '', '', '', 'baner 2000', 'pune', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '7879465', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ptx_lead_allocations`
--

CREATE TABLE `ptx_lead_allocations` (
  `id` int(10) UNSIGNED NOT NULL,
  `bank_id` int(11) NOT NULL,
  `total_lead` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `new_lead` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_lead` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isDeleted` tinyint(4) NOT NULL COMMENT '1 = Deleted, 0 = Not Deleted',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ptx_lead_history`
--

CREATE TABLE `ptx_lead_history` (
  `id` int(11) NOT NULL,
  `loan_no` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `telecaller_id` int(11) DEFAULT NULL,
  `field_executive_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ptx_loan_comment`
--

CREATE TABLE `ptx_loan_comment` (
  `id` int(11) NOT NULL,
  `loan_no` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ptx_loan_comment`
--

INSERT INTO `ptx_loan_comment` (`id`, `loan_no`, `comment`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
(1, '31411749', 'demo', '2021-07-05 11:32:29', 1, '2021-07-05 06:32:34', 1, '2021-07-05 06:32:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ptx_manage_salaries`
--

CREATE TABLE `ptx_manage_salaries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ptx_menu`
--

CREATE TABLE `ptx_menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `lable` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class` varchar(199) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menu_id` varchar(199) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent` int(11) NOT NULL,
  `Status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = De-active',
  `isDeleted` tinyint(1) NOT NULL COMMENT '1 = Deleted, 0 = Not Deleted',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ptx_menu`
--

INSERT INTO `ptx_menu` (`id`, `lable`, `link`, `icon`, `class`, `menu_id`, `parent`, `Status`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'Dashboard', 'home', 'bx bx-home-circle', 'dashboard', 'Dashboard', 0, 1, 0, '2021-02-16 04:45:00', '2021-02-16 04:45:00'),
(2, 'Master', '#', 'bx bx-user-circle', 'master', 'Master', 0, 0, 0, '2021-03-04 10:30:00', '2021-03-04 10:30:00'),
(3, 'Daily Entry', '#', 'bx bxs-watch', 'daily_entry ', 'Daily_Entry', 0, 1, 0, '2021-03-04 10:49:36', '2021-03-04 10:49:36'),
(4, 'Uploads', '#', 'bx bx-import\r\n', 'upload', 'Upload', 0, 1, 0, '2021-03-04 10:55:13', '2021-03-04 10:55:13'),
(5, 'HRMs', '#', 'bx bx-building', 'hrm', 'Hrm_Master', 0, 1, 0, '2021-03-04 11:01:36', '2021-03-04 11:01:36'),
(6, 'Account', '#', 'bx bx-money', 'account', 'Account', 0, 1, 0, '2021-03-04 11:10:54', '2021-03-04 11:10:54'),
(7, 'Roles & Permission', 'department', 'bx bx-building-house', 'dept', 'Roles_And_Permission', 0, 1, 0, '2021-02-16 04:50:42', '2021-02-16 04:50:42'),
(8, 'Settings', 'setting', 'bx bx-cog', 'setting', 'Settings', 0, 1, 0, '2021-02-16 04:51:33', '2021-02-16 04:51:33'),
(10, 'Company/Branch', '#', 'bx bx-building', 'company_branch', 'Company_Branch_Master', 2, 1, 0, '2021-03-04 10:32:37', '2021-03-04 10:32:37'),
(12, 'Agent/Telecaller', 'agetel/user', 'bx bx-user-circle', 'agent_telecaller', 'Agent_Telecaller_Master', 2, 1, 0, '2021-03-04 10:37:14', '2021-03-04 10:37:14'),
(13, 'Bank', '#', 'bx bx-building', 'bank', 'Bank_Master', 2, 1, 0, '2021-03-04 10:37:14', '2021-03-04 10:37:14'),
(14, 'Backet', '#', 'bx bx-building', 'backet', 'Backet_Master', 2, 1, 0, '2021-03-04 10:42:44', '2021-03-04 10:42:44'),
(15, 'Recollection Roles', '#', 'bx bx-user-circle', 'recollection_role', 'Recollection_role_Master', 2, 1, 0, '2021-03-04 10:42:44', '2021-03-04 10:42:44'),
(16, 'Employee', 'system/user', 'bx bx-user', 'sys_user', 'Employee_Master', 2, 1, 0, '2021-02-16 04:48:07', '2021-02-16 04:48:07'),
(18, 'Cash Collection', '#', 'bx bx-money', 'cash_collection', 'Cash_Collection', 3, 1, 0, '2021-03-04 10:49:36', '2021-03-04 10:49:36'),
(45, 'Backup Database', 'bkp_database', 'bx bx-import\r\n', 'bkp_data', 'Backup_Database', 100, 1, 0, '2021-02-16 09:19:32', '2021-02-16 09:19:32'),
(55, 'Agent/Telecaller Master', 'agetel/user', 'bx bx-user-circle', 'var_user', 'Agetele_Master', 100, 0, 0, '2021-02-22 10:21:45', '2021-02-22 04:50:42'),
(57, 'Bank Master Upload', '#', 'bx bx-upload', 'bank_master', 'Bank_Master', 4, 1, 0, '2021-03-04 10:55:13', '2021-03-04 10:55:13'),
(58, 'Company Master Upload', '#', 'bx bx-upload', 'company_master', 'Company_Master', 4, 1, 0, '2021-03-04 10:58:22', '2021-03-04 10:58:22'),
(60, 'Manage Salary', '#', 'bx bx-money', 'manage_salary', 'Manage_Salary', 5, 1, 0, '2021-03-04 11:01:36', '2021-03-04 11:01:36'),
(61, 'Make Payment', '#', 'bx bx-money', 'make_payment', 'Make_Payment', 5, 1, 0, '2021-03-04 11:04:12', '2021-03-04 11:04:12'),
(62, 'Generate Page Slip', '#', 'bx bx-paper', 'generate_slip', 'Generate Slip', 5, 1, 0, '2021-03-04 11:05:40', '2021-03-04 11:05:40'),
(66, 'Generate Invoice', '#', 'bx-bx-pdf', 'generate_invoice', 'Generate_Invoice', 6, 1, 0, '2021-03-04 11:16:44', '2021-03-04 11:16:44'),
(67, 'Payment Received', '#', 'bx bx-money', 'payment_received', 'Payment_Received', 6, 1, 0, '2021-03-04 11:18:58', '2021-03-04 11:18:58');

-- --------------------------------------------------------

--
-- Table structure for table `ptx_menus`
--

CREATE TABLE `ptx_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Status` tinyint(4) NOT NULL COMMENT '1 = Active, 0 = De-active',
  `isDeleted` tinyint(4) NOT NULL COMMENT '1 = Deleted, 0 = Not Deleted',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ptx_migrations`
--

CREATE TABLE `ptx_migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ptx_migrations`
--

INSERT INTO `ptx_migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2021_02_09_100410_create_departments_table', 2),
(6, '2021_02_10_053335_create_designations_table', 3),
(7, '2021_02_10_092348_create_user_roles_table', 4),
(8, '2021_02_10_092348_create_user_role_table', 5),
(9, '2021_02_10_093917_create_user_roles_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `ptx_otp_verification`
--

CREATE TABLE `ptx_otp_verification` (
  `id` int(11) UNSIGNED NOT NULL,
  `mobile` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `otp` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isVerified` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ptx_otp_verification`
--

INSERT INTO `ptx_otp_verification` (`id`, `mobile`, `otp`, `isVerified`, `created_at`, `updated_at`) VALUES
(1, '9623002651', '74059', NULL, '2021-04-10 13:06:38', '2021-04-10 13:06:38'),
(2, '9096683298', '36788', NULL, '2021-08-09 11:35:14', '2021-08-09 11:35:14'),
(3, '9096683298', '94302', NULL, '2021-08-09 11:35:41', '2021-08-09 11:35:41'),
(4, '9637430435', '69444', NULL, '2021-08-18 12:59:09', '2021-08-18 12:59:09'),
(5, '9326947594', '20789', NULL, '2021-08-20 11:02:15', '2021-08-20 11:02:15'),
(6, '9326947594', '52336', NULL, '2021-08-20 11:04:07', '2021-08-20 11:04:07'),
(7, '9326947594', '27759', NULL, '2021-08-20 11:04:56', '2021-08-20 11:04:56'),
(8, '8010904077', '45098', NULL, '2021-08-23 11:54:01', '2021-08-23 11:54:01'),
(9, '9373736425', '02988', NULL, '2021-09-03 11:12:41', '2021-09-03 11:12:41'),
(10, '8830705470', '86972', NULL, '2021-10-29 05:30:40', '2021-10-29 05:30:40');

-- --------------------------------------------------------

--
-- Table structure for table `ptx_products`
--

CREATE TABLE `ptx_products` (
  `id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT '0' COMMENT '1 = Deleted, 0 = Not Deleted',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ptx_products`
--

INSERT INTO `ptx_products` (`id`, `product_name`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'Auto', 1, '2021-04-03 12:51:45', '2021-06-25 06:31:17'),
(2, 'Auto 8', 1, '2021-04-03 12:51:51', '2021-04-03 07:53:14'),
(3, 'Car', 1, '2021-04-07 06:45:05', '2021-06-25 06:31:56'),
(4, 'PL', 1, '2021-06-24 07:48:02', '2021-06-25 06:32:28'),
(5, 'CTG', 1, '2021-06-25 10:24:23', '2021-06-25 06:32:06'),
(6, 'HOME LOAN', 1, '2021-06-25 10:33:11', '2021-06-25 06:32:16'),
(7, 'PERSNOL LOAN', 1, '2021-06-25 10:56:38', '2021-06-25 05:57:14'),
(8, 'PERSONAL LOAN', 1, '2021-06-25 10:57:52', '2021-06-25 06:32:19'),
(9, 'CONSUMER LOAN', 1, '2021-06-25 10:58:45', '2021-06-25 06:32:10'),
(10, 'BUSINESS LOAN', 1, '2021-06-25 11:00:43', '2021-06-25 06:31:52'),
(11, 'COMMERCIAL VEHICLE', 1, '2021-06-25 11:02:04', '2021-06-25 06:32:01'),
(12, 'AGERICLUTER LOAN', 1, '2021-06-25 11:03:04', '2021-06-25 06:31:14'),
(13, 'AUTO LOAN', 1, '2021-06-25 11:04:55', '2021-06-25 06:31:35'),
(14, 'EDUCATION LOAN', 1, '2021-06-25 11:05:46', '2021-06-25 06:32:13'),
(15, 'THREE WHEELER', 1, '2021-06-25 11:10:51', '2021-06-25 06:32:31'),
(16, 'Business Loan', 0, '2021-06-25 11:30:17', '2021-08-25 06:19:50'),
(17, 'AL', 1, '2021-07-08 04:43:20', '2021-07-07 23:45:04'),
(18, 'CV', 0, '2021-07-08 08:05:16', '2021-07-08 05:18:32'),
(19, 'PL', 0, '2021-07-08 08:05:37', '2021-07-08 05:42:23'),
(20, 'TW', 0, '2021-07-08 10:28:13', '2021-07-08 10:28:13'),
(21, 'THREE WHELER', 0, '2021-07-08 10:31:12', '2021-07-08 10:31:12'),
(22, 'Education Loan', 0, '2021-07-08 12:02:55', '2021-07-08 07:03:04'),
(23, 'Agriculture Loan', 0, '2021-07-08 12:07:13', '2021-07-27 12:45:19'),
(24, 'Commercial Loan', 0, '2021-07-10 05:22:56', '2021-07-10 05:22:56'),
(25, 'FLIP', 0, '2021-07-12 09:48:38', '2021-07-12 09:48:38'),
(26, 'LACR', 0, '2021-07-12 10:34:29', '2021-07-12 10:34:29'),
(27, 'X CELL', 0, '2021-07-12 10:44:41', '2021-07-12 10:44:41'),
(28, 'CVL/AFH', 0, '2021-07-12 10:47:35', '2021-07-12 10:47:35'),
(29, 'U C', 0, '2021-07-12 11:01:10', '2021-07-12 11:01:10'),
(30, 'MSE & OTHER ADVANCE', 0, '2021-07-12 11:01:38', '2021-07-12 11:01:38'),
(31, 'MSME LOAN', 0, '2021-07-12 11:01:56', '2021-07-12 11:01:56'),
(32, 'BLPL', 0, '2021-07-12 11:02:17', '2021-07-12 11:02:17'),
(33, 'HL', 0, '2021-07-12 11:37:13', '2021-07-12 11:37:13'),
(34, 'CREDIT CARD', 0, '2021-07-12 11:39:22', '2021-07-12 11:39:22'),
(35, 'New Test', 1, '2021-07-27 17:51:43', '2021-09-08 05:27:52'),
(36, 'TWO WHELER', 0, '2021-08-25 11:23:44', '2021-08-25 11:23:44'),
(37, '4 WHEELER', 0, '2021-08-25 13:29:33', '2021-08-25 13:29:33'),
(38, 'PAYTM FINANCE', 0, '2021-08-28 06:37:54', '2021-08-28 06:37:54'),
(39, 'PLCL', 0, '2021-09-08 11:41:35', '2021-09-08 11:41:35'),
(40, 'PLTW', 0, '2021-09-08 11:43:23', '2021-09-08 11:43:23'),
(41, 'CRED', 0, '2021-10-14 09:46:27', '2021-10-14 09:46:27'),
(42, 'BILPL', 0, '2021-10-14 09:47:15', '2021-10-14 09:47:15'),
(43, 'AUTO/PL', 0, '2021-10-14 09:48:05', '2021-10-14 09:48:05'),
(44, '3 WHL', 0, '2021-10-14 09:49:08', '2021-10-14 09:49:08'),
(45, 'BL', 0, '2021-10-14 09:49:42', '2021-10-14 09:49:42'),
(46, 'W OFF', 0, '2021-10-14 09:50:19', '2021-10-14 09:50:19'),
(47, 'CV,AUTO', 0, '2021-10-14 09:50:51', '2021-10-14 09:50:51'),
(48, 'BL,LACR,FLIP', 0, '2021-10-14 09:51:44', '2021-10-14 09:51:44');

-- --------------------------------------------------------

--
-- Table structure for table `ptx_product_categories`
--

CREATE TABLE `ptx_product_categories` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT '0' COMMENT '1-Deleted, 0-Not Deletec',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ptx_product_categories`
--

INSERT INTO `ptx_product_categories` (`id`, `product_id`, `category_name`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 3, 'Bicyle', 1, '2021-04-07 06:46:02', '2021-04-07 02:34:56'),
(2, 3, 'BMW', 0, '2021-04-07 07:51:46', '2021-04-07 07:51:46'),
(3, 1, 'Three Wheeler', 1, '2021-04-07 07:52:13', '2021-06-25 05:52:01'),
(4, 4, 'Personal Loan', 0, '2021-06-24 07:49:47', '2021-06-24 07:49:47'),
(5, 6, 'Home Loan', 1, '2021-06-25 10:34:41', '2021-06-25 05:49:38'),
(6, 4, 'ICIC Bank', 0, '2021-06-25 10:48:56', '2021-06-25 10:48:56'),
(7, 6, 'Axis', 0, '2021-06-25 10:49:28', '2021-06-25 10:49:28'),
(8, 1, 'Bajaj', 0, '2021-06-25 10:50:13', '2021-06-25 10:50:13'),
(9, 5, 'HDFC', 0, '2021-06-25 10:55:11', '2021-06-25 10:55:11'),
(10, 1, 'HDFC', 0, '2021-06-25 10:55:38', '2021-06-25 10:55:38'),
(11, 8, 'IDFC', 0, '2021-06-25 10:58:12', '2021-06-25 10:58:12'),
(12, 9, 'IDFC', 0, '2021-06-25 10:58:55', '2021-06-25 10:58:55'),
(13, 8, 'FULLERTON  INDIA CREDIT LIMITED', 0, '2021-06-25 10:59:45', '2021-06-25 10:59:45'),
(14, 10, 'PAYTM', 0, '2021-06-25 11:00:53', '2021-06-25 11:00:53'),
(15, 11, 'YES BANK', 0, '2021-06-25 11:02:12', '2021-06-25 11:02:12'),
(16, 6, 'YES BANK', 0, '2021-06-25 11:02:29', '2021-06-25 11:02:29'),
(17, 12, 'IDBI', 0, '2021-06-25 11:03:16', '2021-06-25 11:03:16'),
(18, 13, 'IDBI BANK', 0, '2021-06-25 11:05:23', '2021-06-25 11:05:23'),
(19, 14, 'IDBI BANK', 0, '2021-06-25 11:05:59', '2021-06-25 11:05:59'),
(20, 10, 'IDBI BANK', 0, '2021-06-25 11:06:30', '2021-06-25 11:06:30'),
(21, 6, 'PUNJAB NATIONAL BANK', 0, '2021-06-25 11:07:03', '2021-06-25 11:07:03'),
(22, 13, 'KOTAK MAHINDRA PRIME', 0, '2021-06-25 11:09:46', '2021-06-25 11:09:46'),
(23, 15, 'BAJAJ FINANCE', 0, '2021-06-25 11:11:01', '2021-06-25 11:11:01'),
(24, 13, 'DAIMLAR FINANCE', 0, '2021-06-25 11:11:25', '2021-06-25 11:11:25'),
(25, 13, 'BMW FINANCE', 0, '2021-06-25 11:11:43', '2021-06-25 11:11:43'),
(26, 8, 'TATA CAPITAL', 0, '2021-06-25 11:12:06', '2021-06-25 11:12:06'),
(27, 4, 'Personal Loan', 0, '2021-06-25 11:25:55', '2021-06-25 11:25:55'),
(28, 4, 'Personal Loan', 0, '2021-06-25 11:26:47', '2021-06-25 11:26:47'),
(29, 1, 'Auto Loan', 0, '2021-06-25 11:28:52', '2021-06-25 11:28:52'),
(30, 16, 'Business Loan1', 0, '2021-06-25 11:30:30', '2021-07-10 01:24:56'),
(31, 4, 'PERSNOL LOAN', 0, '2021-06-25 11:40:03', '2021-06-25 11:40:03'),
(32, 13, 'AUTO LOAN', 0, '2021-06-25 11:48:03', '2021-06-25 11:48:03'),
(33, 6, 'HOME-LOAN', 0, '2021-06-29 12:33:36', '2021-06-29 12:33:36'),
(34, 16, 'Sample', 1, '2021-07-08 04:42:55', '2021-07-07 23:43:06'),
(35, 17, 'Sample', 0, '2021-07-08 04:44:46', '2021-07-08 04:44:46'),
(36, 18, 'Abc', 0, '2021-07-08 08:05:16', '2021-07-08 08:05:16'),
(37, 19, 'PERSONAL LOAN', 0, '2021-07-08 08:05:37', '2021-07-08 08:05:37'),
(38, 5, 'CTG', 0, '2021-07-08 10:27:57', '2021-07-08 10:27:57'),
(39, 20, 'TW', 0, '2021-07-08 10:28:13', '2021-07-08 10:28:13'),
(40, 9, 'CONSUMER LOAN', 0, '2021-07-08 10:28:51', '2021-07-08 10:28:51'),
(41, 12, 'AGERICLUTER LOAN', 0, '2021-07-08 10:29:58', '2021-07-08 10:29:58'),
(42, 14, 'EDUCATION LOAN', 0, '2021-07-08 10:30:17', '2021-07-08 10:30:17'),
(43, 10, 'BUSINESS LOAN', 0, '2021-07-08 10:30:36', '2021-07-08 10:30:36'),
(44, 21, 'THREE WHELER', 0, '2021-07-08 10:31:12', '2021-07-08 10:31:12'),
(45, 1, 'AUTO', 0, '2021-07-08 10:32:17', '2021-07-08 10:32:17'),
(46, 21, 'Education Loan', 0, '2021-07-08 10:58:25', '2021-07-08 10:58:25'),
(47, 22, 'Education Loan', 0, '2021-07-08 12:02:55', '2021-07-08 07:03:17'),
(48, 23, 'Agriculture Loan', 0, '2021-07-08 12:07:13', '2021-07-08 12:07:13'),
(49, 5, '1', 0, '2021-07-08 12:08:32', '2021-07-08 12:08:32'),
(50, 5, 'CTG1', 0, '2021-07-08 12:09:29', '2021-07-08 12:09:29'),
(51, 24, 'Commercial Loan', 0, '2021-07-10 05:22:56', '2021-07-10 05:22:56'),
(52, 19, 'Sample', 1, '2021-07-10 06:07:21', '2021-07-10 01:07:38'),
(53, 25, 'FLIP', 0, '2021-07-12 09:48:38', '2021-07-12 09:48:38'),
(54, 26, 'LACR', 0, '2021-07-12 10:34:29', '2021-07-12 10:34:29'),
(55, 27, 'X CELL', 0, '2021-07-12 10:44:41', '2021-07-12 10:44:41'),
(56, 28, 'CVL/AFH', 0, '2021-07-12 10:47:35', '2021-07-12 10:47:35'),
(57, 29, 'U C', 0, '2021-07-12 11:01:10', '2021-07-12 11:01:10'),
(58, 30, 'MSE & OTHER ADVANCE', 0, '2021-07-12 11:01:38', '2021-07-12 11:01:38'),
(59, 31, 'MSME LOAN', 0, '2021-07-12 11:01:56', '2021-07-12 11:01:56'),
(60, 32, 'BLPL', 0, '2021-07-12 11:02:17', '2021-07-12 11:02:17'),
(61, 33, 'HL', 0, '2021-07-12 11:37:13', '2021-07-12 11:37:13'),
(62, 34, 'CREDIT CARD', 0, '2021-07-12 11:39:22', '2021-07-12 11:39:22'),
(63, 35, 'Test', 0, '2021-07-27 17:51:43', '2021-07-27 17:51:43'),
(64, 35, 'AUTO', 0, '2021-08-20 09:22:22', '2021-08-20 09:22:22'),
(65, 36, 'TWO WHELER', 0, '2021-08-25 11:23:44', '2021-08-25 11:23:44'),
(66, 37, '4 WHEELER', 0, '2021-08-25 13:29:33', '2021-08-25 13:29:33'),
(67, 38, 'PAYTM FINANCE', 0, '2021-08-28 06:37:54', '2021-08-28 06:37:54'),
(68, 39, 'PLCL', 0, '2021-09-08 11:41:35', '2021-09-08 11:41:35'),
(69, 40, 'PLTW', 0, '2021-09-08 11:43:23', '2021-09-08 11:43:23'),
(70, 41, 'CRED', 0, '2021-10-14 09:46:27', '2021-10-14 09:46:27'),
(71, 42, 'BILPL', 0, '2021-10-14 09:47:15', '2021-10-14 09:47:15'),
(72, 43, 'AUTO/PL', 0, '2021-10-14 09:48:05', '2021-10-14 09:48:05'),
(73, 44, '3 WHL', 0, '2021-10-14 09:49:08', '2021-10-14 09:49:08'),
(74, 45, 'BL', 0, '2021-10-14 09:49:42', '2021-10-14 09:49:42'),
(75, 46, 'W OFF', 0, '2021-10-14 09:50:19', '2021-10-14 09:50:19'),
(76, 47, 'CV,AUTO', 0, '2021-10-14 09:50:51', '2021-10-14 09:50:51'),
(77, 48, 'BL,LACR,FLIP', 0, '2021-10-14 09:51:44', '2021-10-14 09:51:44');

-- --------------------------------------------------------

--
-- Table structure for table `ptx_product_short_names`
--

CREATE TABLE `ptx_product_short_names` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bank_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `short_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT '0' COMMENT '0=Deleted, 1=Not Deleted',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ptx_product_short_names`
--

INSERT INTO `ptx_product_short_names` (`id`, `bank_id`, `product_id`, `short_name`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 'AUT', 0, '2021-04-03 12:57:48', '2021-04-03 12:57:48'),
(2, 46, 16, '11', 0, '2021-07-12 11:31:10', '2021-07-28 23:52:58'),
(3, 46, 18, '22', 0, '2021-07-12 11:31:10', '2021-07-28 23:52:58'),
(4, 46, 19, '32', 0, '2021-07-12 11:31:10', '2021-07-28 23:52:58'),
(5, 46, 20, '33', 0, '2021-07-12 11:31:10', '2021-07-28 23:52:58'),
(6, 46, 21, '', 0, '2021-07-12 11:31:10', '2021-07-12 11:31:10'),
(7, 46, 22, '', 0, '2021-07-12 11:31:10', '2021-07-12 11:31:10'),
(8, 46, 23, '', 0, '2021-07-12 11:31:10', '2021-07-12 11:31:10'),
(9, 46, 24, '', 0, '2021-07-12 11:31:10', '2021-07-12 11:31:10'),
(10, 46, 25, '', 0, '2021-07-12 11:31:10', '2021-07-12 11:31:10'),
(11, 46, 26, '', 0, '2021-07-12 11:31:10', '2021-07-12 11:31:10'),
(12, 46, 27, '', 0, '2021-07-12 11:31:10', '2021-07-12 11:31:10'),
(13, 46, 28, '', 0, '2021-07-12 11:31:10', '2021-07-12 11:31:10'),
(14, 46, 29, '', 0, '2021-07-12 11:31:10', '2021-07-12 11:31:10'),
(15, 46, 30, '', 0, '2021-07-12 11:31:10', '2021-07-12 11:31:10'),
(16, 46, 31, '', 0, '2021-07-12 11:31:10', '2021-07-12 11:31:10'),
(17, 46, 32, '', 0, '2021-07-12 11:31:10', '2021-07-12 11:31:10'),
(18, 47, 16, 'test1', 0, '2021-07-29 01:20:31', '2021-07-28 20:21:01'),
(19, 47, 18, 't2', 0, '2021-07-29 01:20:31', '2021-07-28 20:21:01'),
(20, 47, 19, '', 0, '2021-07-29 01:20:31', '2021-07-29 01:20:31'),
(21, 47, 20, '', 0, '2021-07-29 01:20:31', '2021-07-29 01:20:31'),
(22, 47, 21, '', 0, '2021-07-29 01:20:31', '2021-07-29 01:20:31'),
(23, 47, 22, '', 0, '2021-07-29 01:20:31', '2021-07-29 01:20:31'),
(24, 47, 23, '', 0, '2021-07-29 01:20:31', '2021-07-29 01:20:31'),
(25, 47, 24, '', 0, '2021-07-29 01:20:31', '2021-07-29 01:20:31'),
(26, 47, 25, '', 0, '2021-07-29 01:20:31', '2021-07-29 01:20:31'),
(27, 47, 26, '', 0, '2021-07-29 01:20:31', '2021-07-29 01:20:31'),
(28, 47, 27, '', 0, '2021-07-29 01:20:31', '2021-07-29 01:20:31'),
(29, 47, 28, '', 0, '2021-07-29 01:20:31', '2021-07-29 01:20:31'),
(30, 47, 29, '', 0, '2021-07-29 01:20:31', '2021-07-29 01:20:31'),
(31, 47, 30, '', 0, '2021-07-29 01:20:31', '2021-07-29 01:20:31'),
(32, 47, 31, '', 0, '2021-07-29 01:20:31', '2021-07-29 01:20:31'),
(33, 47, 32, '', 0, '2021-07-29 01:20:31', '2021-07-29 01:20:31'),
(34, 47, 33, '', 0, '2021-07-29 01:20:31', '2021-07-29 01:20:31'),
(35, 47, 34, '', 0, '2021-07-29 01:20:31', '2021-07-29 01:20:31'),
(36, 47, 35, 'testing1', 0, '2021-07-29 01:20:31', '2021-07-28 20:21:01'),
(37, 12, 16, 'test', 0, '2021-07-29 00:46:46', '2021-07-29 00:46:46'),
(38, 10, 16, 'BLL', 0, '2021-07-30 06:18:11', '2021-07-30 06:18:11'),
(39, 10, 18, 'CV', 0, '2021-07-30 06:18:53', '2021-07-31 09:45:32'),
(40, 16, 16, 'BL', 0, '2021-07-31 09:46:25', '2021-07-31 09:46:25'),
(41, 48, 16, 'BL', 0, '2021-08-19 12:56:11', '2021-08-25 06:20:11'),
(42, 48, 18, 'CUMMERSIAL VEHIKAL', 0, '2021-08-19 12:56:11', '2021-08-19 12:56:11'),
(43, 48, 19, 'PERSNOL LOAD', 0, '2021-08-19 12:56:11', '2021-08-19 12:56:11'),
(44, 48, 20, 'TWO WHEELOR LOAN', 0, '2021-08-19 12:56:11', '2021-08-19 12:56:11'),
(45, 48, 21, '', 0, '2021-08-19 12:56:11', '2021-08-19 12:56:11'),
(46, 48, 22, '', 0, '2021-08-19 12:56:11', '2021-08-19 12:56:11'),
(47, 48, 23, '', 0, '2021-08-19 12:56:11', '2021-08-19 12:56:11'),
(48, 48, 24, '', 0, '2021-08-19 12:56:11', '2021-08-19 12:56:11'),
(49, 48, 25, '', 0, '2021-08-19 12:56:11', '2021-08-19 12:56:11'),
(50, 48, 26, '', 0, '2021-08-19 12:56:11', '2021-08-19 12:56:11'),
(51, 48, 27, '', 0, '2021-08-19 12:56:11', '2021-08-19 12:56:11'),
(52, 48, 28, '', 0, '2021-08-19 12:56:11', '2021-08-19 12:56:11'),
(53, 48, 29, '', 0, '2021-08-19 12:56:11', '2021-08-19 12:56:11'),
(54, 48, 30, '', 0, '2021-08-19 12:56:11', '2021-08-19 12:56:11'),
(55, 48, 31, '', 0, '2021-08-19 12:56:11', '2021-08-19 12:56:11'),
(56, 48, 32, '', 0, '2021-08-19 12:56:11', '2021-08-19 12:56:11'),
(57, 48, 33, '', 0, '2021-08-19 12:56:11', '2021-08-19 12:56:11'),
(58, 48, 34, '', 0, '2021-08-19 12:56:11', '2021-08-19 12:56:11'),
(59, 48, 35, '', 0, '2021-08-19 12:56:11', '2021-08-19 12:56:11'),
(60, 49, 16, '', 0, '2021-08-20 09:13:44', '2021-08-20 09:13:44'),
(61, 49, 18, 'COMMERCIAL VEHICLE', 0, '2021-08-20 09:13:44', '2021-08-20 04:15:13'),
(62, 49, 19, '', 0, '2021-08-20 09:13:44', '2021-08-20 09:13:44'),
(63, 49, 20, '', 0, '2021-08-20 09:13:44', '2021-08-20 09:13:44'),
(64, 49, 21, '', 0, '2021-08-20 09:13:44', '2021-08-20 09:13:44'),
(65, 49, 22, '', 0, '2021-08-20 09:13:44', '2021-08-20 09:13:44'),
(66, 49, 23, '', 0, '2021-08-20 09:13:44', '2021-08-20 09:13:44'),
(67, 49, 24, '', 0, '2021-08-20 09:13:44', '2021-08-20 09:13:44'),
(68, 49, 25, '', 0, '2021-08-20 09:13:44', '2021-08-20 09:13:44'),
(69, 49, 26, '', 0, '2021-08-20 09:13:44', '2021-08-20 09:13:44'),
(70, 49, 27, '', 0, '2021-08-20 09:13:44', '2021-08-20 09:13:44'),
(71, 49, 28, '', 0, '2021-08-20 09:13:44', '2021-08-20 09:13:44'),
(72, 49, 29, '', 0, '2021-08-20 09:13:44', '2021-08-20 09:13:44'),
(73, 49, 30, '', 0, '2021-08-20 09:13:44', '2021-08-20 09:13:44'),
(74, 49, 31, '', 0, '2021-08-20 09:13:44', '2021-08-20 09:13:44'),
(75, 49, 32, '', 0, '2021-08-20 09:13:44', '2021-08-20 09:13:44'),
(76, 49, 33, '', 0, '2021-08-20 09:13:44', '2021-08-20 09:13:44'),
(77, 49, 34, '', 0, '2021-08-20 09:13:44', '2021-08-20 09:13:44'),
(78, 49, 35, '', 0, '2021-08-20 09:13:44', '2021-08-20 09:13:44'),
(79, 50, 16, '', 0, '2021-08-20 09:21:56', '2021-08-20 09:21:56'),
(80, 50, 18, '', 0, '2021-08-20 09:21:56', '2021-08-20 09:21:56'),
(81, 50, 19, '', 0, '2021-08-20 09:21:56', '2021-08-20 09:21:56'),
(82, 50, 20, '', 0, '2021-08-20 09:21:56', '2021-08-20 09:21:56'),
(83, 50, 21, '', 0, '2021-08-20 09:21:56', '2021-08-20 09:21:56'),
(84, 50, 22, '', 0, '2021-08-20 09:21:56', '2021-08-20 09:21:56'),
(85, 50, 23, '', 0, '2021-08-20 09:21:56', '2021-08-20 09:21:56'),
(86, 50, 24, '', 0, '2021-08-20 09:21:56', '2021-08-20 09:21:56'),
(87, 50, 25, '', 0, '2021-08-20 09:21:56', '2021-08-20 09:21:56'),
(88, 50, 26, '', 0, '2021-08-20 09:21:56', '2021-08-20 09:21:56'),
(89, 50, 27, '', 0, '2021-08-20 09:21:56', '2021-08-20 09:21:56'),
(90, 50, 28, '', 0, '2021-08-20 09:21:56', '2021-08-20 09:21:56'),
(91, 50, 29, '', 0, '2021-08-20 09:21:56', '2021-08-20 09:21:56'),
(92, 50, 30, '', 0, '2021-08-20 09:21:56', '2021-08-20 09:21:56'),
(93, 50, 31, '', 0, '2021-08-20 09:21:56', '2021-08-20 09:21:56'),
(94, 50, 32, '', 0, '2021-08-20 09:21:56', '2021-08-20 09:21:56'),
(95, 50, 33, '', 0, '2021-08-20 09:21:56', '2021-08-20 09:21:56'),
(96, 50, 34, '', 0, '2021-08-20 09:21:56', '2021-08-20 09:21:56'),
(97, 50, 35, '4 WHEELOR LOAN', 0, '2021-08-20 09:21:56', '2021-08-20 09:21:56'),
(98, 51, 16, '', 0, '2021-08-21 06:13:45', '2021-08-21 06:13:45'),
(99, 51, 18, '', 0, '2021-08-21 06:13:45', '2021-08-21 06:13:45'),
(100, 51, 19, '', 0, '2021-08-21 06:13:45', '2021-08-21 06:13:45'),
(101, 51, 20, '', 0, '2021-08-21 06:13:45', '2021-08-21 06:13:45'),
(102, 51, 21, '', 0, '2021-08-21 06:13:45', '2021-08-21 06:13:45'),
(103, 51, 22, '', 0, '2021-08-21 06:13:45', '2021-08-21 06:13:45'),
(104, 51, 23, '', 0, '2021-08-21 06:13:45', '2021-08-21 06:13:45'),
(105, 51, 24, '', 0, '2021-08-21 06:13:45', '2021-08-21 06:13:45'),
(106, 51, 25, '', 0, '2021-08-21 06:13:45', '2021-08-21 06:13:45'),
(107, 51, 26, '', 0, '2021-08-21 06:13:45', '2021-08-21 06:13:45'),
(108, 51, 27, '', 0, '2021-08-21 06:13:45', '2021-08-21 06:13:45'),
(109, 51, 28, '', 0, '2021-08-21 06:13:45', '2021-08-21 06:13:45'),
(110, 51, 29, '', 0, '2021-08-21 06:13:45', '2021-08-21 06:13:45'),
(111, 51, 30, '', 0, '2021-08-21 06:13:45', '2021-08-21 06:13:45'),
(112, 51, 31, '', 0, '2021-08-21 06:13:45', '2021-08-21 06:13:45'),
(113, 51, 32, '', 0, '2021-08-21 06:13:45', '2021-08-21 06:13:45'),
(114, 51, 33, 'HOUSING LOAN', 0, '2021-08-21 06:13:45', '2021-08-21 06:13:45'),
(115, 51, 34, '', 0, '2021-08-21 06:13:45', '2021-08-21 06:13:45'),
(116, 51, 35, '', 0, '2021-08-21 06:13:45', '2021-08-21 06:13:45'),
(117, 52, 16, 'BL', 0, '2021-08-21 06:41:14', '2021-08-25 06:25:17'),
(118, 52, 18, '4 WHEELOR LOAN', 0, '2021-08-21 06:41:14', '2021-08-21 06:41:14'),
(119, 52, 19, 'PERSNOL LOAN', 0, '2021-08-21 06:41:14', '2021-08-21 06:41:14'),
(120, 52, 20, '', 0, '2021-08-21 06:41:14', '2021-08-21 06:41:14'),
(121, 52, 21, '', 0, '2021-08-21 06:41:14', '2021-08-21 06:41:14'),
(122, 52, 22, '', 0, '2021-08-21 06:41:14', '2021-08-21 06:41:14'),
(123, 52, 23, '', 0, '2021-08-21 06:41:14', '2021-08-21 06:41:14'),
(124, 52, 24, '', 0, '2021-08-21 06:41:14', '2021-08-21 06:41:14'),
(125, 52, 25, '', 0, '2021-08-21 06:41:14', '2021-08-21 06:41:14'),
(126, 52, 26, '', 0, '2021-08-21 06:41:14', '2021-08-21 06:41:14'),
(127, 52, 27, '', 0, '2021-08-21 06:41:14', '2021-08-21 06:41:14'),
(128, 52, 28, '', 0, '2021-08-21 06:41:14', '2021-08-21 06:41:14'),
(129, 52, 29, '', 0, '2021-08-21 06:41:14', '2021-08-21 06:41:14'),
(130, 52, 30, '', 0, '2021-08-21 06:41:14', '2021-08-21 06:41:14'),
(131, 52, 31, '', 0, '2021-08-21 06:41:14', '2021-08-21 06:41:14'),
(132, 52, 32, '', 0, '2021-08-21 06:41:14', '2021-08-21 06:41:14'),
(133, 52, 33, '', 0, '2021-08-21 06:41:14', '2021-08-21 06:41:14'),
(134, 52, 34, '', 0, '2021-08-21 06:41:14', '2021-08-21 06:41:14'),
(135, 52, 35, '', 0, '2021-08-21 06:41:14', '2021-08-21 06:41:14'),
(136, 53, 16, 'BUSINESS LOAN', 0, '2021-08-21 06:54:40', '2021-08-21 06:54:40'),
(137, 53, 18, '', 0, '2021-08-21 06:54:40', '2021-08-21 06:54:40'),
(138, 53, 19, '', 0, '2021-08-21 06:54:40', '2021-08-21 06:54:40'),
(139, 53, 20, 'AUTO LOAN', 0, '2021-08-21 06:54:40', '2021-08-21 06:54:40'),
(140, 53, 21, '', 0, '2021-08-21 06:54:40', '2021-08-21 06:54:40'),
(141, 53, 22, 'EDUCATION LOAN', 0, '2021-08-21 06:54:40', '2021-08-21 06:54:40'),
(142, 53, 23, 'AGRICULTURE LOAN', 0, '2021-08-21 06:54:40', '2021-08-21 06:54:40'),
(143, 53, 24, '', 0, '2021-08-21 06:54:40', '2021-08-21 06:54:40'),
(144, 53, 25, '', 0, '2021-08-21 06:54:40', '2021-08-21 06:54:40'),
(145, 53, 26, '', 0, '2021-08-21 06:54:40', '2021-08-21 06:54:40'),
(146, 53, 27, '', 0, '2021-08-21 06:54:40', '2021-08-21 06:54:40'),
(147, 53, 28, '', 0, '2021-08-21 06:54:40', '2021-08-21 06:54:40'),
(148, 53, 29, '', 0, '2021-08-21 06:54:40', '2021-08-21 06:54:40'),
(149, 53, 30, '', 0, '2021-08-21 06:54:40', '2021-08-21 06:54:40'),
(150, 53, 31, 'MSME LOAN', 0, '2021-08-21 06:54:40', '2021-08-21 06:54:40'),
(151, 53, 32, '', 0, '2021-08-21 06:54:40', '2021-08-21 06:54:40'),
(152, 53, 33, '', 0, '2021-08-21 06:54:40', '2021-08-21 06:54:40'),
(153, 53, 34, '', 0, '2021-08-21 06:54:40', '2021-08-21 06:54:40'),
(154, 53, 35, '', 0, '2021-08-21 06:54:40', '2021-08-21 06:54:40'),
(155, 54, 16, '', 0, '2021-08-21 07:04:33', '2021-08-21 07:04:33'),
(156, 54, 18, '4 WHEELOR LOAN', 0, '2021-08-21 07:04:33', '2021-08-21 07:04:33'),
(157, 54, 19, '', 0, '2021-08-21 07:04:33', '2021-08-21 07:04:33'),
(158, 54, 20, '', 0, '2021-08-21 07:04:33', '2021-08-21 07:04:33'),
(159, 54, 21, '', 0, '2021-08-21 07:04:33', '2021-08-21 07:04:33'),
(160, 54, 22, '', 0, '2021-08-21 07:04:33', '2021-08-21 07:04:33'),
(161, 54, 23, '', 0, '2021-08-21 07:04:33', '2021-08-21 07:04:33'),
(162, 54, 24, '', 0, '2021-08-21 07:04:33', '2021-08-21 07:04:33'),
(163, 54, 25, '', 0, '2021-08-21 07:04:33', '2021-08-21 07:04:33'),
(164, 54, 26, '', 0, '2021-08-21 07:04:33', '2021-08-21 07:04:33'),
(165, 54, 27, '', 0, '2021-08-21 07:04:33', '2021-08-21 07:04:33'),
(166, 54, 28, '', 0, '2021-08-21 07:04:33', '2021-08-21 07:04:33'),
(167, 54, 29, '', 0, '2021-08-21 07:04:33', '2021-08-21 07:04:33'),
(168, 54, 30, '', 0, '2021-08-21 07:04:33', '2021-08-21 07:04:33'),
(169, 54, 31, '', 0, '2021-08-21 07:04:33', '2021-08-21 07:04:33'),
(170, 54, 32, '', 0, '2021-08-21 07:04:33', '2021-08-21 07:04:33'),
(171, 54, 33, '', 0, '2021-08-21 07:04:33', '2021-08-21 07:04:33'),
(172, 54, 34, '', 0, '2021-08-21 07:04:33', '2021-08-21 07:04:33'),
(173, 54, 35, '', 0, '2021-08-21 07:04:33', '2021-08-21 07:04:33'),
(174, 55, 16, '', 0, '2021-08-21 07:12:53', '2021-08-21 07:12:53'),
(175, 55, 18, '', 0, '2021-08-21 07:12:53', '2021-08-21 07:12:53'),
(176, 55, 19, '', 0, '2021-08-21 07:12:53', '2021-08-21 07:12:53'),
(177, 55, 20, '', 0, '2021-08-21 07:12:53', '2021-08-21 07:12:53'),
(178, 55, 21, '', 0, '2021-08-21 07:12:53', '2021-08-21 07:12:53'),
(179, 55, 22, '', 0, '2021-08-21 07:12:53', '2021-08-21 07:12:53'),
(180, 55, 23, '', 0, '2021-08-21 07:12:53', '2021-08-21 07:12:53'),
(181, 55, 24, '', 0, '2021-08-21 07:12:53', '2021-08-21 07:12:53'),
(182, 55, 25, '', 0, '2021-08-21 07:12:53', '2021-08-21 07:12:53'),
(183, 55, 26, '', 0, '2021-08-21 07:12:53', '2021-08-21 07:12:53'),
(184, 55, 27, '', 0, '2021-08-21 07:12:53', '2021-08-21 07:12:53'),
(185, 55, 28, '', 0, '2021-08-21 07:12:53', '2021-08-21 07:12:53'),
(186, 55, 29, '', 0, '2021-08-21 07:12:53', '2021-08-21 07:12:53'),
(187, 55, 30, '', 0, '2021-08-21 07:12:53', '2021-08-21 07:12:53'),
(188, 55, 31, '', 0, '2021-08-21 07:12:53', '2021-08-21 07:12:53'),
(189, 55, 32, '', 0, '2021-08-21 07:12:53', '2021-08-21 07:12:53'),
(190, 55, 33, '', 0, '2021-08-21 07:12:53', '2021-08-21 07:12:53'),
(191, 55, 34, 'CREDIT CARD', 0, '2021-08-21 07:12:53', '2021-08-21 07:12:53'),
(192, 55, 35, '', 0, '2021-08-21 07:12:53', '2021-08-21 07:12:53'),
(193, 56, 16, 'BUSINESS LOAN', 0, '2021-08-24 11:24:30', '2021-08-24 11:24:30'),
(194, 56, 18, '', 0, '2021-08-24 11:24:30', '2021-08-24 11:24:30'),
(195, 56, 19, '', 0, '2021-08-24 11:24:30', '2021-08-24 11:24:30'),
(196, 56, 20, '', 0, '2021-08-24 11:24:30', '2021-08-24 11:24:30'),
(197, 56, 21, '', 0, '2021-08-24 11:24:30', '2021-08-24 11:24:30'),
(198, 56, 22, '', 0, '2021-08-24 11:24:30', '2021-08-24 11:24:30'),
(199, 56, 23, '', 0, '2021-08-24 11:24:30', '2021-08-24 11:24:30'),
(200, 56, 24, '', 0, '2021-08-24 11:24:30', '2021-08-24 11:24:30'),
(201, 56, 25, '', 0, '2021-08-24 11:24:30', '2021-08-24 11:24:30'),
(202, 56, 26, '', 0, '2021-08-24 11:24:30', '2021-08-24 11:24:30'),
(203, 56, 27, '', 0, '2021-08-24 11:24:30', '2021-08-24 11:24:30'),
(204, 56, 28, '', 0, '2021-08-24 11:24:30', '2021-08-24 11:24:30'),
(205, 56, 29, '', 0, '2021-08-24 11:24:30', '2021-08-24 11:24:30'),
(206, 56, 30, '', 0, '2021-08-24 11:24:30', '2021-08-24 11:24:30'),
(207, 56, 31, '', 0, '2021-08-24 11:24:30', '2021-08-24 11:24:30'),
(208, 56, 32, '', 0, '2021-08-24 11:24:30', '2021-08-24 11:24:30'),
(209, 56, 33, 'HOME LOAN', 0, '2021-08-24 11:24:30', '2021-08-24 11:24:30'),
(210, 56, 34, '', 0, '2021-08-24 11:24:30', '2021-08-24 11:24:30'),
(211, 56, 35, '', 0, '2021-08-24 11:24:30', '2021-08-24 11:24:30'),
(212, 57, 16, '', 0, '2021-08-24 11:27:19', '2021-08-24 11:27:19'),
(213, 57, 18, '', 0, '2021-08-24 11:27:19', '2021-08-24 11:27:19'),
(214, 57, 19, '', 0, '2021-08-24 11:27:19', '2021-08-24 11:27:19'),
(215, 57, 20, '', 0, '2021-08-24 11:27:19', '2021-08-24 11:27:19'),
(216, 57, 21, '', 0, '2021-08-24 11:27:19', '2021-08-24 11:27:19'),
(217, 57, 22, '', 0, '2021-08-24 11:27:19', '2021-08-24 11:27:19'),
(218, 57, 23, '', 0, '2021-08-24 11:27:19', '2021-08-24 11:27:19'),
(219, 57, 24, '', 0, '2021-08-24 11:27:19', '2021-08-24 11:27:19'),
(220, 57, 25, '', 0, '2021-08-24 11:27:19', '2021-08-24 11:27:19'),
(221, 57, 26, '', 0, '2021-08-24 11:27:19', '2021-08-24 11:27:19'),
(222, 57, 27, '', 0, '2021-08-24 11:27:19', '2021-08-24 11:27:19'),
(223, 57, 28, '', 0, '2021-08-24 11:27:19', '2021-08-24 11:27:19'),
(224, 57, 29, '', 0, '2021-08-24 11:27:19', '2021-08-24 11:27:19'),
(225, 57, 30, '', 0, '2021-08-24 11:27:19', '2021-08-24 11:27:19'),
(226, 57, 31, '', 0, '2021-08-24 11:27:19', '2021-08-24 11:27:19'),
(227, 57, 32, '', 0, '2021-08-24 11:27:19', '2021-08-24 11:27:19'),
(228, 57, 33, '', 0, '2021-08-24 11:27:19', '2021-08-24 11:27:19'),
(229, 57, 34, '', 0, '2021-08-24 11:27:19', '2021-08-24 11:27:19'),
(230, 57, 35, 'OTHER PRODUCT', 0, '2021-08-24 11:27:19', '2021-08-24 11:27:19'),
(231, 58, 16, '', 0, '2021-08-24 11:29:52', '2021-08-24 11:29:52'),
(232, 58, 18, '', 0, '2021-08-24 11:29:52', '2021-08-24 11:29:52'),
(233, 58, 19, '', 0, '2021-08-24 11:29:52', '2021-08-24 11:29:52'),
(234, 58, 20, '', 0, '2021-08-24 11:29:52', '2021-08-24 11:29:52'),
(235, 58, 21, '', 0, '2021-08-24 11:29:52', '2021-08-24 11:29:52'),
(236, 58, 22, '', 0, '2021-08-24 11:29:52', '2021-08-24 11:29:52'),
(237, 58, 23, '', 0, '2021-08-24 11:29:52', '2021-08-24 11:29:52'),
(238, 58, 24, '', 0, '2021-08-24 11:29:52', '2021-08-24 11:29:52'),
(239, 58, 25, '', 0, '2021-08-24 11:29:52', '2021-08-24 11:29:52'),
(240, 58, 26, '', 0, '2021-08-24 11:29:52', '2021-08-24 11:29:52'),
(241, 58, 27, '', 0, '2021-08-24 11:29:52', '2021-08-24 11:29:52'),
(242, 58, 28, '', 0, '2021-08-24 11:29:52', '2021-08-24 11:29:52'),
(243, 58, 29, '', 0, '2021-08-24 11:29:52', '2021-08-24 11:29:52'),
(244, 58, 30, '', 0, '2021-08-24 11:29:52', '2021-08-24 11:29:52'),
(245, 58, 31, '', 0, '2021-08-24 11:29:52', '2021-08-24 11:29:52'),
(246, 58, 32, '', 0, '2021-08-24 11:29:52', '2021-08-24 11:29:52'),
(247, 58, 33, '', 0, '2021-08-24 11:29:52', '2021-08-24 11:29:52'),
(248, 58, 34, '', 0, '2021-08-24 11:29:52', '2021-08-24 11:29:52'),
(249, 58, 35, 'OTHER PRODUCT', 0, '2021-08-24 11:29:52', '2021-08-24 11:29:52'),
(250, 59, 16, 'BUSINESS LOAN', 0, '2021-08-24 12:11:46', '2021-08-24 12:11:46'),
(251, 59, 18, '', 0, '2021-08-24 12:11:46', '2021-08-24 12:11:46'),
(252, 59, 19, '', 0, '2021-08-24 12:11:46', '2021-08-24 12:11:46'),
(253, 59, 20, '', 0, '2021-08-24 12:11:46', '2021-08-24 12:11:46'),
(254, 59, 21, '', 0, '2021-08-24 12:11:46', '2021-08-24 12:11:46'),
(255, 59, 22, '', 0, '2021-08-24 12:11:46', '2021-08-24 12:11:46'),
(256, 59, 23, '', 0, '2021-08-24 12:11:46', '2021-08-24 12:11:46'),
(257, 59, 24, '', 0, '2021-08-24 12:11:46', '2021-08-24 12:11:46'),
(258, 59, 25, '', 0, '2021-08-24 12:11:46', '2021-08-24 12:11:46'),
(259, 59, 26, '', 0, '2021-08-24 12:11:46', '2021-08-24 12:11:46'),
(260, 59, 27, '', 0, '2021-08-24 12:11:46', '2021-08-24 12:11:46'),
(261, 59, 28, '', 0, '2021-08-24 12:11:46', '2021-08-24 12:11:46'),
(262, 59, 29, '', 0, '2021-08-24 12:11:46', '2021-08-24 12:11:46'),
(263, 59, 30, '', 0, '2021-08-24 12:11:46', '2021-08-24 12:11:46'),
(264, 59, 31, '', 0, '2021-08-24 12:11:46', '2021-08-24 12:11:46'),
(265, 59, 32, '', 0, '2021-08-24 12:11:46', '2021-08-24 12:11:46'),
(266, 59, 33, '', 0, '2021-08-24 12:11:46', '2021-08-24 12:11:46'),
(267, 59, 34, '', 0, '2021-08-24 12:11:46', '2021-08-24 12:11:46'),
(268, 59, 35, '', 0, '2021-08-24 12:11:46', '2021-08-24 12:11:46'),
(269, 60, 16, '', 0, '2021-08-25 11:03:56', '2021-08-25 11:03:56'),
(270, 60, 18, '', 0, '2021-08-25 11:03:56', '2021-08-25 11:03:56'),
(271, 60, 19, '', 0, '2021-08-25 11:03:56', '2021-08-25 11:03:56'),
(272, 60, 20, '', 0, '2021-08-25 11:03:56', '2021-08-25 11:03:56'),
(273, 60, 21, '', 0, '2021-08-25 11:03:56', '2021-08-25 11:03:56'),
(274, 60, 22, '', 0, '2021-08-25 11:03:56', '2021-08-25 11:03:56'),
(275, 60, 23, '', 0, '2021-08-25 11:03:56', '2021-08-25 11:03:56'),
(276, 60, 24, '', 0, '2021-08-25 11:03:56', '2021-08-25 11:03:56'),
(277, 60, 25, '', 0, '2021-08-25 11:03:56', '2021-08-25 11:03:56'),
(278, 60, 26, '', 0, '2021-08-25 11:03:56', '2021-08-25 11:03:56'),
(279, 60, 27, '', 0, '2021-08-25 11:03:56', '2021-08-25 11:03:56'),
(280, 60, 28, '', 0, '2021-08-25 11:03:56', '2021-08-25 11:03:56'),
(281, 60, 29, '', 0, '2021-08-25 11:03:56', '2021-08-25 11:03:56'),
(282, 60, 30, '', 0, '2021-08-25 11:03:56', '2021-08-25 11:03:56'),
(283, 60, 31, '', 0, '2021-08-25 11:03:56', '2021-08-25 11:03:56'),
(284, 60, 32, '', 0, '2021-08-25 11:03:56', '2021-08-25 11:03:56'),
(285, 60, 33, '', 0, '2021-08-25 11:03:56', '2021-08-25 11:03:56'),
(286, 60, 34, '', 0, '2021-08-25 11:03:56', '2021-08-25 11:03:56'),
(287, 60, 35, '', 0, '2021-08-25 11:03:56', '2021-08-25 11:03:56'),
(288, 61, 16, 'BUSINESS LOAN', 0, '2021-08-25 13:35:01', '2021-08-25 13:35:01'),
(289, 61, 18, '', 0, '2021-08-25 13:35:01', '2021-08-25 13:35:01'),
(290, 61, 19, 'PERSNOL LOAN', 0, '2021-08-25 13:35:01', '2021-08-25 13:35:01'),
(291, 61, 20, '', 0, '2021-08-25 13:35:01', '2021-08-25 13:35:01'),
(292, 61, 21, '', 0, '2021-08-25 13:35:01', '2021-08-25 13:35:01'),
(293, 61, 22, '', 0, '2021-08-25 13:35:01', '2021-08-25 13:35:01'),
(294, 61, 23, '', 0, '2021-08-25 13:35:01', '2021-08-25 13:35:01'),
(295, 61, 24, '', 0, '2021-08-25 13:35:01', '2021-08-25 13:35:01'),
(296, 61, 25, '', 0, '2021-08-25 13:35:01', '2021-08-25 13:35:01'),
(297, 61, 26, '', 0, '2021-08-25 13:35:01', '2021-08-25 13:35:01'),
(298, 61, 27, '', 0, '2021-08-25 13:35:01', '2021-08-25 13:35:01'),
(299, 61, 28, '', 0, '2021-08-25 13:35:01', '2021-08-25 13:35:01'),
(300, 61, 29, '', 0, '2021-08-25 13:35:01', '2021-08-25 13:35:01'),
(301, 61, 30, '', 0, '2021-08-25 13:35:01', '2021-08-25 13:35:01'),
(302, 61, 31, '', 0, '2021-08-25 13:35:01', '2021-08-25 13:35:01'),
(303, 61, 32, '', 0, '2021-08-25 13:35:01', '2021-08-25 13:35:01'),
(304, 61, 33, '', 0, '2021-08-25 13:35:01', '2021-08-25 13:35:01'),
(305, 61, 34, '', 0, '2021-08-25 13:35:01', '2021-08-25 13:35:01'),
(306, 61, 35, '', 0, '2021-08-25 13:35:01', '2021-08-25 13:35:01'),
(307, 61, 36, '', 0, '2021-08-25 13:35:01', '2021-08-25 13:35:01'),
(308, 61, 37, '', 0, '2021-08-25 13:35:01', '2021-08-25 13:35:01'),
(309, 62, 16, '', 0, '2021-08-25 13:39:00', '2021-08-25 13:39:00'),
(310, 62, 18, '', 0, '2021-08-25 13:39:00', '2021-08-25 13:39:00'),
(311, 62, 19, '', 0, '2021-08-25 13:39:00', '2021-08-25 13:39:00'),
(312, 62, 20, '', 0, '2021-08-25 13:39:00', '2021-08-25 13:39:00'),
(313, 62, 21, '', 0, '2021-08-25 13:39:00', '2021-08-25 13:39:00'),
(314, 62, 22, '', 0, '2021-08-25 13:39:00', '2021-08-25 13:39:00'),
(315, 62, 23, '', 0, '2021-08-25 13:39:00', '2021-08-25 13:39:00'),
(316, 62, 24, '', 0, '2021-08-25 13:39:00', '2021-08-25 13:39:00'),
(317, 62, 25, '', 0, '2021-08-25 13:39:00', '2021-08-25 13:39:00'),
(318, 62, 26, '', 0, '2021-08-25 13:39:00', '2021-08-25 13:39:00'),
(319, 62, 27, '', 0, '2021-08-25 13:39:00', '2021-08-25 13:39:00'),
(320, 62, 28, '', 0, '2021-08-25 13:39:00', '2021-08-25 13:39:00'),
(321, 62, 29, '', 0, '2021-08-25 13:39:00', '2021-08-25 13:39:00'),
(322, 62, 30, '', 0, '2021-08-25 13:39:00', '2021-08-25 13:39:00'),
(323, 62, 31, '', 0, '2021-08-25 13:39:00', '2021-08-25 13:39:00'),
(324, 62, 32, '', 0, '2021-08-25 13:39:00', '2021-08-25 13:39:00'),
(325, 62, 33, '', 0, '2021-08-25 13:39:00', '2021-08-25 13:39:00'),
(326, 62, 34, '', 0, '2021-08-25 13:39:00', '2021-08-25 13:39:00'),
(327, 62, 35, '', 0, '2021-08-25 13:39:00', '2021-08-25 13:39:00'),
(328, 62, 36, '', 0, '2021-08-25 13:39:00', '2021-08-25 13:39:00'),
(329, 62, 37, '', 0, '2021-08-25 13:39:00', '2021-08-25 13:39:00'),
(330, 63, 16, '', 0, '2021-08-25 13:40:50', '2021-08-25 13:40:50'),
(331, 63, 18, '', 0, '2021-08-25 13:40:50', '2021-08-25 13:40:50'),
(332, 63, 19, '', 0, '2021-08-25 13:40:50', '2021-08-25 13:40:50'),
(333, 63, 20, '', 0, '2021-08-25 13:40:50', '2021-08-25 13:40:50'),
(334, 63, 21, '', 0, '2021-08-25 13:40:50', '2021-08-25 13:40:50'),
(335, 63, 22, '', 0, '2021-08-25 13:40:50', '2021-08-25 13:40:50'),
(336, 63, 23, '', 0, '2021-08-25 13:40:50', '2021-08-25 13:40:50'),
(337, 63, 24, '', 0, '2021-08-25 13:40:50', '2021-08-25 13:40:50'),
(338, 63, 25, '', 0, '2021-08-25 13:40:50', '2021-08-25 13:40:50'),
(339, 63, 26, '', 0, '2021-08-25 13:40:50', '2021-08-25 13:40:50'),
(340, 63, 27, '', 0, '2021-08-25 13:40:50', '2021-08-25 13:40:50'),
(341, 63, 28, '', 0, '2021-08-25 13:40:50', '2021-08-25 13:40:50'),
(342, 63, 29, '', 0, '2021-08-25 13:40:50', '2021-08-25 13:40:50'),
(343, 63, 30, '', 0, '2021-08-25 13:40:50', '2021-08-25 13:40:50'),
(344, 63, 31, '', 0, '2021-08-25 13:40:50', '2021-08-25 13:40:50'),
(345, 63, 32, '', 0, '2021-08-25 13:40:50', '2021-08-25 13:40:50'),
(346, 63, 33, '', 0, '2021-08-25 13:40:50', '2021-08-25 13:40:50'),
(347, 63, 34, '', 0, '2021-08-25 13:40:50', '2021-08-25 13:40:50'),
(348, 63, 35, '', 0, '2021-08-25 13:40:50', '2021-08-25 13:40:50'),
(349, 63, 36, '', 0, '2021-08-25 13:40:50', '2021-08-25 13:40:50'),
(350, 63, 37, '', 0, '2021-08-25 13:40:50', '2021-08-25 13:40:50'),
(351, 64, 16, '', 0, '2021-08-26 04:51:48', '2021-08-26 04:51:48'),
(352, 64, 18, '', 0, '2021-08-26 04:51:48', '2021-08-26 04:51:48'),
(353, 64, 19, '', 0, '2021-08-26 04:51:48', '2021-08-26 04:51:48'),
(354, 64, 20, '', 0, '2021-08-26 04:51:48', '2021-08-26 04:51:48'),
(355, 64, 21, '', 0, '2021-08-26 04:51:48', '2021-08-26 04:51:48'),
(356, 64, 22, '', 0, '2021-08-26 04:51:48', '2021-08-26 04:51:48'),
(357, 64, 23, '', 0, '2021-08-26 04:51:48', '2021-08-26 04:51:48'),
(358, 64, 24, '', 0, '2021-08-26 04:51:48', '2021-08-26 04:51:48'),
(359, 64, 25, '', 0, '2021-08-26 04:51:48', '2021-08-26 04:51:48'),
(360, 64, 26, '', 0, '2021-08-26 04:51:48', '2021-08-26 04:51:48'),
(361, 64, 27, '', 0, '2021-08-26 04:51:48', '2021-08-26 04:51:48'),
(362, 64, 28, '', 0, '2021-08-26 04:51:48', '2021-08-26 04:51:48'),
(363, 64, 29, '', 0, '2021-08-26 04:51:48', '2021-08-26 04:51:48'),
(364, 64, 30, '', 0, '2021-08-26 04:51:48', '2021-08-26 04:51:48'),
(365, 64, 31, '', 0, '2021-08-26 04:51:48', '2021-08-26 04:51:48'),
(366, 64, 32, '', 0, '2021-08-26 04:51:48', '2021-08-26 04:51:48'),
(367, 64, 33, '', 0, '2021-08-26 04:51:48', '2021-08-26 04:51:48'),
(368, 64, 34, '', 0, '2021-08-26 04:51:48', '2021-08-26 04:51:48'),
(369, 64, 35, '', 0, '2021-08-26 04:51:48', '2021-08-26 04:51:48'),
(370, 64, 36, 'TW', 0, '2021-08-26 04:51:48', '2021-08-26 04:51:48'),
(371, 64, 37, '', 0, '2021-08-26 04:51:48', '2021-08-26 04:51:48'),
(372, 65, 16, '', 0, '2021-08-28 12:22:09', '2021-08-28 12:22:09'),
(373, 65, 18, '', 0, '2021-08-28 12:22:09', '2021-08-28 12:22:09'),
(374, 65, 19, '', 0, '2021-08-28 12:22:09', '2021-08-28 12:22:09'),
(375, 65, 20, '', 0, '2021-08-28 12:22:09', '2021-08-28 12:22:09'),
(376, 65, 21, '', 0, '2021-08-28 12:22:09', '2021-08-28 12:22:09'),
(377, 65, 22, '', 0, '2021-08-28 12:22:09', '2021-08-28 12:22:09'),
(378, 65, 23, '', 0, '2021-08-28 12:22:09', '2021-08-28 12:22:09'),
(379, 65, 24, '', 0, '2021-08-28 12:22:09', '2021-08-28 12:22:09'),
(380, 65, 25, '', 0, '2021-08-28 12:22:10', '2021-08-28 12:22:10'),
(381, 65, 26, '', 0, '2021-08-28 12:22:10', '2021-08-28 12:22:10'),
(382, 65, 27, '', 0, '2021-08-28 12:22:10', '2021-08-28 12:22:10'),
(383, 65, 28, '', 0, '2021-08-28 12:22:10', '2021-08-28 12:22:10'),
(384, 65, 29, '', 0, '2021-08-28 12:22:10', '2021-08-28 12:22:10'),
(385, 65, 30, '', 0, '2021-08-28 12:22:10', '2021-08-28 12:22:10'),
(386, 65, 31, '', 0, '2021-08-28 12:22:10', '2021-08-28 12:22:10'),
(387, 65, 32, '', 0, '2021-08-28 12:22:10', '2021-08-28 12:22:10'),
(388, 65, 33, '', 0, '2021-08-28 12:22:10', '2021-08-28 12:22:10'),
(389, 65, 34, '', 0, '2021-08-28 12:22:10', '2021-08-28 12:22:10'),
(390, 65, 35, '', 0, '2021-08-28 12:22:10', '2021-08-28 12:22:10'),
(391, 65, 36, '', 0, '2021-08-28 12:22:10', '2021-08-28 12:22:10'),
(392, 65, 37, '', 0, '2021-08-28 12:22:10', '2021-08-28 12:22:10'),
(393, 65, 38, '', 0, '2021-08-28 12:22:10', '2021-08-28 12:22:10'),
(394, 66, 16, 'BL', 0, '2021-08-28 12:40:35', '2021-08-28 12:40:35'),
(395, 66, 18, '', 0, '2021-08-28 12:40:35', '2021-08-28 12:40:35'),
(396, 66, 19, '', 0, '2021-08-28 12:40:35', '2021-08-28 12:40:35'),
(397, 66, 20, '', 0, '2021-08-28 12:40:35', '2021-08-28 12:40:35'),
(398, 66, 21, '', 0, '2021-08-28 12:40:35', '2021-08-28 12:40:35'),
(399, 66, 22, '', 0, '2021-08-28 12:40:35', '2021-08-28 12:40:35'),
(400, 66, 23, '', 0, '2021-08-28 12:40:35', '2021-08-28 12:40:35'),
(401, 66, 24, '', 0, '2021-08-28 12:40:35', '2021-08-28 12:40:35'),
(402, 66, 25, '', 0, '2021-08-28 12:40:35', '2021-08-28 12:40:35'),
(403, 66, 26, '', 0, '2021-08-28 12:40:35', '2021-08-28 12:40:35'),
(404, 66, 27, '', 0, '2021-08-28 12:40:35', '2021-08-28 12:40:35'),
(405, 66, 28, '', 0, '2021-08-28 12:40:35', '2021-08-28 12:40:35'),
(406, 66, 29, '', 0, '2021-08-28 12:40:35', '2021-08-28 12:40:35'),
(407, 66, 30, '', 0, '2021-08-28 12:40:35', '2021-08-28 12:40:35'),
(408, 66, 31, '', 0, '2021-08-28 12:40:35', '2021-08-28 12:40:35'),
(409, 66, 32, '', 0, '2021-08-28 12:40:35', '2021-08-28 12:40:35'),
(410, 66, 33, '', 0, '2021-08-28 12:40:35', '2021-08-28 12:40:35'),
(411, 66, 34, '', 0, '2021-08-28 12:40:35', '2021-08-28 12:40:35'),
(412, 66, 35, '', 0, '2021-08-28 12:40:35', '2021-08-28 12:40:35'),
(413, 66, 36, '', 0, '2021-08-28 12:40:35', '2021-08-28 12:40:35'),
(414, 66, 37, '', 0, '2021-08-28 12:40:35', '2021-08-28 12:40:35'),
(415, 66, 38, '', 0, '2021-08-28 12:40:35', '2021-08-28 12:40:35'),
(416, 67, 16, '', 0, '2021-09-11 07:17:54', '2021-09-11 07:17:54'),
(417, 67, 18, '', 0, '2021-09-11 07:17:54', '2021-09-11 07:17:54'),
(418, 67, 19, '', 0, '2021-09-11 07:17:54', '2021-09-11 07:17:54'),
(419, 67, 20, '', 0, '2021-09-11 07:17:54', '2021-09-11 07:17:54'),
(420, 67, 21, '', 0, '2021-09-11 07:17:54', '2021-09-11 07:17:54'),
(421, 67, 22, '', 0, '2021-09-11 07:17:54', '2021-09-11 07:17:54'),
(422, 67, 23, '', 0, '2021-09-11 07:17:54', '2021-09-11 07:17:54'),
(423, 67, 24, '', 0, '2021-09-11 07:17:54', '2021-09-11 07:17:54'),
(424, 67, 25, '', 0, '2021-09-11 07:17:54', '2021-09-11 07:17:54'),
(425, 67, 26, '', 0, '2021-09-11 07:17:54', '2021-09-11 07:17:54'),
(426, 67, 27, '', 0, '2021-09-11 07:17:54', '2021-09-11 07:17:54'),
(427, 67, 28, '', 0, '2021-09-11 07:17:54', '2021-09-11 07:17:54'),
(428, 67, 29, '', 0, '2021-09-11 07:17:54', '2021-09-11 07:17:54'),
(429, 67, 30, '', 0, '2021-09-11 07:17:54', '2021-09-11 07:17:54'),
(430, 67, 31, '', 0, '2021-09-11 07:17:54', '2021-09-11 07:17:54'),
(431, 67, 32, '', 0, '2021-09-11 07:17:54', '2021-09-11 07:17:54'),
(432, 67, 33, '', 0, '2021-09-11 07:17:54', '2021-09-11 07:17:54'),
(433, 67, 34, '', 0, '2021-09-11 07:17:54', '2021-09-11 07:17:54'),
(434, 67, 36, '', 0, '2021-09-11 07:17:54', '2021-09-11 07:17:54'),
(435, 67, 37, '', 0, '2021-09-11 07:17:54', '2021-09-11 07:17:54'),
(436, 67, 38, '', 0, '2021-09-11 07:17:54', '2021-09-11 07:17:54'),
(437, 67, 39, '', 0, '2021-09-11 07:17:54', '2021-09-11 07:17:54'),
(438, 67, 40, '', 0, '2021-09-11 07:17:54', '2021-09-11 07:17:54');

-- --------------------------------------------------------

--
-- Table structure for table `ptx_pvrs`
--

CREATE TABLE `ptx_pvrs` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `pvr_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_letter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `letter_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_receipt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receipt_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_report` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `report_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `due_for_renewal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1 = Deleted, 0 = Not Deleted',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ptx_pvrs`
--

INSERT INTO `ptx_pvrs` (`id`, `user_id`, `pvr_status`, `date_of_letter`, `letter_img`, `date_of_receipt`, `receipt_img`, `date_of_report`, `report_img`, `due_for_renewal`, `remark`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 36, 'Certificate', '2021-02-24', 'letter/letter_210615060921.pdf', '2021-02-25', 'receipt/receipt_210615060921.pdf', '2021-03-22', 'report/report_210615060921.pdf', 'Not Due', '7620573273-Password-Anant@123', 0, NULL, '2021-09-08 05:11:53'),
(2, 38, 'Untouched', '2001-01-02', 'letter/letter_210408074648.pdf', '2001-01-02', 'receipt/receipt_210408074648.pdf', '2001-01-02', 'report/report_210408074648.pdf', 'Overdue', NULL, 0, NULL, '2021-04-08 02:53:24'),
(4, 1, 'Certificate', '2001-1-04', 'letter/letter_210408084854.pdf', '2006-11-16', 'receipt/receipt_210408084854.pdf', '2021-7-11', 'report/report_210408084854.pdf', 'Not Due', NULL, 0, NULL, NULL),
(6, 58, 'Letter', '2006-06-03', 'letter/letter_210412103239.pdf', '2006-06-03', 'receipt/receipt_210412103239.pdf', '2006-06-03', 'report/report_210412103239.pdf', 'Overdue', NULL, 0, NULL, '2021-07-30 06:15:29'),
(7, 32, 'Untouched', '2021-07-17', 'letter/letter_210617072104.doc', '2021-05-14', 'receipt/receipt_210519080935.doc', '2021-12-18', 'report/report_210519080935.doc', 'Not Due', 'Sample', 0, NULL, '2021-06-17 02:21:04'),
(8, 61, 'Certificate', '2021-02-18', 'letter/letter_210607093212.pdf', '2021-02-18', 'receipt/receipt_210607093212.pdf', '2021-02-18', 'report/report_210607093212.pdf', 'Not Due', '9922581111-Omsai@123', 0, NULL, '2021-09-06 04:22:23'),
(9, 137, 'Certificate', '2021-01-01', 'letter/letter_210609070547.pdf', '2021-02-15', 'receipt/receipt_210609070547.pdf', '2021-02-02', 'report/report_210609070547.pdf', 'Not Due', 'M-Ringing(9922131699-Omsai@123)', 0, NULL, '2021-09-07 05:52:43'),
(10, 147, 'Certificate', '2020-11-06', 'letter/letter_210610104018.pdf', '2020-11-06', 'receipt/receipt_210610104018.pdf', '2020-11-11', 'report/report_210610104018.pdf', 'Not Due', NULL, 0, NULL, NULL),
(11, 69, 'Certificate', '2021-02-18', 'letter/letter_210610104253.pdf', '2021-02-18', 'receipt/receipt_210610104253.pdf', '2021-02-20', 'report/report_210610104253.pdf', 'Not Due', 'POLICE STATION VISIT ON 20 TH FEB(9021619657-Omsai@1234)/On 10 th March will Do', 0, NULL, '2021-09-07 05:53:52'),
(12, 141, 'Certificate', '2021-02-18', 'letter/letter_210610104648.pdf', '2021-02-22', 'receipt/receipt_210610104648.pdf', '2021-02-25', 'report/report_210610104648.pdf', 'Not Due', 'PTP 20/ On 24th Procedure for police station-Light Bill issue(9552500372-Omsai@123)', 0, NULL, '2021-09-07 05:54:25'),
(13, 140, 'Certificate', '2021-02-23', 'letter/letter_210610104918.pdf', '2021-02-25', 'receipt/receipt_210610104918.pdf', '2021-03-12', 'report/report_210610104918.pdf', 'Not Due', 'On 25 th will Do(9850951525-Omsai@123)', 0, NULL, '2021-09-07 05:55:33'),
(14, 177, 'Certificate', '2020-11-06', 'letter/letter_210610105119.pdf', '2021-02-20', 'receipt/receipt_210610105119.pdf', '2021-03-17', 'report/report_210610105119.pdf', 'Not Due', '9850062481-Omsai@123', 0, NULL, '2021-09-07 08:07:13'),
(15, 199, 'Certificate', '2021-02-10', 'letter/letter_210610105342.pdf', '2021-02-10', 'receipt/receipt_210610105342.pdf', '2021-02-16', 'report/report_210610105342.pdf', 'Not Due', 'POLICE VERIFICATION DONE(8424846685-Omsai@123)', 0, NULL, '2021-09-07 08:08:15'),
(16, 202, 'Certificate', '2021-02-05', 'letter/letter_210610105629.pdf', '2021-02-05', 'receipt/receipt_210610105629.pdf', '2021-02-18', 'report/report_210610105629.pdf', 'Not Due', 'On 18 th Police station(9321817977-Omsai@123)', 0, NULL, '2021-09-07 08:10:16'),
(17, 155, 'Certificate', '2020-11-10', 'letter/letter_210610110019.pdf', '2020-11-13', 'receipt/receipt_210610110019.pdf', '2021-02-20', 'report/report_210610110019.pdf', 'Not Due', 'PIMPRI CHINCHWAD-Police Duty -22 ND-Message Share(8380804446-Omsai@123)', 0, NULL, '2021-09-07 08:10:57'),
(18, 222, 'Certificate', '2021-02-', 'letter/letter_210610110303.pdf', '2021-02-8', 'receipt/receipt_210610110303.pdf', '2021-02-18', 'report/report_210610110303.pdf', 'Not Due', 'M-NO REPLY -Ringing (MOTHER NO-8600513708-Omsai@123)', 0, NULL, '2021-09-07 08:11:42'),
(19, 201, 'Certificate', '2021-02-06', 'letter/letter_210610110459.pdf', '2021-02-06', 'receipt/receipt_210610110459.pdf', '2021-02-22', 'report/report_210610110459.pdf', 'Not Due', 'POLICE STATION VISIT ON 20 TH FEB(9076067250-Omsai@123)', 0, NULL, '2021-09-07 08:13:50'),
(20, 157, 'Certificate', '2020-11-06', 'letter/letter_210610110728.pdf', '2020-02-24', 'receipt/receipt_210610110728.pdf', '2020-02-24', 'report/report_210610110728.pdf', 'Not Due', 'POLICE STATION VISIT ON 22 ND FEB/On 24 th(9145279048-Omsai@123)', 0, NULL, '2021-09-07 08:19:31'),
(21, 152, 'Certificate', '2021-02-', 'letter/letter_210610111005.pdf', '2021-02-26', 'receipt/receipt_210610111005.pdf', '2021-03-8', 'report/report_210610111005.pdf', 'Not Due', 'OM SAI RAM,9922112824-Omsai@123', 0, NULL, '2021-09-07 08:20:25'),
(22, 165, 'Certificate', '2021-02-', 'letter/letter_210610111316.pdf', '2021-02-8', 'receipt/receipt_210610111316.pdf', '2021-03-12', 'report/report_210610111316.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 05:10:16'),
(23, 196, 'Certificate', '2021-2-26', 'letter/letter_210610111603.pdf', '2021-2-26', 'receipt/receipt_210610111603.pdf', '2021-3-01', 'report/report_210610111603.pdf', 'Not Due', NULL, 0, NULL, NULL),
(24, 219, 'Certificate', '2020-11-06', 'letter/letter_210610111911.pdf', '2020-11-06', 'receipt/receipt_210610111911.pdf', '2020-11-19', 'report/report_210610111911.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 05:10:49'),
(25, 136, 'Certificate', '2021-02-25', 'letter/letter_210610112136.pdf', '2021-02-25', 'receipt/receipt_210610112136.pdf', '2021-03-05', 'report/report_210610112136.pdf', 'Not Due', 'On 25 th will DO PVR PROCESS(7020737699-Omsai@123)', 0, NULL, '2021-09-07 08:21:16'),
(26, 145, 'Certificate', '2021-2-22', 'letter/letter_210610112500.pdf', '2021-2-23', 'receipt/receipt_210610112500.pdf', '2021-2-23', 'report/report_210610112500.pdf', 'Not Due', NULL, 0, NULL, NULL),
(27, 66, 'Certificate', '2021-02-16', 'letter/letter_210610112757.pdf', '2021-02-19', 'receipt/receipt_210610112757.pdf', '2021-03-01', 'report/report_210610112757.pdf', 'Not Due', 'Police Station Visit-3 Time,CALL On 26 th FEB/On 27th CB(7776065333-Password-Omsai@123)', 0, NULL, '2021-09-07 08:21:50'),
(28, 97, 'Certificate', '2021-02-18', 'letter/letter_210610113026.pdf', '2021-02-24', 'receipt/receipt_210610113026.pdf', '2021-03-11', 'report/report_210610113026.pdf', 'Not Due', 'Receipt procedure on 22 ND/On 24 th-M-Ringing/On 8 th ,On 11 th March will Do Process-9307848057-Omsai@123', 0, NULL, '2021-09-07 08:22:46'),
(29, 153, 'Certificate', '2021-02-24', 'letter/letter_210610113259.pdf', '2021-03-25', 'receipt/receipt_210610113259.pdf', '2021-04-03', 'report/report_210610113259.pdf', 'Not Due', 'On 25 th will Do/On 28 th will Do Process/Ringing-Ringing(letter head)2 Times -Police Station (Nigadi Police Station  Wife)/On 10 th March ,On 16 th will Do Process/On 23rd will Process/On 27 th will Do/M-will CB', 0, NULL, '2021-09-07 08:23:42'),
(30, 82, 'Certificate', '2021-03-06', 'letter/letter_210610113513.pdf', '2021-03-06', 'receipt/receipt_210610113513.pdf', '2021-03-11', 'report/report_210610113513.pdf', 'Not Due', 'On 26 th will Come/On 11 th March will go to Police Station(8888834938-Omsai@123)', 0, NULL, '2021-09-07 08:26:35'),
(31, 95, 'Certificate', '2020-11-20', 'letter/letter_210610113757.pdf', '2020-11-20', 'receipt/receipt_210610113757.pdf', '2020-12-10', 'report/report_210610113757.pdf', 'Not Due', 'OMSAI CREDIT -9359210670-Vikas@123/GROWTH -8999433987-Omsai@123', 0, NULL, '2021-09-07 08:27:12'),
(32, 139, 'Certificate', '2020-11-03', 'letter/letter_210610114257.pdf', '2020-11-15', 'receipt/receipt_210610114257.pdf', '2020-12-23', 'report/report_210610114257.pdf', 'Not Due', '5 TIMES VISIT TO CP/SP OFF(9657448448-Omsai@123)', 0, NULL, '2021-09-07 08:31:09'),
(33, 101, 'Certificate', '2021-02-18', 'letter/letter_210610114552.pdf', '2021-02-19', 'receipt/receipt_210610114552.pdf', '2021-02-22', 'report/report_210610114552.pdf', 'Not Due', 'POLICE STATION VISIT ON 20 TH FEB (8767585901-nilesh@123)', 0, NULL, '2021-09-07 08:34:55'),
(34, 63, 'Certificate', '2021-02-22', 'letter/letter_210610114747.pdf', '2021-03-15', 'receipt/receipt_210610114747.pdf', '2021-03-24', 'report/report_210610114747.pdf', 'Not Due', 'On 24th Procedure for police station/Police Station will CB  On 8 th March-3 Time visit Call On 15 th March(Amit Phale),On 17 th March will Do Process 9850903212-Omsai@123,', 0, NULL, '2021-09-07 08:36:44'),
(35, 149, 'Certificate', '2021-02-10', 'letter/letter_210610115011.pdf', '2021-02-10', 'receipt/receipt_210610115011.pdf', '2021-02-16', 'report/report_210610115011.pdf', 'Not Due', 'POLICE VERIFICATION DONE(Trunali@123)', 0, NULL, '2021-09-07 08:39:08'),
(36, 87, 'Certificate', '2020-11-03', 'letter/letter_210610115354.pdf', '2021-02-20', 'receipt/receipt_210610115354.pdf', '2021-04-05', 'report/report_210610115354.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 05:13:02'),
(37, 205, 'Certificate', '2021-01-18', 'letter/letter_210610115644.pdf', '2021-02-12', 'receipt/receipt_210610115644.pdf', '2021-02-23', 'report/report_210610115644.pdf', 'Not Due', 'POLICE STATION VISIT ON 13 TH FEB(9850534648-Password-Santosh@123)', 0, NULL, '2021-09-07 08:40:56'),
(38, 135, 'Certificate', '2021-02-22', 'letter/letter_210610115846.pdf', '2021-03-15', 'receipt/receipt_210610115846.pdf', '2021-03-16', 'report/report_210610115846.pdf', 'Not Due', 'On 24 TH will do PVR PROCESS/On 26 th /On 6 thMarch  will Do/Verification Done(9673471212-Omsai@123)', 0, NULL, '2021-09-07 08:45:43'),
(39, 218, 'Certificate', '2021-02-01', 'letter/letter_210610120335.pdf', '2021-02-15', 'receipt/receipt_210610120335.pdf', '2021-02-20', 'report/report_210610120335.pdf', 'Not Due', 'On 20 th will do PVR PROCESS/M-Swich Off(Omsai@123)', 0, NULL, '2021-09-07 08:47:37'),
(40, 138, 'Certificate', '2020-8-15', 'letter/letter_210611054946.pdf', '2021-9-11', 'receipt/receipt_210911092318.pdf', '2021-9-12', 'report/report_210923045310.pdf', 'Not Due', '9922269009-Kiran@123', 0, NULL, '2021-09-22 23:53:38'),
(41, 146, 'Certificate', '2020-11-17', 'letter/letter_210611055327.pdf', '2020-11-18', 'receipt/receipt_210611055327.pdf', '2020-11-20', 'report/report_210611055327.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 05:14:49'),
(42, 216, 'Certificate', '2020-10-26', 'letter/letter_210611055547.pdf', '2020-11-27', 'receipt/receipt_210611055547.pdf', '2020-12-04', 'report/report_210611055547.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 05:15:42'),
(43, 105, 'Certificate', '2020-11-04', 'letter/letter_210611055744.pdf', '2020-11-04', 'receipt/receipt_210611055744.pdf', '2021-02-23', 'report/report_210611055744.pdf', 'Not Due', 'CYBER CAFÉ VISIT - QUARRY/M-Ringing(arun@123)', 0, NULL, '2021-09-07 08:48:17'),
(44, 106, 'Certificate', '2020-11-02', 'letter/letter_210611060151.pdf', '2020-11-15', 'receipt/receipt_210611060151.pdf', '2020-11-28', 'report/report_210611060151.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 05:16:24'),
(45, 65, 'Certificate', '2021-02-22', 'letter/letter_210611060459.pdf', '2021-03-16', 'receipt/receipt_210611060459.pdf', '2021-03-17', 'report/report_210611060459.pdf', 'Not Due', 'On 26 th will Do Procedure/ M-Ringing 3./Do On 16 th Process-Ringing(9146928841-Omsai@123)', 0, NULL, '2021-09-07 08:48:55'),
(46, 242, 'Certificate', '2020-11-20', 'letter/letter_210611060818.pdf', '2020-11-25', 'receipt/receipt_210611060818.pdf', '2020-12-28', 'report/report_210611060818.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 05:17:56'),
(47, 109, 'Certificate', '2020-12-03', 'letter/letter_210611061049.pdf', '2020-12-05', 'receipt/receipt_210611061049.pdf', '2020-12-07', 'report/report_210611061049.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 05:18:35'),
(48, 198, 'Certificate', '2021-02-05', 'letter/letter_210611061255.pdf', '2021-02-05', 'receipt/receipt_210611061255.pdf', '2021-02-26', 'report/report_210611061255.pdf', 'Not Due', 'On 20 th Procedure for police station(7304066640-Omsai@123)', 0, NULL, '2021-09-07 08:49:42'),
(49, 91, 'Certificate', '2020-11-10', 'letter/letter_210611061542.pdf', '2020-11-21', 'receipt/receipt_210611061542.pdf', '2021-2-19', 'report/report_210611061542.pdf', 'Not Due', NULL, 0, NULL, NULL),
(50, 99, 'Certificate', '2021-02-16', 'letter/letter_210611061952.pdf', '2020-02-20', 'receipt/receipt_210611061952.pdf', '2020-02-22', 'report/report_210611061952.pdf', 'Not Due', 'MARRIAGE CERTIFICATE ISSUE/On 22 nd Evening(Girija@1234)', 0, NULL, '2021-09-07 08:52:20'),
(51, 126, 'Certificate', '2020-11-10', 'letter/letter_210611062528.pdf', '2020-11-18', 'receipt/receipt_210611062528.pdf', '2020-12-17', 'report/report_210611062528.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 05:19:22'),
(52, 181, 'Certificate', '2021-02-15', 'letter/letter_210611062759.pdf', '2021-02-16', 'receipt/receipt_210611062759.pdf', '2021-02-25', 'report/report_210611062759.pdf', 'Not Due', 'POLICE STATION VERIFICATION DONE(Omsai@123)', 0, NULL, '2021-09-07 08:52:51'),
(53, 70, 'Certificate', '2021-2-18', 'letter/letter_210611062953.pdf', '2021-2-20', 'receipt/receipt_210611062953.pdf', '2021-3-03', 'report/report_210611062953.pdf', 'Not Due', NULL, 0, NULL, NULL),
(54, 212, 'Certificate', '2020-10-02', 'letter/letter_210611063253.pdf', '2020-10-15', 'receipt/receipt_210611063253.pdf', '2020-11-23', 'report/report_210611063253.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 05:22:19'),
(55, 144, 'Certificate', '2020-10-01', 'letter/letter_210611063529.pdf', '2020-10-15', 'receipt/receipt_210611063529.pdf', '2020-11-07', 'report/report_210611063529.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 05:22:45'),
(56, 243, 'Certificate', '2020-11-06', 'letter/letter_210611063729.pdf', '2020-11-10', 'receipt/receipt_210611063729.pdf', '2020-11-18', 'report/report_210611063729.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 05:23:42'),
(57, 160, 'Certificate', '2021-02-24', 'letter/letter_210611010920.pdf', '2021-03-17', 'receipt/receipt_210611010920.pdf', '2021-03-19', 'report/report_210611010920.pdf', 'Not Due', '8329679507-Omsai@123', 0, NULL, '2021-09-07 08:55:10'),
(58, 142, 'Certificate', '2020-11-12', 'letter/letter_210611011106.pdf', '2020-11-12', 'receipt/receipt_210611011106.pdf', '2020-11-17', 'report/report_210611011106.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 05:25:06'),
(59, 134, 'Certificate', '2020-11-05', 'letter/letter_210611011424.pdf', '2020-11-05', 'receipt/receipt_210611011424.pdf', '2020-12-13', 'report/report_210611011424.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 05:24:22'),
(60, 86, 'Certificate', '2021-07-16', 'letter/letter_210811124736.pdf', '2020-07-06', 'receipt/receipt_210611011844.pdf', '2021-07-24', 'report/report_210911093038.pdf', 'Not Due', 'ID-7770077714-Omsai@123', 0, NULL, '2021-09-11 04:38:18'),
(61, 92, 'Certificate', '2020-11-10', 'letter/letter_210614051428.pdf', '2020-11-11', 'receipt/receipt_210614051428.pdf', '2021-02-20', 'report/report_210614051428.pdf', 'Not Due', 'RECEIPT EXPIERED-Proced again-On 24 th will Do/ (Omsai@123)-On 17 th March Visit to CP office', 0, NULL, '2021-09-07 08:53:43'),
(62, 78, 'Certificate', '2021-02-17', 'letter/letter_210614051624.pdf', '2021-02-19', 'receipt/receipt_210614051624.pdf', '2021-03-24', 'report/report_210614051624.pdf', 'Not Due', '(9370299511-Nehajoshi@123)', 0, NULL, '2021-09-07 08:54:20'),
(63, 104, 'Certificate', '2020-12-18', 'letter/letter_210614051854.pdf', '2021-01-11', 'receipt/receipt_210614051854.pdf', '2021-02-15', 'report/report_210614051854.pdf', 'Not Due', 'Sai@1234', 0, NULL, '2021-09-07 08:55:50'),
(64, 93, 'Certificate', '2020-11-01', 'letter/letter_210614052631.pdf', '2020-11-15', 'receipt/receipt_210614052631.pdf', '2020-11-24', 'report/report_210614052631.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 05:25:23'),
(65, 108, 'Certificate', '2020-12-16', 'letter/letter_210614052825.pdf', '2020-12-17', 'receipt/receipt_210614052825.pdf', '2020-12-18', 'report/report_210614052825.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 05:25:49'),
(66, 195, 'Certificate', '2021-02-05', 'letter/letter_210614053103.pdf', '2021-02-05', 'receipt/receipt_210614053103.pdf', '2021-03-05', 'report/report_210614053103.pdf', 'Not Due', '8828775757-Password-Omsai@123', 0, NULL, '2021-09-07 08:56:28'),
(67, 206, 'Certificate', '2021-02-01', 'letter/letter_210614053503.pdf', '2021-02-02', 'receipt/receipt_210614053503.pdf', '2021-02-22', 'report/report_210614053503.pdf', 'Not Due', '8208199219-Vkudale@900', 0, NULL, '2021-09-07 08:57:09'),
(68, 84, 'Certificate', '2021-02-23', 'letter/letter_210614053712.pdf', '2021-02-24', 'receipt/receipt_210614053712.pdf', '2021-03-24', 'report/report_210614053712.pdf', 'Not Due', 'Omsai@123', 0, NULL, '2021-09-07 08:57:39'),
(69, 74, 'Certificate', '2021-02-16', 'letter/letter_210614053901.pdf', '2021-02-23', 'receipt/receipt_210614053901.pdf', '2021-02-26', 'report/report_210614053901.pdf', 'Not Due', 'Pooja@1234', 0, NULL, '2021-09-07 08:58:12'),
(70, 71, 'Certificate', '2021-02-20', 'letter/letter_210614054108.pdf', '2021-02-22', 'receipt/receipt_210614054108.pdf', '2021-02-24', 'report/report_210614054108.pdf', 'Not Due', '8888999570-Pritam@123', 0, NULL, '2021-09-07 08:58:44'),
(71, 77, 'Certificate', '2021-02-25', 'letter/letter_210614054540.pdf', '2021-03-01', 'receipt/receipt_210614054540.pdf', '2021-03-17', 'report/report_210614054540.pdf', 'Not Due', '9607760207-Deepikakale@143', 0, NULL, '2021-09-07 08:59:20'),
(72, 94, 'Certificate', '2021-03-16', 'letter/letter_210614054756.pdf', '2021-03-26', 'receipt/receipt_210614054756.pdf', '2021-04-13', 'report/report_210614054756.pdf', 'Not Due', '9527601220-Saurabh@123', 0, NULL, '2021-09-07 09:00:12'),
(73, 89, 'Certificate', '2020-9-01', 'letter/letter_210614055005.pdf', '2021-9-17', 'receipt/receipt_211012081128.pdf', '2021-10-01', 'report/report_211109095702.pdf', 'Not Due', 'USER ID - 9860029943\r\n\r\nPASSWORD - Shafik@2021', 0, NULL, '2021-11-09 04:58:45'),
(74, 76, 'Certificate', '2021-02-18', 'letter/letter_210614055223.pdf', '2021-02-22', 'receipt/receipt_210614055223.pdf', '2021-03-17', 'report/report_210614055223.pdf', 'Not Due', '7620262185-Neharanpise@123', 0, NULL, '2021-09-07 09:00:50'),
(75, 72, 'Certificate', '2020-11-17', 'letter/letter_210614055604.pdf', '2020-11-18', 'receipt/receipt_210614055604.pdf', '2021-01-13', 'report/report_210614055604.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 05:26:53'),
(76, 179, 'Certificate', '2021-02-17', 'letter/letter_210614060137.pdf', '2021-02-19', 'receipt/receipt_210614060137.pdf', '2021-02-22', 'report/report_210614060137.pdf', 'Not Due', '7768904807-Password-Omsai@123', 0, NULL, '2021-09-07 09:02:06'),
(77, 233, 'Certificate', '2020-10-28', 'letter/letter_210614060349.pdf', '2020-11-01', 'receipt/receipt_210614060349.pdf', '2020-11-19', 'report/report_210614060349.pdf', 'Not Due', NULL, 0, NULL, NULL),
(78, 110, 'Certificate', '2020-12-27', 'letter/letter_210614061127.pdf', '2020-12-27', 'receipt/receipt_210614061127.pdf', '2020-12-28', 'report/report_210614061127.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 05:27:23'),
(79, 194, 'Certificate', '2020-12-01', 'letter/letter_210614061348.pdf', '2020-12-07', 'receipt/receipt_210614061348.pdf', '2021-02-23', 'report/report_210614061348.pdf', 'Not Due', 'Omsai@123', 0, NULL, '2021-09-07 09:02:41'),
(80, 131, 'Certificate', '2020-12-28', 'letter/letter_210614061711.pdf', '2020-12-28', 'receipt/receipt_210614061711.pdf', '2021-01-01', 'report/report_210614061711.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 05:38:52'),
(81, 171, 'Certificate', '2021-02-01', 'letter/letter_210614061843.pdf', '2021-02-21', 'receipt/receipt_210614061843.pdf', '2021-02-23', 'report/report_210614061843.pdf', 'Not Due', '7218881615-Password-Pallavi@1996', 0, NULL, '2021-09-07 09:04:25'),
(82, 156, 'Certificate', '2020-10-01', 'letter/letter_210614062106.pdf', '2020-10-15', 'receipt/receipt_210614062106.pdf', '2020-11-03', 'report/report_210614062106.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 05:39:11'),
(83, 127, 'Certificate', '2021-01-12', 'letter/letter_210614062330.pdf', '2021-02-22', 'receipt/receipt_210614062330.pdf', '2021-02-24', 'report/report_210614062330.pdf', 'Not Due', '7796492382-Pass@123', 0, NULL, '2021-09-08 05:40:41'),
(84, 182, 'Certificate', '2021-02-18', 'letter/letter_210614062516.pdf', '2021-02-22', 'receipt/receipt_210614062516.pdf', '2021-02-23', 'report/report_210614062516.pdf', 'Not Due', 'Receipt procedure on 20 th Time-4 O Clk(Omsai@123)', 0, NULL, '2021-09-08 05:43:29'),
(85, 125, 'Certificate', '2021-2-25', 'letter/letter_210614062735.pdf', '2021-3-16', 'receipt/receipt_210614062735.pdf', '2021-3-18', 'report/report_210614062735.pdf', 'Not Due', NULL, 0, NULL, NULL),
(86, 154, 'Certificate', '2020-11-06', 'letter/letter_210614062926.pdf', '2021-02-10', 'receipt/receipt_210614062926.pdf', '2021-02-20', 'report/report_210614062926.pdf', 'Not Due', 'M-Sister(Saba@123)', 0, NULL, '2021-09-08 05:44:03'),
(87, 197, 'Certificate', '2021-02-01', 'letter/letter_210614063110.pdf', '2021-02-06', 'receipt/receipt_210614063110.pdf', '2021-02-19', 'report/report_210614063110.pdf', 'Not Due', '8652189341-Omsai@123', 0, NULL, '2021-09-08 05:45:36'),
(88, 215, 'Certificate', '2021-02-16', 'letter/letter_210614063255.pdf', '2021-02-18', 'receipt/receipt_210614063255.pdf', '2021-03-03', 'report/report_210614063255.pdf', 'Not Due', 'Arohi@ak1', 0, NULL, '2021-09-08 06:49:08'),
(89, 80, 'Certificate', '2020-11-01', 'letter/letter_210614063449.pdf', '2020-11-01', 'receipt/receipt_210614063449.pdf', '2020-11-11', 'report/report_210614063449.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 06:49:58'),
(90, 231, 'Certificate', '2020-11-07', 'letter/letter_210614063635.pdf', '2020-11-10', 'receipt/receipt_210614063635.pdf', '2020-11-19', 'report/report_210614063635.pdf', 'Not Due', NULL, 0, NULL, NULL),
(91, 210, 'Certificate', '2021-02-16', 'letter/letter_210614063814.pdf', '2021-02-18', 'receipt/receipt_210614063814.pdf', '2021-02-20', 'report/report_210614063814.pdf', 'Not Due', 'POLICE STATION VISIT ON 20 TH FEB(Omsai@123)', 0, NULL, '2021-09-08 06:50:33'),
(92, 124, 'Certificate', '2020-12-24', 'letter/letter_210614063953.pdf', '2021-02-17', 'receipt/receipt_210614063953.pdf', '2021-02-22', 'report/report_210614063953.pdf', 'Not Due', 'Omsai@123', 0, NULL, '2021-09-08 07:17:11'),
(93, 96, 'Certificate', '2020-11-06', 'letter/letter_210614064158.pdf', '2020-11-10', 'receipt/receipt_210614064158.pdf', '2020-11-19', 'report/report_210614064158.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 07:24:56'),
(94, 100, 'Certificate', '2021-3-18', 'letter/letter_210614064339.pdf', '2021-3-18', 'receipt/receipt_210614064339.pdf', '2021-3-19', 'report/report_210614064339.pdf', 'Not Due', NULL, 0, NULL, NULL),
(95, 200, 'Certificate', '2021-02-01', 'letter/letter_210614064544.pdf', '2021-02-05', 'receipt/receipt_210614064544.pdf', '2021-02-20', 'report/report_210614064544.pdf', 'Not Due', 'On 20 th Procedure for police station(Omsai@123)', 0, NULL, '2021-09-08 07:26:12'),
(96, 98, 'Certificate', '2020-11-04', 'letter/letter_210614064743.pdf', '2020-11-04', 'receipt/receipt_210614064743.pdf', '2020-11-9', 'report/report_210614064743.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 07:26:52'),
(97, 211, 'Certificate', '2020-11-01', 'letter/letter_210614064937.pdf', '2020-11-01', 'receipt/receipt_210614064937.pdf', '2020-11-03', 'report/report_210614064937.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 07:27:31'),
(98, 130, 'Certificate', '2020-9-12', 'letter/letter_210614065112.pdf', '2020-12-17', 'receipt/receipt_210614065112.pdf', '2020-12-23', 'report/report_210614065112.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 07:28:07'),
(99, 132, 'Certificate', '2021-02-19', 'letter/letter_210614065238.pdf', '2021-02-19', 'receipt/receipt_210614065238.pdf', '2021-03-19', 'report/report_210614065238.pdf', 'Not Due', 'POLICE STATION VISIT ON 20 TH FEB(Omsai@123)-On 18 th will Do', 0, NULL, '2021-09-08 07:28:42'),
(100, 115, 'Certificate', '2021-02-16', 'letter/letter_210614065414.pdf', '2021-02-20', 'receipt/receipt_210614065414.pdf', '2021-02-24', 'report/report_210614065414.pdf', 'Not Due', 'Will Do on 20 th(Omsai@123)', 0, NULL, '2021-09-08 07:41:53'),
(101, 129, 'Certificate', '2020-12-02', 'letter/letter_210614065620.pdf', '2020-12-02', 'receipt/receipt_210614065620.pdf', '2020-12-14', 'report/report_210614065620.pdf', 'Not Due', 'M-No Reply,M2-Ringing(9765722168-Pass@2222)', 0, NULL, '2021-09-08 07:43:19'),
(102, 175, 'Certificate', '2021-02-01', 'letter/letter_210614065755.pdf', '2021-02-20', 'receipt/receipt_210614065755.pdf', '2021-03-8', 'report/report_210614065755.pdf', 'Not Due', '9158712593-Omsai@123', 0, NULL, '2021-09-08 07:43:58'),
(103, 116, 'Certificate', '2021-03-02', 'letter/letter_210614065923.pdf', '2021-03-03', 'receipt/receipt_210614065923.pdf', '2021-03-06', 'report/report_210614065923.pdf', 'Not Due', '9322877206-Omsai@123', 0, NULL, '2021-09-08 07:47:15'),
(104, 223, 'Certificate', '2021-03-17', 'letter/letter_210614070101.pdf', '2021-03-19', 'receipt/receipt_210614070101.pdf', '2021-03-29', 'report/report_210614070101.pdf', 'Not Due', 'GRAMIN POLICE STATION-will Do process On 16 th(Omsai@123', 0, NULL, '2021-09-08 07:50:58'),
(105, 189, 'Certificate', '2021-3-05', 'letter/letter_210614070225.pdf', '2021-3-10', 'receipt/receipt_210614070225.pdf', '2021-3-15', 'report/report_210614070225.pdf', 'Not Due', NULL, 0, NULL, NULL),
(106, 83, 'Certificate', '2020-11-18', 'letter/letter_210614070520.pdf', '2020-11-18', 'receipt/receipt_210614070520.pdf', '2020-11-21', 'report/report_210614070520.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 07:55:52'),
(107, 81, 'Certificate', '2021-02-17', 'letter/letter_210614070645.pdf', '2021-03-17', 'receipt/receipt_210614070645.pdf', '2021-04-8', 'report/report_210614070645.pdf', 'Not Due', 'On 24 th will Do RECIEPT/On 26 th will Do/On 27 th PUP/On 1 st will Do Process/On 16 th March will do Process/Reject Sammati patra- Till 25 Th March will Do/Sammatipatra Problem-New Joint(7841000725)-CB On 5 th', 0, NULL, '2021-09-08 07:58:40'),
(108, 48, 'Certificate', '2021-02-04', 'letter/letter_210615060138.pdf', '2021-02-04', 'receipt/receipt_210615060138.pdf', '2021-02-05', 'report/report_210615060138.pdf', 'Not Due', 'Satish@123', 0, NULL, '2021-09-08 04:52:34'),
(109, 35, 'Certificate', '2021-02-25', 'letter/letter_210615060325.pdf', '2021-02-25', 'receipt/receipt_210615060325.pdf', '2021-03-12', 'report/report_210615060325.pdf', 'Not Due', '7385245678-Omsai@123', 0, NULL, '2021-09-08 04:53:12'),
(110, 37, 'Certificate', '2021-02-10', 'letter/letter_210615060506.pdf', '2021-02-10', 'receipt/receipt_210615060506.pdf', '2021-02-18', 'report/report_210615060506.pdf', 'Not Due', 'POLICE VERIFICATION DONE(9850904141-Omsai@123)', 0, NULL, '2021-09-08 05:06:48'),
(111, 230, 'Certificate', '2021-02-25', 'letter/letter_210615060658.pdf', '2021-02-25', 'receipt/receipt_210615060658.pdf', '2021-04-23', 'report/report_210615060658.pdf', 'Not Due', '9850900800-Amit@123', 0, NULL, '2021-09-08 05:09:22'),
(112, 244, 'Certificate', '2021-03-15', 'letter/letter_210616100013.pdf', '2021-03-17', 'receipt/receipt_210616100013.pdf', '2021-03-18', 'report/report_210616100013.pdf', 'Not Due', 'M-will do On 17 th March(Omsai@123', 0, NULL, '2021-09-07 08:51:42'),
(113, 102, 'Certificate', '2020-11-04', 'letter/letter_210616113946.pdf', '2020-11-23', 'receipt/receipt_210616113946.pdf', '2021-02-20', 'report/report_210816102932.pdf', 'Not Due', 'POLICE STATION VISIT ON 20 TH FEB(9904534660-Omsai@123)', 0, NULL, '2021-09-08 08:05:40'),
(114, 150, 'Certificate', '2020-11-04', 'letter/letter_210616114258.pdf', '2020-11-04', 'receipt/receipt_210616114258.pdf', '2020-11-21', 'report/report_210617012205.pdf', 'Not Due', 'POLICE STATION VISIT ON 20 TH FEB(Omsai@123)-NIL CASE (KHADKi COURT JUDGEMENT COPY PENDING)', 0, NULL, '2021-09-08 07:54:13'),
(115, 176, 'Certificate', '2021-02-11', 'letter/letter_210617123335.pdf', '2021-02-12', 'receipt/receipt_210617123335.pdf', '2021-02-25', 'report/report_210617123335.pdf', 'Not Due', 'POLICE STATION VISIT ON 20 TH FEB/On 23rd will Go to Police station-5.30 To 7.30 /On 25 th will Do Procedure(Omsai@123)', 0, NULL, '2021-09-08 07:55:00'),
(116, 64, 'Certificate', '2021-06-15', 'letter/letter_210618072225.pdf', '2021-06-17', 'receipt/receipt_210816101002.pdf', '2021-07-07', 'report/report_210816100910.pdf', 'Not Due', 'On 28 th will Do Procedure( Usmanabad)(Omsai@1234)/M-Switch Off(M-NR(ID-9921849716-Password-Omsai@1234)', 0, NULL, '2021-09-08 08:09:38'),
(117, 168, 'Certificate', '2020-11-12', 'letter/letter_210618072440.pdf', '2020-11-17', 'receipt/receipt_210618072440.pdf', '2020-11-18', 'report/report_210823070143.pdf', 'Not Due', 'CASE-CB 19,(8888883898-Password-Omsai@123)ISSUE-CASE', 0, NULL, '2021-09-08 08:06:17'),
(118, 203, 'Certificate', '2020-11-12', 'letter/letter_210618072658.pdf', '2020-11-12', 'receipt/receipt_210618072658.pdf', '2020-12-07', 'report/report_211021082150.pdf', 'Not Due', 'PVR Done Certificate will send on Group/Exe will come on 2nd august(M-Executive will CB/7TH Aug will DO8180080037-Omsai@123)On 17 th March will Go/No contact/M-Ringing', 0, NULL, '2021-10-21 03:21:50'),
(119, 107, 'Certificate', '2021-06-23', 'letter/letter_210618073049.pdf', '2020-12-04', 'receipt/receipt_210816103144.pdf', '2021-07-30', 'report/report_210816103144.pdf', 'Not Due', 'VERIFICTION Department MR. Jagtap  On Leave On 22 nd,1 St CASE ONLINE FOR PVR (Password-Omsai@123)(Due To covid Verification Dep is Closed/Will do Re process on 21 st/ M-Call forwarded to another no(9423447339-Password-Omsai@123/On 3 rd july will Do proce', 0, NULL, '2021-09-08 08:12:41'),
(120, 73, 'Certificate', '2021-02-24', 'letter/letter_210618074152.pdf', '2021-02-24', 'receipt/receipt_210816101747.pdf', '2021-07-22', 'report/report_210816101747.pdf', 'Not Due', 'On 19 th PROCESS/M-Ringing-OLD Before 3 Month,PANCARD IN PROCESS-10 days-5 th March-7 th March(9049922229-Pad@7925)<SCAN LETEST LIGHT BILL AND CRIME RECORD SHOW AT POLICE STATION.>/Will Do On 5 th(Remarks: c r show at police stattion visit cp office)', 0, NULL, '2021-09-08 08:07:13'),
(121, 79, 'Certificate', '2021-06-17', 'letter/letter_210618074852.pdf', '2021-03-26', 'receipt/receipt_210712070005.pdf', '2021-07-13', 'report/report_210816102221.pdf', 'Not Due', 'M-Bhima Koregao Gramin On 2 nd /6 th will Give Direct Report/ M-Ringing 3/On 18 th will Do/On 24 th will do/Till 28 th will Do/SUBMIT ALL DOCCUMENTS BUT NO CALL SO HE WILL GO POLICE STATION TOMORROW/On 5 th will go to Police Station/M-Exe will send copy/', 0, NULL, '2021-09-09 01:20:44'),
(122, 117, 'Letter', '2021-06-25', 'letter/letter_210625112545.pdf', '2021-03-16', 'receipt/receipt_210618075050.pdf', '--', NULL, 'Due', 'Left job-sep 2021', 0, NULL, '2021-09-14 01:32:54'),
(123, 118, 'Certificate', '2021-03-11', 'letter/letter_210618075757.pdf', '2021-03-11', 'receipt/receipt_210618075757.pdf', '2021-8-07', 'report/report_210816103350.pdf', 'Not Due', 'On 13 th will Do PVR Process,On 17 th March Go For PVR(8605449366-Omsai@123)<upload birth certificateProceed to D>//Will Do again/On 9 th will uplod& visit to Police station/On 1 st July will Do Process/Upload Birth certificate On 2 nd July will Do verifi', 0, NULL, '2021-09-08 08:10:55'),
(124, 120, 'Certificate', '2021-06-21', 'letter/letter_210621122949.pdf', '2021-03-13', 'receipt/receipt_210618075942.pdf', '2021-06-21', 'report/report_210623074839.pdf', 'Not Due', 'On 13 th will Do PVR Process,Omsai@123<CR Registered at police stationProceed to Documents>/On 4 /8 th will go Police Station/Re Process New Letter taken On 21 st', 0, NULL, '2021-09-08 07:57:15'),
(125, 122, 'Certificate', '2021-03-18', 'letter/letter_210618080121.pdf', '2021-03-18', 'receipt/receipt_210618080121.pdf', '2021-9-20', 'report/report_210921062255.pdf', 'Not Due', '(8857029826-Purav@123)(Remarks: CR Registered at police station )', 0, NULL, '2021-09-21 01:22:55'),
(126, 191, 'Receipt', '2021-02-06', 'letter/letter_210618080258.pdf', '2021-02-06', 'receipt/receipt_210618080258.pdf', '--', NULL, 'Due', 'AKASH SHILIMKAR', 0, NULL, '2021-09-06 06:01:42'),
(127, 193, 'Receipt', '2021-02-', 'letter/letter_210618080714.pdf', '2021-02-07', 'receipt/receipt_210618080714.pdf', '--', NULL, 'Due', 'On 20 th Procedure for police station(case),2-3 Times visit Police station..No response-7000 RS(8928933199-Omsai@123)/On 27 th willDo/ M-Switch off,Ringing', 0, NULL, '2021-09-06 06:02:24'),
(128, 208, 'Certificate', '2021-02-15', 'letter/letter_210618085455.pdf', '2021-02-19', 'receipt/receipt_210712083417.pdf', '2021-07-9', 'report/report_210816102654.pdf', 'Not Due', 'DOCUMENT-RENT AGREEMENT DATE ISSUE,POLICE STATION VISIT ON 22 nd FEB/ M-Ringing(Harish@123) willDo till 28th March<upload proper address agreements with owner light bill>/M-Exe Light bill Issue,Last Owner name on Light bill,exe will revisit to Police stat', 0, NULL, '2021-09-08 08:11:33'),
(129, 164, 'Certificate', '2021-02-22', 'letter/letter_210618085707.pdf', '2021-07-13', 'receipt/receipt_210827080607.pdf', '2021-8-19', 'report/report_210911102716.pdf', 'Not Due', '{Complete-9579624023-Admin@1234}(pending-8407996555-Admin@123}', 0, NULL, '2021-09-11 05:27:43'),
(130, 75, 'Certificate', '2021-06-04', 'letter/letter_210618085830.pdf', '2021-06-12', 'receipt/receipt_210618085830.pdf', '2021-10-24', 'report/report_211109055608.pdf', 'Not Due', 'ID-7020936807-Atul@2021', 0, NULL, '2021-11-09 00:56:08'),
(131, 68, 'Certificate', '2021-06-10', 'letter/letter_210618090002.pdf', '2021-9-06', 'receipt/receipt_210914062941.pdf', '2021-9-16', 'report/report_210918062218.pdf', 'Not Due', '8956797582-Vikas@12345-GRAMIN POLICE STATION-will Do process  On 8 th will Do process', 0, NULL, '2021-09-18 01:25:15'),
(132, 204, 'Receipt', '2021-06-04', 'letter/letter_210618090105.pdf', '2021-10-23', 'receipt/receipt_211023075804.pdf', '--', NULL, 'Due', 'ID-8308615582-Komal@16', 0, NULL, '2021-10-23 04:57:06'),
(133, 221, 'Certificate', '2021-06-05', 'letter/letter_210618090836.pdf', '2021-06-21', 'receipt/receipt_210630121813.pdf', '2021-8-30', 'report/report_210917072618.pdf', 'Not Due', 'On 10 th will do process(8830388031-Omsai@123)', 0, NULL, '2021-09-17 02:26:18'),
(134, 166, 'Letter', '2021-6-04', 'letter/letter_210618091009.pdf', '', NULL, '', NULL, 'Not Due', NULL, 0, NULL, NULL),
(135, 170, 'Letter', '2021-06-04', 'letter/letter_210618091216.pdf', '--', NULL, '--', NULL, 'Due', NULL, 0, NULL, '2021-09-01 07:03:22'),
(136, 183, 'Certificate', '2021-06-04', 'letter/letter_210618123121.pdf', '2021--9', 'receipt/receipt_210816111704.pdf', '2021-8-10', 'report/report_210904101054.pdf', 'Not Due', '(ID-919372014217,PASS WORD -Omsai@123', 0, NULL, '2021-09-09 01:50:43'),
(137, 180, 'Certificate', '2021-06-', 'letter/letter_210618123344.pdf', '2021-06-18', 'receipt/receipt_210618123344.pdf', '2021-8-30', 'report/report_210911100422.pdf', 'Not Due', 'OUT OF STATION/ Online Process Done will Send receipt/On 27 th will DO/M-Ringing/On 16 th will Do Process/On 24 th will Do Proecss/ M-No Reply-Exe will Come for Process On 5 th /M-Exe will Do Process on 16 th will Do(Light Bill-Uncle name-Stamp Paper affi', 0, NULL, '2021-09-11 05:04:22'),
(138, 190, 'Certificate', '2021-06-9', 'letter/letter_210618123716.pdf', '2021-06-11', 'receipt/receipt_210618123716.pdf', '2021-06-14', 'report/report_210908010403.pdf', 'Not Due', 'On 16 th will Do Process will send Receipt (ID-7507817171-Password-Omsai@123)', 0, NULL, '2021-09-08 08:04:21'),
(139, 225, 'Receipt', '2021-06-02', 'letter/letter_210618123856.pdf', '2021-06-17', 'receipt/receipt_210624050510.pdf', '--', NULL, 'Due', 'AKASH SHILIMKAR-888896258-Omsai@123', 0, NULL, '2021-10-21 03:52:47'),
(140, 237, 'Receipt', '2021-06-9', 'letter/letter_210618124602.pdf', '2021-06-17', 'receipt/receipt_210618124602.pdf', '--', NULL, 'Due', 'M-Exe will Come for PVR Letter in Off//On 5 th will do online process/will Do Process on 10 th/M-Exe will Do Process on 11 th/M-Ringing (2 Time)(9021010746-Omsai@123)/will do process(NOT SUBMIT PHOTO) will Go for Police sttion on 10 th', 0, NULL, '2021-09-06 06:08:34'),
(141, 234, 'Receipt', '2021-06-05', 'letter/letter_210618124752.pdf', '2021-06-17', 'receipt/receipt_210618124752.pdf', '--', NULL, 'Due', 'M-Exe will Come for PVR Letter in Off//Letter missplace will come for new PVR letter/M-Ringing-No Reply(9762850860-Omsai@123)/ M-No Reply', 0, NULL, '2021-09-06 06:09:24'),
(142, 213, 'Certificate', '2021-04-01', 'letter/letter_210621081151.pdf', '2021-04-10', 'receipt/receipt_210621081151.pdf', '2021-05-14', 'report/report_210621081151.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 07:58:13'),
(143, 88, 'Certificate', '2021-06-10', 'letter/letter_210621090720.pdf', '2021-9-24', 'receipt/receipt_211005104121.pdf', '2021-9-28', 'report/report_211007081223.pdf', 'Not Due', 'ID-9552688766-PASSWORD-Prajkta@260499', 0, NULL, '2021-10-07 03:12:23'),
(144, 192, 'Receipt', '2021-02-', 'letter/letter_210621090947.pdf', '2021-02-07', 'receipt/receipt_210621090947.pdf', '--', NULL, 'Due', 'DOCUMENT ISSUE-RENT AGREEMENT(Omsai@123)/ M-will go to Police station-Salary Issue/ M-No Reply<Rejected (INSPITE OF HAVING REPETEDLY INFORM TO APPLICANT FOR POLICE VERIFICATION BUT APPLICANT NOT APPROCH FOR /POLICE VERIFICATION)/Cordinated with Ganesh Sir', 0, NULL, '2021-09-06 06:11:28'),
(145, 240, 'Certificate', '2021-06-', 'letter/letter_210621091102.pdf', '2021-06-27', 'receipt/receipt_210712065103.pdf', '2021-07-06', 'report/report_210816105147.pdf', 'Not Due', 'M-Exe will Do Process on 17 th/On 3 rd july will go for Police verification(9307233957-9307233957)', 0, NULL, '2021-09-09 01:49:30'),
(146, 143, 'Certificate', '2021-02-19', 'letter/letter_210621120423.pdf', '2021-06-21', 'receipt/receipt_210621120423.pdf', '2021-07-06', 'report/report_210816105958.pdf', 'Not Due', 'M-Ringing-On 27 th will Do PVR Procedure/Revised Letter/ M-Ringing/will Do Process on 5 th/M-Exe will Do re process on 19 th/On 3 rd will go for Police verification', 0, NULL, '2021-09-09 01:52:28'),
(147, 56, 'CP/SP Process Pending', '2021-2-17', 'letter/letter_210623081117.pdf', '2021-2-17', 'receipt/receipt_210623081117.pdf', '', NULL, 'Not Due', NULL, 0, NULL, NULL),
(148, 133, 'Certificate', '2021-07-06', NULL, '2021-02-01', 'receipt/receipt_210710031844.pdf', '2021-8-27', 'report/report_210906105052.pdf', 'Not Due', 'ID-7620600936-Password-Ayesha@12345', 0, NULL, '2021-09-06 05:51:12'),
(149, 163, 'Untouched', '--', NULL, '--', NULL, '--', NULL, 'Due', 'AKASH SHILIMKAR', 0, NULL, '2021-09-06 06:18:08'),
(150, 232, 'Letter', '2021-1-23', NULL, '2021-3-15', NULL, '', NULL, 'Due', NULL, 0, NULL, NULL),
(151, 119, 'Receipt', '2021-03-05', NULL, '2021-03-11', NULL, '--', NULL, 'Due', 'On 13 th will Do PVR Process/ M-NOT REACHABLE-Switch Off-Ringing/exe will do till 10 th/M-Exe will go to Police station on 10 th for verification/On 17 th Exe go for verification/On 7 th July will go for verification(will do process 7057359668-Omsai@123/w', 0, NULL, '2021-09-06 06:10:43'),
(152, 238, 'Certificate', '2021-03-16', NULL, '2021-03-23', NULL, '2021-8-05', 'report/report_210816104300.pdf', 'Not Due', '9145316466-Omsai@123', 0, NULL, '2021-09-09 01:51:19'),
(153, 236, 'Certificate', '2021-05-30', 'letter/letter_210625102031.pdf', '2021-06-03', 'receipt/receipt_210625102031.pdf', '2021-07-02', 'report/report_210816112318.pdf', 'Not Due', 'On 4 th will Do Process again(ID-9545292929-Password-Gaurav@1359)/M-Exe will do re process On 12 th (Name change in LC))-On 2 nd July will Do PVR Verification Process', 0, NULL, '2021-09-08 08:12:06'),
(154, 123, 'Certificate', '2020-11-25', NULL, '2021-02-19', NULL, '2021-9-02', 'report/report_210914110026.pdf', 'Not Due', 'RENT AGREEMENT-after 6 Month-Samatti patra/POLICE STATION VISIT ON 20 TH FEB/After 3 Month May 2021/NO PVR/will Do Process/M-TCL will do re process On17th/will Do 1 st august-7083562618-Gautami@123', 0, NULL, '2021-09-14 06:01:24'),
(155, 169, 'Receipt', '2021-06-25', 'letter/letter_210625102957.pdf', '2021-07-15', 'receipt/receipt_210904102625.pdf', '--', NULL, 'Due', 'M-EXE not allocate Dump/ M-Exe will Do Re process on 16 th /On 3 rd,4 th  July will Do Process/ M-Ringing', 0, NULL, '2021-09-06 06:01:07'),
(156, 151, 'Receipt', '2021-02-24', NULL, '2021-02-25', NULL, '--', NULL, 'Due', 'On 26 th will Do Procedure(9011878685-Omsai@123)/Ringing-On 29 th will Do/M-Switch Off/M-Ringing-On 17 th will re visit to Police station for verification/he will Do PVR process again/will Do on 12 th aug Reprocess', 0, NULL, '2021-09-06 06:14:53'),
(157, 148, 'Receipt', '2021-03-17', NULL, '2021-03-17', 'receipt/receipt_211021085355.pdf', '--', NULL, 'Due', 'AKASH SHILIMKAR-8806290029-Omsai@123', 0, NULL, '2021-10-21 03:53:55'),
(158, 162, 'Receipt', '2021-02-27', NULL, '2021-03-17', NULL, '--', NULL, 'Due', 'Junnar-On 1 st will DoProcess/On 16 th will(9075663636-Speed@123)/On 25th March will Do/On 29 th will Do/ M-Ringing/Exe will Go To Junnar & Revert till 10 th/M-Exe will go to Police station on 20 th for verification (Junnar)/he will go for Police verifica', 0, NULL, '2021-09-06 05:38:35'),
(159, 161, 'Receipt', '2021-03-04', NULL, '2021-03-04', NULL, '--', NULL, 'Due', NULL, 0, NULL, '2021-09-08 05:11:09'),
(160, 159, 'Certificate', '2021-06-17', 'letter/letter_210625104404.pdf', '2021-06-17', 'receipt/receipt_210625104404.pdf', '2021-07-9', 'report/report_210816101143.pdf', 'Not Due', 'DOCUMENT ISSUE-PAN CARD-CB 5 th March(Vijay Kole)/M-Ringing-On 17 th March  will DO/After Closing will Done/M-Ringing/M-TCL will Do Re Process on 18th(7020054336-Deepali@1234)', 0, NULL, '2021-09-08 08:10:18'),
(161, 174, 'Receipt', '2021-02-22', NULL, '2021-02-23', NULL, '--', NULL, 'Due', 'POLICE STATION VISIT ON 21 ST FEB/ On 25 th will Send Document-GRAMIN/On 8th March will DO/Call Fowarded-9307902944,M2-Ringing-On 14 th will Do Process/ M-Ringing-Switch Off-will Do till 24 th March/M-No Reply-Customer /M-No Reply,M2-Ringing-Busy(82379741', 0, NULL, '2021-09-06 05:44:37'),
(162, 247, 'Certificate', '2021-06-10', NULL, '2021-04-29', NULL, '2021-07-8', 'report/report_210816104609.pdf', 'Not Due', 'M-will Sent PVR Receipt/M-Exe will come for Letter on 9/M-Exe will Do Process on 11 th/ M-will Do Process Till 17/M-Ringing(9850555775-Omsai@123)', 0, NULL, '2021-09-09 01:23:21'),
(163, 248, 'Untouched', '--', 'letter/letter_210708075115.pdf', '--', NULL, '--', NULL, 'Due', NULL, 0, NULL, '2021-09-01 07:20:20'),
(164, 246, 'Untouched', '--', NULL, '--', NULL, '--', NULL, 'Due', NULL, 0, NULL, '2021-09-01 07:20:37'),
(165, 60, 'Untouched', '--', NULL, '--', NULL, '--', NULL, 'Due', NULL, 0, NULL, '2021-09-01 07:21:13'),
(166, 239, 'Certificate', '2021-07-01', 'letter/letter_210712074307.pdf', '2021-8-02', 'receipt/receipt_210816112518.pdf', '2021-8-26', 'report/report_210911095209.pdf', 'Not Due', 'ID-8483087553-Password-Omsai@123-On 10 th will Do Process', 0, NULL, '2021-09-11 04:52:34'),
(167, 85, 'Receipt', '2021-8-10', NULL, '2021-8-28', 'receipt/receipt_210907080650.pdf', '--', NULL, 'Due', 'PVR Done Certificate will send on Group/Exe will come on 2nd august(M-Executive will CB/7TH Aug will DO,9096364080-Daund@123', 0, NULL, '2021-09-07 03:06:50'),
(168, 256, 'Certificate', '2021-06-10', 'letter/letter_210816114557.pdf', '2021-06-15', 'receipt/receipt_210816114557.pdf', '2021-06-21', 'report/report_210816114557.pdf', 'Not Due', '9922950155-Omsai@123', 0, NULL, '2021-09-08 08:08:31'),
(169, 259, 'Certificate', '2021-06-05', NULL, '2021-06-05', 'receipt/receipt_210816115010.pdf', '2021-07-9', 'report/report_210816115010.pdf', 'Not Due', '9158158128 Password-Omsai@123', 0, NULL, '2021-09-08 08:09:05'),
(170, 262, 'Certificate', '2021-06-10', NULL, '2021-07-20', NULL, '2021-07-24', 'report/report_210816115340.pdf', 'Not Due', 'M-Exe will Come for PVR Letter in Off/On 7 th will Do Process(Light Bill Problem)/M-Switch Off/ m-Not rechable(9970663643-Maruti@123)', 0, NULL, '2021-09-09 01:22:19'),
(171, 273, 'Certificate', '2021-07-05', NULL, '2021-07-9', NULL, '2021-07-14', 'report/report_210816120318.pdf', 'Not Due', 'ID-8208614248-Omsai@123', 0, NULL, '2021-09-09 01:26:36'),
(172, 254, 'Certificate', '2021-07-03', NULL, '2021-07-15', NULL, '2021-07-16', 'report/report_210816120657.pdf', 'Not Due', '8087735502-Omsai@123', 0, NULL, '2021-09-09 01:27:11'),
(173, 272, 'Certificate', '2021-8-01', NULL, '2021-8-03', 'receipt/receipt_210816121000.pdf', '2021-8-03', 'report/report_210911093716.pdf', 'Not Due', '9765936044-Omsai@123', 0, NULL, '2021-09-14 02:59:54'),
(174, 253, 'Certificate', '2021-07-05', NULL, '2021-07-15', 'receipt/receipt_210816121903.pdf', '2021-8-25', 'report/report_210826011044.pdf', 'Not Due', 'LOGIN ID=7620600941, PASSWORD=Omsai@123', 0, NULL, '2021-09-09 01:27:52'),
(176, 271, 'Receipt', '2021-07-', 'letter/letter_210816123530.pdf', '2021-9-12', 'receipt/receipt_210914052317.pdf', '--', NULL, 'Due', 'M-Prashant will convey the msg(9767790009-Amolk@2021', 0, NULL, '2021-09-14 00:24:07'),
(177, 251, 'Certificate', '2021-07-8', 'letter/letter_210816123820.pdf', '2021-07-26', 'receipt/receipt_210816123820.pdf', '2021-07-29', 'report/report_210816123820.pdf', 'Not Due', 'M-Switch Off-ID 8007763669-Omsai@123', 0, NULL, '2021-09-09 01:52:57'),
(178, 250, 'Letter', '2021-07-13', 'letter/letter_210816124300.pdf', '--', NULL, '--', NULL, 'Due', NULL, 0, NULL, '2021-08-16 07:43:51'),
(179, 252, 'Letter', '2021-7-14', NULL, '--', NULL, '--', NULL, 'Due', NULL, 0, NULL, NULL),
(180, 282, 'Certificate', '2021-07-23', NULL, '2021-8-13', 'receipt/receipt_210826012521.pdf', '2021-8-19', 'report/report_210827070257.pdf', 'Not Due', 'will Do On 13 th  august(9970871173-Ankita@1234)', 0, NULL, '2021-09-09 01:57:05'),
(181, 269, 'Certificate', '2021-07-17', NULL, '2021-07-19', 'receipt/receipt_210816125004.pdf', '2021-07-20', 'report/report_210906061222.pdf', 'Not Due', '9373702581-Gurubane@956', 0, NULL, '2021-09-09 01:53:37'),
(182, 281, 'Certificate', '2021-7-02', NULL, '2021-7-12', NULL, '2021-7-19', 'report/report_210816125138.pdf', 'Not Due', NULL, 0, NULL, NULL),
(183, 274, 'Letter', '2021-8-02', 'letter/letter_210816125253.pdf', '--', NULL, '--', NULL, 'Due', NULL, 0, NULL, NULL),
(184, 261, 'Certificate', '2021-07-14', NULL, '2021-07-16', NULL, '2021-07-16', 'report/report_210816125632.pdf', 'Not Due', '9067038348-Password-Omsai@123', 0, NULL, '2021-09-09 01:55:17'),
(185, 277, 'Certificate', '2021-07-17', NULL, '2021-8-03', NULL, '2021-9-16', 'report/report_211006050043.pdf', 'Not Due', '(ID-7620384174-Pass word-Omsai@123)', 0, NULL, '2021-10-06 00:00:43'),
(186, 276, 'Certificate', '2021-8-05', NULL, '2021-8-07', 'receipt/receipt_210816010041.pdf', '2021-8-07', 'report/report_210823070411.pdf', 'Not Due', 'M-Ringing(7887493913-Komal@123)', 0, NULL, '2021-09-09 01:56:34'),
(187, 266, 'Letter', '2021-07-21', NULL, '--', NULL, '--', NULL, 'Due', 'M-EXE will do till 16 th', 0, NULL, '2021-09-06 05:53:52'),
(189, 270, 'Untouched', '--', NULL, '--', NULL, '--', NULL, 'Due', NULL, 0, NULL, '2021-09-01 07:21:37'),
(190, 278, 'Receipt', '2021-8-16', NULL, '2021-8-16', 'receipt/receipt_210816011311.pdf', '--', NULL, 'Due', 'ID-8237140203-PASSWORD-Omsai@123', 0, NULL, '2021-09-06 06:16:46'),
(191, 279, 'Letter', '--', NULL, '--', NULL, '--', NULL, 'Due', NULL, 0, NULL, '2021-09-01 07:22:09'),
(192, 280, 'Receipt', '--', NULL, '2021-9-14', 'receipt/receipt_210914094610.pdf', '--', NULL, 'Due', '7972471063-Gaur@5742', 0, NULL, '2021-09-14 05:28:39'),
(193, 268, 'Certificate', '2021-9-11', NULL, '2021-9-13', 'receipt/receipt_210917081341.pdf', '2021-9-16', 'report/report_210917083226.pdf', 'Not Due', 'ID-9309969614-Omsai@123', 0, NULL, '2021-09-17 03:32:53'),
(194, 286, 'Certificate', '2021-07-16', 'letter/letter_210825124917.pdf', '2021-07-18', 'receipt/receipt_210825124917.pdf', '2021-8-14', 'report/report_210827065937.pdf', 'Not Due', '7057053857-Password-Omsi@123/Pass@123', 0, NULL, '2021-09-09 01:56:01'),
(196, 294, 'Certificate', '2021-06-19', NULL, '2021-06-19', NULL, '2021-06-23', 'report/report_210825014557.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 07:59:08'),
(197, 295, 'Certificate', '2021-05-28', NULL, '2021-05-28', NULL, '2021-06-05', 'report/report_210826050212.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 07:59:32'),
(198, 296, 'Certificate', '2021-06-01', NULL, '2021-06-02', NULL, '2021-06-04', 'report/report_210826050940.pdf', 'Not Due', NULL, 0, NULL, '2021-09-08 07:59:54'),
(199, 293, 'Receipt', '2021-8-11', NULL, '2021-8-18', 'receipt/receipt_210826053247.pdf', '--', NULL, 'Due', 'DOCUMENT ISSUE-RENT AGREEMENT(PROCESS TILL 1 ST AUG(9373736425-Gayatri@1234)/Remarks: upload rent agreement incomplete and staying period not sufficient', 0, NULL, '2021-09-14 03:07:51'),
(200, 287, 'Certificate', '2021-06-16', NULL, '2021-07-04', 'receipt/receipt_210826053448.pdf', '2021-07-18', 'report/report_210906060705.pdf', 'Not Due', 'M-Exe will Do Process on 5 th(7276055459-Pass@123)/M-Ringing', 0, NULL, '2021-09-09 01:23:54'),
(201, 297, 'Certificate', '2021-07-02', NULL, '2021-07-12', NULL, '2021-07-24', 'report/report_210826053624.pdf', 'Not Due', NULL, 0, NULL, '2021-09-09 01:24:19'),
(202, 285, 'Certificate', '2021-07-26', NULL, '2021-8-03', NULL, '2021-8-04', 'report/report_210826054019.pdf', 'Not Due', 'ID-9022216902-PASSWORD Omsai@123', 0, NULL, '2021-09-09 01:57:42'),
(203, 292, 'Certificate', '2021-06-04', NULL, '2021-06-05', NULL, '2021-06-05', 'report/report_210904105932.pdf', 'Not Due', 'On 4 th will Do Process again/Exe will go to police station for verification on 7 th(ID-9011091067-Password-Omsai@123)', 0, NULL, '2021-09-08 08:07:55'),
(204, 263, 'Receipt', '--', NULL, '2021-9-8', 'receipt/receipt_210908123125.pdf', '--', NULL, 'Due', 'M-will do till pvr process  till 1st Aug/ M-Switch Off(7517355602\r\nLeena@1234', 0, NULL, '2021-09-08 07:31:25'),
(205, 300, 'Letter', '2021-07-8', 'letter/letter_210914070210.pdf', '--', NULL, '--', NULL, 'Due', 'M-will do till pvr process  till 1st Aug(( Left job-sep 2021)', 0, NULL, '2021-09-14 02:02:48');
INSERT INTO `ptx_pvrs` (`id`, `user_id`, `pvr_status`, `date_of_letter`, `letter_img`, `date_of_receipt`, `receipt_img`, `date_of_report`, `report_img`, `due_for_renewal`, `remark`, `isDeleted`, `created_at`, `updated_at`) VALUES
(206, 303, 'Certificate', '2021-9-06', NULL, '2021-9-8', 'receipt/receipt_210909072909.pdf', '2021-9-13', 'report/report_210923050230.pdf', 'Not Due', '7977433813-Shubhada@2021', 0, NULL, '2021-09-23 00:02:30'),
(207, 304, 'Untouched', '--', NULL, '--', NULL, '--', NULL, '', NULL, 0, NULL, NULL),
(208, 309, 'Certificate', '2021-8-03', NULL, '2021-8-06', 'receipt/receipt_210906011155.pdf', '2021-9-19', 'report/report_210920051039.pdf', 'Not Due', 'ID-7040000900-Password-Omsai@123(He will go to police station for verification', 0, NULL, '2021-09-20 00:10:39'),
(209, 308, 'Receipt', '2021-8-05', NULL, '2021-9-07', 'receipt/receipt_210909073559.pdf', '--', NULL, NULL, '9822668001-Vaibhav@1234', 0, NULL, '2021-09-09 05:35:13'),
(210, 288, 'Certificate', '2021-2-12', NULL, '--', NULL, '2021-3-05', 'report/report_210908125332.pdf', 'Not Due', '2 Ref Required-On 20 th Visit POLICE STATION AT 6 O Clk(8369983572-Omsai@123)-On 9 th March will go toPolice station(9819788176-Omsai@123)', 0, NULL, NULL),
(211, 305, 'Certificate', '--', NULL, '--', NULL, '2021-8-10', 'report/report_210909062557.pdf', 'Not Due', NULL, 0, NULL, NULL),
(212, 313, 'Receipt', '--', NULL, '2021-7-28', 'receipt/receipt_210909071254.pdf', '--', NULL, '', 'RENT AGREEMENT ISSUE', 0, NULL, NULL),
(213, 311, 'Certificate', '--', NULL, '2021-9-14', 'receipt/receipt_210915071632.pdf', '2021-9-16', 'report/report_210925052043.pdf', 'Not Due', 'user id 8888889045\r\n\r\npass Sujit@2021', 0, NULL, '2021-09-25 00:20:43'),
(214, 316, 'Receipt', '2021-9-14', NULL, '2021-9-15', 'receipt/receipt_210916073821.pdf', '--', NULL, 'Due', 'USER ID  9021880306\r\n\r\nPASSWORD Raj@2021', 0, NULL, NULL),
(215, 312, 'Receipt', '--', NULL, '2021-9-24', 'receipt/receipt_210928090107.pdf', '--', NULL, 'Due', NULL, 0, NULL, '2021-09-28 04:01:07'),
(216, 318, 'Certificate', '2021-9-03', NULL, '2021-9-15', 'receipt/receipt_211006050423.pdf', '2021-9-16', 'report/report_211006050423.pdf', 'Not Due', 'USER NAME 9112847533\r\n\r\nPASSWORD- Akash@2021', 0, NULL, '2021-10-06 00:04:45');

-- --------------------------------------------------------

--
-- Table structure for table `ptx_user_details`
--

CREATE TABLE `ptx_user_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `area_id` int(10) UNSIGNED DEFAULT NULL,
  `comp_id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `blood_group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `covid_vaccination` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_leader` int(11) DEFAULT NULL,
  `date_of_joining` date DEFAULT NULL,
  `exit_document` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_leaving` date DEFAULT NULL,
  `current_address` text COLLATE utf8mb4_unicode_ci,
  `permanent_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_1_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_1_mobile` bigint(20) DEFAULT NULL,
  `reference_1_relation` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_2_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_2_mobile` bigint(20) DEFAULT NULL,
  `reference_2_relation` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fri_reference_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fri_reference_1_address` text COLLATE utf8mb4_unicode_ci,
  `fri_reference_1_mobile` bigint(20) DEFAULT NULL,
  `fri_reference_1_relation` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fri_reference_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fri_reference_2_address` text COLLATE utf8mb4_unicode_ci,
  `fri_reference_2_mobile` bigint(20) DEFAULT NULL,
  `fri_reference_2_relation` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ptx_user_details`
--

INSERT INTO `ptx_user_details` (`id`, `user_id`, `area_id`, `comp_id`, `branch_id`, `blood_group`, `covid_vaccination`, `team_leader`, `date_of_joining`, `exit_document`, `date_of_leaving`, `current_address`, `permanent_address`, `reference_1`, `reference_1_address`, `reference_1_mobile`, `reference_1_relation`, `reference_2`, `reference_2_address`, `reference_2_mobile`, `reference_2_relation`, `fri_reference_1`, `fri_reference_1_address`, `fri_reference_1_mobile`, `fri_reference_1_relation`, `fri_reference_2`, `fri_reference_2_address`, `fri_reference_2_mobile`, `fri_reference_2_relation`, `isDeleted`, `created_at`, `updated_at`, `updated_by`) VALUES
(3, 1, 94, 2, 2, 'AB+', 'No', 1, '2007-11-16', NULL, NULL, 'Pune, Maharashtra', 'Pune, Maharashtra', NULL, NULL, NULL, 'Relation', NULL, NULL, NULL, 'Relation', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-03-03 01:05:49', '2021-07-31 03:20:44', 1),
(20, 31, 1, 1, 1, 'A-', 'No', 1, '2006-05-15', NULL, NULL, 'Pune, Maharashtra', 'Pune, Maharashtra', NULL, NULL, NULL, 'Relation', NULL, NULL, NULL, 'Relation', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-04-05 00:09:43', '2021-04-05 00:09:43', 1),
(21, 32, 102, 2, 2, 'B+', 'No', 35, '2021-03-01', '', '2021-07-01', 'Manikbaug Sinhagad Road Pune-51', 'Manikbaug Sinhagad Road Pune-51', 'SHEETAL HIRALKAR', 'Manikbaug Sinhagad Road Pune-51', 8660507346, 'Relation', 'ATISH HIRALKAR', 'Manikbaug Sinhagad Road Pune-51', NULL, 'Relation', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-04-05 00:57:00', '2021-09-13 23:42:54', 1),
(22, 33, 102, 2, 2, 'AB+', 'Fully Vaccinated', 35, '2021-02-22', '', '2021-08-03', '482 SOMWAR PERTH DARUWALA POOL PUNE-411011', '482 SOMWAR PERTH DARUWALA POOL PUNE-411011', 'Amit Ladgaonkar', '482 SOMWAR PERTH DARUWALA POOL PUNE-411011', 9822636643, 'Spouse', 'ALKA LADGAONKAR', '482 SOMWAR PERTH DARUWALA POOL PUNE-411011', 7385629188, 'Mother', 'RASHMI TIKONE', '82/1 RAHUL APT , ARANYESHWAR PUNE 411009', 8999433987, 'FRIEND', 'PRACHI DHAMANE', '482 SOMWAR PERTH DARUWALA POOL PUNE-411011', 9823427767, 'FRIEND', 0, '2021-04-05 01:47:12', '2021-09-14 23:43:08', 1),
(23, 34, 4, 1, 1, 'O+', 'No', 32, '2007-05-09', NULL, NULL, 'Pune, Maharashtra', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-04-07 04:51:21', '2021-04-07 04:51:21', 1),
(24, 36, 102, 2, 2, 'O+', 'Fully Vaccinated', 35, '2005-06-01', NULL, NULL, 'S.NO.227/1,BLDG –CA-3 FLAT 17, RADHANAGARI COMPLEX BHOSARI PUNE-39', 'S.NO.227/1,BLDG –CA-3 FLAT 17, RADHANAGARI COMPLEX BHOSARI PUNE-39', NULL, NULL, NULL, 'Relation', NULL, NULL, NULL, 'Relation', 'RAMESH JADHAV', 'RADHANAGARI SOC BHOSARI PUNE 39', 9822843847, 'FRIEND', 'SANDIP POTAWADE', 'DIGHI ROAD ADARSH SOC BHOSARI PUNE 39', 9850555999, 'FRIEND', 0, '2021-04-07 05:52:44', '2021-10-18 06:10:13', 1),
(25, 37, 102, 2, 2, 'A+', 'Fully Vaccinated', 35, '2000-04-01', NULL, NULL, 'FL NO 27 SAI GANESH VIHAR SN-14/15/12/2 WADGAON BK PUNE 411 041', 'FL NO 27 SAI GANESH VIHAR SN-14/15/12/2 WADGAON BK PUNE 411 041', 'DATTARAM MAHADIK', 'SAI GANESH VIHAR C BILLDING 3 RD FLOOR ANAND NAGAR PUNE 41', 8308843022, 'Brother', 'AMOL MAHADIK', 'B-12/18 INDIRA NAGAR PUNE 37', 9923963605, 'Brother', NULL, NULL, NULL, 'BROTHER', NULL, NULL, NULL, NULL, 0, '2021-04-07 06:01:26', '2021-10-18 06:11:08', 1),
(26, 38, 102, 2, 2, 'AB+', 'Fully Vaccinated', 35, '2018-04-15', NULL, NULL, '287/ B, Ganj Peth, Harikrupa Appatment, Pune-411002.', '287/ B, Ganj Peth, Harikrupa Appatment, Pune-411002.', 'SANDEEP KARALI', '291 /92 GANJ PETH PUNE 2', 9921947067, 'Brother', 'NEETA VANGARI', 'BHAVANI PETH ,OPP VITTHAL MANDIR', 9921925450, 'Sister', 'RASHMI R TIKONE', '82/1,RAHUL APT,SECTOR NO-14,ARANESHWAR,PUNE-9', 9226134909, 'friend', 'MOKSHADA EKBOTE', 'UPPER INDIRA NAGAR,NEAR BUS DEPO,PUNE', 7875683787, 'SISTER', 0, '2021-04-08 01:02:47', '2021-09-14 23:45:47', 1),
(27, 39, 102, 2, 2, 'AB+', 'No', 32, '2020-08-10', NULL, NULL, 'BLDG NO M-16 T NO 542 M I G LAXMINAGAR PARVATI NR DATTAWADI POLICE STSTION PUNE-411009', 'BLDG NO M-16 T NO 542 M I G LAXMINAGAR PARVATI NR DATTAWADI POLICE STSTION PUNE-411009', 'NILESH UTARI', 'M16/536 DAVRAN SOC PARVATI SHAHU CALLAGE ROAD PUNE 411009', 9822251128, 'Brother', NULL, NULL, NULL, 'Relation', 'RAJENDRA BHOR', 'SR NO 93 LAXMINAGAR PARAVTI PUNE', 9822034613, 'friend', 'NILESH UTARI', 'M16/536 DAVRAN SOC PARVATI SHAHU CALLAGE ROAD PUNE 411009', 9822251128, 'friend', 1, '2021-04-08 01:06:19', '2021-08-27 08:38:53', 1),
(28, 40, 17, 2, 3, 'A-', 'No', 1, '2007-04-14', NULL, NULL, 'Pune, maharashtra', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-04-08 03:03:05', '2021-04-08 03:03:05', 1),
(29, 41, 102, 2, 2, 'AB+', 'Fully Vaccinated', 230, '2011-09-13', NULL, NULL, '1001 OLD WADARWADI OPP KUSALKAR BLDG SHIVAJI NAGAR PUNE-411016', '1001 OLD WADARWADI OPP KUSALKAR BLDG SHIVAJI NAGAR PUNE-411016', NULL, NULL, NULL, 'Relation', NULL, NULL, NULL, 'Relation', 'DATTU ORSE', '1001 OLD WADARWADI SHIVAJI NAGAR PUNE', 9371064219, 'FRIEND', 'BALRAJ DHALE', '1001 OLD WADAR WADI OPP KUSALKAR BLD SHIVAJINAGAR', 9850239519, 'FRIEND', 0, '2021-04-08 05:02:49', '2021-10-18 06:12:02', 1),
(30, 42, 102, 2, 2, 'O+', 'No', 230, '2013-07-01', NULL, NULL, '56 GHORPADI BAJAR PUNE CITY 411001', '56 GHORPADI BAJAR PUNE CITY 411001', 'DALPATRAJ KOTHARI', '56 GHORPADI BAZAR OPP POST OFF PUNE', 9423532838, 'Brother', NULL, NULL, NULL, 'Relation', 'SALIM NADAP', '168 RASTA PETH OPP AYYAPA MANDIR', 9552569035, 'FRIEND', NULL, NULL, NULL, NULL, 0, '2021-04-08 05:09:40', '2021-06-25 07:42:00', 1),
(31, 43, 102, 2, 2, 'O+', 'Partially Vaccinated', 230, '2017-07-10', NULL, NULL, '415,GURWAR PETH,NEAR KENCHU WEDI WORKERS,PUNE-42', '415,GURWAR PETH,NEAR KENCHU WEDI WORKERS,PUNE-42', 'MAHAVIR BHALERAO', '45/355 MAHARSHI NAGAR PUNE 37', 9028570663, 'Brother', 'HEMANT BHALERAO', '45/355,MAHARSHI NAGAR,NEAR BY ZAMBRE PALACE,PUNE-37', 8788482800, 'Brother', 'RAJESH KOLATE', 'GURUVAR PETH GAVRI ALI 415', 9850662296, 'FATHER IN LAW', 'MOHINI JADHAV', 'MUNDHWA,DHAYARKAR COLONY', 9146302877, 'SISTER', 0, '2021-04-08 05:53:13', '2021-10-18 06:12:29', 1),
(33, 45, 102, 2, 2, 'O+', 'Fully Vaccinated', 35, '2020-08-17', NULL, NULL, '82/1 RAHUL APPARTMENT SECTER 14 NEAR ARNEYESHWAR MANDIR ,ARANESHWAR,NEAR ICICI BANK,PARVATI PUNE-411009', '82/1 RAHUL APPARTMENT SECTER 14 NEAR ARNEYESHWAR MANDIR ,ARANESHWAR,NEAR ICICI BANK,PARVATI PUNE-411009', 'GAURAV C.KAMBLE', 'YASH TOURS & TRAVELLS,NEHARU CHOWK,KACHERI ROAD,RAJGURUNAGAR,KHED -410505', 9604656766, 'Brother', 'VIKESH CHANDRAKANT KAMBLE', 'NEHARU CHOWK,KACHERI ROAD,RAJGURUNAGAR,KHED -410505', 9082210570, 'Brother', 'SANDESH SHETTY', 'SANDY MOBILE,NEAR PRABHAT THEATER,SHANIWAR PETH', 9860611114, 'FRIEND', 'PRADNYA GAWANDE', 'Swami Samarth soc,Anand nagar,Pune', 9403924820, 'FRIEND', 0, '2021-04-08 06:26:10', '2021-10-09 01:12:13', 1),
(36, 48, 102, 2, 2, 'O+', 'Fully Vaccinated', 35, '2017-12-01', NULL, NULL, '719 GURUWAR PETH KHADAK MAL ALI RATHI WADA PUNE', '719 GURUWAR PETH KHADAK MAL ALI RATHI WADA PUNE', 'KUNDAN SALUNKE', '719 GURUVAR PETH KHADAK MAL ALI RATHI WADA PUNE 42', 9922735757, 'Brother', 'ABHISHEK MANE', '719 GURUWAR PETH KHADAK MAL ALI RATHI WADA PUNE', 7385622856, 'Relation', 'UTTAM MALI', 'SR NO 131 KATRAJ GAON PUNE SATARA ROAD GHOLAP BULDING FL NO 104 PUNE', 9850728849, 'FRIEND', 'SANDIP KHULE', 'DHAYARI PUNE', 7066482278, 'FRIEND', 0, '2021-04-09 01:58:50', '2021-09-11 08:18:07', 1),
(37, 49, 102, 2, 2, 'O+', 'Fully Vaccinated', 32, '2011-09-13', NULL, NULL, 'S.NO. 1360 PLOT NO-15 UBALE NAGAR BIHND TVS GODOWN DR. KOLTE PARK HOUSING SOC, AT POST WAGHOLI PUNE', 'S.NO. 1360 PLOT NO-15 UBALE NAGAR BIHND TVS GODOWN DR. KOLTE PARK HOUSING SOC, AT POST WAGHOLI PUNE', 'OMPRAKASH BHAJANE', 'AT POST MAHAGAON TALUKA-ARJUNI MORGAON DIST -GONDIYA', 9637306017, 'Brother', 'SHARDA RAMDASH BHAJANE', 'S.NO. 1360 PLOT NO-15 UBALE NAGAR BIHND TVS GODOWN DR. KOLTE PARK HOUSING SOC, AT POST WAGHOLI PUNE', 9156608169, 'Mother', 'KAILAS PATALE', 'C/O TANAJI EKNATH GAIKWad ,ATHARWA PALACE BEHIND PHC RASOINI COLLEGE ROAD , WAGHOLI PUNE', 9921217837, 'Friends', 'AKASH BADHE', '411 RASTA PETH NEAR ORINILAS HIGS SCHOOL GHOD MALA PUNE 411011', 9011119900, 'Friends', 0, '2021-04-09 02:01:14', '2021-09-14 23:45:11', 1),
(38, 50, 102, 2, 2, 'B+', 'Fully Vaccinated', 35, '2019-04-12', NULL, NULL, 'SARVE NO 13 FLAT NO 34 DHAMALE COMPEX SUKHASAGAR NAGAR KATRAJ PUNE-411046', 'SARVE NO 13 FLAT NO 34 DHAMALE COMPEX SUKHASAGAR NAGAR KATRAJ PUNE-411046', 'VINAYAK MORE', 'FLAT NO 35 DKAMALE COMPLEX KATRAJ PUNE', 9527001741, 'Brother', 'KUMAR MORE', 'DANDEKAR PUL SINHAGAD ROAD PUNE', 8605462695, 'Brother', 'SUKUMAR MORE', 'DECCAN APTE ROAD JEEVAN APT PUNE', 9545503045, 'FRIEND', 'AKASH CHAVAN', 'PMC COLONY PUNE', 7276879593, 'FRIEND', 0, '2021-04-09 02:05:52', '2021-09-15 00:07:17', 1),
(39, 51, 102, 2, 2, 'B+', 'No', 39, '2020-08-07', NULL, NULL, '648/2 SARGAM VILAS UPPER INDIRA NAGAR  DATTA NAFARBIBAWEADI PUNE-411007', '648/2 SARGAM VILAS UPPER INDIRA NAGAR  DATTA NAFARBIBAWEADI PUNE-411007', NULL, NULL, NULL, 'Relation', NULL, NULL, NULL, 'Relation', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-04-09 02:13:46', '2021-05-07 08:08:11', 1),
(40, 52, 102, 2, 2, 'B+', 'No', 39, '2016-11-17', NULL, NULL, 'CTS NO 395 A NANA PETH GRACE GARDAN NR DARSHAN HALL PUNE-411002', 'CTS NO 395 A NANA PETH GRACE GARDAN NR DARSHAN HALL PUNE-411002', 'ABHAY WAGHMARE', 'CTS NO 395 A NANA PETH GRACE GARDAN NR DARSHAN HALL PUNE-411002', 9970574304, 'Brother', NULL, NULL, NULL, 'Relation', 'PRATIKSHA SAKAT', '395 NANA PETH NR GAYANI MOTORS PUNE 411002', 8412087715, 'FRIEND', NULL, NULL, NULL, NULL, 0, '2021-04-09 02:20:44', '2021-06-25 08:00:43', 1),
(41, 53, 98, 2, 2, 'B+', 'No', 35, '2020-02-25', NULL, NULL, 'SEC28 PLOT 122 NIGADI PRADHIKARAN PUNE', 'SEC28 PLOT 122 NIGADI PRADHIKARAN PUNE', 'NITIN BARKE', 'SEC28 PLOT 122 NIGADI PRADHIKARAN PUNE', 9404963200, 'Spouse', NULL, NULL, NULL, 'Spouse', 'NEETA BADHE', '413 RASTA PETH QUARTER GATE PUNE 411011', 9404962525, 'FRIEND', NULL, NULL, NULL, NULL, 0, '2021-04-09 02:24:48', '2021-09-11 07:30:22', 1),
(42, 54, 102, 2, 2, 'B+', 'No', 32, '2020-11-10', NULL, NULL, '425 2 TMV COLONY SAI PRADAD BULIDING BEHIND BANK OF MAGARASHTRA MUKUND NAGAR PUNE 411037', '425 2 TMV COLONY SAI PRADAD BULIDING BEHIND BANK OF MAGARASHTRA MUKUND NAGAR PUNE 411037', 'TUSHAR SHETTY', 'PURAM SOCTY FLAT NO 160 NEAR SHELL PETROL PUMP PUNE 58', 7057457455, 'Brother', NULL, NULL, NULL, 'Relation', 'GANESH BADHE', '413 RASTA PETH QUARTER GATE PUNE 411011', 9922981111, 'FRIEND', NULL, NULL, NULL, NULL, 1, '2021-04-09 02:31:34', '2021-09-11 07:22:37', 1),
(43, 55, 102, 2, 2, 'B+', 'Partially Vaccinated', 38, '2021-02-08', NULL, NULL, 'Eat & repeat Hotel Shivaji Nagar Pune', 'bhusani, taluka Umarga, Dist. Osmanabad', 'Arjun Dudhbhate', 'bhusani,taluka Umarga, Dist Osmanabad', 9373575008, 'Brother', 'SUNIL DUDHBHATE', 'bhusani,taluka Umarga, Dist Osmanabad', 8459994269, 'Brother', 'Laxman Gadekar', 'bhusani,taluka Umarga, Dist Osmanabad', 8830482705, 'FRIEND', 'LAXMAN GADEKAR', 'PUNE', 8830482705, 'FRIEND', 0, '2021-04-09 02:43:47', '2021-09-11 08:06:21', 1),
(44, 56, 102, 2, 2, 'A+', 'No', 36, '2020-11-01', NULL, NULL, 'FLAT NO 5 SR NO 90 KALAS GAON VISHAL PARISAR  NEAR DATTA MANDIR VISHARANTWADI PUNE-411015', 'FLAT NO 5 SR NO 90 KALAS GAON VISHAL PARISAR  NEAR DATTA MANDIR VISHARANTWADI PUNE-411015', 'ANAND BHOSALE', 'C-207 PURPUL PARK DIGHI PUNE 411015', 9272668742, 'Brother', 'RAJESHREE', 'PUNE', 9527106736, 'Mother', 'G BEEN', 'SHREE KRUPA RESODENCY DHANKAWADI PUNE', 9890777222, 'FRIEND', 'RAJARAM', 'PUNE', 7887983238, 'FATHER', 0, '2021-04-09 02:47:18', '2021-07-07 07:24:48', 1),
(46, 58, 94, 2, 3, 'O+', 'No', 1, '2007-02-08', '', '2021-08-01', 'Pune,  Maharashtra', 'Pune,  Maharashtra', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-04-09 04:02:02', '2021-07-30 06:12:16', 1),
(47, 59, 102, 2, 2, 'B+', 'No', 43, '2020-12-29', NULL, NULL, '1113 SADASHIV PETH JADHAV WADA PUNE 411030', '1113 SADASHIV PETH JADHAV WADA PUNE 411030', 'CAREER ENTERPRISES', 'SHIVAJI ROAD PUNE SWARGET CHOWK PUNE', 9822742871, 'Relation', NULL, NULL, NULL, 'Relation', 'PRABHAKAR BHOGALE', 'SHIVAJI ROAD PUNE SWARGET CHOWK PUNE', 7588901193, 'father', NULL, NULL, NULL, NULL, 0, '2021-04-09 04:25:51', '2021-06-26 00:27:49', 1),
(48, 60, 94, 2, 3, 'AB-', 'No', 60, '2004-08-16', '', '2021-07-31', 'Pune, Maharashtra', 'Pune, Maharashtra', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-04-09 04:28:59', '2021-07-30 06:08:51', 1),
(49, 61, 96, 2, 2, 'AB+', 'Partially Vaccinated', 61, '2005-05-05', '', '2021-08-02', '413/3, RASTA PETH NR ORNELLAS HIGH SCHOOL PUNE 411011', '413/3, RASTA PETH NR ORNELLAS HIGH SCHOOL PUNE 411011', 'GANESH BADHE', '413/3,RASTA PETH', 9922981111, 'Brother', 'SACHIN BADHE', '413/3,RASTA PETH', 7620160449, 'Brother', 'PRITAM KALE', 'MANGALWAR PETH', 9922369052, 'FRIEND', 'HEMANT PINJAN', '411 RASTA PETH PUNE 411011', 9960783040, 'FRIEND', 0, '2021-04-09 04:38:45', '2021-09-14 01:13:07', 1),
(50, 62, 94, 2, 2, 'B+', 'No', 62, '2015-01-01', '', '2021-08-10', 'SECT NO-28,PLOT NO 122, PRADHIKARAN PUNE 44,', 'SECT NO-28,PLOT NO 122, PRADHIKARAN PUNE 44,', 'VISHVANATH BARKE', 'SECT NO-28,PLOT NO 122, PRADHIKARAN PUNE 44,', 7745895566, 'Father', 'PRIYANKA BARKE', NULL, 9403979717, 'Spouse', 'PRASHANT SHINDE', 'AKURDI GAVTHAN PUNE', 9860745043, 'FRIEND', 'MANDAR DHORE', 'PLOT NO 48 SNO 28 NIGADI PUNE', 9168603131, 'FRIEND', 1, '2021-04-10 01:46:52', '2021-08-03 05:23:44', 1),
(51, 63, 99, 2, 2, 'A+', 'Partially Vaccinated', 36, '2016-06-08', NULL, NULL, 'SE NO 44 ERANDAWANA HANUMAN NAGAR NEAR MORE VIDYALAY PUNE-411004', 'SE NO 44 ERANDAWANA HANUMAN NAGAR NEAR MORE VIDYALAY PUNE-411004', 'PRAMILA KUDALE', 'SE NO 44 ERANDAWANA HANUMAN NAGAR NEAR MORE VIDYALAY PUNE-411004', 9763520992, 'Sister', 'KALPANA KUDALE', 'SE NO 44 ERANDAWANA HANUMAN NAGAR NEAR MORE VIDYALAY PUNE-411004', 8623098532, 'Sister', 'SHAILESH BARE', 'SR NO 44 KELEWADI PAUD RAOD PUNE', 9881771254, 'FRIEND', 'NAVNATH HULAWALE', 'SHRI SIDDHI VINAYAK APP SANAS NAGAR BHUGAV PUNE-', 9850903212, 'FRIEND', 0, '2021-04-10 01:51:18', '2021-10-20 04:46:26', 1),
(52, 64, 101, 2, 2, 'B+', 'Partially Vaccinated', 36, '2014-05-05', NULL, NULL, 'AT POST ,NALDURG, TAL TULJAPUR JILLHA OSMANABAD NEAR RELIANCE TOWER PUNE-413602', 'AT POST ,NALDURG, TAL TULJAPUR JILLHA OSMANABAD NEAR RELIANCE TOWER PUNE-413602', 'ASHOK MANE', 'TULJAPUR', 7218353572, 'Father', 'LALITA MANE', 'TULJAPUR', 9011157230, 'Mother', 'SOMNATH TAMBILE', 'AP NALDURGA TAL TULJAPUR DIST OSMANABAD', 8087866419, 'FRIEND', 'BABU CHOPDE', 'AP NALDURGA TAL TULJAPUR DIST OSMANABAD', 9637321347, 'FRIEND', 0, '2021-04-10 01:53:16', '2021-10-20 04:47:07', 1),
(53, 65, 97, 2, 2, 'O+', 'Partially Vaccinated', 36, '2018-05-15', NULL, NULL, '314, Somwar Peth, J.K. Classic, Bldg, B-opp-Shahu Udyan, Pune: 11', '314, somwar peth, J.K. Classic, Bldg, B-opp-Shahu Udyan, Pune : 11', 'CHETRALI SHINDE', '314, Somewhat Peth, J.K. Classic, Bldg, B-opp-Shahu Udyan, Pune: 11', 8983448076, 'Spouse', NULL, '314, Somewhat Peth, J.K. Classic, Bldg, B-opp-Shahu Udyan, Pune: 11', 9146928841, 'Spouse', 'ATUL KANBARGE', 'SOMWAR PETH PUNE 11', 8446663332, 'FRIEND', 'AMOL KADAM', 'SOMWAR PETH PUNE 11', 9890999880, 'FRIEND', 1, '2021-04-10 01:54:54', '2021-10-20 04:44:47', 1),
(54, 66, 102, 2, 2, 'A+', 'Partially Vaccinated', 36, '2008-02-13', NULL, NULL, '410,CENTER STRRET , BHOPAL CHOWK CAMP PUNE -1, OPP AK JEWELERS', '410,CENTER STRRET , BHOPAL CHOWK CAMP PUNE -1, OPP AK JEWELERS', 'MAYURESH PAWAR', '410,CENTER STRRET , BHOPAL CHOWK CAMP PUNE -1, OPP AK JEWELERS', 9881324561, 'Spouse', 'GEETALI YADAV', 'RADHA NAGARI HOUSING SOC BHOSARI , PUNE,', 9850455552, 'Spouse', 'ASHOK AVHALE', '433 SOMWAR PETH BARKE ALI PUNE', 9922279441, 'FRIEND', 'ANANT YADAV', 'SANT TUKARAM NAGAR RADHA NAGARI BHOSARI PUNE', 9881000356, 'FRIEND', 0, '2021-04-10 01:56:49', '2021-10-20 04:59:52', 1),
(55, 67, 95, 2, 2, 'B+', 'No', 36, '2014-04-03', NULL, NULL, 'H NO 8 RANJE TAL BHOR DIST PUNE 412210', 'H NO 8 RANJE TAL BHOR DIST PUNE 412210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'BALASAHEB GHATE', 'AT POST TAL HAVELI DIT PUNE 412205', 9881046105, 'FRIEND', 'GANESH MORE', 'AT POST SHRIRAM NAGAR TAL HAVELI DIST PUNE 412205', 8605403952, 'FRIEND', 1, '2021-04-10 01:59:09', '2021-10-08 05:46:52', 1),
(56, 68, 98, 2, 2, 'B+', 'No', 36, '2015-08-15', NULL, NULL, 'NEAR GANPATI MANDIR AT/POST WADGAON 134/2 KASHIMBEG TEL- AMBEGAON PUNE-410503', 'NEAR GANPATI MANDIR AT/POST WADGAON 134/2 KASHIMBEG TEL- AMBEGAON PUNE-410503', 'SURESH TARU', 'NEAR GANPATI MANDIR AT/POST WADGAON 134/2 KASHIMBEG TEL- AMBEGAON PUNE-410503', 9766837583, 'Father', 'RAMESH TARU', 'VADGAON KASHIMBEB TAL ABEGAON , NR ASHTAVINAYAK MANDIR PUNE-410503', 9850907528, 'Brother', 'GAJANAN NIRVEAL', 'DATTAWADI SONWANE APT, 1ST FLOOR PUNE', 9890673882, 'FRIEND', 'DIVYA TARU', 'PUNE', 9766827582, 'WIFE', 1, '2021-04-10 07:25:31', '2021-10-20 05:02:17', 1),
(57, 69, 94, 2, 2, 'O', 'Fully Vaccinated', 36, '2014-05-02', NULL, NULL, 'KONDHANPUR ROAD AT RANJE POST ARVI TAL BHOR ARVI HAVELI PUNE 412205', 'KONDHANPUR ROAD AT RANJE POST ARVI TAL BHOR ARVI HAVELI PUNE 412205', 'GANESH MORE', 'AT POST HAVELI DIST PUNE 412205', 8605403952, 'Brother', 'BABASAHEB KONDE', 'AT-RANJE,TAL-BHOR,NEAR KHED SHIVAPUR PUNE-412205', 8308837198, 'Father', 'BALASAHEB GHATE', 'AT POST HAVELI DIST PUNE 412205', 9881046105, 'FRIEND', 'BALASAHEB KONDE', 'AT POST HAVELI DIST PUNE 412205', 9850547108, 'BROTHER', 0, '2021-04-10 07:29:15', '2021-10-14 01:38:46', 1),
(58, 70, 98, 2, 2, 'B+', 'Partially Vaccinated', 70, '2015-09-01', NULL, NULL, 'BANER GAON 163 3 A NEAR DATTA NAGAR, PRIME ROAD MALL PUNE', 'BANER GAON 163 3 A NEAR DATTA NAGAR, PRIME ROAD MALL PUNE', 'MANOHAR NARAYAN BARKE', 'BANER GAON 163 3 A NEAR DATTA NAGAR, PRIME ROAD MALL PUNE', 9075097248, 'Father', 'NITIN BARKE', 'BANER GAON 163 3 A NEAR DATTA NAGAR, PRIME ROAD MALL PUNE', 9404963200, 'Brother', 'PRASHANT SHINDE', 'AKURDI GAVTHAN PUNE', 9860745043, 'FRIEND', 'MANDHAR DHORE', 'PLOT NO 48 SNO 28 NIGADI PUNE', 9767204959, 'FRIEND', 0, '2021-04-10 07:30:45', '2021-08-19 05:35:24', 1),
(59, 71, 94, 2, 2, 'B+', 'No', 36, '2015-07-15', NULL, NULL, '251,MANGALWAR PETH INDIRA NGR PUNE 11 SHRI KRUSHNA MANDAL NR BHUTADA KIRANA STORE', '251,MANGALWAR PETH INDIRA NGR PUNE 11 SHRI KRUSHNA MANDAL NR BHUTADA KIRANA STORE', 'DIPIKA KALE', '251,MANGALWAR PETH INDIRA NGR PUNE 11 SHRI KRUSHNA MANDAL NR BHUTADA KIRANA STORE', 9822677965, 'Spouse', 'JITENDRA KALE', '251,MANGALWAR PETH INDIRA NGR PUNE 11 SHRI KRUSHNA MANDAL NR BHUTADA KIRANA STORE', 9767582228, 'Brother', 'SANTOSH MAZIRE', '251 MANGALWAR PETH OUNE 411011', 9823554129, 'FRIEND', 'SACHIN AAGLAVE', '251 MANGALWAR PETH OUNE 411011', 9881031301, 'FRIEND', 0, '2021-04-10 07:31:56', '2021-07-01 00:34:28', 1),
(60, 72, 98, 2, 2, 'B+', 'No', 72, '2016-03-03', NULL, NULL, 'SR NO 169/3/1 PAWAR WASTI TATHWADE PUNE-411033', 'SR NO 169/3/1 PAWAR WASTI TATHWADE PUNE-411033', 'PRAKASH SHINGE', NULL, 8485840340, 'Brother', 'DINESH SHINGE', NULL, 9623008024, 'Brother', 'SURESJ DISALE', 'OLD MUMBAI PUNE HIGHWAY NR NARSINGH  MANDIR TATHWADE PUNE', 7744934949, 'FRIEND', 'SAGAR SONWANE', 'OLD MUMBAI PUNE HIGHWAY NR NARSINGH  MANDIR TATHWADE PUNE', 9850506072, 'FRIEND', 0, '2021-04-10 07:33:52', '2021-06-30 02:19:24', 1),
(61, 73, 98, 2, 2, 'B+', 'No', 36, '2015-01-01', NULL, NULL, 'NEAR SURBHI GENERAL STORES B-41 DALVINAGAR CHINCWAD PUNE-411033', 'NEAR SURBHI GENERAL STORES B-41 DALVINAGAR CHINCWAD PUNE-411033', 'TAI THOSAR', 'DALVI NAGAR CHINCHWAD PUNE', 7030472112, 'Spouse', 'DATTATRAYA THOSAR', 'TRIVENI NAGAR CHINCHWAD PUNE', 9765843560, 'Brother', 'LAXMAN THOSAR', 'NEAR SURBHI GENERAL STORES B-41 DALVINAGAR CHINCWAD PUNE-411033', 9881237488, 'FATHER', 'TIPPANA NIKAM', 'KALEWADI RAHATNE NR KUNAL HOTEL , PUNE', 9765161936, 'FRIEND', 0, '2021-04-10 07:37:08', '2021-10-20 04:45:52', 1),
(62, 74, 102, 2, 2, 'AB-', 'Partially Vaccinated', 36, '2016-04-05', NULL, NULL, 'NEAR AJAD ALI 622 RASTA PETH PRASANT HIGHTS PUNE 411002', 'NEAR AJAD ALI 622 RASTA PETH PRASANT HIGHTS PUNE 411002', 'AARTI GAIKWAD', 'RASTA PETH', 992281522, 'Sister', 'DEEPALI GAIKWAD', 'RASTA PETH', 7773995669, 'Mother', 'DURGA BHOSALE', 'SOMWAR PETH PUNE 411011', 976634705, 'FRIEND', 'SAYLI DARAWDE', 'KASBA PETH  PUNE 411011', 9604374671, 'FRIEND', 0, '2021-04-10 07:39:37', '2021-10-20 05:05:36', 1),
(63, 75, 97, 2, 2, 'B+', 'Fully Vaccinated', 75, '2016-11-17', NULL, NULL, 'H BUILDING FLAT NO 405, UNITY PARK KONDHWA KHURD PUNE -48', 'H BUILDING FLAT NO 405, UNITY PARK KONDHWA KHURD PUNE -48', 'AJAY LODHI', 'FL NO 405 BLD H UNITY PARK SN 43 KONDHWA KHURD OUNE 411048', 9371012241, 'Brother', 'SANGITA LODHI', 'FL NO 405 BLD H UNITY PARK SN 43 KONDHWA KHURD OUNE 411048', 7385806535, 'Sister', 'AMAN LODHI', 'UNITY PARK FLAT NO 607 KONDHWA KHURD PUNE 411048', 9527128512, 'FRIEND', 'HANUMANT KORE', 'FLAT NO 406 UNITY PARK KONDHWA KHURD PUNE 48', 7972621179, 'FRIEND', 0, '2021-04-10 07:41:02', '2021-09-16 00:29:50', 1),
(64, 76, 102, 2, 2, 'B+', 'No', 36, '2016-02-01', NULL, NULL, '312,MANGALWAR PETH,PUNE', '312,MANGALWAR PETH,PUNE', 'SURESH RANPISE', '312,MANGALWAR PETH,PUNE', 8308924899, 'Father', 'NIKHIL RANPISE', NULL, 8796140578, 'Brother', 'MAHENDRA GAIKWAD', '312 MANGALWAR PETH PUNE NEAR SHANKAR TEMPLE', 8208402646, 'FRIEND', 'SMITA SHETRE', 'SWARGET\r\n PUNE', 9096292623, 'FRIEND', 0, '2021-04-10 07:42:20', '2021-08-28 05:02:23', 1),
(65, 77, 102, 2, 2, 'AB-', 'Partially Vaccinated', 36, '2017-05-18', NULL, NULL, '433/E SOMWAR PETH BARKE ALI PUSHKRAJ APARTMENT PUNE-411001', '433/E SOMWAR PETH BARKE ALI PUSHKRAJ APARTMENT PUNE-411001', 'ANANT YADAV', 'SNO 227/1 BLG CA 3FLAT 17 RADHANAGARI COMPLEX BHOSARI PUNE 411039', 9881000356, 'Brother', 'SANTOSH DHUMAL', '433/F SOMWAR PETH NARPATGIRI CHOWK PUSHKARAJ APP BARKE ALI PUNE', 9689929514, 'Father', 'JYOTI DHUMAL', NULL, 9130429330, 'MOTHER', 'JITENDRA KALE', '251-MANGALWAR PETH SHRIKRUSHNA CHOWK INDIRANAGAR PUNE 11', 9767582228, 'FRIEND', 0, '2021-04-10 07:45:19', '2021-09-01 02:40:37', 1),
(66, 78, 102, 2, 2, 'B+', 'Partially Vaccinated', 36, '2017-07-07', NULL, NULL, '413, GANESH PETH NR GURUDWARA VITEKAR HEIGHTS FLAT NO 11, PUNE 411002', '413, GANESH PETH NR GURUDWARA VITEKAR HEIGHTS FLAT NO 11, PUNE 411002', 'SURAJ J JOSHI', 'SAI KRISHNA SOCIETY NEAR SHANTI NAGAR  FLAT NO A-101 GANGADHAM PUNE 411048', 8983747666, 'Spouse', 'HEENA GOHIL', 'VADGOAN SHERI , ANAD PARK BUS STOP RAJENDRA NAGAR PUNE', 8421691987, 'Spouse', 'KHUSHBO CHAJED', '67 A SOMWAR PETH OPP NAGESHWAR MANDIR DATT MAULI SOCIETY PUNE 411011', 7276764171, 'FRIEND', 'HEENA SUTHER', 'KASBA PETH 1469, DARUWALA POOL OPP BALAJI MANDIR PUNE 411011', 8240000000, 'FRIEND', 0, '2021-04-10 07:51:17', '2021-10-20 05:06:38', 1),
(67, 79, 97, 2, 2, 'B+', 'Partially Vaccinated', 36, '2017-07-25', NULL, NULL, 'NEW STAR BEKARI KOREGAON WADHU ROAD TAL SHIRUR KOREGAON BHIMA PUNE- 412216', 'NEW STAR BEKARI KOREGAON WADHU ROAD TAL SHIRUR KOREGAON BHIMA PUNE- 412216', 'ABBAS HUNDEKARI', 'KOREGAON BHIMA TAL SHIRUR DIST PUNE', 9922863795, 'Father', 'AFREEN HUNDAKARI', 'NEW STAR BEKARI KOREGAON WADHU ROAD TAL SHIRUR KOREGAON BHIMA PUNE- 412216', 9834132611, 'Spouse', 'SANTOSH PAWAR', 'KOREGAON BHIMA TAL SHIRUR DIST PUNE', 9049346005, 'FRIEND', 'FIROZ HUNDEKARI', '223, GURUWAR PETH NR MIRZA MASJID PUNE', 8668287141, 'BROTHER', 0, '2021-04-10 07:55:29', '2021-09-03 03:13:26', 1),
(68, 80, 96, 2, 2, 'O+', 'Partially Vaccinated', 36, '2020-03-19', NULL, NULL, '648 BHAWANI PETH SIDHHARTH NAGAR BEHIND BSNL OFFICE PUNE-411042', '648 BHAWANI PETH SIDHHARTH NAGAR BEHIND BSNL OFFICE PUNE-411042', 'LAXMI SURESH MISAL', '648 BHAWANI PETH SIDHHARTH NAGAR BEHIND BSNL OFFICE PUNE-411042', 9370651131, 'Mother', 'SURESH BABAJI MISAL', '648 BHAWANI PETH SIDHHARTH NAGAR BEHIND BSNL OFFICE PUNE-411042', 8412088970, 'Father', 'ABHIJEET JADHAV', '648 BHAVANI PETH SIDDHARTH NGR PUNE', 7798422234, 'FRIEND', 'TUSHAR VAIRAT', 'SIDDHARTH NAGAR BHAVANI PETH PUNE', 9588633266, 'FRIEND', 0, '2021-04-10 07:58:03', '2021-10-20 05:07:05', 1),
(69, 81, 96, 2, 2, 'O+', 'Partially Vaccinated', 36, '2017-10-01', NULL, NULL, '715, GURUWAR PETH, KHADAKMAL ALI, GANPATI SAMORE, PUNE 42', '715, GURUWAR PETH, KHADAKMAL ALI, GANPATI SAMORE, PUNE 42', 'MANOJ KOMKAR', '715 GURUWAR PETH KHADAKMAL ALI PUNE', 9901803968, 'Father', 'VAISHALI KOMKAR', '553 GANESH PETH DHOR GALLI  NR.BORA HOSPITAL PUNE', 9767777366, 'Mother', 'GANESH HOLE', '715 GURUWAR PETH KHADAKMAL ALI PUNE', 9860271783, 'FRIEND', 'VAISHNAVI KOMKAR', '553 GANESH PETH DHOR GALLI  NR.BORA HOSPITAL PUNE', 7620168969, 'WIFE', 0, '2021-04-10 07:59:41', '2021-10-20 05:07:57', 1),
(70, 82, 99, 2, 2, 'B+', 'Fully Vaccinated', 36, '2018-06-19', NULL, NULL, 'Anant Prabha Construction, C Building, B wing, 5th floor, Flat No : 19, Survey no 78, NDA Rd, Near AdityaHotel, Shivane, Ahire, Pune : 411023', 'Anant Prabha Construction, C Building, B wing, 5th floor, Flat No : 19, Survey no 78, NDA Rd, Near AdityaHotel, Shivane, Ahire, Pune : 411023', 'SHEETAL DATTATRAY KANDHARE', 'Anant Prabha Construction, C Building, B wing, 5th floor, Flat No : 19, Survey no 78, NDA Rd, Near AdityaHotel, Shivane, Ahire, Pune : 411023', 7722034937, 'Spouse', 'YOGESH DASHRATH KANDHARE', 'Anant Prabha Construction, C Building, B wing, 5th floor, Flat No : 19, Survey no 78, NDA Rd, Near AdityaHotel, Shivane, Ahire, Pune : 411023', 8329405608, 'Spouse', 'SHAILESH BARE', 'SR NO 44 KALEWADI SERVICE MAN COLONY NEAR WATER TANK', 9623211555, 'FRIEND', 'NAVNATH HULAWALE', 'SANAS NAGAR BHUGAV 415112', 9850903212, 'FRIEND', 0, '2021-04-10 08:01:04', '2021-09-15 00:10:04', 1),
(71, 83, 95, 2, 2, 'B+', 'Partially Vaccinated', 83, '2018-10-01', NULL, NULL, 'A22,/20/, 276, Aota, Upper Indira Nagar, Bibewadi MARKET YARD Pune -411037', 'A22,/20/, 276, Aota, Upper Indira Nagar, Bibewadi MARKET YARD Pune -411037', 'SUDHIR GAIKWAD', 'A22,/20/, 276, Aota, Upper Indira Nagar, Bibewadi MARKET YARD Pune -411037', 9850759296, 'Father', 'SUDHIR OHAL', 'A22,/20/, 276, Aota, Upper Indira Nagar, Bibewadi MARKET YARD Pune -411037', 9325986072, 'Brother', 'SHEKHAR KHIRSAGAAR', 'A19/23 UPPER INDRA NAGAR BIBEWADI PUNE', 9637152242, 'FRIEND', 'ANIL SAPKAL', 'A22/23 UPPER INDRA NAGAR BIBEWADI PUNE', 9850759296, 'FRIEND', 0, '2021-04-10 08:03:35', '2021-09-13 01:35:49', 1),
(72, 84, 99, 2, 2, 'B+', 'Fully Vaccinated', 36, '2018-12-07', NULL, NULL, 'MNO 173  NANDGAON TAL MULSHI DIST PUNE-412108', 'MNO 173  NANDGAON TAL MULSHI DIST PUNE-412108', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'NAVNATH KUDALE', 'AT POST BHUGAO TEL MULSHI PUNE 9850903212', 9850903212, 'FRIEND', 'PRAMOD FALE', 'NO 173 NANDGAO TAL MULSHI DIST PUNE', 9921430357, 'FRIEND', 0, '2021-04-10 08:09:27', '2021-09-15 00:08:19', 1),
(73, 85, 103, 2, 2, 'B+', 'No', 36, '2019-09-04', NULL, NULL, 'SAI KUNJ AAPT, FLAT NO 18 GOPALWADI ROAD  BEHAIND MANE HOSPITAL SARPANCH VASTI DAUND PUNE-413801', 'SAI KUNJ AAPT, FLAT NO 18 GOPALWADI ROAD  BEHAIND MANE HOSPITAL SARPANCH VASTI DAUND PUNE-413801', 'GANESH BESALE', 'SAI KUNJ AAPT, FLAT NO 18 GOPALWADI ROAD  BEHAIND MANE HOSPITAL SARPANCH VASTI DAUND PUNE-413801', 9970960966, 'Brother', NULL, NULL, NULL, NULL, 'PRASHANT NETWATE', 'SARPANCH WASTI GOPALWADI ROAD DAUND', 8888337111, 'FRIEND', NULL, NULL, NULL, NULL, 0, '2021-04-10 08:13:45', '2021-06-26 01:47:10', 1),
(74, 86, 98, 2, 2, 'A+', 'Partially Vaccinated', 36, '2019-11-06', NULL, NULL, 'SR NO 77/1 SAHAKAR COLONY JYOTIBA NAGAR KALEWADI PRIMPRI PUNE-411017', 'SR NO 77/1 SAHAKAR COLONY JYOTIBA NAGAR KALEWADI PRIMPRI PUNE-411017', 'RAJARAM JADHAV', 'SR NO 77/1 SAHAKAR COLONY JYOTIBA NAGAR KALEWADI PIMPRI PUNE-411017', 9922504960, 'Father', 'RAJASHREE JADHAV', 'SR NO 77/1 SAHAKAR COLONY JYOTIBA NAGAR KALEWADI PIMPRI PUNE-411017', 9922504960, 'Mother', 'LAXMIKANT PATIL', 'BHARAT MATA CHOK KALEWADI  PUNE', 9623312331, 'FRIEND', 'AKASH SHINGE', 'SR NO 169/3/1 PAWAR WASTI TATHWADE PUNE-411033', 7770077714, 'FRIEND', 0, '2021-04-10 08:15:01', '2021-10-20 07:47:39', 1),
(75, 87, 98, 2, 2, 'B+', 'No', 36, '2020-08-04', NULL, NULL, 'FL NO 2B- 604 RAVIKIRAN SR NO 154/2 NR ALANKPURAM WADMUKHWADI CHARHOLI HAVELI PUNE- 412105', 'FL NO 2B- 604 RAVIKIRAN SR NO 154/2 NR ALANKPURAM WADMUKHWADI CHARHOLI HAVELI PUNE- 412105', 'RAVINDRA DHULEKAR', 'NIGDI THARMAX COLONY', 8788831325, 'Brother', 'RAGHVENDRA PANIBHATE', 'SR NO ,72/5 PLOT NO 24, SAYNIK COLONY,  SAI PARK, DIGHI PUNE-411015/', 9922221645, 'Brother', 'PRASHANT KHAIRHAR', 'SR NO 32 RAHATNI PCMC KRUSNHA NAGAR NAKHATE WASTI  PUNE', 9923609575, 'FRIEND', 'PRATIBHA MORE', 'DEHU PHATA ALANDI FLAT 206 PRCM SAI APT NR HP PETROL PUMP DEHUGAO PUNE', 9923386047, 'FRIEND', 0, '2021-04-10 08:18:41', '2021-06-30 02:29:48', 1),
(76, 88, 102, 2, 2, 'O+', 'Partially Vaccinated', 36, '2020-06-25', NULL, NULL, 'G.F HOUS NO 3 SAMBHAJI MOHAN NAGAR DHANKWDI PUNE', 'G.F HOUS NO 3 SAMBHAJI MOHAN NAGAR DHANKWDI PUNE', 'PRATHAM BULE', 'G.F HOUS NO 3 SAMBHAJI MOHAN NAGAR DHANKWDI PUNE', 7498743465, 'Brother', 'SHREYA BULE', 'NAGARKHANA NEAR DATTA MANDIR JALGOAN', 9322203072, 'Sister', 'SNEHAL CHANDANE', 'MALI GALI JAMNER DIS. JALGOAN', 9370520924, 'FRIEND', 'RUCHIKA JAIN', 'JALGAO/ PUNE', 8975877267, 'FRIEND', 0, '2021-04-10 08:20:25', '2021-10-20 05:08:54', 1),
(77, 89, 94, 2, 2, 'B+', 'Partially Vaccinated', 36, '2020-07-07', NULL, NULL, '230 MANGALWAR PETH NR WAJAN KATA KASABA PETHPUNE-411011', '230 MANGALWAR PETH NR WAJAN KATA KASABA PETHPUNE-411011', 'NIGAR SHAIKH', 'MANGALWAR PETH PUNE', 8459805057, 'Brother', 'SALIM  SHAIKH', 'KEDAR BUILDING FLAT NO 715 230 MANGALWAR PETH PUNE', 9145390300, 'Father', 'RAFIK SHAIKH', '230 MANGALWAR PETH NEAR WAJAN KATA PUNE', 9881666983, 'FRIEND', 'RAJJAK SHAIKH', 'KEDAR BUILDING FLAT NO 715 230 MANGALWAR PETH PUNE', 9822929543, 'FRIEND', 0, '2021-04-10 08:21:35', '2021-10-20 06:55:28', 1),
(78, 90, 102, 2, 2, 'O+', 'No', 90, '2020-08-13', '', NULL, 'NEAR TULJABHAVANI MANDIR 570 AMBEDKAR NAGAR MARKET YARD PUNE- 411037', 'NEAR TULJABHAVANI MANDIR 570 AMBEDKAR NAGAR MARKET YARD PUNE- 411037', 'NEELAM TIWARI', 'NEAR TULJABHAVANI MANDIR 570 AMBEDKAR NAGAR MARKET YARD PUNE- 411037', 8459677037, 'Sister', 'AVINASH TIWARI', 'NEAR TULJABHAVANI MANDIR 570 AMBEDKAR NAGAR MARKET YARD PUNE- 411037', 7385513593, 'Sister', 'PRAJAKTA BULE', 'KATRAJ', 9579045163, 'FRIEND', 'GAURAV', 'PIMPRI', 7057359668, 'FRIEND', 1, '2021-04-10 08:24:05', '2021-08-25 07:50:07', 1),
(79, 91, 102, 2, 2, 'O+', 'No', 91, '2020-08-27', NULL, NULL, '797 BHAWANI PETH PUNE', '797 BHAWANI PETH PUNE', 'SUJATA RASAL', 'M 23 SARTHAK SOCIETY HADAPSAR', 8767058972, 'Mother', 'GULAB SONAWANE', '1009 NEW NANA PETH PUNE', 9273808816, 'Father', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-04-10 08:26:18', '2021-08-21 08:06:04', 1),
(80, 92, 98, 2, 2, 'B+', 'Partially Vaccinated', 36, '2020-08-14', NULL, NULL, 'SR NO 121/1/2 JAIHIND COLONY RAJWADE NAGAR KALEWADI PUNE', 'SR NO 121/1/2 JAIHIND COLONY RAJWADE NAGAR KALEWADI PUNE', 'SHARAD KADAM', 'KALEWADI', 9270808148, 'Brother', 'SWAPNIL PAWAR', 'THANE', 8691983418, 'Brother', 'SANGRAM JADHAV', 'SAHNAKAR COLONY JOTIBA NAGAR KALEWADI PUNE', 7770077714, 'FRIEND', 'MAHESH BIRADAR', 'JAI HIND COLONY RAJWADE NAGAR KALEWADI', 8087321616, 'FRIEND', 0, '2021-04-10 09:24:59', '2021-10-20 06:55:50', 1),
(81, 93, 98, 2, 2, 'B+', 'No', 36, '2020-09-01', NULL, NULL, 'KHADAKAWADI NEAR PAVNA DAM AT/POST SHIVALI MAWAL PUNE- 410406', 'KHADAKAWADI NEAR PAVNA DAM AT/POST SHIVALI MAWAL PUNE- 410406', 'BALU JADHAV', 'AT, POST SHIVALI TAL MAVAL DIST PUNE', 9823061901, 'Father', 'SACHIN JADHAV', 'AT-POST-SHIVALI-TAL MAVAL DIST PUNE', 9325921185, 'Brother', 'KIRAN JADHAV', 'AT, POST SHIVALI TAL MAVAL DIST PUNE', 951878978, 'FRIEND', 'SAGAR ADKAR', 'AT-POST-SHIVALI TAL-MAVAL DIST PUNE', 9096325798, 'FRIEND', 0, '2021-04-10 09:28:02', '2021-10-20 06:58:25', 1),
(82, 94, 97, 2, 2, 'AB+', 'Partially Vaccinated', 36, '2020-09-09', NULL, NULL, 'GAT NO 166/1/1 FLAT NO 302  SAI GANESH NAGAR PHURSNGI PUNE-412308', 'GAT NO 166/1/1 FLAT NO 302  SAI GANESH NAGAR PHURSNGI PUNE-412308', 'NITIN JADHAV', 'YERWADA SR NO 1, 10 KAMRAJ NAGAR NR SIDDESWAR NAGAR PUNE-', 8237778600, 'Brother', 'Suraj Jadhav', ':ADD-:SR NO 166/1/1 SAI HEIGHTS’ FLAT NO-:302 PHURSUNGI PUNE-412308', 9284637695, 'Brother', 'RAKESH SHINDE', 'SOMWAR PETH 314 J.K CLASSIC BIDG PUNE-411011', 8983107009, 'FRIEND', 'sangita jadhv', ':ADD-:SR NO 166/1/1 SAI HEIGHTS’ FLAT NO-:302 PHURSUNGI PUNE-412308', 7350036621, 'MOTHER', 0, '2021-04-10 09:35:12', '2021-10-20 06:53:30', 1),
(83, 95, 94, 2, 2, 'AB+', 'Partially Vaccinated', 36, '2020-09-07', NULL, NULL, '1087 KASBA PETH MADHAV NIWAS NEAR MUTTAN MARKET KASBA PETH PUNE-411011', '1087 KASBA PETH MADHAV NIWAS NEAR MUTTAN MARKET KASBA PETH PUNE-411011', 'AMIT WAGH', 'SHIVAJI NAGAR GAVTHAN PUNE', 9822034934, 'Brother', 'MEERA HONRAO', 'Apte ghat,shaniwar peth', 9921588661, 'Sister', 'ABHIJEET WAGH', 'SHIVAJI NAGAR GAVTHAN PUNE', 9822991359, 'FRIEND', 'RAJESH DHOKE', 'KASBA PETH PAVLE CHOOK PUNE', 9921772577, 'FRIEND', 0, '2021-04-10 10:20:40', '2021-10-20 07:03:00', 1),
(84, 96, 102, 2, 2, 'B+', 'No', 36, '2020-09-20', NULL, NULL, 'PLOT NO 36 OPP PMC SCHOOL NR ANJALI BEAUTY PARLOUR JANTA VASAHAT  PUNE- 411009', 'PLOT NO 36 OPP PMC SCHOOL NR ANJALI BEAUTY PARLOUR JANTA VASAHAT  PUNE- 411009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'AKASH GUJAR', '209 BHAIRAVNATH SOC SAHAKAR NAGAR PUNE', 7720012608, 'FRIEND', 'PRATIK JADHAV', 'PARVATI DARSHAN NEAR PARVATI DARSHAN POLICE CHOWKI PUNE', 9049568625, 'FRIEND', 0, '2021-04-10 10:23:26', '2021-08-24 07:21:06', 1),
(85, 97, 99, 2, 2, 'A+', 'Partially Vaccinated', 97, '2020-10-08', NULL, NULL, '503 B NARAYAN PETH MANKESHWAR APPT RAMAN BAG CHOWK PUNE', '503 B NARAYAN PETH MANKESHWAR APPT RAMAN BAG CHOWK PUNE', 'ADITI PINGALE', '503 B NARAYAN PETH RAMANBAG CHOWK PUNE', 8284387418, 'Spouse', 'SUVARNA VAIDYA', 'LAXMINAGAR PARVATI', 9307848057, 'Sister', 'CHANDRAKANT KULKARNI', 'KASBAPETH SHIMPIYALI', 9325756819, 'FRIEND', 'NILESH GAIKWAD', 'DATTA WADI NEAR SHANI MANDIR', 9370767111, 'FRIEND', 0, '2021-04-10 10:26:48', '2021-10-20 07:08:17', 1),
(86, 98, 95, 2, 2, 'B+', 'Partially Vaccinated', 98, '2020-09-18', NULL, NULL, '73/ 1 PARVATI DARSHAN NEAR PARVATI POLICE CHOWKI PUNE- 411009', '73/ 1 PARVATI DARSHAN NEAR PARVATI POLICE CHOWKI PUNE- 411009', 'MEGHA DHUMAL', 'APPER RAJU GANDHI NAGAR,NEAR DATTA MANDIR', 8007624843, 'Sister', 'VAISHALI NIVANGUNE', 'JANTA VASAHAT,NEAR VAGHJAI MATA MANDIR,PARVATI', 9309883882, 'Sister', 'MAYURESH SONAWANE', 'JANTA VASAHAT OPP OSITE PMC SCHOOL PUNE', 7972946942, 'FRIEND', 'RAJESH BELHEKAR', 'PARVATI DARSHAN NEAR NEHARU BAL VIKAS SANGH PUNE', 7276242437, 'FRIEND', 0, '2021-04-11 07:00:18', '2021-08-19 04:41:26', 1),
(87, 99, 102, 2, 2, 'B+', 'Partially Vaccinated', 36, '2020-09-18', NULL, NULL, '1202 DATTA VASAHAT SHIROLE ROAD SHIVAJI NAGAR DECCAN GYMKHANA PUNE- 411005', '1202 DATTA VASAHAT SHIROLE ROAD SHIVAJI NAGAR DECCAN GYMKHANA PUNE- 411005', 'SARMILA JADHAV', '1202 DATTA VASAHAT SHIROLE ROAD SHIVAJI NAGAR DECCAN GYMKHANA PUNE- 411005', 9921155114, 'Sister', 'PRASHANT TARAL', '1202 SHIRAVALE RD FERGUESSEN CLG MAIN GATE PUNE04', 8698421847, 'Spouse', 'ANKITA PAWAR', 'SUBHANKAR APP, SURVE NO 85/1/1 K SHIVANE PRESTIGE PUBLIC SCHOOL PUNE-411041', 9112701866, 'FRIEND', 'RESHMA SAPKAL', '1202 SHIRAVALE RD FERGUESSEN CLG MAIN GATE PUNE04', 9579983617, 'FRIEND', 0, '2021-04-11 07:02:43', '2021-10-20 07:08:46', 1),
(88, 100, 99, 2, 2, 'B+', 'No', 36, '2020-10-05', NULL, NULL, 'SR NO 112 FLAT NO 301 GALI NO 08 SHRIRAM NAGARI PUNE SUTAR DARA KOTHRUD PUNE', 'SR NO 112 FLAT NO 301 GALI NO 08 SHRIRAM NAGARI PUNE SUTAR DARA KOTHRUD PUNE', 'HEMANT MULE', 'SR NO 112 GALLI NO 08 SHREERAM COLONY SUTARDARA KOTHRUD PUNE- 411038', 965765710, 'Brother', 'CHAITALI MULE', 'SR NO 112 GALLI NO 08 SHREERAM COLONY  SUTARDARA KOTHRUD PUNE-411038', 9112244748, 'Spouse', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-04-11 07:03:48', '2021-08-23 00:37:50', 1),
(89, 101, 99, 2, 2, 'O+', 'Partially Vaccinated', 36, '2020-10-15', NULL, NULL, 'HOUSE NO 195 DATTAWADI NR SHANI MANDIR TEMPLE DATTAWADI PUNE-411030', 'HOUSE NO 195 DATTAWADI NR SHANI MANDIR TEMPLE DATTAWADI PUNE-411030', 'SURESH GAIKWAD', 'HOUSE NO -195 NEAR SHANI MARUTI TEMPAL DATTA WADI PUNE 30', 9920135404, 'Father', 'RACHNA GAIKWAD', 'HOUSE NO -195 NEAR SHANI MARUTI TEMPAL DATTA WADI PUNE 30', 9762855503, NULL, 'KEDAR MULE', 'SR NO 112 FLAT NO 301 GALI NO 08 SHRIRAM NAGARI PUNE SUTAR DARA KOTHRUD PUNE', 9657645710, 'FRIEND', 'SAMEER UBALE', 'HOUSE NO 420 NR SHANI MARUTI TEMPAL DATTAWADI PUNE-30', 7276752009, 'FRIEND', 0, '2021-04-11 07:11:01', '2021-10-01 02:47:54', 1),
(90, 102, 97, 2, 2, 'B+', 'No', 36, '2020-11-02', NULL, NULL, 'FLAT 03 SUCHITRA APT NEW SAI NAGAR CHANDAN NAGAR PUNE 411014', 'FLAT 03 SUCHITRA APT NEW SAI NAGAR CHANDAN NAGAR PUNE 411014', 'OM JAGTAP', 'CHANDAN NAGAR A2AD CHOWK PUNE', 8347993502, 'Brother', 'NIRMALA JAGTAP', 'SUCHITRA APPARTMENT ,NEW SAINAGRI,FLAT NO-3 CANDAN NAGAR', 9723756534, 'Spouse', 'RAJ SURYAWANSHI', 'NO 10 TADIWALA ROAD PUNE 7', 9325231396, 'FRIEND', 'BHAGESHREE PHADTARE', 'SUCHITRA APPARTMENT ,NEW SAINAGRI,FLAT NO-3 CANDAN NAGAR', 9822111599, 'FRIEND', 0, '2021-04-11 07:17:04', '2021-06-30 04:39:19', 1),
(91, 103, 102, 2, 2, 'B+', 'No', 36, '2020-11-03', NULL, NULL, '224 MANGALWAR PETH PUNE 411011', '224 MANGALWAR PETH PUNE 411011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'SAFIK SHAIKH', 'FLAT NO 601 6th FLOOR MANGALWAR PETH', 9860029943, 'FRIEND', 'ALMASH PATHAN', 'ROOM NO 19 MANGALWAR PETH PUNE 11', 9890696435, 'FRIEND', 0, '2021-04-11 07:20:29', '2021-08-25 04:48:57', 1),
(92, 104, 102, 2, 2, 'O+', 'Partially Vaccinated', 36, '2020-12-08', NULL, NULL, '18/148 MAHARSHI NAGAR OPP SANT DHYANDEV VIDYALAY MARKET YARD', '18/148 MAHARSHI NAGAR OPP SANT DHYANDEV VIDYALAY MARKET YARD', 'PRAKASH PHADTARE', 'NILKANTH APT,FLAT NO-9,LEFT BHUSARI,PAUD ROAD,KOTHRUD', 9860094406, 'Father', 'AJAY DINKAR DHAMDHARE', '18/148 MAHARSHI NAGAR', 9604604004, 'Spouse', 'ANUSHKA SASANE', 'FLAT NO 304 GREEN HILL APARTMENT KATRAJ PUNE 16', 9130762938, 'FRIEND', 'AFSANA SHAIKH', '224 MANGALWAR PETH PUNE 11', 9767904844, 'FRIEND', 0, '2021-04-11 07:24:51', '2021-08-31 06:57:43', 1),
(93, 105, 98, 2, 2, 'B+', 'No', 36, '2020-11-04', NULL, NULL, 'PANCHAWATI A/907 AMBETHAN ROAD ZITRAI MALA CHAKAN PUNE-410501', 'PANCHAWATI A/907 AMBETHAN ROAD ZITRAI MALA CHAKAN PUNE-410501', 'PALLAVI GAWARE', 'FLAT NO 907 A WING PANCHWATI HOUSING SOCITY AMBETHAN ROAD CHAKAN', 9689174565, 'Spouse', 'MAHESH GAWARE', 'PIMPRI', 9730352525, 'Spouse', 'JAGDISH KHOT', 'CHAKRPANI VASAHAT BHOSRI PUNE', 830848715, 'FRIEND', 'SUNIL WARE', 'PANCHAWATI AMBETHAN ROAD ZITRAI MALA CHAKAN PUNE-410501', 9763840339, 'FRIEND', 0, '2021-04-11 07:27:44', '2021-10-20 04:43:41', 1),
(94, 106, 97, 2, 2, 'AB+', 'Partially Vaccinated', 36, '2020-11-21', NULL, NULL, '319 GURU DATTA APARTMENT FLAT NO 6 3RD FLOOR NERA KARISHMA HOTEL RASTA PETH 411011', '591 BUDHWAR PETH OLD TAPKIR LANE OPP VASANT TEATER AMBEKAR BOL PUNE-411002', 'DEEPAK TRIVEDI', '319,GURU DATTA APPARTMENT FLAT NO -6 RASTA PETH', 9850816478, 'Brother', 'DEEPIKA TRIVEDI', '319,GURU DATTA APPARTMENT FLAT NO -6 RASTA PETH', 8010056792, NULL, 'SIDDHARTH SIRKHE', 'OPP. INDIRANAGAR RASTA PETH PUNE', 8767645332, 'FRIEND', 'RAKESH SHINDE', 'SOMWAR PETH PUNE', 8983107009, 'FRIEND', 0, '2021-04-11 07:30:12', '2021-10-20 04:44:09', 1),
(95, 107, 96, 2, 2, 'B+', 'Partially Vaccinated', 107, '2020-12-03', NULL, NULL, '10 AMAR BAUG GADITAL HADAPSAR PUNE411028', '10 AMAR BAUG GADITAL HADAPSAR PUNE411028', 'SULBHA LONDHE', '10 AMARBAG SOCITY HADAPSAR GADITAL PUNE 411024', 9890158879, 'Spouse', 'RAHUL GAIKWAD', 'GONDHALE NAGAR AWHO SOCITY HADAPSAR 28', 9370905040, 'Spouse', 'NILESH GAIKWAD', 'HOUSE NO 195 DATTAWADI NR SHANI MANDIR TEMPLE DATTAWADI PUNE-411030', 8767585901, 'FRIEND', 'PRADIP DIVEKAR', 'HOUSE NO 195 DATTAWADI NR SHANI MANDIR TEMPLE DATTAWADI PUNE-411030', 9370905040, 'FRIEND', 1, '2021-04-11 07:31:40', '2021-09-14 09:00:35', 1),
(96, 108, 95, 2, 2, 'B+', 'Partially Vaccinated', 36, '2020-12-03', NULL, NULL, 'SR NO 133 DANDEKAR POOL Z.P.  BAVAN CHAL INDIRA GANDHI NAGAR PUNE-411030', 'NR NO 133 DANDEKAR POOL Z.P.  BAVAN CHAL PUNE-411030', 'SHANKAR NATKAR', 'SR 133 INDIRA GANDHI NAGAR SINHAGAD ROAD PUNE 30', 9011476310, 'Brother', 'SANDEEP NATKAR', 'sr no 133 indira gandhi nagar singhgath road near chaitanya hospital', 7038482626, 'Brother', 'SHAFIK SHAIKH', '230 MANGALWAR PETH PUNE 411011', 9860029943, 'FRIEND', 'ARUN JATITHOR', 'sr no 133 indira gandhi nagar singhgath road near chaitanya hospital', 9527473196, 'FRIEND', 0, '2021-04-11 07:35:51', '2021-10-01 01:07:48', 1),
(97, 109, 94, 2, 2, 'B+', 'Partially Vaccinated', 36, '2020-12-05', NULL, NULL, '47 RASTA PETH NEAR VEER TANAJI MANDAL SAI NIWAS BLDG 1ST FLOOR FLAT NO 2 PUNE 411011', '47 RASTA PETH NEAR VEER TANAJI MANDAL SAI NIWAS BLDG 1ST FLOOR FLAT NO 2 PUNE 411011', 'PRITI HANDRALMATH', '47 RASTA PETH NEAR VER TANAJI MANDAL SAI NIWAS BILD 2ND FLOOR FLT NO 2 SWAMI NIWAS PUNE11', 8888336534, 'Spouse', 'PRAMMA HANDARMATH', '47 RASTH PETH  SAI NIVAS BUILDING FLT NO 2  . 1 ST FLOOR NR NEW TANAJI MANDAL TRUST', 8888336534, 'Spouse', 'VINAY NAIDU', '30/31 RASTA PETH SANTOSH APT FLAT NO 7 PUNE11', 9881203666, 'FRIEND', 'SIDDHART TRIVEDI', 'RASTA PETH', 8788367069, 'FRIEND', 0, '2021-04-11 07:37:48', '2021-10-20 07:10:06', 1),
(98, 110, 98, 2, 2, 'B+', 'Partially Vaccinated', 36, '2020-12-04', NULL, NULL, 'GHAR NO 31 ARUN KADAM CHOWK NEAR TRIMURTI MITRA MANDAL JANWADI PUNE-411016', 'GHAR NO 31 ARUN KADAM CHOWK NEAR TRIMURTI MITRA MANDAL JANWADI PUNE-411016', 'KIRAN VISHWAS GAIKWAD', 'TRIMURTI MITRA MANDAL GHAR NO 31 ARUN KADAM CHOWLK GOKHALE NAGAR PUNE', 7040935320, 'Brother', 'VILAS NAMDEV GAIKWAD', 'TRIMURTI MITRA MANDAL GHAR NO 31 ARUN KADAM CHOWLK GOKHALE NAGAR PUNE', 7888242967, 'Father', 'BHIMRAJ CHALWADI', 'SHIVAJI HOSING SOCIETY SB ROAD NEAR LALALAS PATRAI HOSPITAL', 7775927361, 'FRIEND', 'SACHIN KHANDAGLE', 'GANESH MALA DATTAWADI SINHAGAD ROAD BEHIND ASHA HOTEL', 8208380332, 'FRIEND', 0, '2021-04-11 07:42:48', '2021-10-20 07:10:48', 1),
(99, 111, 96, 2, 2, 'B+', 'No', 36, '2020-12-04', NULL, NULL, 'JANWADI CHAWL NO 40 H NO 187 PMC COLONY ARAHANA M. JANWADI', 'JANWADI CHAWL NO 40 H NO 187 PMC COLONY ARAHANA M. JANWADI', 'MAHBOOB SAYYED', 'ASHRAF NAGAR LANE NO 3 MANMOD CLASSIC KONDWA PUNE', 9370063900, 'Father', 'AISHA SAYYED', 'ASHRAF NAGAR LANE NO 3 MANMOD CLASSIC KONDWA PUNE', 8600074687, 'Mother', 'BHIMRAJ CHALWADI', 'SHIVAJI HOSING SOCIETY SB ROAD NEAR LALALAS PATRAI HOSPITAL PUNE', 7775927361, 'FRIEND', 'SACHIN KHANDAGLE', 'GANESH MALA DATTAWADI SINHAGAD ROAD BEHIND ASHA HOTEL PUNE', 8208380332, 'FRIEND', 1, '2021-04-11 07:43:53', '2021-08-27 06:06:04', 1),
(100, 112, 96, 2, 2, 'B+', 'No', 36, '2020-12-11', NULL, NULL, 'SR NO 204 HOUSE NO 3950, SHREENATH COLONY LANE NO 7 PAPDE WASTI PHURSUNGI PUNE-412308', 'SR NO 204 HOUSE NO 3950, SHREENATH COLONY LANE NO 7 PAPDE WASTI PHURSUNGI PUNE-412308', 'KARISHMA BANDAL', 'SR NO 204HOUSE NO 3950 SHREENATH COLONY LANE NO 7 PAPDE WASTI PHURSUNGI PUNE 412308', 7420918246, 'Spouse', 'MITHUN BANDAL', 'SR NO 204HOUSE NO 3950 SHREENATH COLONY LANE NO 7 PAPDE WASTI PHURSUNGI PUNE 412308', 9850161227, 'Brother', 'HANUMANTH KADAM', 'NEAR VITTAL MANDAL MOHMADWADI HADAPSAR PUNE', 9021955736, 'FRIEND', 'VISHAL BARGADE', 'BEHIND SAMAJ TEMPAL SR NO 110 RAMTEKADI PUNE', 8552852681, 'FRIEND', 1, '2021-04-11 07:47:10', '2021-10-08 03:13:23', 1),
(101, 113, 102, 2, 2, 'AB+', 'No', 36, '2021-01-18', NULL, NULL, 'FLAT NO 301 3RD FLOOR SHREE SAO MOTI COMPLEX NEAR DATTA NAGAR BUS STOP DIGHI PUNE 15', 'FLAT NO 301 3RD FLOOR SHREE SAO MOTI COMPLEX NEAR DATTA NAGAR BUS STOP DIGHI PUNE 15', 'DNYANESHWAR YADAV', '300 NANA PETH NEAR DOKE TALIM OPP ARYA SAMAJ PUNE 411002', 9921635372, 'Father', NULL, NULL, NULL, NULL, 'SWATI AWARE', 'FLAT NO 302 3RD FLOOR SHREE SAI MOTI COMPLEX NEAR DATTANAGAR BUS STOP DIGHI PUNE 15', 9175645689, 'FRIEND', NULL, NULL, NULL, NULL, 1, '2021-04-11 07:48:26', '2021-08-21 08:10:20', 1),
(102, 114, 99, 2, 2, 'O+', 'No', 114, '2021-01-19', NULL, NULL, 'FLAT NO 108 VIGHNAHARTA APT NEAR MHATRE BRIDGE ERANDAWANE GAONTHAN PUNE 4', 'FLAT NO 108 VIGHNAHARTA APT NEAR MHATRE BRIDGE ERANDAWANE GAONTHAN PUNE 4', 'AKASH KHANDAGRE', 'FLAT NO 108 VIGHNAHARTA APPARTMENT NEAR MHATRE BRIGH ERAWNE GAVTHAN PUNE -4', 9067785307, 'Brother', 'SUNIL KHANDAGRE', 'FLAT NO 108 VIGHNAHARTA APPARTMENT NEAR MHATRE BRIGH ERAWNE GAVTHAN PUNE -4', 8805896359, 'Brother', 'SAURABH JADHAV', 'SAI HEIGHTS GAJANAN MAHARAJ MANDIR FURSUNGI PUNE 412308', 9850380103, 'FRIEND', 'MANOJ BHANDARI', 'NANDAKUMAR APT FLAT NO 10 PASHAN GAON', 9921557446, 'FRIEND', 1, '2021-04-11 07:50:15', '2021-10-08 03:13:37', 1),
(103, 115, 102, 2, 2, 'O+', 'Partially Vaccinated', 36, '2021-01-25', NULL, NULL, '106-4/A PARVATI DARSHAN MITRA MANDAL COLONY PUNE 411009', '106-4/A PARVATI DARSHAN MITRA MANDAL COLONY PUNE 411009', 'NISAR ATTAR', '106-4/A PARVATI DARSHAN MITRA MANDAL COLONY PUNE 411009', 9011626198, 'Father', 'RUKSAR ATTAR', 'PIMPRI', 7875702635, 'Sister', 'SAMIYA SAIYAD', 'PARVATI DARSHAN PUNE.', 7768987745, 'FRIEND', 'MAYURI NAKADE', 'KOTHRUD', 8698353434, 'FRIEND', 0, '2021-04-11 07:51:50', '2021-10-20 07:14:43', 1),
(104, 116, 99, 2, 2, 'O+', 'No', 36, '2021-03-02', NULL, NULL, 'SR No-133,Parvati Paytha ,Singhgad Rd,Pune-411030', 'SR No-133,Parvati Paytha ,Singhgad Rd,Pune-411030', 'Nilesh Jethithor', 'Sr No 133, Parvati Payatha Pune -411030', 9552479443, 'Brother', 'SAVITA ARUN JETHITHOR', 'SURVEY NO 133 PARVATI PAYTHA SIGHAGAD RD NR CHAITANYA HOSPITAL PUNE 30', 9527473196, NULL, 'RahulPunchwade', 'Hingne Sinhagad Road Pune 411051', 9890598069, 'FRIEND', 'POOJA JETHITHOR', 'SURVEY NO 133 PARVATI PAYTHA SIGHAGAD RD NR CHAITANYA HOSPITAL PUNE 30', 8149113575, 'SISTER', 0, '2021-04-11 07:53:07', '2021-10-20 07:15:45', 1),
(105, 117, 102, 2, 2, 'B+', 'Partially Vaccinated', 117, '2021-02-22', NULL, NULL, '108/7,Parvati darshan, Near Mitra Mandal Colony,Pune-411009', '108/7,Parvati darshan, Near Mitra Mandal Colony,Pune-411009', 'NAZMEEN BANDAR', '108/7,Parvati darshan, Near Mitra Mandal Colony,Pune-411009', 9370657564, 'Mother', 'HASAN BANDAR', '108/07 PARVATI DARSHAN NR MITRA MANDAL COLONY PUNE 09', 9371055816, 'Father', 'PRAJAKTA BULE', 'DHANKAWADI', 9579045163, 'FRIEND', 'DIPTI BHAMBURE', 'LOKMANYA NAGAR', 727815529, 'FRIEND', 1, '2021-04-11 07:54:35', '2021-10-20 04:42:10', 1),
(106, 118, 98, 2, 2, 'O+', 'No', 118, '2021-03-03', NULL, NULL, 'Greenlan Building A9/F20, Sector-20,  Scehme no 3, Krishna Nagar, Chinchwad Pune -411019', 'Greenlan Building A9/F20, Sector-20,  Scehme no 3, Krishna Nagar, Chinchwad Pune -411019', 'Ashwini Gulhane', 'Greenlan Building A9/F12, Sector-20,  Scehme no 3, Krishna Nagar, Chinchwad Pune -411019', 7517039491, 'Sister', 'AVINASH GULHANE', 'GREENLAND HOU SOC,BLD NO-A-9,FLAT NO-12,KRISHNA NAGAR,CHINCHWAD PUNE-19', 9422364191, 'Father', 'Amit Tanpure', 'Shanti Society,Sane Chowk, Near Hanuman Mandir, Chinchwad Pune-19', 9175772773, 'FRIEND', 'SANKET CHOUDHARI', 'RACHANA APT,MALSAKANT CHOWK,NIGADI PRADHIKARAN', 7414925436, 'FRIEND', 1, '2021-04-11 07:56:12', '2021-09-11 07:49:01', 1),
(107, 119, 102, 2, 2, 'A+', 'Partially Vaccinated', 36, '2021-03-01', NULL, NULL, 'S/o Gajanan Rane, Gat no 1414 Navchetanya Hostel, Vishal Aprtment, More Vasti, Chikali Pune -412114', 'S/o Gajanan Rane, Gat no 1414 Navchetanya Hostel, Vishal Aprtment, More Vasti, Chikali Pune -412114', 'DEEPALI RANE', 'NAVCHAITANYA HOU SOC,NEAr VISHAL APT,MORE WASTI,CHIKHALI', 9890371976, 'Mother', 'NEHA MORE', 'SILVER CITY,MOSHI', 7447766599, 'Sister', 'Sanket Chodhari', 'Rachana Residency, Mhalsakant Chowk,Akurdi Pune.', 8793176569, 'FRIEND', 'Aditi Jedhe', 'Buniyad Hos Society, Yamuna Nagar,Nigadi.', 9960330118, 'FRIEND', 0, '2021-04-11 07:59:25', '2021-09-01 05:52:34', 1),
(108, 120, 98, 2, 2, 'B+', 'Partially Vaccinated', 36, '2021-03-06', NULL, NULL, 'Kadam Niwas, Near Ganpati Mandir,Bajrang Nagar,Kharalwadi, Pimpri, Pune-41108', 'Kadam Niwas, Near Ganpati Mandir,Bajrang Nagar,Kharalwadi, Pimpri, Pune-41108', 'Priyanka Kadam', 'Kadam Niwas, Near Ganpati Mandir,Bajrang Nagar,Kharalwadi, Pimpri, Pune-41108', 7719942314, 'Spouse', 'SANTOSH KADAM', 'KADAM NIVAS BAJRANG NAGAR KHARALWADI PIMPRI PUNE 18', 9765905401, 'Spouse', 'Harish Dhavale', 'Kamgar Nagar, Kharalwadi Bajarang Nagar,Pimpri Pune 41108', 9922912750, 'FRIEND', 'YOGESH SAWANT', 'NIGDI', 9028002843, 'FRIEND', 0, '2021-04-11 08:00:36', '2021-09-01 01:14:52', 1),
(109, 121, 98, 2, 2, 'B+', 'No', 36, '2021-03-06', NULL, NULL, 'Sairaj Nivas Lakxmi Nagar, Pimple Gurav Pune-411061', 'Sairaj Nivas Lakxmi Nagar, Pimple Gurav Pune-411061', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rishikesh Wakale', 'Bhalekar Nagar, Pimple Gurav Pune', 7507307470, 'FRIEND', 'Ranjit Kamble', 'Ranjit Kamble Lane no 3, Laxmi Nagar, Pimple Gurav Pune', 8080955167, 'FRIEND', 1, '2021-04-11 08:03:25', '2021-10-08 03:14:14', 1),
(110, 122, 98, 2, 2, 'B+', 'No', 36, '2021-03-06', NULL, NULL, 'Sr No 158, Matru Pitru Chaya Nehru Nagar, Behind Ganpati Mandir, Bajrang Nagar, Pimpri Pune411018', 'Sr No 158, Matru Pitru Chaya Nehru Nagar, Behind Ganpati Mandir, Bajrang Nagar, Pimpri Pune411018', 'SANTOSH NAKATE', 'AKASH HEIGHTS BAJRANG NAGAR KHARALWADI PIMPRI PUNE 18', 9503428562, 'Father', 'NANDAKISHOR NAKATE', 'KHARALWADI PIMPRI', 8788524052, 'Brother', 'PRASHANT KADAM', 'PIMPRI', 7276651972, 'FRIEND', 'ANANT YADAV', 'BHOSARI', 9881000356, 'FRIEND', 0, '2021-04-11 08:05:10', '2021-10-20 07:13:42', 1);
INSERT INTO `ptx_user_details` (`id`, `user_id`, `area_id`, `comp_id`, `branch_id`, `blood_group`, `covid_vaccination`, `team_leader`, `date_of_joining`, `exit_document`, `date_of_leaving`, `current_address`, `permanent_address`, `reference_1`, `reference_1_address`, `reference_1_mobile`, `reference_1_relation`, `reference_2`, `reference_2_address`, `reference_2_mobile`, `reference_2_relation`, `fri_reference_1`, `fri_reference_1_address`, `fri_reference_1_mobile`, `fri_reference_1_relation`, `fri_reference_2`, `fri_reference_2_address`, `fri_reference_2_mobile`, `fri_reference_2_relation`, `isDeleted`, `created_at`, `updated_at`, `updated_by`) VALUES
(111, 123, 98, 2, 2, 'AB+', 'Partially Vaccinated', 36, '2020-11-18', NULL, NULL, 'GANJ PETH MASHEALI PUNE 359/60 MAHATMA PHULE PETH 3RD FLOOR FLAT NO 5 PHULE SMARAK PUNE 411042', 'GANJ PETH MASHEALI PUNE 359/60 MAHATMA PHULE PETH 3RD FLOOR FLAT NO 5 PHULE SMARAK PUNE 411042', 'LAXMI RACCHA', 'GANJ PETH MASHEALI PUNE 359/60 MAHATMA PHULE PETH 3RD FLOOR FLAT NO 5 PHULE SMARAK PUNE 411042', 9175569736, 'Mother', 'AMBRISH RACCHA', 'GANJ PETH MASHEALI PUNE 359/60 MAHATMA PHULE PETH 3RD FLOOR FLAT NO 5 PHULE SMARAK PUNE 411042', 9766316732, 'Father', 'JAGDISH SHERLA', 'GANJ PETH MASHEALI PUNE 359/60 MAHATMA PHULE PETH 2ND FLOOR  PHULE SMARAK PUNE 411042', 7276237454, 'FRIEND', 'RITHIK SHERLA', 'GANJ PETH MASHEALI PUNE 359/60 MAHATMA PHULE  PUNE 411042', 7219498927, 'FRIEND', 0, '2021-04-11 08:08:53', '2021-10-20 07:11:42', 1),
(112, 124, 94, 2, 2, 'AB+', 'Partially Vaccinated', 36, '2020-12-03', NULL, NULL, '197 MANGALWAR PETH COLONY NO 2 ROOM NO 6 PUNE-411011', '197 MANGALWAR PETH COLONY NO 2 ROOM NO 6 PUNE-411011', 'ADARSH SHAM AARDE', 'PUNE', 9011056704, 'Brother', 'SHAM SHANKAR AARDE', 'PUNE', 7058506588, 'Father', 'REVA SUKALE', 'MANGALWAR PETH KANYA SCHOOL PUNE', 9623209700, 'FRIEND', 'ARTI SHAM AARDE', '197 MANGALWAR PETH COLNI NO 2 GADITAL VAJAN KATA JAWAL PUNE', 8605814480, 'FRIEND', 0, '2021-04-11 08:10:08', '2021-10-20 07:15:21', 1),
(113, 125, 96, 2, 2, 'B+', 'No', 125, '2020-11-11', NULL, NULL, '14/2/1 BEHIND TULJA BHAVANI MANGAL KARYALAYA NHAWALE NAGAR  HANDEWADI ROAD HADAPSAR PUNE-411028', '14/2/1 BEHIND TULJA BHAVANI MANGAL KARYALAYA NHAWALE NAGAR  HANDEWADI ROAD HADAPSAR PUNE-411028', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'DHIRYASHIL GAIKWAD', 'NHAVLE NAGAR HANDEWADI ROAD PUNE HADAPSAR PUNE 411028', 9028745855, 'FRIEND', 'ABHIJIT JAWALKAR', 'ALANDI MHATOBACHI PUNE SOLAPUR ROAD LONI KALBHAR 412201', 9707679797, 'FRIEND', 1, '2021-04-11 08:11:49', '2021-08-21 08:14:19', 1),
(114, 126, 98, 2, 2, 'B+', 'No', 36, '2020-10-11', NULL, NULL, 'MM. NO 56/1/20 ASHOK NAGAR BHOSARI GOAN DOLAS TALIM PUNE-411039', 'MM. NO 56/1/20 ASHOK NAGAR BHOSARI GOAN DOLAS TALIM PUNE-411039', 'PRASHANT DOLAS', '878/3 NO 10/11/226 DOLAS VASTI ASHOK NAGAR BHOSARI PUNE 411039', 9881750999, 'Brother', 'PRAFULKUMAR PRAKASH DOLAS', '878/3 NO 10/11/226 DOLAS VASTI ASHOK NAGAR BHOSARI PUNE 411039', 9028501984, NULL, NULL, NULL, NULL, 'FRIEND', 'SUDHIR JADHAV', 'CHAKAPANI VASAHAT BHOSARI SAHAKAR COLONY PUNE', 9021010746, 'FRIEND', 0, '2021-04-11 08:13:18', '2021-10-20 07:17:38', 1),
(115, 127, 94, 2, 3, 'O+', 'Partially Vaccinated', 36, '2020-11-25', NULL, NULL, 'SR NO 658/1B SARGAM MAGAR NR GAVALI MILL UPPER BIBAWEWADI PUNE-411037', 'SR NO 658/1B SARGAM MAGAR NR GAVALI MILL UPPER BIBAWEWADI PUNE-411037', 'SUHAS KAWADE', 'KATRAJ', 9922324611, 'Brother', 'KRUSHNA BAGAL', 'KATRAJ', 8329249485, 'Brother', 'ANIKET NITIN LIMBORE', 'DIGHI', 8181098282, 'FRIEND', 'SHUBHAM CHAVAN', '6581 SARGAM NAGAR BIBWE WADI PUNE', 9850891539, 'FRIEND', 0, '2021-04-11 08:14:31', '2021-10-20 07:19:07', 1),
(116, 129, 99, 2, 2, 'O+', 'Fully Vaccinated', 36, '2020-11-08', NULL, NULL, 'HOUSE NO 194 DATTAWADI MHATRE BRIDGE ROAD NEAR SHANI MARUTI MANDIR  S.P  COLLEGE PUNE-411030', 'HOUSE NO 194 DATTAWADI MHATRE BRIDGE ROAD NEAR SHANI MARUTI MANDIR  S.P  COLLEGE PUNE-411030', 'RANJANA ASHOK DALAVE', 'HOUSE NO 194 DATTAWADI MHATRE BRIDGE ROAD NEAR SHANI MARUTI MANDIR  S.P  COLLEGE PUNE-411030', 8624032180, 'Mother', 'PRATHMENSH ABHANG', 'DATTAWADI', 8605972671, 'Brother', 'AMIT SATPUTE', 'SHUKRWAR PETH OPP NEHARU STEDIAM SWARGET PUNE', 9850900800, 'FRIEND', 'MAHENDRA WAGMARE', '63 HOUSE NO GHORPADI GAON NEAR SAIRAM HOUSPITAL', 9763651685, 'FRIEND', 0, '2021-04-11 08:25:31', '2021-10-20 07:18:27', 1),
(117, 130, 95, 2, 2, 'B+', 'Partially Vaccinated', 36, '2020-12-04', NULL, NULL, 'SR NO 658/ 1 SARGAM NAGAR NR SAGAR TRANSPORT UPPER BIBWEADI PUNE-411037', 'SR NO 658/ 1 SARGAM NAGAR NR SAGAR TRANSPORT UPPER BIBWEADI PUNE-411037', 'SANDIP SHENDGE', 'PAPPER INDIRA NAGAR PUNE', 9604375944, 'Father', 'Santosh shendge', 'Upper sargam bibwewadi pune.37', 9881560343, 'Brother', 'ANIKET NITIN LIMBORE', '23/27 SASUN ROAD NEAR JAHANGIR HOSPITAL LATKATWADI PUNE 411001', 8181098282, 'FRIEND', 'VIKAS DEVGIRI', 'APPUR SANGAM CHAL PUNE', 9595351316, 'FRIEND', 0, '2021-04-11 08:26:47', '2021-10-20 07:19:48', 1),
(118, 131, 98, 2, 2, 'B+', 'Partially Vaccinated', 131, '2021-01-01', NULL, NULL, 'NAGAR GONDHALI CHAL KALEWADI NEAR TAPKIR SCHOOL PUNE 411017', 'NAGAR GONDHALI CHAL KALEWADI NEAR TAPKIR SCHOOL PUNE 411017', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ULHAS INGALE', '68/2/47 TAPKIR NAGAR GOLDI CHOWK', 9371200911, 'FRIEND', 'AAKAS  WAKODE', '68/2/47 TAPKIR NAGAR GOLDI CHOWK', 9689700416, 'FRIEND', 0, '2021-04-11 08:28:55', '2021-08-19 05:06:29', 1),
(119, 132, 97, 2, 2, 'AB+', 'Partially Vaccinated', 36, '2020-12-24', NULL, NULL, '23/27 SASUN ROAD NEAR JAHANGIR HOSPITAL LATKATWADI PUNE 411001', '23/27 SASUN ROAD NEAR JAHANGIR HOSPITAL LATKATWADI PUNE 411001', 'LALITA NITIN LIMBORE', '23/27 SASUN ROAD NEAR JAHANGIR HOSPITAL LATKATWADI PUNE 411001', 7058468182, 'Mother', 'NITIN RAMCHANDRA LIMBORE', '23/27 SASUN ROAD NEAR JAHANGIR HOSPITAL LATKATWADI PUNE 411001', 9552413650, 'Father', 'OMKAR JAWALKAR', '23/27 SASUN ROAD NEAR JAHANGIR HOSPITAL LATKATWADI PUNE 411001', 9673911801, 'FRIEND', 'NITIN LIMBORE', '23/27 SASUN ROAD NEAR JAHANGIR HOSPITAL LATKATWADI PUNE 411001', 9552413650, 'FRIEND', 0, '2021-04-11 08:33:20', '2021-10-01 02:54:59', 1),
(120, 133, 102, 2, 2, 'B+', 'Partially Vaccinated', 133, '2021-03-02', NULL, NULL, 'D/O Alli Sayyed, 806 Shivaji Nagar Kamgar Putala, Pune411005', 'D/O Alli Sayyed, 806 Shivaji Nagar Kamgar Putala, Pune411005', 'SABA ALI SAYYED', 'LN NO 13 VIMAN NAGAR RAJU SAUTH SOCIETY PUNE', 9049058598, 'Sister', 'ALLI SAYYED', 'D/O Alli Sayyed, 806 Shivaji Nagar Kamgar Putala, Pune411005', 9921924954, 'Father', 'GAUTAMI RACCHA', 'MAHATMA PHULE PUNE', 7620600923, 'FRIEND', 'Busra Sayyed', 'Near Sai Mandir Shivaji Nagar Pune 411005', 9075978577, 'FRIEND', 0, '2021-04-11 08:34:38', '2021-10-08 07:16:06', 1),
(121, 134, 98, 2, 2, 'B+', 'Partially Vaccinated', 36, '2020-09-09', NULL, NULL, 'PRIYDARSHNI NAGAR LANE NO 1 OLD SANGVI PUNE411012', 'PRIYDARSHNI NAGAR LANE NO 1 OLD SANGVI PUNE411012', 'KAPIL DONGARE', 'PRIYADARSHANI NAGAR LANE NO 1OLD SANGVI PUNE', 8999876728, 'Brother', 'CHANDRAMANI DONGARE', 'PRIYADARSHANI NAGAR LANE NO 1OLD SANGVI PUNE', 9673759534, 'Brother', 'VILAS SHEWALE', 'OLD SANGAVI', 9860971577, 'FRIEND', 'BABASAHEB LONDE', 'OLD SANGAVI', 9172833412, 'FRIEND', 0, '2021-04-11 08:39:10', '2021-10-20 07:27:19', 1),
(122, 136, 98, 2, 2, 'B+', 'No', 37, '2006-06-12', NULL, NULL, 'BAJRANG NAGAR PIMPRI PUNE 411 018', 'BAJRANG NAGAR PIMPRI PUNE 411 018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'JAYRAJ JOSHI', '196/1/71 SANT TUKARAM NAGAR PUNE 18', 9822332776, 'FRIEND', 'NAVIN HEGADE', 'SONARUPA B PLOT NO 307 KHARAL WADI PIMPRI PUNE 18', 9960699427, 'FRIEND', 0, '2021-04-17 08:56:08', '2021-06-26 02:12:31', 1),
(123, 137, 94, 2, 2, 'A+', 'No', 37, '2004-10-01', NULL, NULL, 'SR.NO 10 NEAR PMC SCHOOL SUBHAS NAGAR YERWADA PUNE', 'SR.NO 10 NEAR PMC SCHOOL SUBHAS NAGAR YERWADA PUNE', 'FHIRDOS SAYYED', 'SR.NO 10 NEAR PMC SCHOOL SUBHAS NAGAR YERWADA PUNE', 8844748172, 'Sister', 'MISABA SAYYED', 'SR.NO 10 NEAR PMC SCHOOL SUBHAS NAGAR YERWADA PUNE', 7720840011, 'Sister', 'VITTHAL GHOD', '411 RASTA PETH PUNE 411011', 9822532112, 'FRIEND', 'MAHENDRA SHINDE', '413 RASTA PETH QUARTER GATE PUNE 411011', 9689924243, 'FRIEND', 0, '2021-04-19 03:11:18', '2021-10-20 04:43:09', 1),
(124, 138, 94, 2, 2, 'A+', 'Fully Vaccinated', 37, '2011-07-05', NULL, NULL, '214 NANA PETH NEAR SAKHALIPIR TALIM PUNE 411 002', '214 NANA PETH NEAR SAKHALIPIR TALIM PUNE 411 002', 'RENUKA DHAYGUDE', '214 NANA PETH NEAR SAKHALIPIR TALIM PUNE 411 002', 9359300375, 'Mother', 'SUREKHA DHAYGUDE', '214 NANA PETH NEAR SAKHALIPIR TALIM PUNE 411 002', 9156505250, NULL, 'AMIT DOLE', '1194 NEW NANA PETH PUNE 411002', 9850951527, 'FRIEND', 'AMOL SARANGKAR', '274 NANA PETH PUNE 411002', 9860777761, 'FRIEND', 0, '2021-04-19 03:14:33', '2021-10-20 07:31:41', 1),
(125, 139, 97, 2, 2, 'A+', 'Fully Vaccinated', 36, '2005-05-23', NULL, NULL, 'S NO 34/1 Shri Sarthak Buld BORATE WASTI OPP SAI BABA MANDIR CHANDAN NAGAR KHARADI PUNE- 411014', 'S NO 34/1 Shri Sarthak Buld BORATE WASTI OPP SAI BABA MANDIR CHANDAN NAGAR KHARADI PUNE- 411014', 'OM GARAD', 'S NO 34/1 Shri Sarthak Buld BORATE WASTI OPP SAI BABA MANDIR CHANDAN NAGAR KHARADI PUNE- 411014', 7447448448, 'Brother', 'SOMESH GARAD', 'S NO 34/1 Shri Sarthak Buld BORATE WASTI OPP SAI BABA MANDIR CHANDAN NAGAR KHARADI PUNE- 411014', 9881490062, NULL, 'JITU GAWARE', 'S NO 1 NEAR BHAJIMANDAI VADGAON SHERI PUNE 14', 9370330320, 'FRIEND', 'NILESH TOPE', 'SHIVRAJ MITRA MANDAL CHANDAN NAGAR PUNE 14', 9881736564, 'FRIEND', 0, '2021-04-19 03:16:23', '2021-10-20 07:32:38', 1),
(126, 140, 95, 2, 2, 'B+', 'No', 37, '2006-01-01', NULL, NULL, 'H NO -1100 SR NO -01 SAHAKAR NAGAR PADMAVATI PUNE-4411003', 'H NO -1100 SR NO -01 SAHAKAR NAGAR PADMAVATI PUNE-4411003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LATA SNGAONKAR', 'H NO 1100 SR NO 01 SHAKAR NAGAR PADMAVATI PUNE 9', 9623607877, 'FRIEND', 'SWAPNIL SARNIKAR', 'H NO 1102 SR NO 01 SHAKAR NAGAR PADMAVATI PUNE 9', 9767959695, 'FRIEND', 0, '2021-04-19 03:17:44', '2021-06-26 02:04:22', 1),
(127, 141, 96, 2, 2, 'B+', 'No', 37, '2010-03-02', NULL, NULL, '411 RASTA PETH NEAR ORINILAS HIGS SCHOOL GHOD MALA PUNE 411011', '411 RASTA PETH NEAR ORINILAS HIGS SCHOOL GHOD MALA PUNE 411011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'VITTHAL GHOD', '411 RASTA PETH NEAR ORINILAS HIGS SCHOOL GHOD MALA PUNE 411011', 9822532112, 'FRIEND', 'MAHENDRA SHINDE', '411 RASTA PETH NEAR ORINILAS HIGS SCHOOL GHOD MALA PUNE 411011', 9689924243, 'FRIEND', 0, '2021-04-19 03:19:30', '2021-06-26 02:01:57', 1),
(128, 142, 94, 2, 2, 'B+', 'Partially Vaccinated', 37, '2008-10-10', NULL, NULL, '411 RASTA PETH NEAR ORINILAS HIGS SCHOOL GHOD MALA PUNE 411011', '411 RASTA PETH NEAR ORINILAS HIGS SCHOOL GHOD MALA PUNE 411011', 'PRAVIN BADHE', 'RASTA PETH', 9859653653, 'Uncle', 'ANANAD BADHE', 'RASTA PETH', 9922581111, 'Brother', 'GANESH PINJAN', '411 RASTA PETH GHOD MALA PUNE', 8793326375, 'FRIEND', 'NILESH GHOD', '415 RASTA PETH GHOD MALA PUNE', 9975828294, 'FRIEND', 0, '2021-04-19 03:21:06', '2021-10-20 07:49:36', 1),
(129, 143, 94, 2, 2, 'B+', 'Partially Vaccinated', 37, '2008-10-01', NULL, NULL, '280 SADASHIV PETH MANTRI VILLA SOC PUNE', '280 SADASHIV PETH MANTRI VILLA SOC PUNE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'NILESH BARKE', 'SR NO 28 PLOT NO 122 JAYHIND CHOWK NIGADI PUNE 411044', 9403979717, 'FRIEND', 'SHASHIKANT KARANDE', 'SR NO 28 PLOT NO 122 JAYHIND CHOWK NIGADI PUNE 411044', 9970897435, 'FRIEND', 0, '2021-04-19 03:22:32', '2021-09-08 04:30:46', 1),
(130, 144, 102, 2, 2, 'B+', 'Partially Vaccinated', 37, '2011-11-01', NULL, NULL, 'S N 48 NIVRUTTI NAGAR VADGAON BK PUNE 411 041', 'S N 48 NIVRUTTI NAGAR VADGAON BK PUNE 411 041', 'GOPAL SHAH', 'S N 48 NIVRUTTI NAGAR VADGAON BK PUNE 411 041', 7263956561, 'Spouse', 'JAYESHREE PARDESHI', 'S N 48 NIVRUTTI NAGAR VADGAON BK PUNE 411 041', 9765458316, 'Spouse', 'VIJAYSHREE PARDESHI', '445 GANESH PETH PUNE 2', 9011412114, 'FRIEND', 'GANESH PARDESHI', 'GANESH PETH PUNE', 8329613243, 'FRIEND', 0, '2021-04-19 03:24:10', '2021-10-20 07:50:51', 1),
(131, 145, 96, 2, 2, 'B+', 'Partially Vaccinated', 145, '2002-03-07', NULL, NULL, '419 RASTA PETH GHOD MALA NEAR CAUTOR GET CHOWK PUNE-411011', '419 RASTA PETH GHOD MALA NEAR CAUTOR GET CHOWK PUNE-411011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'HEMANT GHOD', '411 RASTA PETH GHOD MALA PUNE', 9922501271, 'FRIEND', 'VIJAY DIMBALE', '413 RASTA PETH QUARTER GATE PUNE 411011', 9881078009, 'FRIEND', 0, '2021-04-19 06:48:03', '2021-08-25 04:58:56', 1),
(132, 146, 94, 2, 2, 'B+', 'Partially Vaccinated', 146, '2019-11-01', NULL, NULL, '173 SOMWAR PETH NEAR NAGESHWAR TEMPLE PUNE-411011', '173 SOMWAR PETH NEAR NAGESHWAR TEMPLE PUNE-411011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'MILIND ANGRE', '169 SOMWAR PETH NEAR NAGESHWAR TEMPAL PUNE', NULL, 'FRIEND', 'NILESH NIGDE', '1401 KASBA PETH PUNE', 9850951522, 'FRIEND', 0, '2021-04-19 06:52:01', '2021-08-19 05:17:50', 1),
(133, 147, 98, 2, 2, 'B+', 'No', 37, '2010-09-01', NULL, NULL, 'S NO 50/1 GANGADE NAGAR KATEPURAM CHOWK PIMPLE GURAV NAVI SANGAVI PUNE 411 027', 'S NO 50/1 GANGADE NAGAR KATEPURAM CHOWK PIMPLE GURAV NAVI SANGAVI PUNE 411 027', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'NILESH TANGADE', 'SR NO 50/1 NAVU SANGAVI PIMPLE GURAV PUNE 27', 9921171712, 'FRIEND', 'PANDURANG WAGHMARE', 'PIMPLE GURAV', 9922461224, 'FRIEND', 0, '2021-04-19 06:53:57', '2021-06-26 02:00:20', 1),
(134, 149, 102, 2, 2, 'B+', 'No', 149, '2020-08-21', NULL, NULL, '354 SOMWAR PETH CHANDANI CHOWK NR BHOLA GIR SCHOOL PUNE- 411011', '354 SOMWAR PETH CHANDANI CHOWK NR BHOLA GIR SCHOOL PUNE- 411011', 'BHARTI MOHITE', '472 SOMWAR PETH', 8805531604, 'Mother', 'ANUPAMA MOHITE', '472 SOMWAR PETH', 9850919624, 'Sister', 'TRUNALI MORE', 'KUSHALA HOUS CHINCHWAD PUNE', 9665231313, 'FRIEND', 'ANIKET PAWAR', 'H11 HOUSING BORD YERWADA PUNE', 8446651743, 'FRIEND', 0, '2021-04-19 07:27:33', '2021-07-06 23:22:10', 1),
(135, 150, 97, 2, 2, 'AB+', 'Partially Vaccinated', 150, '2005-08-01', NULL, NULL, '8 MULA RD , KHADKI PUNE -411003', '8 MULA RD , KHADKI PUNE -411003', 'DIGAMBAR SUBHANRAO MOHALE', '8 MULA RD , KHADKI PUNE -411003', 9922951319, 'Father', 'LAXMI DIGAMBAR MOHALE', '8 MULA RD , KHADKI PUNE -411003', 9922951319, 'Mother', 'RAJU MISHRA', '08 MULA ROAD PUNE', 9850823240, 'FRIEND', 'VISHAL YADAV', 'SHASTRI NAGAR PUNE', 9822270283, 'FRIEND', 0, '2021-04-19 07:32:33', '2021-09-03 06:12:07', 1),
(136, 151, 95, 2, 2, 'B+', 'No', 37, '2015-10-04', NULL, NULL, 'SR.NO 567 FLAT NO 1 DRUPADI APT VASTU NAGAR SOCIETY BIBWEWADI KONDHWA ROAD NEAR ZALA COMPLEX BIBWEWADI PUNE- 411037', 'SR.NO 567 FLAT NO 1 DRUPADI APT VASTU NAGAR SOCIETY BIBWEWADI KONDHWA ROAD NEAR ZALA COMPLEX BIBWEWADI PUNE- 411037', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'SACHIN DESAI', 'SR NO 567 FLAT NO 1 DRUPADI APT VASTU NAGAR SOCIETY KONDHWA ROAD NEAR ZALA COMPLEX BIBWE WADI PUNE 411037', 9850909884, 'FRIEND', 'NEMARAM CHOUDHARI', 'AMBEGAON PUNE', 9096200233, 'FRIEND', 0, '2021-04-19 07:35:16', '2021-06-26 02:00:47', 1),
(137, 152, 98, 2, 2, 'B+', 'No', 37, '2010-01-01', NULL, NULL, 'BAJRANG NAGAR PIMPRI PUNE 411018', 'BAJRANG NAGAR PIMPRI PUNE 411018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'HARESH DHAWALE', 'BAJEANG NAGAR PIMPRI PUNE 411018', 9922912750, 'FRIEND', 'MAHESH JADHAV', 'F/8 3RD FLOOR HNO 1314/2 KASBA PETH PUNE 411011', 9850904151, 'FRIEND', 0, '2021-04-19 07:37:18', '2021-06-26 02:07:59', 1),
(138, 153, 98, 2, 2, 'O+', 'No', 37, '2020-10-01', NULL, NULL, 'SET NO 28 PLOT 118 NIGADI PRADHIKARAN PUNE 411044', 'SET NO 28 PLOT 118 NIGADI PRADHIKARAN PUNE 411044', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-04-19 07:39:57', '2021-05-14 02:17:00', 1),
(139, 154, 102, 2, 2, 'O+', 'Partially Vaccinated', 154, '2015-03-15', NULL, NULL, 'GHORPADI PETH NR 13 NO SCHOOL JYOTILING APP FLAT NO 408 PUNE-411042', 'GHORPADI PETH NR 13 NO SCHOOL JYOTILING APP FLAT NO 408 PUNE-411042', 'SANA SHAIKH', 'JYOTIRLING APT,GHORPADI PETH', 9657066590, 'Sister', 'FAIMEDA SHAIKH', 'JYOTIRLING APT,GHORPADI PETH', 9552906950, 'Mother', 'ASLAM SHAIKH', 'JYOTIRLING APT,GHORPADI PETH', 9921049726, 'FATHER', 'FIZAL SHAIKH', '26 GULTAKEDI AUDOGEK VASHAT PUNE', 9921049726, 'FRIEND', 0, '2021-04-19 07:42:14', '2021-09-13 09:18:43', 1),
(140, 155, 98, 2, 2, 'B+', 'Partially Vaccinated', 155, '2013-06-01', NULL, NULL, '424/2229 SANT TUKARAM NAGAR PIMPRI PUNE- 411018', '424/2229 SANT TUKARAM NAGAR PIMPRI PUNE- 411018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PRAMOD TELGU', '424/2229 SANT TUKARAM NAGAR PIMPRI PUNE 411018', 8180044557, 'FRIEND', 'DHIRAJ TELGU', '424/2229 SANT TUKARAM NAGAR PIMPRI PUNE 411018', 9552826700, 'FRIEND', 0, '2021-04-19 07:43:45', '2021-08-19 05:23:27', 1),
(141, 156, 98, 2, 2, 'B+', 'No', 37, '2015-01-01', NULL, NULL, 'S NO 33/1 LANE NO 8 NR GANPATI MANDIR SHJWAL PARK BORATE WASTI OPP SAI BABA MANDIR CHANDAN NAGAR PUNE-', 'S NO 33/1 LANE NO 8 NR GANPATI MANDIR SHJWAL PARK BORATE WASTI OPP SAI BABA MANDIR CHANDAN NAGAR PUNE-', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'SANTOSH PATIL', 'OLD SAIBABA MANDIR SHEJWAL PARK LANE NO 8 AKANSHA BUILDNIG PUNE 14', 9921547744, 'FRIEND', 'NITI GARAD', 'OLD SAIBABA MANDIR SHEJWAL PARK LANE NO 8 AKANSHA BUILDNIG PUNE 14', 8007448448, 'FRIEND', 0, '2021-04-19 07:46:12', '2021-06-26 04:14:46', 1),
(142, 157, 102, 2, 2, 'AB+', 'Partially Vaccinated', 157, '2015-08-01', NULL, NULL, '437 SOMWAR PETH OLD Z P PUNE-411011', '437 SOMWAR PETH OLD Z P PUNE-411011', 'YAMUNA PAWAR', 'YUGMA PRESTIGE,GAURI ALI,GURUWAR PETH', 9823935939, 'Mother', 'RAJENDRA SHINDE', '209 SATYAI APT OPP PARANDE HOUSE,PARANDE NAGAR DHANORI,PUNE-15', 9822255923, 'Spouse', 'RAHUL PAWAR', 'YUGMA PRESTIGE,GAURI ALI,GURUWAR PETH', 9021097339, 'FRIEND', 'SNEHA KHOPADE', '378 SHUKRAVAR PETH PUNE', 9145279042, 'FRIEND', 0, '2021-04-19 07:49:22', '2021-09-13 09:14:21', 1),
(143, 158, 96, 2, 2, 'B+', 'No', 37, '2017-05-09', NULL, NULL, '429 /30 DAISPLOTGULTEKADI NEAR  NURANI MASJJID MARKET YARD PUNE-411037', '429 /30 DAISPLOTGULTEKADI NEAR  NURANI MASJJID MARKET YARD PUNE-411037', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PRASAD BANDAL', 'SANTOSH NAGAR KATRAJ PUNE 411046', 9665030292, 'FRIEND', 'RAHUL KATARE', 'HNO182 SNO 119 KATRAJ ANJALI NAGAR PUNE 411046', 7774006419, 'FRIEND', 0, '2021-04-19 07:51:13', '2021-06-26 04:01:34', 1),
(144, 159, 102, 2, 2, 'O+', 'Partially Vaccinated', 159, '2019-01-02', NULL, NULL, 'SNO 34/19/1 VIDAY DHAN APT DHANKAWADI FL NO C/26 PUNE', 'SNO 34/19/1 VIDAY DHAN APT DHANKAWADI FL NO C/26 PUNE', 'HEMANT SURESH MELAWANE', 'FLAT NO-404,SHREE GANESH COMPLEX,DHAYRI GAON,PUNE-411041', 7020722372, 'Brother', 'SURESH N MELAWANE', 'FLAT NO-404,SHREE GANESH COMPLEX,DHAYRI GAON,PUNE-411041', 9764220725, 'Father', 'PRATIK BANKAR', 'FLAT NO C1 26 VIDHYADHAN SOCIETY BHARATI VIDHYAPITH', 9673966722, 'HUSBAND', 'CHANDAN SALUNKE', 'KHADAKMAL ALI GURUVAR PETH', 9850951517, 'FRIEND', 0, '2021-04-19 07:55:44', '2021-09-13 09:36:56', 1),
(145, 160, 98, 2, 2, 'B+', 'Partially Vaccinated', 160, '2015-10-01', NULL, NULL, '197 GANESH PETH NEAR GURUNANK HIGH SCHOOL PUNE 411002_x000D_', '197 GANESH PETH NEAR GURUNANK HIGH SCHOOL PUNE 411002_x000D_', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'SUHAS SHINDE', '200 NANA PETH 411002', 8149195374, 'FRIEND', 'VIKRAM NALAVDE', '197 GANESH PETH PUNE 411002', 9325406086, 'FRIEND', 1, '2021-04-19 07:57:34', '2021-10-21 01:12:04', 1),
(146, 161, 95, 2, 2, 'B+', 'Partially Vaccinated', 161, '2016-06-01', NULL, NULL, '618 GHORPADE PETH KHADAK MAL ALI NEARGANESH TEMPLE PUNE411042', '618 GHORPADE PETH KHADAK MAL ALI NEARGANESH TEMPLE PUNE411042', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'NITIN UPHALE', '617 GHORPADE PETH KHADAKMAL ALI PUNE', 9763826391, 'FRIEND', 'SANTOSH PAIGUDE', '617 GHORPADE PETH KHADAKMAL ALI PUNE', 9689940192, 'FRIEND', 1, '2021-04-19 08:00:05', '2021-09-15 04:42:11', 1),
(147, 162, 98, 2, 2, 'A+', 'No', 37, '2016-07-12', NULL, NULL, 'G N 530 NR NAMDEV NIVAS BANKAR WASTI PUNE-412105', 'G N 530 NR NAMDEV NIVAS BANKAR WASTI PUNE-412105', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'AKSHAY GAWADE', 'G N 535 NR NAMDEV NIVAS BANKAR WASTI PUNE', 9881331579, 'FRIEND', 'SUNIL BANKAR', 'G N 535 NR NAMDEV NIVAS BANKAR WASTI PUNE', 9075663636, 'FRIEND', 0, '2021-04-19 08:03:59', '2021-06-26 02:20:10', 1),
(148, 163, 97, 2, 2, 'O+', 'Partially Vaccinated', 37, '2015-10-01', NULL, NULL, 'Nutan Maharashtra Soc,Ghar no.626,Padmavati,Sahakarnagar No.1,Police Station Near Pune 411009', 'Nutan Maharashtra Soc,Ghar no.626,Padmavati,Sahakarnagar No.1,Police Station Near Pune 411009', 'INDU BHARAT BALWAR', 'Nutan Maharashtra Soc,Ghar no.626,Padmavati,Sahakarnagar No.1,Police Station Near Pune 411009', 9665874386, 'Mother', 'GAYATRI MAHESH BALWAR', 'Nutan Maharashtra Soc,Ghar no.626,Padmavati,Sahakarnagar No.1,Police Station Near Pune 411009', 7057022138, 'Spouse', 'ABHIJEET RAJPUT', '255 GURWAR PETH BALWAR ALI 411042', 9922964328, 'FRIEND', 'GAURAV BALWAR', '308 GURUVAR PETH BALWAR ALI 411042', 9503685884, 'FRIEND', 0, '2021-04-19 08:49:29', '2021-09-03 06:11:39', 1),
(149, 164, 98, 2, 2, 'B+', 'No', 37, '2020-08-01', NULL, NULL, 'SR NO 113/7 MAJEET WALHEKARWADI PUNE DA NIGADI PUNE 411033', 'SR NO 113/7 MAJEET WALHEKARWADI PUNE DA NIGADI PUNE 411033', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'NILESH BARKE', 'P NO 122 SECTOR 28 PCNT DA NIGADI PUNE 411044', 7745895566, 'FRIEND', 'KESHAV KARDE', 'AP BASHI SOLAPUR ROAD TAL BARSHI DIST SOLAPUR', 9850177839, 'FRIEND', 0, '2021-04-19 08:54:05', '2021-06-26 03:09:02', 1),
(150, 165, 95, 2, 2, 'B+', 'Partially Vaccinated', 165, '2016-10-01', NULL, NULL, '67A SADASHIV PETH NEAR OLD MOTOR STAND SATARA 415002', '67A SADASHIV PETH NEAR OLD MOTOR STAND SATARA 415002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-04-19 08:57:00', '2021-08-19 05:33:02', 1),
(151, 166, 99, 2, 2, 'B+', 'Partially Vaccinated', 166, '2016-03-10', NULL, NULL, 'SR NO.15/7/1,GHAR NO. 7, KHADAKWASLA, KOLHEWADI, PUNE 411024', 'SR NO.15/7/1,GHAR NO. 7, KHADAKWASLA, KOLHEWADI, PUNE 411024', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'NITIN FALE', '617 GHORPADI PETH PUNE', 9763826391, 'FRIEND', 'SANTOSH PAYGUDE', '581 GHORPADE PETH PUNE', 9689940192, 'FRIEND', 0, '2021-04-19 08:59:12', '2021-08-21 07:37:42', 1),
(152, 167, 94, 2, 2, 'B+', 'No', 37, '2019-02-02', NULL, NULL, 'KATRAJ KONDHWA ROAD MANSING VILLA SR NO 48 GOKULNAGAR LANE NO 3 KONDHWA BK PUNE- 41146', 'KATRAJ KONDHWA ROAD MANSING VILLA SR NO 48 GOKULNAGAR LANE NO 3 KONDHWA BK PUNE- 41146', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ANUPAMA MOHITE', '430 MANGALWAR PETH PUNE', 9850919624, 'FRIEND', 'GANESH BHONGALE', 'SV UNION SCHOOL FLAT NO 202 2ND DLOOR CHANDRABHAGA APP', 9822550856, 'FRIEND', 1, '2021-04-19 09:02:10', '2021-08-23 05:21:55', 1),
(153, 168, 97, 2, 2, 'B+', 'Partially Vaccinated', 37, '2017-05-13', NULL, NULL, '468,BHAWANI PETH HARKA NAGAR NEAR RAM MANDIR PUNE - 411042', '468,BHAWANI PETH HARKA NAGAR NEAR RAM MANDIR PUNE - 411042', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'MAHENDRA KARASIYA', '412 BHAVAI PETH HARKA NAGAR PUNE', 9158979691, 'FRIEND', 'AMIT KARASIYA', '412 BHAVANI PETH HARKA NAGAR', 9637114143, 'FRIEND', 0, '2021-04-19 09:05:34', '2021-10-20 07:57:02', 1),
(154, 169, 94, 2, 2, 'B+', 'No', 37, '2017-05-10', NULL, NULL, '214 GURUVAR PETH MATAN MARKET OPP NAKODA DHARMSHALA', '214 GURUVAR PETH MATAN MARKET OPP NAKODA DHARMSHALA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'CHETAN JAGTAP', '343 GURUVAR PETH KISHOR SUPAR MARKET PUNE', 8383730583, 'FRIEND', 'KRUSHNA THAKUR', '343 GURUVAR PETH KISHOR SUPAR MARKET PUNE', 8446841040, 'FRIEND', 0, '2021-04-19 09:07:16', '2021-06-26 03:58:06', 1),
(155, 170, 99, 2, 2, 'B+', 'No', 37, '2017-05-10', '', '2021-08-01', 'AMBEGAON PADHAR SR. NO. 16 GALI NO. 4 NEAR GANGAJ APPARTMENT', 'AMBEGAON PADHAR SR. NO. 16 GALI NO. 4 NEAR GANGAJ APPARTMENT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'SACHIN RENUSE', '343 GURUVAR PETH PUNE', 8888237723, 'FRIEND', NULL, NULL, NULL, NULL, 1, '2021-04-19 09:45:51', '2021-08-18 07:44:23', 1),
(156, 171, 102, 2, 2, 'B+', 'Partially Vaccinated', 171, '2018-06-19', NULL, NULL, 'HO. NO 692 FL 9 GURUWAR PETH PUNE-411042', 'HO. NO 692 FL 9 GURUWAR PETH PUNE-411042', 'NASEN BANSOD', 'NEAR RICE GODOWN WARTHI BHANARA ROAD NAGPUR 441905', 8421404419, 'Father', 'NEHA  BANSOD', 'NEAR RICE GODOWN WARTHI BHANARA ROAD NAGPUR 441905', 8208258882, 'Sister', 'PRIYASHIT MESHROM', 'FLAT NO 19 SHARDA PLAZA 342 RASTA PETH PUNE 411011', 9923692037, 'FRIEND', 'SANDHYA BENDURE', '87 RASTA PETH SONU APP NEAR KADAR BHAI CHOWK PUNE', 9284970873, 'FRIEND', 0, '2021-04-19 09:50:11', '2021-09-09 07:27:13', 1),
(157, 172, 94, 2, 2, 'B+', 'No', 37, '2018-06-01', NULL, NULL, '456 GANESH PETH DAGALI NAGABA TEMPAAL PUNE-02', '456 GANESH PETH DAGALI NAGABA TEMPAAL PUNE-02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'GAURAV SEDGE', '455 GANESH PETH PUNE', 9890047663, 'FRIEND', 'RAKESH POTE', '477 GANESH PETH PUNE', 9960422005, 'FRIEND', 1, '2021-04-19 09:52:54', '2021-10-08 03:13:10', 1),
(158, 173, 102, 2, 2, 'B+', 'No', 37, '2019-06-01', NULL, NULL, 'A WING FLAT NO 505 ANANDI PARK BELHEKAR VASTI MANJARI BUDRUK NEAR GANPATI TEMPLE  MANJARI BK PUNE', 'A WING FLAT NO 505 ANANDI PARK BELHEKAR VASTI MANJARI BUDRUK NEAR GANPATI TEMPLE  MANJARI BK PUNE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'MAHENDRA SURYWANSHI', 'A WING FLAT NO 505 ANANDI PARK BELHEKAR VASTI MANJARI BUDRUK NEAR GANPATI TEMPLE  MANJARI BK PUNE', 9623967716, 'FRIEND', 'RASHMI KADAM', 'MODI KHANA NEAR POONA COLLAGE', 9370569389, 'FRIEND', 1, '2021-04-19 09:54:59', '2021-08-18 07:44:29', 1),
(159, 174, 102, 2, 2, 'B+', 'Partially Vaccinated', 36, '2020-01-12', NULL, NULL, 'TAL DAUND DIST PUNE HN 225 KHOPODI PUNE 412214', 'TAL DAUND DIST PUNE HN 225 KHOPODI PUNE 412214', 'ASHOK C. MEMANE', 'TAL DAUND DIST PUNE HN 225 KHOPODI PUNE 412214', 9359686179, 'Father', 'VAISHALi MEMANE', 'TAL DAUND DIST PUNE HN 225 KHOPODI PUNE 412214', 9359686179, 'Mother', 'ATUL BHALERAO', 'A/P BORIPARDHI TAL DAUND DIST PUNE-412203', 7350668195, 'FRIEND', 'MAHESH RATHOD', 'WALEAWADI NEAR PUSHKAR BUILDING GAURAV WADA KIREGAON BK PUNE 410401', 9158606249, 'FRIEND', 0, '2021-04-19 09:57:34', '2021-10-20 07:55:41', 1),
(160, 175, 99, 2, 2, 'B+', 'No', 36, '2019-08-07', NULL, NULL, 'SR NO 464 KANJANE WASTI URAWADE PIRANGUD MULSHI PUNE-412115', 'SR NO 464 KANJANE WASTI URAWADE PIRANGUD MULSHI PUNE-412115', 'NARENDRA KHEDEKAR', 'SR NO 464 KANJANE WASTI URAWADE PIRANGUD MULSHI PUNE-412115', 9158712593, 'Brother', NULL, 'SR NO 464 KANJANE WASTI URAWADE PIRANGUD MULSHI PUNE-412115', 9370421311, 'Father', 'PRATIK MARNE', 'URAWADE TEL MULSHI DIST PUNE', 7030642952, 'FRIEND', 'KUNAL SUTAR', 'URAWADE TEL MULSHI DIST PUNE', 9762865294, 'FRIEND', 0, '2021-04-19 10:04:11', '2021-10-20 07:57:51', 1),
(161, 176, 94, 2, 2, 'B+', 'Partially Vaccinated', 37, '2019-09-22', NULL, NULL, 'SR NO 35/3/3 GANRAJ HOUSHING SOCIETY FLAT NO 13 MOHAN NAGAR DHANKWADI PUNE-411043', 'SR NO 35/3/3 GANRAJ HOUSHING SOCIETY FLAT NO 13 MOHAN NAGAR DHANKWADI PUNE-411043', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ANAND JAGTAP', 'RASTE WADA RASTA PETH PUNE 411011', 8830625976, 'FRIEND', 'SUNIL JAGTAP', '456 SOMWAR PETH PUNE 411011', 9422438543, 'FRIEND', 0, '2021-04-19 10:06:24', '2021-10-20 08:02:43', 1),
(162, 177, 95, 2, 2, 'O+', 'Fully Vaccinated', 36, '2020-02-05', NULL, NULL, '29/1/2 GULMAGAR PARK CO.SOCITY NEAR VIJAY BEKRI SOMWAR PETH PUNE 411011', '29/1/2 GULMAGAR PARK CO.SOCITY NEAR VIJAY BEKRI SOMWAR PETH PUNE 411011', 'SHOBHA SAMBHAJI LOKHANDE', '29/1/2 GULMAGAR PARK CO.SOCITY NEAR VIJAY BEKRI SOMWAR PETH PUNE 411011', 9146100850, 'Mother', 'SANGITA YOGESH LOKHANDE', '29/1/2 GULMAGAR PARK CO.SOCITY NEAR VIJAY BEKRI SOMWAR PETH PUNE 411011', 9028444740, NULL, 'PIYUSH TUPE', '29/1/2 GULMAGAR PARK CO.SOCITY NEAR VIJAY BEKRI SOMWAR PETH PUNE 411011', 9673858673, 'FRIEND', 'SUJATA MULE', '29/1/2 GULMAGAR PARK CO.SOCITY NEAR VIJAY BEKRI SOMWAR PETH PUNE 411011', 9552607982, 'FRIEND', 0, '2021-04-19 10:08:42', '2021-10-20 08:01:50', 1),
(163, 178, 102, 2, 2, 'B+', 'No', 56, '2020-08-17', NULL, NULL, 'SR NO 67/1/2 PATHARE VASTI LOHAGAON PUNE 411047', 'SR NO 67/1/2 PATHARE VASTI LOHAGAON PUNE 411047', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'VIKAS TIWARI', 'WAGHOLI PUNE', 9484318220, 'FRIEND', 'ANNU TIWARI', '77 JABALPUR', 7722814305, 'FRIEND', 1, '2021-04-19 10:29:50', '2021-09-09 07:55:46', 1),
(164, 179, 102, 2, 2, 'B+', 'Partially Vaccinated', 37, '2020-11-07', NULL, NULL, '209 GURUWAR PETH NEAR NAKODA DHARAMSHALA KRISHNA HEIGHTS APT FLAT NO 19 PUNE-411042', '209 GURUWAR PETH NEAR NAKODA DHARAMSHALA KRISHNA HEIGHTS APT FLAT NO 19 PUNE-411042', 'KISHOR OSWAL', '209 GURUWAR PETH NEAR NAKODA DHARAMSHALA KRISHNA HEIGHTS APT FLAT NO 19 PUNE-411042', 9422016849, 'Father', 'SANGITA OSWAL', '209 GURUWAR PETH NEAR NAKODA DHARAMSHALA KRISHNA HEIGHTS APT FLAT NO 19 PUNE-411042', 2024436370, 'Mother', 'YASHWANT RAJPUT', 'GURUVAR PETH NEAR NAKODA DHARMASHALA KRISHNA HATTI CHOWK PUNE 42', 7038174401, 'FRIEND', 'MAMTA RANAWAT', 'GURUVAR PETH NEAR NAKODA DHARMASHALA KRISHNA HATTI CHOWK PUNE 42', 9822950123, 'FRIEND', 0, '2021-04-19 10:32:00', '2021-10-20 08:15:00', 1),
(165, 180, 96, 2, 2, 'B+', 'No', 180, '2020-11-01', NULL, NULL, 'KAKDE WASTI LANE NO 3 KONDHWA BK PUNE 48', 'KAKDE WASTI LANE NO 3 KONDHWA BK PUNE 48', 'SURYAKANT LONDHE', 'KAKDE WASTI LANE NO 3 KONDHWA BK PUNE 48', 9370600915, 'Father', 'GANESH LONDHE', 'KAKDE WASTI LANE NO 3 KONDHWA BK PUNE 48', 7447774424, 'Brother', 'AKASH SHILIMKAR', 'KAKADE WASTI KONDHWA BK PUNE', 9921079181, 'FRIEND', 'HARSHAL GAIKWAD', 'SAI NAGAR KONDHWA BK.PUNE', 7620804550, 'FRIEND', 0, '2021-04-19 10:34:26', '2021-10-08 05:49:23', 1),
(166, 181, 104, 2, 2, 'B+', 'Partially Vaccinated', 37, '2020-10-01', NULL, NULL, 'GHAR NO 1403 TAKSAL LEN BHADRAKALI NASHIK 422001', 'GHAR NO 1403 TAKSAL LEN BHADRAKALI NASHIK 422001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PRADIP BADGE', 'NASHIK', 9767833701, 'FRIEND', 'SANGEETA BUDHKAR', 'NASHIK', 9823321726, 'FRIEND', 0, '2021-04-19 10:37:50', '2021-10-20 08:20:36', 1),
(167, 182, 94, 2, 2, 'B+', 'No', 37, '2020-07-05', NULL, NULL, '415/1 NEAR GHOD MALA RASTA PETH PUNE 411011', '415/1 NEAR GHOD MALA RASTA PETH PUNE 411011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ROHIT KALAMBE', '395 RASTA PETH QWATER GATE OPP YMCA PUNE 411011', 7709713554, 'FRIEND', 'OMKAR BHALERAO', '415/2 GHOD MALA OPP ORNELLAS HIGH SCHOL PUNE 411011', 9834092922, 'FRIEND', 1, '2021-04-19 10:40:31', '2021-10-21 01:12:24', 1),
(168, 183, 94, 2, 2, 'O+', 'No', 37, '2021-01-01', NULL, NULL, 'GANANJAY SOCIETY SAMARTH NIWAS BUILDING KOTHRUD PUNE 38', 'GANANJAY SOCIETY SAMARTH NIWAS BUILDING KOTHRUD PUNE 38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'TEJAS VICHARE', '150 CHINCHWALI TARPE DIWAL POST TALAVALI TAL ROHA DIST RAIGAD', 8149165981, 'FRIEND', 'URMILA GAIKWAD', 'GANANJAY SOCIETY SAMARTH NIWAS BUILDING KOTHRUD PUNE 38', 7972070976, 'FRIEND', 0, '2021-04-19 10:42:59', '2021-06-26 03:56:07', 1),
(169, 184, 102, 2, 2, 'B+', 'No', 56, '2021-01-20', NULL, NULL, 'HOUSE NO 3 JANAWADI BUS STOP KUSALKAR CHOEK GOKHALE NAGAR PUNE', 'HOUSE NO 3 JANAWADI BUS STOP KUSALKAR CHOEK GOKHALE NAGAR PUNE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'YASH YENPURE', NULL, 8459416304, 'FRIEND', 'AKASH KATARIA', 'FLAT NO 5 SHIVAM RESIDENCY SANJAY NAGAR DAHIWALI KARJAT 410201', 9323887533, 'FRIEND', 1, '2021-04-19 10:45:34', '2021-08-18 07:45:06', 1),
(170, 185, 98, 2, 2, 'O+', 'No', 185, '2021-02-08', '', '2021-08-01', 'BANER GAON NEAR PMC OFFICE PUNE', 'BANER GAON NEAR PMC OFFICE PUNE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-04-19 10:52:25', '2021-08-19 05:18:52', 1),
(171, 186, 96, 2, 2, 'B+', 'No', 37, '2021-02-17', NULL, NULL, 'SAMTA NAGAR 601 KONDHWA ROAD KONDHWA KHURD SAI BABA TEMPLE PUNE 411048', 'SAMTA NAGAR 601 KONDHWA ROAD KONDHWA KHURD SAI BABA TEMPLE PUNE 411048', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'KHANDU JADHAV', 'SAMTA NAGAR 601 KONDHWA ROAD KONDHWA KHURD SAI BABA TEMPLE PUNE 411048', 9881032571, 'FRIEND', 'NIRANJAN BANSODE', 'SR NO 601 SAMTA NAGAR KONDHWA ROAD PUNE 411048', 7744001806, 'FRIEND', 1, '2021-04-19 10:54:57', '2021-10-08 03:13:54', 1),
(172, 187, 103, 2, 2, 'B+', 'No', 37, '2021-02-01', NULL, NULL, '638/2A/2, Tuljabhavani Ngar, PMT Colony, Bibvewadi Pune', '638/2A/2, Tuljabhavani Ngar, PMT Colony, Bibvewadi Pune', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Appasaheb Khedkar', 'Tuljabhavani Ngar, PMT Colony, Bibvewadi Pune', 9503508632, 'FRIEND', 'Pratiksha Nevase', 'Bajbhog Society,Apper Depo, Dolffin Chowk,', 7057029379, 'FRIEND', 1, '2021-04-19 10:57:59', '2021-08-18 07:45:19', 1),
(173, 188, 98, 2, 2, 'B+', 'No', 37, '2021-03-05', NULL, NULL, 'Dwarka Housing Society,Near Gharjai Mandir, Rupeen Nagar, Talawade Pune-411062', 'Dwarka Housing Society,Near Gharjai Mandir, Rupeen Nagar, Talawade Pune-411062', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Suman Shinde', 'Dwarka Housing Society,Near Gharjai Mandir, Rupeen Nagar, Talawade Pune-411062', 9767363567, 'FRIEND', 'Vishnu Harale', 'Sainath Housicing, Gharjai Mandir Rupee Nagar, Nigadi', 8888237700, 'FRIEND', 1, '2021-04-19 10:59:37', '2021-10-08 03:14:04', 1),
(174, 189, 97, 2, 2, 'B+', 'No', 37, '2021-03-05', NULL, NULL, 'Flat No 601,1445, Shukrawar Peth, Jai Gandhi Chembers Aprt, Shanipar Pune-411002', 'Flat No 601,1445, Shukrawar Peth, Jai Gandhi Chembers Aprt, Shanipar Pune-411002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Anand Nadkhile', 'Manjari Budruk, Pune', 9623765773, 'FRIEND', 'Anup Panchwadlkar', 'Sadashiv Peth Pune', 9921480063, 'FRIEND', 0, '2021-04-19 11:04:06', '2021-08-18 07:45:26', 1),
(175, 190, 98, 2, 2, 'B+', 'Partially Vaccinated', 190, '2020-12-12', NULL, NULL, '172 Ganesh Peth, Near Ranka Jewelares, gamesh Peth Pune 411002', '172 Ganesh Peth, Near Ranka Jewelares, gamesh Peth Pune 411002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Amit Yewale', 'Wade Bhulai Road Pune-412207', 9881117911, 'FRIEND', 'Sumit Bavaskar', 'Nanapeth Pune', 9765412777, 'FRIEND', 0, '2021-04-19 11:09:01', '2021-08-19 05:52:28', 1),
(176, 191, 100, 2, 4, 'B+', 'No', 191, '2019-04-11', NULL, NULL, 'FLAT NO-402 JANKABAI, NIWAS NEAR GANESH MARUTI, BEHIND R.P.F POLICE CHOUKI DIVA(WEST) THANE 400612.', 'FLAT NO-402 JANKABAI,NIWAS NEAR GANEESH MARUTI,BEHID R.P.F POLICE CHOUKI DIVA(WEST) THANE 400612.', 'SANJAY BHOIR', 'FLAT NO-401 JANKABAI, NIWAS NEAR GANESH MARUTI, BEHIND R.P.F POLICE CHOUKI DIVA(WEST) THANE 400612.', 9224407090, 'Brother', 'RUPAM GAJANAN BHOIR', 'J.M.BHOJI HOUSE NEAR R.B.I QUARTERS BHANDUP EAST MUMBAI 4', 9321817977, 'Brother', 'NEELWANT SHIVAJI BANSODE', 'JAY MATA DI COMPLEX PHASE 3 MARS BUILDING FLAT NO106 1ST FLOOR BEFORE KASHELI TOLL NAKA KASHELI THANE 421302', 8180982248, 'FRIEND', 'LAHU SITARAM APARAJ', 'E E 301 FLOOR 3 PLOT CS 85 MANGALMURTI CHS NM JOSHI MARG PRAGATI INDL EST LOWER PAREL JACOB CIRCLS MUMBAI 400011', 8424846685, 'FRIEND', 0, '2021-04-19 11:26:51', '2021-09-11 04:42:24', 1),
(177, 192, 100, 5, 7, 'B+', 'No', 191, '2019-04-06', NULL, NULL, 'JAY MATA DI COMPLEX PHASE 3 MARS BUILDING FLAT NO106 1ST FLOOR BEFORE KASHELI TOLL NAKA KASHELI THANE 421302', 'JAY MATA DI COMPLEX PHASE 3 MARS BUILDING FLAT NO106 1ST FLOOR BEFORE KASHELI TOLL NAKA KASHELI THANE 421302', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LAVKIK SKIRSAGAR', 'JAY MATAJI COMPLEX MARS BULDING KASHELI THANE', 7303201994, 'FRIEND', 'VITTAL GOSAVI', 'JAY MATAJI COMPLEX MARS BULDING KASHELI THANE', 8652430730, 'FRIEND', 0, '2021-04-19 11:30:44', '2021-06-26 04:49:56', 1),
(178, 193, 100, 5, 7, 'B+', 'No', 191, '2019-04-17', '', NULL, 'AT KUDAVE POST PALASPE TALUKA PANVEL RAIGAD', 'AT KUDAVE POST PALASPE TALUKA PANVEL RAIGAD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-04-19 11:32:47', '2021-10-29 00:49:12', 1),
(179, 194, 100, 5, 7, 'B+', 'No', 191, '2019-06-04', NULL, NULL, 'JAY BHOLE  CHWAL NO 4 ROOM NO 15 BEDEKAR NAGAR NEAR KANISKA CHWAL 400612', 'JAY BHOLE  CHWAL NO 4 ROOM NO 15 BEDEKAR NAGAR NEAR KANISKA CHWAL 400612', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'SANJAY MAHADIK', 'JAY BHOLE  CHWAL NO 4 ROOM NO 15 BEDEKAR NAGAR NEAR KANISKA CHWAL 400612', 8693062238, 'FRIEND', 'PRATIKSHA MAHADIK', 'JAY BHOLE  CHWAL NO 4 ROOM NO 15 BEDEKAR NAGAR NEAR KANISKA CHWAL 400612', 7039287267, 'FRIEND', 1, '2021-04-20 11:42:14', '2021-10-27 04:44:23', 1),
(180, 195, 100, 5, 7, 'B+', 'No', 191, '2019-09-21', NULL, NULL, 'ROOM NO 306 VIJAY APARTMENT KORAS ROAD NEAR R J THAKUR COLLEGE THANE WEST JEKEGRAM THANE 400606', 'ROOM NO 306 VIJAY APARTMENT KORAS ROAD NEAR R J THAKUR COLLEGE THANE WEST JEKEGRAM THANE 400606', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'SHIVAJI PAWAR', 'ROOM NO 306 3RD FLOOR VIJAYA APP CORSS ROAD THANE', 9594899526, 'FRIEND', 'MEENA PAWAR', 'ROOM NO 306 3RD FLOOR VIJAYA APP CORSS ROAD THANE', 7039463082, 'FRIEND', 0, '2021-04-20 11:45:23', '2021-06-26 04:57:29', 1),
(181, 196, 100, 2, 4, 'B+', 'No', 196, '2019-11-02', NULL, NULL, 'C/104 CHAITANYA APP SARASWATI BAG VUVA COLLEGE DAMODAR NAGAR VIRAR THANE', 'C/104 CHAITANYA APP SARASWATI BAG VUVA COLLEGE DAMODAR NAGAR VIRAR THANE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'RUPALI GHORGE', 'C/104 CHAITANYA APP SARASWATI BAG VUVA COLLEGE DAMODAR NAGAR VIRAR THANE', 9284777923, 'FRIEND', 'GAYATRI', 'C/104 CHAITANYA APP SARASWATI BAG VUVA COLLEGE DAMODAR NAGAR VIRAR THANE', 9309934917, 'FRIEND', 0, '2021-04-20 11:52:56', '2021-09-11 06:38:15', 1),
(182, 197, 100, 5, 7, 'B+', 'No', 191, '2020-01-05', NULL, NULL, 'SHAHAPUR MURBAD ROAD GOKULGAON LENAD BK THANE 421601', 'SHAHAPUR MURBAD ROAD GOKULGAON LENAD BK THANE 421601', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-04-20 11:54:23', '2021-05-17 03:26:12', 1),
(183, 198, 100, 5, 7, 'B+', 'No', 191, '2020-02-03', NULL, NULL, 'DANYDEEP COL 2/6 KATEMANIVALI SHREE SAI SAMART SOC SAHYADRI PARK SATEMAIVALI THANE 421306', 'DANYDEEP COL 2/6 KATEMANIVALI SHREE SAI SAMART SOC SAHYADRI PARK SATEMAIVALI THANE 421306', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ASHOK MAKWANA', 'ROOM NO 10 CHWAL NO 2 OM SAMARTH KRUPA SOC DANDEEP COLONY SURYA SHOOL ROAD VITTHAL WADI E 421306', 9702910721, 'FRIEND', 'MARIA PERNANDIS', 'ROOM NO 10 CHWAL NO 2 OM SAMARTH KRUPA SOC DANDEEP COLONY SURYA SHOOL ROAD VITTHAL WADI E 421306', 8850213057, 'FRIEND', 1, '2021-04-20 11:56:12', '2021-10-20 01:22:26', 1),
(184, 199, 100, 5, 7, 'B+', 'No', 191, '2020-07-01', NULL, NULL, 'E E 301 FLOOR 3 PLOT CS 85 MANGALMURTI CHS NM JOSHI MARG PRAGATI INDL EST LOWER PAREL JACOB CIRCLS MUMBAI 400011', 'E E 301 FLOOR 3 PLOT CS 85 MANGALMURTI CHS NM JOSHI MARG PRAGATI INDL EST LOWER PAREL JACOB CIRCLS MUMBAI 400011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'SHARAD WADEKAR', 'MANGAL MARUTI CO-OP HC SO CL WING 5 FLOOR ROOM NO 501 N M JOSHI MARG MUMBAI', 9324477835, 'FRIEND', 'SANDEEP NAIK', 'MANGAL MARUTI CO-OP HC SO CL WING 2 FLOOR ROOM NO 501 N M JOSHI MARG MUMBAI', 9594739744, 'FRIEND', 0, '2021-04-20 11:57:28', '2021-06-26 04:43:45', 1),
(185, 200, 100, 5, 7, 'B+', 'No', 191, '2020-10-07', NULL, NULL, 'B 208 NAGESH CHS SECOND FLOOR LBS MARG HARINIWAS NAGOBACHI WADI NAUOADA THANE WEST 400602', 'B 208 NAGESH CHS SECOND FLOOR LBS MARG HARINIWAS NAGOBACHI WADI NAUOADA THANE WEST 400602', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-04-20 11:58:43', '2021-05-17 03:36:55', 1),
(186, 201, 100, 2, 4, 'B+', 'Partially Vaccinated', 191, '2019-09-08', NULL, NULL, 'ROOM NO 3 NAKTE CHAWL RAMCHANDRA NAGAR NO.1 YHANE WEST 400604', 'ROOM NO 3 NAKTE CHAWL RAMCHANDRA NAGAR NO.1 YHANE WEST 400604', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-04-20 12:00:24', '2021-10-20 08:05:27', 1),
(187, 202, 100, 5, 7, 'B+', 'No', 202, '2020-09-12', NULL, NULL, 'J.M.BHOJI HOUSE NEAR R.B.I QUARTERS BHANDUP EAST MUMBAI 4', 'J.M.BHOJI HOUSE NEAR R.B.I QUARTERS BHANDUP EAST MUMBAI 4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'SAMEER BHOIR', 'J.M.BHOJI HOUSE NEAR R.B.I QUARTERS BHANDUP EAST MUMBAI 4', 9619090351, 'FRIEND', 'AMIT GAWAI', 'ROOM NO 4 KOPARKAR CHAWL NEAR SHANKAR TEMPLE BHANDUP EAST MUMBAI 4', 8422918207, 'FRIEND', 0, '2021-04-20 12:02:19', '2021-08-20 06:48:36', 1),
(188, 203, 94, 2, 2, 'B+', 'Partially Vaccinated', 203, '2013-07-20', NULL, NULL, 'SR.NO.74, GALLI NO. 28, SAYYED NAGAR, HADAPSAR ,UNION BANK ATM PUNE 411028', 'SR.NO.74, GALLI NO. 28, SAYYED NAGAR, HADAPSAR ,UNION BANK ATM PUNE 411028', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'AZIM SHAIKH', 'LANE NO 28 SAYYED NAGAR MOHAMMAD WADI ROAD HADAPSAR PUNE 411028', 9767961547, 'FRIEND', 'AMAN SHAIKH', 'LANE NO 28 SAYYED NAGAR MOHAMMAD WADI ROAD HADAPSAR PUNE 411028', 9767961547, 'FRIEND', 0, '2021-04-25 05:38:13', '2021-08-19 05:59:06', 1),
(189, 204, 102, 2, 2, 'AB+', 'No', 36, '2016-05-21', NULL, NULL, '1009 NEW NANA PETH NEAR NISHAT CINEMA PUNE -411002', '1009 NEW NANA PETH NEAR NISHAT CINEMA PUNE -411002', 'ARCHANA BANEKAR', '1009 NEW NANA PETH NR NISHAT RHEATER PUNE 411002', 9689296228, 'Sister', 'KETAN JAGTAP', '1009 NEW NANA PETH NEAR NISHAT CINEMA PUNE -411002', 7517411383, 'Brother', 'KANCHAN GAIKWAD', '786 BHAVANI PETH SIDHAVTH HOUSHING SOCIETY ROOM NO 504 PUNE', 8308615582, 'FRIEND', 'SIDDHI BANEKAR', '1009 NEW NANA PETH NR NISHAT RHEATER PUNE 411002', 7720093744, 'SISTER', 0, '2021-04-25 05:40:00', '2021-08-31 07:17:13', 1),
(190, 205, 94, 2, 2, 'O+', 'No', 36, '2017-06-01', NULL, NULL, 'SAUDAGAR GARDEN BUIDING NO A-2 PLOT NO 5 BEHIND MAHALE FLOOR MIL VIKAS NAGAR KIWALE DEHU ROAD PUNE-41201', 'SAUDAGAR GARDEN BUIDING NO A-2 PLOT NO 5 BEHIND MAHALE FLOOR MIL VIKAS NAGAR KIWALE DEHU ROAD PUNE-41201', 'RUPALI SANTOSH KENGAR', 'SAUDAGAR GARDEN BUIDING NO A-2 PLOT NO 5 BEHIND MAHALE FLOOR MIL VIKAS NAGAR KIWALE DEHU ROAD PUNE-41201', 9850534648, 'Spouse', 'VAIJYANTI KENGAR', 'SAUDAGAR GARDEN BUIDING NO A-2 PLOT NO 5 BEHIND MAHALE FLOOR MIL VIKAS NAGAR KIWALE DEHU ROAD PUNE-41201', 7743852766, 'Mother', 'SANTOSH WAGMARE', 'VIKAS NAGAR WIWALE DEHU ROAD PUNE', 7507501968, 'FRIEND', 'SUDHIR SHARMA', 'VIKAS NAGAR WIWALE DEHU ROAD PUNE', 9545844044, 'FRIEND', 0, '2021-04-25 05:46:34', '2021-09-01 02:21:24', 1),
(191, 206, 94, 2, 2, 'B+', 'No', 36, '2018-04-01', NULL, NULL, 'NEAR PIMPRI POWER HOUSE, BEHIND ATITHI HOTEL, NAHARI KUDALE CHAL, SR NO 228,/3/A, PIMPRI GAON 411017', 'NEAR PIMPRI POWER HOUSE, BEHIND ATITHI HOTEL, NAHARI KUDALE CHAL, SR NO 228,/3/A, PIMPRI GAON 411017', 'ROHIT KUDALE', 'NR PIMPRI POWER HOUSE PIMPRI GAON PUNE 411017', 8983534395, 'Brother', 'PRANALI KUDALE', 'NR NVM SCHOOL PIPRI GAON PUNE 17', 9767998102, 'Spouse', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-04-25 05:48:17', '2021-06-26 03:55:39', 1),
(192, 207, 96, 2, 2, 'B+', 'No', 36, '2018-09-05', NULL, NULL, '37/285 LAXMINAGAR, HARI KADAM ROAD NEAR SHIVAJI MITRA MANDAL PARVATI PUNE- 411009', '37/285 LAXMINAGAR, HARI KADAM ROAD NEAR SHIVAJI MITRA MANDAL PARVATI PUNE- 411009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-04-25 05:51:16', '2021-10-20 08:00:09', 1),
(193, 208, 94, 2, 2, 'B+', 'No', 37, '2020-07-19', NULL, NULL, 'MONIKA CENTER KAMGAR NAGAR KHARALWADI NEAR ROPLAS COMPANY PIMPRI PUNE- 411018', 'MONIKA CENTER KAMGAR NAGAR KHARALWADI NEAR ROPLAS COMPANY PIMPRI PUNE- 411018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'HARESH DHAWALE', 'BAJRAN NAGAR PIMPRI PUNE 411018', 9922912750, 'FRIEND', 'NAVIN HEGADE', 'BAJRAN NAGAR PIMPRI PUNE 411018', 9922112824, 'FRIEND', 0, '2021-04-25 05:53:55', '2021-06-26 02:04:53', 1),
(194, 209, 102, 2, 2, 'B+', 'No', 36, '2019-07-23', NULL, NULL, 'WALKAIWADI NEAR PAUSKAR BUILDING GURAV WADA  KUSGAON BK  LONAVALA PUNE-410401', 'WALKAIWADI NEAR PAUSKAR BUILDING GURAV WADA  KUSGAON BK  LONAVALA PUNE-410401', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ATUL BHALERAO', 'A/P BORIPARDHI TAL DAUND DIST PUNE-412203', 7350668395, 'FRIEND', 'AKSHAY SHIRKHE', 'NEAR PAUSKAR BUILDING GURAV CHAVAL LKOAIWADHI KUSGAON 410401', 8805771466, 'FRIEND', 0, '2021-04-25 05:56:19', '2021-08-21 07:39:01', 1),
(195, 210, 102, 2, 2, 'B+', 'No', 36, '2020-06-04', NULL, NULL, '1370 WADKE WADA OPP SHIVSHANKAR DAIRY KASBA PETH PUNE-411011', '1370 WADKE WADA OPP SHIVSHANKAR DAIRY KASBA PETH PUNE-411011', 'PUSHPA GAIKWAD', '1370 KASBA PETH PUNE', 8788543457, 'Mother', NULL, NULL, NULL, NULL, 'SHEETAL PAWAR', 'NANA PETH', 7620588224, 'FRIEND', 'NEHA JOSHI', 'NANA PETH', 8999737368, 'FRIEND', 0, '2021-04-25 05:59:13', '2021-10-20 08:13:12', 1),
(196, 211, 97, 2, 2, 'AB+', 'Partially Vaccinated', 36, '2020-09-15', NULL, NULL, 'H NO 487 KOREGAON BHIMA TAL SHIRUR PUNE-412207', 'H NO 487 KOREGAON BHIMA TAL SHIRUR PUNE-412207', 'BAPUSAHEB SOPAN TAPKIR', 'H NO 487 KOREGAON BHIMA TAL SHIRUR PUNE-412207', 9273601601, 'Father', 'DEEPALI BAPUSAHEB TAPKIR', 'H NO 487 KOREGAON BHIMA TAL SHIRUR PUNE-412207', 9921201614, 'Mother', 'MANOJ SAPTPUTE', 'CHAVAN NAGAR CHANNAGAR KHARADI PUNE 411014', 7385802860, 'FRIEND', 'OM GARAD', 'SHEJWAL PARK L NO3 BORATE WASTI CHANDAN NAGAR PUNE 411014', 7447448448, 'FRIEND', 0, '2021-04-25 06:00:32', '2021-09-01 04:29:49', 1),
(197, 212, 98, 2, 2, 'O+', 'No', 37, '2020-08-09', NULL, NULL, 'SR NO  8/4 LAN NO 4 KARVE NAGAR OPP. SAI JIM KOTHRUD PUNE- 411052', 'SR NO  8/4 LAN NO 4 KARVE NAGAR OPP. SAI JIM KOTHRUD PUNE- 411052', 'SOJAR MADHUKAR ALHAT', 'AT POST PUNAWALE,NEAR BHIRAVNATH MANDIR,PUNAWALE GAVTHAN', 9011491717, 'Mother', 'TIRUPATI MADHUKAR ALHAT', 'AT POST PUNAWALE,NEAR BHIRAVNATH MANDIR,PUNAWALE GAVTHAN', 8888011017, 'Brother', 'BHAGWAN GAIKWAD', 'HINJAWADI MAAN PUNE', 9689212355, 'FRIEND', 'AMOL KALE', 'KARVE NAGAR PUNE', 9881552667, 'FRIEND', 0, '2021-04-25 06:04:55', '2021-09-01 05:09:15', 1),
(198, 213, 94, 2, 2, 'O+', 'Partially Vaccinated', 37, '2020-02-01', NULL, NULL, 'KAKADE WASTI GALLI NO 3 S.RO 4/28/2 KONDHWA ROAD KONDHWA BK NIBM PUNE', 'KAKADE WASTI GALLI NO 3 S.RO 4/28/2 KONDHWA ROAD KONDHWA BK NIBM PUNE', 'SWAPNIL ANKUSH SHILIMKAR', 'KAKADE WASTI GALLI NO 3 S.RO 4/28/2 KONDHWA ROAD KONDHWA BK NIBM PUNE', 8390138293, 'Brother', 'SAVITA ANKUSH SHILIMKAR', 'KAKADE WASTI GALLI NO 3 S.RO 4/28/2 KONDHWA ROAD KONDHWA BK NIBM PUNE', 9822061532, 'Mother', 'RAJU HATTKAR', 'PUNE STATION RAILWAY COUTER PUNE 411011', 7057377712, 'FRIEND', 'RADHIKA JADHAV', 'DAARU WALA PUL PUNE 411011', 8956826858, 'SISTER', 0, '2021-04-25 07:05:58', '2021-08-31 07:02:05', 1),
(199, 214, 6, 2, 2, 'B+', 'No', 37, '2020-02-01', NULL, NULL, '361 SOMESH APT 4TH FLOOR J.M. ROAD BEHIND PANCHALI HOTEL SHIVAJI NAGAR PUNE', '361 SOMESH APT 4TH FLOOR J.M. ROAD BEHIND PANCHALI HOTEL SHIVAJI NAGAR PUNE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-04-25 07:08:10', '2021-08-18 07:48:08', 1),
(200, 215, 102, 2, 2, 'O+', 'Partially Vaccinated', 37, '2020-08-05', NULL, NULL, 'SR NO 20/3  PAWAR HOSPITAL BACK SIDE VENKATESH SCHOOL GADE CHAWAL BALAJI NAGAR DHANKAWADI PUNE-411043', 'SR NO 20/3  PAWAR HOSPITAL BACK SIDE VENKATESH SCHOOL GADE CHAWAL BALAJI NAGAR DHANKAWADI PUNE-411043', 'NIRMALA LAXMAN KALPANDE', 'R NO 20/3  PAWAR HOSPITAL BACK SIDE VENKATESH SCHOOL GADE CHAWAL BALAJI NAGAR DHANKAWADI PUNE-411043', 9503200228, 'Mother', 'ROHINI GADE', 'BALAJI NAGAR PUNE', 8055864193, 'Sister', 'HITESH PUSE', 'GURUKRUPA BUILDING SR NO 86/28/2 MAHARASHTRA COLONY LANE NO 4 JAWALKAR NAGAR', 9767633676, 'FRIEND', 'SHRADDHA GAIKAWAD', '1370 KASBA PETH PAWALE CHOWK PUNE 11', 9834873061, 'FRIEND', 0, '2021-04-25 07:16:26', '2021-10-20 08:13:46', 1),
(201, 216, 99, 2, 2, 'O+', 'Partially Vaccinated', 216, '2020-10-06', NULL, NULL, '279 MANTRI VILLA SOCIETY SADASHIV PETH OPP BEHERE CLASS  PUNE-411030', '279 MANTRI VILLA SOCIETY SADASHIV PETH OPP BEHERE CLASS  PUNE-411030', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'GANESH MENGE', '235 WAGHCHOWRE WADA SONI APPARTMENT PUNE 30', 9561115826, 'FRIEND', 'KIRAN NIMBALKAR', '279 MANTRI VILLA SOC OPP BEHERE CLASS PUNE 30', 9923352525, 'FRIEND', 0, '2021-04-25 07:22:57', '2021-08-19 06:07:11', 1),
(202, 217, 97, 2, 2, 'B+', 'No', 37, '2020-09-01', NULL, NULL, 'SR NO 12 LAXMI NAGARYERWADA NEAR KRINTIKARI MITRA MANDAL JAWAL LAXMI MADIR HINE PUNE- 411006', 'SR NO 12 LAXMI NAGARYERWADA NEAR KRINTIKARI MITRA MANDAL JAWAL LAXMI MADIR HINE PUNE- 411006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-04-25 07:26:19', '2021-08-21 08:06:41', 1),
(203, 218, 97, 2, 2, 'B+', 'No', 37, '2020-04-10', NULL, NULL, 'subhash nagar navi khadki bhujbal saycal mart javal pune', 'subhash nagar navi khadki bhujbal saycal mart javal pune', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'IQBAL SAYYAD', 'subhash nagar navi khadki bhujbal saycal mart javal pune', 9922131699, 'FRIEND', 'IRFAN SAYYAD', 'subhash nagar navi khadki bhujbal saycal mart javal pune', 9923533744, 'FRIEND', 0, '2021-04-25 07:29:19', '2021-09-14 09:01:45', 1),
(204, 219, 99, 2, 2, 'B+', 'No', 37, '2020-11-20', NULL, NULL, 'SR NO 8/7/1 SHREE CHAWA COLONY SANTOSH NIVAS LANE NO 3 NEAR VIKAS MITRA MANDAL CHOWK KARVE NAGAR  PUNE-411052', 'SR NO 8/7/1 SHREE CHAWA COLONY SANTOSH NIVAS LANE NO 3 NEAR VIKAS MITRA MANDAL CHOWK KARVE NAGAR  PUNE-411052', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'AJIT PASALKAR', 'KARVE NAGAR LEAN 3 SANTOSH NAGAR PUNE', 8888101109, 'FRIEND', NULL, NULL, NULL, NULL, 0, '2021-04-25 07:34:27', '2021-06-26 02:10:50', 1),
(205, 220, 98, 2, 2, 'A+', 'No', 37, '2020-10-10', NULL, NULL, 'SR NO 157/2 JIVAN NAGAR NEAR SAI PETROL PUMP AT/POST TATHAWADE TAL- MULASHI PUNE-411033', 'SR NO 157/2 JIVAN NAGAR NEAR SAI PETROL PUMP AT/POST TATHAWADE TAL- MULASHI PUNE-411033', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-04-25 07:37:46', '2021-08-18 07:48:27', 1);
INSERT INTO `ptx_user_details` (`id`, `user_id`, `area_id`, `comp_id`, `branch_id`, `blood_group`, `covid_vaccination`, `team_leader`, `date_of_joining`, `exit_document`, `date_of_leaving`, `current_address`, `permanent_address`, `reference_1`, `reference_1_address`, `reference_1_mobile`, `reference_1_relation`, `reference_2`, `reference_2_address`, `reference_2_mobile`, `reference_2_relation`, `fri_reference_1`, `fri_reference_1_address`, `fri_reference_1_mobile`, `fri_reference_1_relation`, `fri_reference_2`, `fri_reference_2_address`, `fri_reference_2_mobile`, `fri_reference_2_relation`, `isDeleted`, `created_at`, `updated_at`, `updated_by`) VALUES
(206, 221, 98, 2, 2, 'A+', 'No', 37, '2020-08-29', NULL, NULL, 'ASHOK NAGAR NR DOLAS TALIM BHOSARI PUNE-411039', 'ASHOK NAGAR NR DOLAS TALIM BHOSARI PUNE-411039', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'SUDHIL JADHAV', 'SHAKAR COLONY PANDAV NAGAR BHOSARI PUNE', 9021010746, 'FRIEND', 'LAHU POL', 'AT POST KHANAPUR POLWADI TAL BHOR PUNE', 9689049351, 'FRIEND', 0, '2021-04-25 07:40:22', '2021-06-26 02:32:33', 1),
(207, 222, 98, 2, 2, 'O+', 'No', 37, '2020-07-12', NULL, NULL, 'SR NO 165/2 PLOT NO 20 VIJAY PARK  CHITRAL APP  BEHIND PODAR INTERNATIONAL SCHOOL PIMPARI PUNE- 411018', 'SR NO 165/2 PLOT NO 20 VIJAY PARK  CHITRAL APP  BEHIND PODAR INTERNATIONAL SCHOOL PIMPARI PUNE- 411018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'HARISH GHOSALKAR', 'MONIKA CENTER B BULDING T1 KAMGAR NAGAR DATTA MANDIR PIMPRI PUNE 18', 9767021632, 'FRIEND', 'HARESH DHAVALE', 'BAJRANG NAGAR PIMPRI PUNE 411018', 9922912750, 'FRIEND', 1, '2021-04-25 07:43:55', '2021-10-21 01:11:28', 1),
(208, 223, 105, 2, 2, 'B+', 'No', 37, '2021-02-01', NULL, NULL, 'AT Post Galandawadi, Taluka Daund, Pune412203', 'AT Post Galandawadi, Taluka Daund, Pune412203', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Sandip Pasalkar', 'Karve Nagar Pune', 8605052929, 'FRIEND', 'Ramesh Pasalkar', 'AT Post Galandawadi, Taluka Daund, Pune412203', 9922619696, 'FRIEND', 1, '2021-04-25 07:48:09', '2021-10-21 01:12:39', 1),
(209, 224, 98, 2, 2, 'B+', 'No', 37, '2021-04-06', NULL, NULL, 'INDRA NAGAR VASAHAT ROOM NO 75 KHADKi PUNE 411003', 'INDRA NAGAR VASAHAT ROOM NO 75 KHADKi PUNE 411003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'TINA SUNGAT', 'INDRA NAGAR VASAHAT ROOM NO 75 KHADKEE PUNE 411003', 7040210467, 'FRIEND', 'AKASH KHUDE', 'INDRA NAGAR VASAHAT ROOM NO 167 KHADKEE PUNE 411003', 9158482583, 'FRIEND', 1, '2021-04-25 07:51:52', '2021-10-08 03:14:45', 1),
(210, 225, 96, 2, 2, 'O+', 'Partially Vaccinated', 225, '2021-04-01', NULL, NULL, 'Sr No 193, Bahirat Wasti, sade satara nall, Hadapsar 411028', 'Sr No 193, Bahirat Wasti, sade satara nall, Hadapsar 411028', 'GURU RADESHAM PANDE', 'ASHOK CHAYA ,LAXMI NAGAR,NEAR GANESH CHITRA MANDIR,BUDHGAO,DIST SANGLI,TAL-MIRAJ', 7350567442, 'Father', 'GHANASHYAM GURU PANDE', 'NANDINI ORCHID,D 205,WADKI,PHUSUNGI ROAD,WADKI-412308', 8805664950, 'Brother', 'VARSH GOPAL PANDE', 'D 205 NANDAN ORCHID WADKI 412308', 7057512027, 'FRIEND', 'SWAPNIL S DHAKANE', 'SADE SATARA NALI, HADAPSAR PUNE 40028', 8380088885, 'FRIEND', 0, '2021-04-25 07:53:41', '2021-08-19 06:13:29', 1),
(211, 35, 102, 2, 2, 'A+', 'No', 35, '2007-05-09', NULL, NULL, 'VAIBHAV GAD APTS FLAT 7 CTS 123/4 SHIVALI NAGAR APTE RD NEAR HOTEL SHREYAS PUNE 411004', 'VAIBHAV GAD APTS FLAT 7 CTS 123/4 SHIVALI NAGAR APTE RD NEAR HOTEL SHREYAS PUNE 411004', 'SHEETAL BADHE', 'VAIBHAV GAD APTS FLAT 7 CTS 123/4 SHIVALI NAGAR APTE RD NEAR HOTEL SHREYAS PUNE 411004', 9850900900, 'Relation', NULL, NULL, NULL, 'Relation', 'AMIT PAWAR', 'DSK VISHWA SINHAGAD ROAD PUNE 411041', 9822147442, 'FRIEND', NULL, NULL, NULL, NULL, 0, '2021-04-07 04:51:21', '2021-06-25 07:23:08', 1),
(212, 135, 94, 2, 2, 'B+', 'Partially Vaccinated', 135, '2020-09-09', NULL, NULL, 'PRIYDARSHNI NAGAR LANE NO 1 OLD SANGVI PUNE411012', 'PRIYDARSHNI NAGAR LANE NO 1 OLD SANGVI PUNE411012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'AMOL SARANGAKAR', '274 NANA PETH PUNE 2', 9860777761, 'FRIEND', 'PRASHANT KHADE', 'BHD.DHAYRESHWAR MANDIR WADGAON DHAYRI PUNE', 9158348840, 'FRIEND', 0, '2021-04-11 08:39:10', '2021-08-19 05:08:36', 1),
(213, 148, 94, 2, 2, 'B+', 'Partially Vaccinated', 37, '2010-09-01', NULL, NULL, 'YASH HEIGHT,FLAT NO 104,GANPATI MATHA,AHIRE GAO,WARJE', 'S NO 50/1 GANGADE NAGAR KATEPURAM CHOWK PIMPLE GURAV NAVI SANGAVI PUNE 411 027', 'GAURI BHANDEKAR', 'YASH HEIGHT,FLAT NO 104,GANPATI MATHA,AHIRE GAO,WARJE', 9922943628, 'Spouse', 'ANKUSH RASANE', '189 BUDHWAR PETH NR KAKA HALWAI SWEET MART PUNE', 9921352352, 'Brother', 'TUKARAM RASNE', '189 BUDHWAR PETH NR KAKA HALWAI SWEET MART PUNE', 9850781821, 'FRIEND', 'DEEPAK JAYDALE', '220 BUDHWAR PETH NR KAKA HALWAI SEEWT MART', 9823231555, 'FRIEND', 0, '2021-04-19 06:53:57', '2021-09-03 06:23:37', 1),
(214, 230, 102, 2, 2, 'AB+', 'No', 230, '1992-05-11', NULL, NULL, '983/3/3 KAMALKUNJ SHUKRAWAR PETH BEHIND ANAND MANGAL KARYALAY PUNE 411002', '983/3/3 KAMALKUNJ SHUKRAWAR PETH BEHIND ANAND MANGAL KARYALAY PUNE 411002', 'NEETA SATPUTRE', '983/3/3 KAMALKUNJ SHUKRAWAR PETH BEHIND ANAND MANGAL KARYALAY PUNE 411002', 9822603141, 'Spouse', 'RUSHIKESH SATPUTE', '983/3/3 KAMALKUNJ SHUKRAWAR PETH BEHIND ANAND MANGAL KARYALAY PUNE 411002', 9067776688, 'Brother', 'AMAR KODRE', 'BUDHWAR PETH PUNE 411002', 7058444499, 'FRIEND', 'PARAG WAMBURE', 'BUDHWAR PETH OPP JOGESHWARI TEMPLE PUNE 411002', 9960848229, 'FRIEND', 0, '2021-05-07 08:04:09', '2021-09-18 02:56:36', 1),
(215, 231, 102, 2, 2, 'B+', 'No', 231, '2020-09-04', NULL, NULL, 'Dhayari Pune', '1343/1 KASBA PETH NR SURYA HOSPITAL  GAIKWAD WADA PUNE-411011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'MRUNALI KADU', 'SARVATRA VIHAR MES COLONY 301/B KHADKI PUNE 3', 7249612418, 'FRIEND', 'SOWRABH WAGHMARE', 'SWARGET POLICE COLONY 6/438 PUNE 411042', 8390889876, 'FRIEND', 0, '2021-05-29 11:47:37', '2021-08-18 07:48:41', 1),
(216, 232, 102, 2, 2, 'B+', 'No', 36, '2019-07-19', NULL, NULL, 'A/P BORIPARDHI TAL DAUND DIST PUNE-412203', 'A/P BORIPARDHI TAL DAUND DIST PUNE-412203', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'SAGAR JADHAV', 'AT POST BHORWADI TAL DAUND DIST PUNE', 8600709777, 'FRIEND', 'MANGESH SHAVAN', 'ATP DAPODHI TAL DAUND DIST PUNE 412203', 8600080493, 'FRIEND', 0, '2021-05-29 11:57:42', '2021-08-21 07:38:44', 1),
(217, 233, 102, 2, 2, 'B+', 'No', 36, '2020-09-02', NULL, NULL, 'SR NO 12 LAXMI NAGARYERWADA NEAR KRINTIKARI MITRA MANDAL JAWAL LAXMI MADIR HINE PUNE- 411006', 'SR NO 12 LAXMI NAGARYERWADA NEAR KRINTIKARI MITRA MANDAL JAWAL LAXMI MADIR HINE PUNE- 411006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PRATIKSHA JADHAV', '2187 NEW MODIKHANA CAMP PUNE1', 8007484831, 'FRIEND', 'BHARTI JADHAV', '2187 NEW MODIKHANA CAMP PUNE1', 8805249556, 'FRIEND', 1, '2021-05-29 12:40:17', '2021-08-18 07:49:03', 1),
(218, 234, 98, 2, 3, 'B+', 'No', 234, '1982-07-17', NULL, NULL, 'LONDHE NAGAR BHOSARI PUNE 26', 'LONDHE NAGAR BHOSARI PUNE 26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Deepak Khedkar', 'Londhe Nagar Bhosari 411026', 9762850860, 'FRIEND', 'SUSHIL JADHAV', 'Sahkar Colony, pandav nagar, namdev nivas, chakrapani vasahat road Bhosari Pune-411039', 9021010746, 'FRIEND', 0, '2021-06-05 06:55:34', '2021-10-08 02:07:00', 1),
(219, 235, 102, 2, 2, 'A-', 'No', 235, '2021-06-01', NULL, NULL, '804 RANGANJALI MITRA NAGAR COLONY, SR NO 110/11/05 BANESR MHALUNGE ROAD BANER', '804 RANGANJALI MITRA NAGAR COLONY, SR NO 110/11/05 BANESR MHALUNGE ROAD BANER', 'RANGRAO PAWR', '804RANGANJALI, MITRA NAGAR COLONY, SR NO 110/11/5 BANER OPP RAJYOG SOCIETY BANER-411045', 9823384743, 'Father', 'SHRADDHA PAWAR', '804 RANGANJALI MITRA NAGAR COLONY, SR NO 110/11/05 BANESR MHALUNGE ROAD BANER', 8237450622, NULL, 'RAJENDRA AHIRE', 'PIMPLE GURAV NEAR CME GATE PUNE', 8408882734, NULL, 'ANANT YADAV', 'SNO 227/1 BLG CA 3FLAT 17 RADHANAGARI COMPLEX BHOSARI PUNE 411039', 9881000356, 'FRIEND', 1, '2021-06-05 07:02:23', '2021-08-18 07:49:18', 1),
(220, 236, 95, 2, 2, 'B+', 'No', 236, '1979-02-06', NULL, NULL, 'K-803, SAI MYSTIQUE AMBEGAON BK KATRAJ PUNE', 'K-803, SAI MYSTIQUE AMBEGAON BK KATRAJ PUNE', 'AJIT BORA', 'K 803,SAI MYSTIQUE,AMBEGAO BUDRUK,PUNE 46', 9850030089, 'Brother', 'SMITA GAURAV BORA', 'K 803,SAI MYSTIQUE,AMBEGAO BUDRUK,PUNE 46', 9767072929, NULL, 'KALPESH KASAR', 'SAGUN APT,NARHE', 9850909197, 'FRIEND', 'PARAG WAMBURE', 'BUDHWAR PETH,PUNE', 9960848229, 'FRIEND', 0, '2021-06-05 07:07:15', '2021-10-08 02:09:43', 1),
(221, 237, 98, 2, 3, 'A+', 'Partially Vaccinated', 237, '1985-02-02', NULL, NULL, 'SAHKAR COLONY NO 1 CHARPANI ROAD PANDAV NAGAR BHOSARI PUNE 411039', 'SAHKAR COLONY NO 1 CHARPANI ROAD PANDAV NAGAR BHOSARI PUNE 411039', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Deepak Khedkar', 'Londhe Nagar Bhosari 411026', 9762850860, 'FRIEND', 'Prashant Dolas', 'Dolas Wada Bhosari 411039', 9881750999, 'FRIEND', 0, '2021-06-05 08:27:52', '2021-10-08 02:05:54', 1),
(222, 238, 98, 2, 3, 'B+', 'Partially Vaccinated', 238, '2021-03-06', NULL, NULL, 'ANNUBAI KALBHOR CHAWL AKURDI PUNE 411035', 'ANNUBAI KALBHOR CHAWL AKURDI PUNE 411035', 'SANGITA KALBHOR', 'AANABHAI KALBHOR CHAWLAKURDI PUNE', 7038139924, 'Mother', 'NIRANJAN KALBHOR', 'ANUBAI KALBHOR CHAWL SUDARSHAN NAGAR NR BHAWANA BUILDING AKURDI', 7620464647, 'Spouse', 'MAHESH B MORE', 'SHIV PARVATI NIVAS MORE VASTI CHIKHALI', 9975579460, 'FRIEND', 'GAURAV KATE', 'AKURDI', 8888436474, 'FRIEND', 0, '2021-06-06 06:53:25', '2021-10-01 01:43:32', 1),
(223, 239, 94, 2, 2, 'B+', 'Partially Vaccinated', 239, '1994-06-05', NULL, NULL, 'JANWADI JANTA VASAHAT MODEL COLONY PUNE 411016', 'JANWADI JANTA VASAHAT MODEL COLONY PUNE 411016', 'HAFIZ SHAIKH', '101 JANWADI JANTA VASAHAT PUNE', 9822018615, 'Brother', 'YASMIN sHAIKH', '101 JANWADI JANTA VASAHAT PUNE', 8483087553, 'Spouse', 'SALMAN HUNDEKARI', 'KOREGAON BHEEMA\r\nPUNE', 8180838114, 'FRIEND', 'SAGAR ASHOK BHANDEKAR', 'WARJE', 9146466606, 'FRIEND', 0, '2021-06-07 06:58:29', '2021-09-23 03:13:14', 1),
(224, 240, 94, 2, 2, 'O+', 'No', 56, '2021-06-06', NULL, NULL, '18/148 maharshi nagar pune 411037', '18/148 maharshi nagar pune 411037', 'SAYALI PHADTARE', '18/148 MAHARSHI NAGAR OPP SANT DHYANDEV VIDYALAY', 9766838225, 'Spouse', 'SUMAN DINKAR DHAMDHARE', '18/148 maharshi nagar pune 411037', 8483848959, 'Spouse', 'DEEPIKA KALE', '433/E SOMWAR PETH BARKE ALI PUSHKRAJ APARTMENT PUNE-411001', 9822677965, 'FRIEND', 'GANESH KONDA', 'MAHARSHI NAGAR', 9890022126, 'FRIEND', 0, '2021-06-07 07:11:49', '2021-08-31 07:26:48', 1),
(225, 241, 94, 2, 2, 'B+', 'No', 36, '2021-05-11', NULL, NULL, '365 EKBOTE COLONY GHORPADI PETH NR KRUPAL ASHRAM SWARGET PUNE', '365 EKBOTE COLONY GHORPADI PETH NR KRUPAL ASHRAM SWARGET PUNE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-06-09 06:44:23', '2021-08-21 08:11:26', 1),
(226, 242, 94, 2, 2, 'AB+', 'Partially Vaccinated', 242, '2020-02-05', NULL, NULL, 'S T OFFICER SHANKAR SHET ROAD SWARGET PUNE', 'S T OFFICER SHANKAR SHET ROAD SWARGET PUNE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'KIRAN SAHANE', 'S T OFFICERS QUARTERS SHANKARSHETH ROAD SWARGET PUNE', 9595399978, 'FRIEND', 'PRAVIN KATORE', 'S T OFFICERS QUARTERS SHANKARSHETH ROAD SWARGET PUNE', 7620502267, 'FRIEND', 1, '2021-06-10 02:47:16', '2021-08-28 04:28:17', 1),
(227, 243, 94, 2, 2, 'B+', 'Partially Vaccinated', 37, '2019-08-07', NULL, NULL, '54/418 264 OPP  FLYOVER BRIDGE  LOHIYA NAGAR PUNE', '54/418 264 OPP  FLYOVER BRIDGE  LOHIYA NAGAR PUNE', 'MUKESH LONARE', 'LOHIYA NAGAR 54,HP 264 IN FRONT OF  FIRE BRIGADE', 8983638370, 'Brother', 'NIKITA KHOBRAGADE', 'LOHIYA NAGAR 54,HP 264 IN FRONT OF  FIRE BRIGADE', 9284853764, 'Sister', 'PRIYANKA KHAWALE', 'GHORPADI PETH,365,EKBOTE COLONY', 9765547027, 'FRIEND', 'MONA LONARE', 'LOHIYA NAGAR 54 269 FIR BRIGADE', 7558233846, 'FRIEND', 0, '2021-06-11 00:57:27', '2021-09-14 03:58:57', 1),
(228, 244, 102, 2, 2, 'A+', 'Partially Vaccinated', 36, '1986-10-02', NULL, NULL, '178 NANA PETH PIMPARI CHOWK NEAR MAHAVIR TRUSH PUNE-', '178 NANA PETH PIMPARI CHOWK NEAR MAHAVIR TRUSH PUNE-', 'YUVRAJ  PAIGUDE', '178 NANA PETH PIMPARI CHOWK NEAR MAHAVIR TRUSH PUNE-', 9028293912, 'Father', 'SANGEETA KARALE', 'KASBA PETH', 8208823239, 'Mother', 'SANGEETA KARALE', 'KASBA PETH TAPKIR GALLI PUNE', 8087555428, 'FRIEND', 'ANKITA MANIYAR', 'IGATPURI', 7798808033, 'FRIEND', 0, '2021-06-15 10:36:14', '2021-10-20 08:11:05', 1),
(230, 246, 102, 2, 2, 'B+', 'No', 32, '2014-05-01', NULL, NULL, 'Bibwewadi Appar Indira Nagar Pune -38', 'Bibwewadi Appar Indira Nagar Pune -38', 'PANDURANG DHEBE', 'OPP APPAR DEPO PUNE', 8975522424, 'Relation', NULL, NULL, NULL, 'Relation', 'ASHWINI BALBHIM RASAL', 'Bibwewadi Appar Indira Nagar Pune -38', 7058200134, 'FRIEND', 'PAYAL BALBHIM RASAL', 'Bibwewadi Appar Indira Nagar Pune -38', 7058200134, NULL, 0, '2021-06-25 07:57:22', '2021-09-08 04:16:15', 1),
(231, 247, 95, 2, 2, 'A+', 'No', 37, '2021-04-15', NULL, NULL, '85/705 LAXMI NAGAR PARVATI PUNE', '85/705 LAXMI NAGAR PARVATI PUNE', 'SANTOSH GHOPANE', '85/705 LAXMI NAGAR PARVATI PUNE', 9422002003, NULL, 'RAJARAM GHOPANE', '85/705 LAXMI NAGAR PARVATI PUNE', 9423566533, NULL, NULL, NULL, NULL, 'FRIEND', 'MAHESH DEVKAMBLE', 'LAXMI NAGAR PARVATI PUNE', 9422303628, 'FRIEND', 0, '2021-06-26 01:17:05', '2021-06-26 01:17:05', 1),
(232, 248, 102, 2, 2, 'B+', 'No', 235, '2021-06-17', NULL, NULL, 'SHIRALSHETH CHOWK  SR NO 163 RASTA PETH PUNE  411011', 'SHIRALSHETH CHOWK  SR NO 163 RASTA PETH PUNE  411011', 'JAYA NAIR', 'SHIRALSHETH CHOWK  SR NO 163 RASTA PETH PUNE  411011', 9850345719, 'Mother', NULL, NULL, NULL, NULL, 'MILIND PATOLE', 'KONDHAWA PUNE', 8888898915, 'FAMILY FRIEND', NULL, NULL, NULL, NULL, 0, '2021-06-26 05:45:21', '2021-06-26 05:45:21', 1),
(234, 250, 102, 2, 2, 'B+', 'Partially Vaccinated', 36, '2021-06-22', NULL, NULL, 'PHULE NAGAR, VISHRANTIWADI PUNE', '718 SAHYADRI NAGAR CIDCO AURANGABAD -431001', 'PRIYA HIWRALE', 'CIDCO AURANGABAD', 7448030195, 'Sister', NULL, NULL, NULL, NULL, 'ASHLESHA PASARE', 'PHULE NAGAR, VISHRANTIWADI PUNE', 9503342237, NULL, NULL, NULL, NULL, NULL, 0, '2021-07-29 06:39:53', '2021-07-29 06:39:53', NULL),
(235, 251, 102, 2, 2, 'O+', 'Partially Vaccinated', 251, '2021-06-15', NULL, NULL, 'MARKETYARD GS TOWER 6B OUNE-37', 'MARKETYARD GS TOWER 6B OUNE-37', 'SHRDDHA BIBWE', 'MARKET YARD GS TOWER 6B PUNE-37', 7264854504, 'Father', 'RAMESH BIBWE', 'MARKET YARD GS TOWER 6B PUNE-37', 9765880421, 'Father', 'SHRUTIKA SABLE', 'MANDOT TOWER KONDHAWA KHURD PUNE', 9356124707, 'FRIEND', 'MEGHANA RAUT', 'AMBEGAON KATRAJ', 7499845374, 'FRIEND', 0, '2021-07-29 06:50:42', '2021-09-23 03:55:07', 1),
(236, 252, 102, 2, 2, 'A+', 'Partially Vaccinated', 252, '2021-06-15', NULL, NULL, 'MANDOT TOWER KONDHAWA KHURD PUNE', 'MANDOT TOWER KONDHAWA KHURD PUNE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ANKITA NAGARE', 'WAGHOLI', 7410540091, 'FRIEND', 'SAYALI PATIL', 'KATRAJ', 9730506133, 'FRIEND', 0, '2021-07-29 06:55:04', '2021-08-21 08:13:08', 1),
(237, 253, 102, 2, 2, 'B+', 'Partially Vaccinated', 253, '2021-06-23', '', NULL, 'C/10,2/3 RAJMUDRA SOCIETY GULAB NAGAR DHANKAWADI', 'SHINGNAPUR ROAD, ANANT MANGAL KARYALAYA JAVAL, PHALTAN SATARA', 'GULABRAW NANA MANE', 'NILKAMAL NEWAI PHALTAN', 8767376443, 'Father', 'SANJAY MANE', 'PHALTAN', 9359649445, 'Brother', 'VIKAS BAIL', 'GULAB NAGAR DHANKAWDI', 9075460297, 'FRIEND', 'SUNIL JADHAV', 'HADAPSAR', 9075967521, 'FRIEND', 0, '2021-07-29 06:58:58', '2021-09-23 04:01:28', 1),
(238, 254, 102, 2, 2, 'O+', 'Partially Vaccinated', 254, '2021-06-28', NULL, NULL, 'LOKMANYA NAGAR DANDEKARPUL PUNE,30', 'LOKMANYA NAGAR DANDEKARPUL PUNE,30', 'SANGITA BHAMBURE', '39 LAXMI PARK RAJLAXMI APT PUNE 411030', 9763432618, 'Mother', 'SUDHIR BHAMBURE', '39 LAXMI PARK RAJLAXMI APT PUNE 411030', 9763436019, 'Father', 'BHAJAN SINGH RANDHEWA', 'TALJAI PATHAR NEAR BIOGAS PLANT PARVATI PUNE', 7276699607, 'FRIEND', 'VEDDAJA BADHE', 'THANE', 7977830537, 'FRIEND', 0, '2021-08-03 04:46:07', '2021-09-23 03:37:54', 1),
(239, 256, 96, 2, 2, 'B+', 'No', 36, '2021-06-10', NULL, NULL, '894/A NISARG SOCIETY, GHOKALE NAGAR', '894/A NISARG SOCIETY, GHOKALE NAGAR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-08-06 03:32:41', '2021-08-18 07:49:54', NULL),
(240, 257, 94, 2, 2, 'O-', 'Partially Vaccinated', 1, '2021-08-09', NULL, NULL, 'BALEWADI PAHATA', 'BALEWADI PHATA', 'Naveen', 'B-2, Western Express Highway\r\nFlat 105 Royal Enclave', 9637430435, 'Father', 'Naveen', 'B-2, Western Express Highway\r\nFlat 105 Royal Enclave', 9637430435, 'Brother', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-08-09 06:05:49', '2021-08-09 06:05:49', NULL),
(241, 258, 95, 5, 7, 'O+', 'Fully Vaccinated', 55, '2021-03-02', NULL, NULL, '5th floor, Aloha technology,', 'Prabhavee Tech Park, Baner', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-08-09 06:37:32', '2021-08-09 06:37:32', NULL),
(242, 259, 96, 2, 2, 'B+', 'No', 259, '2021-06-05', NULL, NULL, '850', '395 NANAPETH NEAR GAYANI MOTORS', 'LATA SUHAS SALVE', '395 NANAPETH NEAR GAYANI MOTORS', 8796405823, 'Mother', 'RASIKA KEDARI', '395 NANAPETH NEAR GAYANI MOTORS', 8668754899, NULL, 'ANUP SALVE', 'FATHIMA NAGAR', 7030189977, 'FRIENDS', 'CYRIL DAVID', 'FATHIMA NAGAR', 9923424455, 'FRIENDS', 0, '2021-08-09 08:07:02', '2021-09-23 03:22:19', 1),
(243, 260, 94, 2, 2, 'B+', 'Partially Vaccinated', 36, '2021-06-21', NULL, NULL, 'PHULE NAGAR,VISRANTWADI ROAD,PUNE', 'SAYADRI NAGAR,CIDCO,AURANGABAD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ASHLESHA PASARE', 'PHULE NAGAR,VISHRANTWADI,PUNE', 9503342237, 'FRIENDS', 'PRIYA HIWRALE', 'CIDCO,AURANGABAD', 7448030195, 'FRIENDS', 1, '2021-08-09 08:20:35', '2021-08-09 08:53:52', NULL),
(244, 261, 97, 2, 2, 'A+', 'Partially Vaccinated', 261, '2021-07-26', NULL, NULL, '1165 FLAT NO -02 SADGURU KRUPA NIWAS, KASBA PETH PUNE', '1165 FLAT NO -02 SADGURU KRUPA NIWAS, KASBA PETH PUNE', 'ABHISHEKH GANESH GADE', '1165 FLAT NO -02 SADGURU KRUPA NIWAS, KASBA PETH PUNE', 9028872821, 'Brother', 'SUNITA GADE', '1165 FLAT NO -02 SADGURU KRUPA NIWAS, KASBA PETH PUNE', 9028589602, 'Mother', 'PRAJAKTA PAWAR', '1165 FLAT NO -02 SADGURU KRUPA NIWAS, KASBA PETH PUNE', 9405610580, 'FRIEND', 'SAYALI AJAY DHAMDHERE', '18/148 MAHARSHI NAGAR OPP SANT DHYANDEV VIDYALAY', 9766838225, 'FRIEND', 0, '2021-08-09 08:47:19', '2021-09-23 09:02:56', 1),
(245, 262, 98, 2, 2, 'O+', 'Partially Vaccinated', 36, '2021-06-03', NULL, NULL, 'SAUDAGAR GARDEN,DEHU ROAD,PUNE', 'SAUDAGAR GARDEN,DEHU ROAD,PUNE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'SANTOSH KENGAR', 'A2/5 VIKASNAGAR,SAUDAGAR GARDEN,DEHU ROAD,PUNE', 9923888148, 'FRIENDS', 'DEVRAM DATE', 'A2/5 VIKASNAGAR,SAUDAGAR GARDEN,DEHU ROAD,PUNE', 9767831599, 'FRIENDS', 0, '2021-08-10 00:46:51', '2021-08-10 00:46:51', NULL),
(246, 263, 94, 2, 2, 'B+', 'Partially Vaccinated', 263, '2021-06-02', NULL, NULL, '204,SHIRSHAL SHETH CHOWK,RASTA PETH,PUNE', '204,SHIRSHAL SHETH CHOWK,RASTA PETH,PUNE', 'MANISH PUNJANI', '204,SHIRSHAL SHETH CHOWK,RASTA PETH,PUNE', 9746197799, 'Father', 'SUNIL PUNJANI', '204,SHIRSHAL SHETH CHOWK,RASTA PETH,PUNE', 7517355602, NULL, 'MAMTA THAKKAR', '204,SHIRSHAL SHETH CHOWK,RASTA PETH,PUNE', 9834853277, 'FRIENDS', 'POONAM SHAHA', 'WADGOAN BK,SINHGAD ROAD,PUNE', 7620055157, 'FRIENDS', 0, '2021-08-10 00:54:01', '2021-10-08 02:11:32', 1),
(247, 265, 94, 2, 2, 'B+', 'Partially Vaccinated', 35, '2021-07-20', NULL, NULL, '91/5 A PARVATI DARSHAN,NEAR MITRA MANDAL COLONY,PUNE-09', '91/5 A PARVATI DARSHAN,NEAR MITRA MANDAL COLONY,PUNE-09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'VINOD GHUME', '61 NARAYAN PETH LAGAD COMPLEX PUNE-30', 7744811072, 'FRIENDS', 'SANJAY BHORKAR', '91/5 A PARVATI DARSHAN,NEAR MITRA MANDAL COLONY,PUNE-09', 9822402840, 'FAMILY', 0, '2021-08-10 01:23:04', '2021-09-11 07:54:22', NULL),
(248, 266, 96, 2, 2, 'O+', 'Partially Vaccinated', 266, '2021-07-17', NULL, NULL, 'BHARAT FORGE ROAD GHORPADI KALYANI STEEL, PUNE', 'LINGAYAT SMSHAMBHUMI BHUMI MANGADE CHAWL TULJAPU ROAD BARSHI SOLAPUR', 'MAYURI HUMBE', 'BHARAT FORGE ROAD GHORPADI KALYANI STEEL, PUNE', 7249437962, 'Spouse', 'ROHIT HUMBE', 'TULJAPUR ROAD MANGDECHALL BARSHI DIST PUNE', 8459709570, 'Spouse', 'ROHIT HUMBE', 'TULJAPUR ROAD MANGADE CHAWL BARSHI SOLAPUR', 8459809570, 'FAMILY', 'MAYURI HUMBE', 'TULJAPUR ROAD MANGADE CHAWL BARSHI SOLAPUR', 9325172318, 'FAMILY', 1, '2021-08-10 01:35:51', '2021-10-08 02:23:14', 1),
(249, 267, 94, 2, 2, 'O+', 'Partially Vaccinated', 267, '2021-07-21', NULL, NULL, 'ISHWARKRUPA BUILDING SAMATA SOCIETY TELEPHONE EXECHANGE NEAR BABA DHUMAL OFC WARJE', 'PRATAPGANJ PETH 208 SATARA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'SAGAR GJMAL', 'WARJE', 9921993773, 'FRIENDS', 'MANISHA SUPEKAR', 'GOKHLENAGAR', 9881487258, 'FRIENDS', 0, '2021-08-10 01:42:40', '2021-09-01 07:41:26', 1),
(250, 268, 97, 2, 2, 'O+', 'Partially Vaccinated', 37, '2021-07-19', NULL, NULL, 'KHED SHIVAPUR,TAL-HAVELI,PUNE', 'KHED SHIVAPUR,TAL-HAVELI,PUNE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'RAM DABADE', 'GUJARAT COLONY KOTHRUD', 9960234905, 'FRIENDS', 'SHIVAJI NIKAM', 'NASRAPUR TAL-BHOR,PUNE', 9272391756, 'FRIENDS', 0, '2021-08-10 01:51:25', '2021-08-10 01:51:25', NULL),
(251, 269, 95, 2, 2, 'B+', 'Partially Vaccinated', 269, '2021-06-11', NULL, NULL, 'ADITYA HEIGHTS FLAT NO 305 RAIKAR MALA DHAYRIPUNE, 41', 'JAI BHAVANI JNATA VASAHAT MAHILA MANDAL PARVATI PUNE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'JAYARAM BANE', 'ADITYA HEIGHTS FLAT NO 305 RAIKAR MALA DHAYRIPUNE, 41', 9657858474, 'FAMILY', 'VISHAL PATERE', 'PUNE', 9529390449, 'FRIENDS', 1, '2021-08-10 01:56:26', '2021-08-21 08:13:30', 1),
(252, 270, 97, 2, 2, 'B+', 'Partially Vaccinated', 270, '2021-06-12', NULL, NULL, 'BUNGLOW NO 21 BEHIND KHADKI POST OFFICE PUNE 03', 'BUNGLOW NO 21 BEHIND KHADKI POST OFFICE PUNE 03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PRADEEP BADE', 'SHIVAJI NAGAR', NULL, 'FRIENDS', 'DEEPA PARDESHI', 'BUNGLOW NO 21 BEHIND KHADKI POST OFFICE PUNE 03', 9112274217, 'FAMILY', 1, '2021-08-10 02:00:12', '2021-09-14 03:58:41', 1),
(253, 271, 96, 2, 2, 'O+', 'Partially Vaccinated', 271, '2021-06-15', NULL, NULL, '400 MANGALWAR PETH PUNE 11', '780 RAVIWAR PETH PHADKE HOUD CHOWK PUNE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ATHARVA KADAM', '400 MANGALWAR PETH PUNE 11', 9730783954, 'FAMILY', 'PRASAD GORE', '508 RASTA PETH PUNE 11', 9922556556, 'FRIENDS', 0, '2021-08-10 02:09:03', '2021-08-19 06:34:07', 1),
(254, 272, 98, 2, 2, 'O+', 'Partially Vaccinated', 272, '2021-06-15', NULL, NULL, 'S NO76 BHARATMATA NAGAR BHOSARI RD PUNE', 'S NO76 BHARATMATA NAGAR BHOSARI RD PUNE', 'MILIND UGHADE', 'DIGHI SAI APPARTMENT', 8459960927, 'Brother', 'MONIKA LONDHE', 'CHIKALI', 8554052628, 'Sister', 'RAVINDRA VARPE', 'KHADAKI', 9307889379, 'FRIENDS', 'NAVNATH  KADAM', 'CHIKHALI', 7030208041, 'FRIENDS', 0, '2021-08-10 02:13:50', '2021-09-23 03:49:26', 1),
(255, 273, 98, 2, 2, 'O+', 'Partially Vaccinated', 36, '2021-06-15', NULL, NULL, 'FLAT NO 506 POKALECRISTAL DHAYRI PUNE 41', 'DATTAWADI MHASOBA CHOWK PUNE', 'KAMINI CHANDRAKANT KIRAD', '682,DATTA WADI,MHASOBA CHOEK,PUNE-30', 9595708693, 'Mother', 'RAKESH CHANDRAKANT KIRAD', '682,DATTA WADI,MHASOBA CHOEK,PUNE-30', 9922460056, 'Brother', 'NILESH GAIKWAD', 'HOUSE 195 NEAR SHANI MANDIR DATTAWADI PUNE 30', 8767585901, 'FRIENDS', 'SUNIL DHAMALE', '680 DATTAWADI ,MHASOBA CHOWK PUNE 30', 9175522733, 'FRIEND', 1, '2021-08-10 05:08:19', '2021-09-15 04:30:03', 1),
(256, 274, 96, 2, 2, 'AB+', 'Partially Vaccinated', 36, '2021-06-15', NULL, NULL, 'MAULI COLONY GANGANAGAR FURSUNGI PUNE 08', 'MAULI COLONY GANGANAGAR FURSUNGI PUNE 08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'RUSHIKESH RAUT', 'MAULI COLONY GANGANAGAR FURSUNGI PUNE 08', 8956317020, 'FRIENDS', 'ARBAZ KHAN', 'RAMTEKDI HADAPSAR PUNE', 9175849190, 'FRIENDS', 1, '2021-08-10 05:16:01', '2021-10-08 03:15:21', NULL),
(258, 276, 94, 2, 2, 'AB+', 'Partially Vaccinated', 276, '2021-07-08', NULL, NULL, 'HOUSE NO 09 MHASKE CHAL SANTOSH NAGAR KATRAJ 46', '34/4 NEAR TRIMURTI CHOWK DHANKAWADI PUNE 46', 'VAISHALI KAMBLE', 'HOUSE NO 09 MHASKE CHAL SANTOSH NAGAR KATRAJ 46', 7887493913, 'Sister', 'SANTOSH KAMBLE', 'HOUSE NO 09 MHASKE CHAL SANTOSH NAGAR KATRAJ 46', 9960905678, 'Brother', 'SMITALI BIBAVE', 'MARKET YARD,J S TOWER,411037', 8007763669, 'FRIEND', 'ANJALI KHIRSAGAR', 'KADAM CHAWL,SANTOSH NAGAR,KATRAJ', 7744035561, 'FRIEND', 0, '2021-08-10 05:26:07', '2021-08-20 02:14:43', 1),
(259, 277, 99, 2, 2, 'A+', 'Partially Vaccinated', 277, '2021-07-14', NULL, NULL, 'SR NO 42 SNDT COLLEGE KARVE NAGAR ERANDWANI PUNE38', 'SR NO 42 SNDT COLLEGE KARVE NAGAR ERANDWANI PUNE38', 'MANGAL KURADE', 'SR NO 42 SNDT COLLEGE KARVE NAGAR ERANDWANI PUNE38', 8999553695, 'Mother', 'SHANTARAM KURADE937600', 'SR NO 42 SNDT COLLEGE KARVE NAGAR ERANDWANI PUNE38', 937603436, 'Father', 'MANGAL KURHADE', 'SR NO 42 SNDT COLLEGE KARVE NAGAR ERANDWANI PUNE38', 8999593695, 'FRIEND', 'SHANTARAM KURHADE', 'SR NO 42 SNDT COLLEGE KARVE NAGAR ERANDWANI PUNE38', 9371603436, 'FRIEND', 0, '2021-08-10 05:38:25', '2021-08-19 06:27:56', 1),
(260, 278, 98, 2, 2, 'O+', 'Partially Vaccinated', 278, '2021-07-15', NULL, NULL, 'CHAKRAPANI VASAT (EAST) BHOSARI PUNE', 'CHAKRAPANI VASAT (EAST) BHOSARI PUNE', 'DASHRAT BELIDKAR', 'CHAKRAPANI VASAT (EAST) BHOSARI PUNE', 9890109550, 'Mother', 'ONKAR BELIDKAR', 'CHAKRAPANI VASAT (EAST) BHOSARI PUNE', 913038330, 'Brother', 'PRASAD BOMBLE', 'BHOSARI PUNE', 9130383330, 'FRIEND', 'VISHAL KHANDVE', 'BHOSARI PUNE', 9604100398, 'FRIEND', 0, '2021-08-10 05:42:36', '2021-10-08 02:21:34', 1),
(261, 279, 94, 2, 2, 'AB+', 'Partially Vaccinated', 36, '2021-06-16', NULL, NULL, 'U 1104 SWARAJ PHASE4 MOSHI TAL- HAVELI DIST- PUNE', 'NAGAR PUNE ROAD MANIKNAGAR NEAR ANAND RUSHIJI HOSPITAL AHEMADNAGAR  414001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'RAJESH BANSI ZAREKAR', 'U 1104 SWARAJ PHASE4 MOSHI TAL- HAVELI DIST- PUNE', 9881723390, 'FRIENDS', 'KAMNI UMESH KEDARE', '15 QUEEN\'S GARDEN PURANDAR VASAHAI PUNE STATION PUNE', 8999863378, 'FRIENDS', 0, '2021-08-10 05:51:40', '2021-08-10 05:51:40', NULL),
(262, 280, 94, 2, 2, 'O+', 'Partially Vaccinated', 36, '2021-06-16', NULL, NULL, 'BUILDING NO 1 FL NO 10 N.B.ARCADE AKURDI PRADHIKARAN ROAD NEAR KIRAN JWELLERS AKURDI PUNE', 'BUILDING NO 1 FL NO 10 N.B.ARCADE AKURDI PRADHIKARAN ROAD NEAR KIRAN JWELLERS AKURDI PUNE', 'SHOBHANA SALVEKAR', 'BUILDING NO 1 FL NO 10 N.B.ARCADE AKURDI PRADHIKARAN ROAD NEAR KIRAN JWELLERS AKURDI PUNE', 9890347756, 'Mother', 'RAVINDRA SALVEKAR', 'BUILDING NO 1 FL NO 10 N.B.ARCADE AKURDI PRADHIKARAN ROAD NEAR KIRAN JWELLERS AKURDI PUNE', 9890347756, 'Father', 'HARSHAL SALVEKAR', 'BUILDING NO 1 FL NO 10 N.B.ARCADE AKURDI PRADHIKARAN ROAD NEAR KIRAN JWELLERS AKURDI PUNE', 9689346526, 'SISTER', 'YASH D JAGTAP', 'BUILDING NO 3 FL NO 5 N.B.ARCADE AKURDI PRADHIKARAN ROAD NEAR KIRAN JWELLERS AKURDI PUNE', 7558596926, 'FRIEND', 0, '2021-08-10 05:59:52', '2021-09-01 05:54:31', 1),
(263, 281, 95, 2, 2, 'AB+', 'Partially Vaccinated', 36, '2021-07-05', NULL, NULL, 'FL NO 13 JAGDEN CORNER GOLIBAR MAIDAN GODOLI SATARA', 'FL NO 13 JAGDEN CORNER GOLIBAR MAIDAN GODOLI SATARA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARJUN GANAPAT KHARMATE', 'FL NO 13 JAGDEN CORNER GOLIBAR MAIDAN GODOLI SATARA', 9870430197, 'FAMILY', 'AMITA NILESH KHADE', 'OMKAR RESIDENCY SATARA', 8600390301, 'FRIENDS', 0, '2021-08-10 06:05:43', '2021-08-10 06:05:43', NULL),
(264, 282, 102, 2, 2, 'AB+', 'Partially Vaccinated', 282, '2021-07-04', NULL, NULL, 'F/403 MATHUBAI SMRUTI NIVAS UTTAMNAGAR, PUNE 23', 'F/403 MATHUBAI SMRUTI NIVAS UTTAMNAGAR, PUNE 23', 'PAYAL ANIL KUMBHAR', 'FLAT NO_403,MATHUBAI SMRUTI NIWAS,ACHANAK CHOWK,UTTAM NAGAR ,PUNE', 9156913601, 'Sister', 'AKASH DATTATRAY KUMBHAR', 'DEHU GAO', 8605283860, 'Brother', 'DILIP WALHEKAR', 'WARJE PUNE', 9850413359, 'FRIEND', 'DIPTI BHUMBURE', '39 LAXMI PARK RAJ LAXMI APT PUNE 30.', 8087735502, 'FRIEND', 1, '2021-08-10 07:57:11', '2021-10-20 03:11:04', 1),
(265, 283, 95, 2, 2, 'A-', 'Fully Vaccinated', 35, '2021-11-18', NULL, NULL, '5th floor, Aloha technology,\r\nPrabhavee Tech Park, Baner', '5th floor, Aloha technology,\r\nPrabhavee Tech Park, Baner', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-08-12 08:35:26', '2021-08-12 08:35:26', NULL),
(266, 285, 94, 2, 2, 'B+', 'Partially Vaccinated', 285, '1992-12-06', NULL, NULL, 'NEHA CON.FLAT NO-3 DHANKWADI GAVTHAN PUNE 43', 'NEHA CON.FLAT NO-3 DHANKWADI GAVTHAN PUNE 43', 'UMESH SHELAR', 'NEHA CON.FLAT NO-3 DHANKWADI GAVTHAN PUNE 43', 8668625405, 'Father', 'ASHWINI SHELAR', 'NEHA CON.FLAT NO-3 DHANKWADI GAVTHAN PUNE 43', 8007743876, 'Mother', 'SNEHAL SHENDI', 'KARVENAGAR', 7498812659, 'FRIENDS', 'SUMIT PAWAR', 'DHANKWADI', 8208496493, 'FRIENDS', 0, '2021-08-19 08:39:19', '2021-09-08 04:26:42', 1),
(267, 286, 104, 2, 2, 'AB+', 'No', 286, '2021-07-16', NULL, NULL, 'PIMPLE JAGTAP ROAD WADHU BK 412216', 'PIMPLE JAGTAP ROAD WADHU BK 412216', 'LADA HANUMANT PHAD', 'PIMPLE JAGTAP ROAD WADHU BK 412216', 9689111794, 'Brother', 'HANUMANT NATHRAO PHAD', 'PIMPLE JAGTAP ROAD WADHU BK 412216', 9578761515, 'Father', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-08-19 09:53:33', '2021-09-04 07:28:07', 1),
(268, 287, 96, 2, 2, 'AB+', 'No', 287, '2021-06-15', NULL, NULL, 'SAHARA CORNER FLAT NO 17 BHEKARAI NAGAR HADAPSAR PUNE 411028', 'SAHARA CORNER FLAT NO 17 BHEKARAI NAGAR HADAPSAR PUNE 411028', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-08-24 08:26:55', '2021-09-08 04:24:09', 1),
(269, 288, 106, 2, 4, 'B+', 'Fully Vaccinated', 191, '2020-06-05', NULL, NULL, 'BHIMAI NIWAS NEAR MUMBAIKAR APP CHENDARI KOLIWADA THANE EAST', 'BHIMAI NIWAS NEAR MUMBAIKAR APP CHENDARI KOLIWADA THANE EAST', NULL, NULL, 7678071423, NULL, 'ANIL SHIRODKAR', '11T POWAI MUMBAI', 9820626804, 'Brother', 'PRATIK KOL', 'BHIMAI NIWAS NEAR MUMBAIKAR APP CHENDARI KOLIWADA THANE EAST', 9820626804, 'FRIEND', NULL, NULL, NULL, NULL, 0, '2021-08-25 06:49:35', '2021-08-25 06:49:35', NULL),
(273, 292, 97, 2, 2, 'B+', 'Partially Vaccinated', 36, '2020-04-16', NULL, NULL, 'SR NO 8 BUSINESS BAY  GATE NO 5 YASHWANT NAGAR YERWADA PUNE-411006', 'SR NO 8 BUSINESS BAY  GATE NO 5 YASHWANT NAGAR YERWADA PUNE-411006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'MAYUR SATHE', 'SR NO 8 YESHWANT NAGAR YERAWADA', 7875885910, 'FRIEND', NULL, 'SR NO 8 YESHWANT NAGAR YERAWADA', 8888014191, 'FRIEND', 1, '2021-08-25 07:52:50', '2021-10-20 01:24:27', NULL),
(274, 293, 102, 2, 2, 'B+', 'Partially Vaccinated', 293, '2021-05-17', NULL, NULL, 'NARHE BHAYARI  PUNE', 'D/O PANDURANG NIKAM, A/P NASARAPUR, ALUKA BHOR, PUNE -412213', 'JAYASHRI NIKAM', 'GOKUL NAGAR NARHE- 411041', 9373736425, 'Sister', 'PRITI SAWANT', 'MANAJI NAGAR, NARE PUNE 411041', 7057286286, 'Mother', 'VIRENDRA GAIKWAD', 'WAKAD PUNE', 9689146146, 'FRIEND', 'TEJAS SWANT', 'NARHE BHAYARI  PUNE', 9373736425, 'FRIEND', 0, '2021-08-25 08:09:17', '2021-09-09 07:59:49', 1),
(275, 294, 104, 2, 10, 'B+', 'Partially Vaccinated', 294, '2021-06-12', NULL, NULL, 'MAHAL PATNE TAL SATANA DIST.NASHIK', 'MAHALPATANE NASHIK', 'KHAIRNAR NISHIKANT PRADIP', 'AT POST NAMPUR, NASHIK', 7030457941, 'Brother', 'BHAUSAHEB G MANCHEWAL', 'MAHAL PATNE TAL SANTA, NASHIK', 9689766583, 'Brother', 'SANGEETA S BUDKAR', 'DWARKA DAMODAR HIGHT OLD TAMBAT NASHIK', 9823321726, 'FRIEND', 'VAIBHAV KAILAS GAWADE', 'NASHIK', 9975042940, 'FRIEND', 0, '2021-08-25 08:30:19', '2021-09-11 04:53:38', 1),
(276, 295, 104, 2, 10, 'AB+', 'Partially Vaccinated', 295, '2021-06-03', NULL, NULL, 'H3892 NANAVALI DARGA KATHDA JUNE NASHIK 422001', 'H3892 NANAVALI DARGA KATHDA JUNE NASHIK 422001', 'JAGDISH BADGE', 'H3892 NANAVALI DARGA KATHADA PLD NASHIK', 9823292924, 'Brother', 'SNGITA VICHARE', 'KALIKA NAGAR NASHIK', 9657265061, 'Sister', 'KAILAS KALE', 'OLD NASHIK', 9922988727, 'FRIEND', 'RUSHIKESH  RAM PHULE', 'GHAR NO 1403 TAKSAL LEN BHADRAKALI NASHIK 422001', 8956660143, 'FRIEND', 0, '2021-08-25 08:32:53', '2021-09-11 06:39:34', 1),
(277, 296, 104, 2, 10, 'O+', 'Fully Vaccinated', 296, '2021-06-03', NULL, NULL, 'FLAT NO 20 DWARKA DAMODAR HEIGHT OLD TAMBATLANE NASHIK', '20.OLD TAMBT LANE OPP.DR.KABARE CLINIC BHADRAKALI NASHIK', 'nitin padmakar pawar', 'tiwanda chowk opp kashiba madical opo nashik', 9881948828, 'Brother', 'satish andra phadol', 'flat no 3 dattshree app mahatma nagar nashik', 9823260726, NULL, 'pradeep omkar badge', 'H .3892 nanavali daga kathada pld nashik', 9823292924, 'FRIEND', 'kailas ratan kale', 'june kumbharrwade old nashik', 9922988727, 'FRIEND', 0, '2021-08-25 08:34:55', '2021-09-11 05:55:46', 1),
(278, 297, 107, 2, 11, 'AB+', 'No', 36, '2021-06-01', NULL, NULL, 'A-9 VIMAL ROYAL CITY RADHIKA ROAD SATARA', 'A-9 VIMAL ROYAL CITY RADHIKA ROAD SATARA', 'MAJJID ABBAS SHIKALGAR', 'A-9 VIMAL ROYAL CITY RADHIKA ROAD SATARA', 7841000121, 'Father', 'SAJIDA SHIKALGAR', 'A-9 VIMAL ROYAL CITY RADHIKA ROAD SATARA', 9284324594, 'Mother', 'PRASAD ZUTING', 'SATARA', 7972075583, 'FRIEND', 'WASHIM SHAIKH', 'MANGALWAR PETH SATARA', 9970033418, 'FRIEND', 0, '2021-08-25 08:36:07', '2021-08-25 08:36:07', NULL),
(279, 298, 102, 2, 2, 'B+', 'Partially Vaccinated', 35, '2021-08-09', NULL, NULL, 'K 103 SAI MYSTIQUE AMBEGOAN BK PUNE 46', '3 E ,PARDESHPURA,JUNNAR,PUNE 410502', 'SANTOSHSINGH RAJPUT', '3 E ,PARDESHPURA,JUNNAR,PUNE 410502', 9881176699, 'Father', 'VIKAS PARDESHI', '3 E ,PARDESHPURA,JUNNAR,PUNE 410502', 9049639009, 'Brother', 'MOHIT BHORKAR', '91/5 A PARVATI DARSHAN PUNE - 09', 8983352440, 'FRIEND', 'KUNAL WATANE', '2101, K WING ASAWARI NANDED CITY ,PUNE', 8605119998, 'FRIEND', 1, '2021-08-25 13:25:37', '2021-10-21 01:11:46', NULL),
(280, 299, 94, 2, 2, 'B+', 'Partially Vaccinated', 35, '2021-08-28', '', NULL, 'kothrud', 'kothrud', NULL, NULL, NULL, 'Relation', NULL, NULL, NULL, 'Relation', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-08-28 00:46:16', '2021-09-08 04:27:24', 1),
(281, 300, 94, 2, 2, 'O+', 'No', 300, '2021-06-10', NULL, NULL, 'FLAT NO 601 , SR NO 926 KASBA PETH KAGZIPURA PUNE 411011', 'FLAT NO 601 , SR NO 926 KASBA PETH KAGZIPURA PUNE 411011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-08-30 03:40:19', '2021-10-08 03:15:13', 1),
(283, 302, 102, 2, 2, 'B+', 'No', 35, '2021-07-01', NULL, NULL, 'VAIBHAV GAD Apartment, 4th Floor, Flat no 7, Near Shruti Mangal Karyalay, Apte Road, opposite FC Road, Pune-411004', 'VAIBHAV GAD Apartment, 4th floor, Flat no 7, Near Shruti Mangal Karyalay, Apte Road, opposite FC Road, Pune-411004', 'PRAVIN LAXMAN BADHE', 'VAIBHAV GAD Apartment, 4th floor, Flat no 7, Near Shruti Mangal Karyalay, Apte Road, opposite FC Road, Pune-411004', 9859653653, 'Father', 'SHEETAL PRAVIN BADHE', 'VAIBHAV GAD Apartment, 4th floor, Flat no 7, Near Shruti Mangal Karyalay, Apte Road, opposite FC Road, Pune-411004', 9956399567, 'Mother', 'RUSHIKESH AMIT SATPUTE', '983/ 3/ 3 KAMALKUNJ SHUKHARWAR PETH, SWARGATE PUNE- 411002', 7875745678, 'FRIEND', 'ANSH RAVINDRA WAGH', 'MAYURESHWAR RD, SAI NAGAR PARK, PIMPLE SAUDAGAR, PIMPRI-CHINCHWAD, MAHARASHTRA PUNE- 411027', 8007587893, 'FRIEND', 1, '2021-09-02 04:47:41', '2021-10-04 08:25:55', NULL),
(284, 303, 94, 2, 2, 'AB+', 'No', 303, '2021-09-01', NULL, NULL, 'SIDHARTH CO-OP HOUSING SOCIETY, SHANIWAR PETH PUNE- 411030', '18, SAI HOUSING SOCIETY, BHUBANESWAR, POST VARSE, ROHA RAIGAD PUNE- 402109', 'VIPUL NILKANTH PATIL', 'MISSISSIPPI US AMERICA', 198523754, 'Father', 'PRATIKSHA PATIL', 'NANDED CITY, PUNE- 411041', 8237174111, 'Mother', 'TRUNALI SANGAM', 'YERWADA PUNE', 9822089623, 'FRIEND', 'MINAKSHRI PARJANE', 'SHANIWAR PETH', 8793878119, 'FRIEND', 0, '2021-09-03 03:03:24', '2021-10-08 02:35:00', 1),
(285, 304, 98, 2, 3, 'B+', 'No', 36, '2021-08-24', NULL, NULL, 'FLAT NO 710 B WING CHIKHALI JADHAWADI ROAD VISHNU VIHAR SOCIETY', 'KOTHRUD SHASTRINAGAR SHINDE COLONY SANGRAM CHOWK,PUNE- 411038', 'ARACHANA N NAIK', 'GURUWAR PETH SHITALADEVI CHOWK PANCHRATHN HEIGHTS 5TH FLOOR', 7058364449, 'Sister', 'JAYASHREE S NAIK', 'GURUWAR PETH SHITALADEVI CHOWK PANCHRATHN HEIGHTS 5TH FLOOR', 9552031577, 'Mother', 'MADHURI KONDE', 'DIGHI', 9309535082, 'FRIEND', 'RASHMI TIKONE', 'ARANESHWAR PUNE- 9', 9226134909, 'FRIENDS', 1, '2021-09-03 07:03:51', '2021-10-08 03:15:32', NULL),
(286, 305, 107, 2, 11, 'B+', 'Partially Vaccinated', 305, '1977-01-08', NULL, NULL, 'AT.POST MALGAON, TAL - DIST SATARA', 'AT.POST MALGAON, TAL - DIST SATARA', 'SUCHITA D.AHIREKAR', 'AT.POST MALGAON TAL - DIST SATARA', 7350369965, 'Spouse', 'YASHMIT D.AHIREKAR', 'AT.POST MALGAON TAL - DIST SATARA', 9763452327, 'Spouse', 'PRASAD R.ZUTING', 'SADASHIV PETH SATARA', 9922355229, 'FRIENDS', 'RAVINDRA DHANDARPHALE', '33,SHUKRAWAR PETH,SATARA', 9850292105, 'FRIENDS', 0, '2021-09-06 04:35:07', '2021-09-23 05:20:24', 1),
(287, 306, 94, 2, 2, 'A+', 'Partially Vaccinated', 35, '1996-05-08', NULL, NULL, '1005, RAJENDRA NAGAR PMC COLONY PUNE - 411030', '1005, RAJENDRA NAGAR PMC COLONY PUNE - 411030', 'NANDA CHAVAN', '1005, RAJENDRA NAGAR PMC COLONY PUNE - 30', 8669114239, 'Mother', 'SURESH CHAVAN', '1005, RAJENDRA NAGAR PMC COLONY PUNE - 30', 7276879593, 'Father', 'HANUMANT TARANGE', 'BARAMATI', 7040790505, 'FRIENDS', 'AKSHAY WAGHAMARE', 'SUSGAON', 8999497687, 'FRIENDS', 0, '2021-09-06 05:13:30', '2021-09-06 05:13:30', NULL),
(288, 307, 94, 2, 2, 'B+', 'Partially Vaccinated', 307, '1993-07-17', NULL, NULL, 'FLAT NO. 301 LAXMI WHITE HOUSE NAHRE - 411041', 'FLAT NO.3/4 PORNIMA HEIGHTS NAHRE MANAJI NAGAR - 411041', 'PRITI AMIT SAWANT', '3/4 PORNIMA HEIGHT NAHRE MANAJI - 411041', 7057286286, 'Mother', 'PANDURANG NIKAM', 'FLAT NO - 304 MANISHA HEIGHTS MANAJI NAGAR PUNE - 411041', 8805364945, 'Father', 'VISHAL MANKAR', 'FLAT NO - 1 SATYASAI RESIDENCY NAHRE', 9923190100, 'FRIENDS', 'SAGAR MANKAR', 'FLAT NO - 1 SATYASAI RESIDENCY NAHRE', 9823088333, 'FRIENDS', 0, '2021-09-06 05:44:47', '2021-09-28 03:05:56', 1),
(289, 308, 94, 2, 2, 'O+', 'Partially Vaccinated', 308, '1977-04-21', NULL, NULL, '222, BUDHWAR PETH, SHREMANT DAGDUSHET CHOWK SINDHU VASANT APP 4TH FLOOR PUNE', '222, BUDHWAR PETH, SHREMANT DAGDUSHET CHOWK SHIDHU VASANT APP 4TH FLOOR PUNE', 'VAISHNAVI V.KADAM', '222,BUDHWAR PETH SINDHU VASANT APP 4TH FLOOR FLAT NO. 9 B PUNE', 9623611251, 'Spouse', 'KETAN VISHNU KADAM', '222,BUDHAWAR PETH PUNE SINDHU DAGDUSHET GANPATI CHOWK SINDHU VASANT APP.PUNE', 9922119191, 'Spouse', 'VISHAL KEDARI', '223,BUDHWAR PETH SHREEMANT GANPATI CHOWK PUNE', 9049303030, 'FRIENDS', 'SAGAR BHANDEKAR', 'WARJE MALWADI', 9307599233, 'FRIENDS', 0, '2021-09-06 06:41:13', '2021-09-06 07:28:24', 1),
(290, 309, 99, 2, 2, 'O+', 'Partially Vaccinated', 309, '1991-07-19', NULL, NULL, 'ADIRAJ CLASTER SOC.FLAT NO. 305 B WING BHUGAON PUNE', 'ADIRAJ CLASTER SOC.FLAT NO. 305 B WING BHUGAON PUNE', 'GOVRADHAN RANGPPA TALWAR', 'ADIRAJ CLASTER SOC.FLAT NO.305 B WING BHUGAON PUNE', 7709877721, 'Father', 'SUMITRA RAVI TALWAR', 'ADIRAJ CLASTER SOC.FLAT NO.305 B WING BHUGAON PUNE', 9673730160, NULL, 'MAHESH MORE', 'ADIRAJ CLASTER SOC.FLAT NO.508 B WING BHUGAON PUNE', 9689125577, 'FRIENDS', 'HOUSRAJ MAHAJAN', 'ADIRAJ CLASTER SOC.FLAT NO.406 B WING BHUGAON PUNE', 9689125577, 'FRIENDS', 0, '2021-09-06 07:42:12', '2021-10-02 04:11:28', 1),
(291, 310, 97, 2, 2, 'O+', 'No', 37, '2021-09-09', NULL, NULL, 'SHANTADEVI APARTMENT FLAT NO 306 NEAR GURUJI TALIM GANESH MANDAL 45 B LAXMI ROAD BUDHWAR PWTH PINE 411002', 'SHANTADEVI APARTMENT FLAT NO 306 NEAR GURUJI TALIM GANESH MANDAL 45 B LAXMI ROAD BUDHWAR PWTH PINE 411002', 'KALPANA BAPU PATIL', 'SHANTADEVI APARTMENT FLAT NO 306 NEAR GURUJI TALIM GANESH MANDAL 45 B LAXMI ROAD BUDHWAR PWTH PINE 411002', 9404236958, 'Mother', 'PALAAVI VISHAL JAMDADE', 'FLAT NO 902 PRAYEJA CITY SINHAGAD ROAD PUNE 410041', 9921044440, 'Sister', 'SAGAR BHANDEKAR', 'WARJE MALWADI PUNE', 9146466606, 'FRIEND', 'MANDAR ASHOK RUDRAKAR', 'GANPATI CHOWK LAXMI ROAD NEAR SAGAR FOOT WEAR PUNE 411041', 9673927999, 'FRIEND', 0, '2021-09-07 02:05:14', '2021-09-07 02:05:14', NULL),
(292, 311, 96, 2, 2, 'AB+', 'Partially Vaccinated', 36, '2021-09-08', NULL, NULL, 'SANE GURUJI NAGAR AMBIL ODHA COLONY 9/362  BEHIND S P COLLEGE SADASHIV PETH PUNE411030', 'SANE GURUJI NAGAR AMBIL ODHA COLONY 9/362  BEHIND S P COLLEGE SADASHIV PETH PUNE411030', 'SANTOSH KENGAR', 'SOUDAGAR GARDEN KIVALE', 9923888148, 'Brother', 'SUYOG KENGAR', 'SANE GURUJI NAGAR AMBIL ODHA COLONY 9/362  BEHIND S P COLLEGE SADASHIV PETH PUNE411030', NULL, 'Father', 'CANDU AAVPVEL', '81 SANE GURUJI NAGAR SADASHIV PETH PUNE', 9881818270, 'FRIEND', 'NAVNATH AAYVALE', 'SADASHIV PETH PUNE', 9850802166, 'FRIEND', 1, '2021-09-08 00:03:22', '2021-10-12 09:07:27', NULL),
(293, 312, 102, 2, 2, 'B+', 'No', 312, '2021-09-09', NULL, NULL, 'JYOTILING APARTMENT FLAT NO 408 4 FLOOR C13 SCHOOL GHORPADE PETH PUNE 411042', '225/26 GULTEKADI AUDYOGIK VASAHAT GULTEKADI PUNE 411037', 'SABA SHAIKH', 'JYOTILING APARTMENT FLAT NO 408 4 FLOOR C13 SCHOOL GHORPADE PETH PUNE 411042', 7721640781, 'Sister', 'ALTAF SHAIKH', 'JYOTILING APARTMENT FLAT NO 408 4 FLOOR C13 SCHOOL GHORPADE PETH PUNE 411042', 7620388195, 'Brother', 'SHABNA SHAIKH', 'TASKAN APARTMENT 11 FLOOR FLAT NO 3011 KHARADI 411042', 9595070786, 'FRIEND', 'SANA SHAIKH', 'GULTAKDI AUDOGIK VASHAD PUNE- 37', 9921049726, 'FRIEND', 0, '2021-09-08 01:42:31', '2021-10-05 09:42:13', 1),
(294, 313, 102, 2, 2, 'A+', 'Partially Vaccinated', 313, '2021-06-14', NULL, NULL, '149 MANGALWAR PETH NEAR TODKAR HOSPITAL PUNE 41006', 'SR NO  12 LAXMI NAGAR YERWADA PUNE 411006', 'REKHA KINDRE', 'RAVIVAR PETH PUNE', 9921378555, 'Sister', 'UMA KAMTHE', 'MANGALWARPETH PUNE', 9028483387, 'Sister', 'ALKA BONDALE', 'MANGALWAR PETH PUNE', 9763657991, 'FRIEND', 'PRANTA PASALKAR', 'BIBWEWADI', 7040764888, 'FRIEND', 0, '2021-09-08 07:06:13', '2021-09-23 03:17:05', 1),
(295, 314, 94, 2, 2, 'O+', 'No', 314, '2021-09-06', NULL, NULL, '13 TADIWALU ROAD PUNE- 411001', '13 TADIWALU ROAD VEER BHARAT TARUN MANDAL  PUNE- 411001', 'MUBASHIRA SIDDIQUI', '13 TADIWALU ROAD PUNE- 411001', 8390480426, NULL, 'SOHAIL SIDDIQUI', '13 TADIWALU ROAD PUNE- 411001', 7379931894, NULL, 'OSWA ANSARI', 'VIMAN NAGAR PUNE- 01', 9975496439, 'FRIEND', 'HAIDER ALI', '13 TADIWALU ROAD PUNE- 411001', 9619318550, 'FRIEND', 0, '2021-09-09 07:41:51', '2021-10-05 09:36:58', 1),
(296, 315, 97, 2, 2, 'AB+', 'No', 36, '2021-07-05', NULL, NULL, '13 TADIWALA ROAD BH DR. AGRAWAL CLINIC PUNE STATION PUNE', '13 TADIWALA ROAD BH DR. AGRAWAL CLINIC PUNE STATION PUNE', 'PRABUDHA LIMRAJ', 'SHEDALKAR 13 TADIWALAROAD PUNE', 9393531531, 'Father', NULL, NULL, NULL, NULL, 'AKASH CHIKARE', 'TADIWALA ROAD PUNE', 8530982937, NULL, NULL, NULL, NULL, NULL, 1, '2021-09-11 08:41:44', '2021-09-11 08:43:42', NULL),
(297, 316, 97, 2, 2, 'O+', 'Partially Vaccinated', 36, '2021-09-13', NULL, NULL, 'NEAR NIMBALKAR TALIMB 1245 SADASHIV PETH PUNE  411030', 'NEAR NIMBALKAR TALIMB 1245 SADASHIV PETH PUNE  411030', 'ANITA RASKAR', 'NEAR NIMBALKAR TALIMB 1245 SADASHIV PETH PUNE  411030', 7387547478, 'Mother', 'MANALI RASKAR', 'NEAR NIMBALKAR TALIMB 1245 SADASHIV PETH PUNE  411030', 9021880306, NULL, 'VIGHNESH PARKHE', 'PASHAN PUNE', 9850081011, 'FRIEND', 'SANKET KALE', 'KOTHRUD PUNE', 8208618056, 'FRIEND', 0, '2021-09-14 07:10:31', '2021-09-28 02:53:46', 1),
(298, 317, 102, 2, 2, 'A+', 'Partially Vaccinated', 36, '2021-09-20', NULL, NULL, 'SR NO 26/2, MUNJABA WASTI ROAD NO 09 D DHANORI PUNE 411015', 'SR NO 26/2, MUNJABA WASTI ROAD NO 09 D DHANORI PUNE 411015', 'GOVARDHAN PATHARE', 'SR NO 26/2, MUNJABA WASTI ROAD NO 09 D DHANORI PUNE 411015', 9834041187, 'Spouse', 'KISHOR BHOSALE', 'NANA PETH PUNE 411002', 9623308280, 'Brother', 'AJAY NAIK', 'SINHAGAD ROAD PUNE', 9923588500, 'FRIEND', 'DEEPAK PATIL', 'HADAPSAR PUNE', 9373085901, 'FRIEND', 0, '2021-09-20 01:38:00', '2021-09-20 01:38:00', NULL),
(299, 318, 96, 2, 2, 'B+', 'No', 318, '2021-08-12', NULL, NULL, 'F NO 401 NATH COMPLEX BT KAWADE ROAD PUNE', 'SHIRKE COMPANY SAMOR BT KAWADE ROAD PUNE', 'MAHESH MHADU SHINDE', 'F NO 401 NATH COMPLEX BT KAWADE ROAD PUNE', 9011282883, 'Brother', 'MHADU LAXMAN SHINDE', 'F NO 401 NATH COMPLEX BT KAWADE ROAD PUNE', 7498485234, 'Brother', 'GANESH LAXMAN KAMBLE', 'SHIRKE COMPANY SAMOR BT KAWADE ROAD PUNE', 9112858378, 'FRIEND', 'GANESH RANAWARE', 'SHIRKE COMPANY SAMOR BT KAWADE ROAD PUNE', 9022366148, 'FRIEND', 0, '2021-09-24 05:07:34', '2021-10-26 07:10:06', 1),
(300, 319, 97, 2, 2, 'B+', 'No', 36, '2021-09-01', NULL, NULL, 'SR NO 191 NAGPUR CHAWL YERWADA PUNE 411006', 'SR NO 191 NAGPUR CHAWL YERWADA PUNE 411006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-09-24 05:51:33', '2021-11-12 06:50:29', NULL),
(301, 320, 102, 2, 2, 'B-', 'No', 36, '2021-09-17', NULL, NULL, 'KATRAJ SANTOSH NAGAR AMBEGAON PUNE 411046', '611 KASEWADI BHAWANI PET PUNE 411042', 'ISHA  WAGH', 'DECCAN', 9322485907, 'Sister', 'GANESH WAGHMARE', 'KARVE NAGAR', 8329179995, 'Sister', 'KAJAL BURUNGE', 'RASTA PETH', 7385261090, 'FRIEND', 'SMITALI BIBWE', 'BIBWEWADI', 9307902951, 'FRIEND', 0, '2021-09-29 05:21:36', '2021-09-29 05:21:36', NULL),
(302, 321, 100, 2, 4, 'O+', 'Fully Vaccinated', 321, '2020-12-02', NULL, NULL, '2372 NEW BALAJI NAGAR AMBERNATH(WEST)', '2372 NEW BALAJI NAGAR AMBERNATH(WEST)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PRAVIN PAWAR', 'NEW BALAJI NAGAR AMBERNATH (W)', 9879828647, 'FRIEND', 'MAHESH BABAR', 'ROOM NO 7249 SHIVLING NAGAR AMBERNATH PUNE', 7744996830, 'FRIEND', 0, '2021-09-29 08:45:45', '2021-10-08 04:15:00', 1),
(303, 322, 99, 2, 2, 'A+', 'Fully Vaccinated', 322, '2021-03-03', NULL, NULL, 'GAT NO 262/3, 406 B AADHIRAJ CLUSTER SOCIETY PAUD ROAD BEHIND DAULAT PETROL PUMP BHUGAON PUNE 412115', 'GAT NO 262/3, 406 B AADHIRAJ CLUSTER SOCIETY PAUD ROAD BEHIND DAULAT PETROL PUMP BHUGAON PUNE 412115', 'SUVARNALATA MAHAJAN', 'GAT NO 262/3, 406 B AADHIRAJ CLUSTER SOCIETY PAUD ROAD BEHIND DAULAT PETROL PUMP BHUGAON PUNE 412115', 7507798757, 'Spouse', 'ASHOK SONAWANE', 'BEED', 9767121968, 'Spouse', 'DATTATRAY DASHRATH  KANDHARE', 'Anant Prabha Construction, C Building, B wing, 5th floor, Flat No : 19, Survey no 78, NDA Rd, Near AdityaHotel, Shivane, Ahire, Pune : 411023', 8888834938, 'FRIEND', 'AKSHAY SUDHIR GAIKWAD', 'A22,/20/, 276, Aota, Upper Indira Nagar, Bibewadi MARKET YARD Pune -411037', 8788086796, 'FRIEND', 0, '2021-10-01 02:14:49', '2021-10-08 07:05:41', 1),
(304, 323, 102, 2, 2, 'B+', 'No', 32, '2021-09-01', NULL, NULL, '1341 NEAR SURYA HOSPITAL KASABA PETH', '1341 NEAR SURYA HOSPITAL KASABA PETH', 'BALKRUSHNA KHADU NIKAM', '1341 NEAR SURYA HOSPITAL KASABA PETH', 7709410689, 'Father', 'NAYANA NIKAM', '1341 NEAR SURYA HOSPITAL KASABA PETH', 7709410689, 'Mother', 'AKASH DHOGADE', '1343 NEAR SURYA HOSPITAL KASABA PETH', 7776840006, 'FRIEND', 'CHAITANYA YADAV', '1345 NEAR SURYA HOSPITAL KASABA PETH', 9890392871, 'FRIEND', 0, '2021-10-02 03:57:10', '2021-10-02 03:57:10', NULL),
(305, 324, 106, 2, 4, 'O+', 'Partially Vaccinated', 191, '2021-08-01', NULL, NULL, '108 GANGA NIWAS TULINJ ROAD MAHESH PARK NEAR SARSWATI SCHOOL NALASOPARA EAST VASAI PALGHAR', '108 GANGA NIWAS TULINJ ROAD MAHESH PARK NEAR SARSWATI SCHOOL NALASOPARA EAST VASAI PALGHAR', 'SANDIP GADANKUSH', 'VIRAR BTT CHAWL', 9987542446, 'Brother', 'REENA GADANKUSH', '108 GANGA NIWAS TULINJ ROAD MAHESH PARK NEAR SARSWATI SCHOOL NALASOPARA EAST VASAI PALGHAR', 8482832633, 'Spouse', 'ROHAN DIPAK RAJE', 'MAHIM', 8600248513, 'FRIEND', 'PRAFULL BHOSALE', 'DADAR', 8779272099, 'FRIEND', 0, '2021-10-02 05:01:51', '2021-10-02 05:01:51', NULL),
(306, 325, 98, 2, 2, 'B+', 'Partially Vaccinated', 37, '2017-08-01', NULL, NULL, 'FL NO 6 SHRADDHA APT INDRA COLONY VIKAS NAGAR DEHU ROAD PUNE 412101', 'FL NO 6 SHRADDHA APT INDRA COLONY VIKAS NAGAR DEHU ROAD PUNE 412101', 'NILESH BAGLAR', 'FL NO 6 SHRADDHA APT INDRA COLONY VIKAS NAGAR DEHU ROAD PUNE 412101', 7507887818, 'Brother', 'RAMCHANDRA BAGLAR', 'FL NO 6 SHRADDHA APT INDRA COLONY VIKAS NAGAR DEHU ROAD PUNE 412101', 7507887818, 'Father', 'SANTOSH KENGAR', 'DEHU ROAD TC COLONY', 9923888148, 'FRIEND', 'MAYUREKHUNVE', 'DEHU ROAD TC COLONY', 7350714197, 'FRIEND', 0, '2021-10-09 06:47:15', '2021-10-09 06:48:08', 1),
(307, 326, 102, 2, 2, 'A+', 'Partially Vaccinated', 326, '2021-09-22', NULL, NULL, '571 SHUKRWAR PETH PUNE 411002', '571 SHUKRWAR PETH PUNE 411002', 'RANI BHOLE', '571 SHUKRWAR PETH PUNE 411002', 9763484776, 'Mother', 'SANTOSH BHOLE', '571 SHUKRWAR PETH PUNE 411002', 7719856143, 'Mother', 'SUMIT AVHADE', '611 BHAWANI PETH PUNE 411042', 762092453, 'FRIEND', 'RUSHIKESH BHOLE', '571 SHUKRWAR PETH PUNE 411002', 7888036143, 'FRIEND', 0, '2021-10-20 03:36:36', '2021-11-12 06:49:34', 1),
(308, 327, 98, 2, 2, 'O+', 'No', 36, '2021-09-11', NULL, NULL, '28/1 O;D TOFHKHANA SHIVAJI NAGAR MARUTI MANDIR JAWAL, PUNE 411005', '28/1 O;D TOFHKHANA SHIVAJI NAGAR MARUTI MANDIR JAWAL, PUNE 411005', 'SWATI SONAWANE', '28/1 O;D TOFHKHANA SHIVAJI NAGAR MARUTI MANDIR JAWAL, PUNE 411005', 9923559988, 'Spouse', 'ASHOK SONAWANE', '28/1 O;D TOFHKHANA SHIVAJI NAGAR MARUTI MANDIR JAWAL, PUNE 411005', 8421411650, 'Father', 'ABHIJIT GHAG', '1309 KASBA PETH PUNE', 9822557505, 'FRIEND', 'AMOL PARDESHI', '1306 GANJ PETH PUNE', 8788007199, 'FRIEND', 0, '2021-10-26 07:24:44', '2021-10-26 07:24:44', NULL),
(309, 328, 98, 2, 3, 'A+', 'Partially Vaccinated', 36, '2021-10-13', NULL, NULL, 'AT POST CHINCHOSHI NEAR MARATHI SCHOLL TAL KHED PUNE', 'AT POST CHINCHOSHI NEAR MARATHI SCHOLL TAL KHED PUNE', 'RUPALI PACHPUTE', 'AT POST CHINCHOSHI NEAR MARATHI SCHOLL TAL KHED PUNE', 8668724160, 'Spouse', 'MEENA PACHPUTE', 'AT POST CHINCHOSHI NEAR MARATHI SCHOLL TAL KHED PUNE', 9920933607, 'Mother', 'NITIN OVHALE', 'AT POST CHINCHOSHI KHED PUNE', 9607683712, 'FRIEND', 'SACHIN OVHALE', 'AT POST CHINCHOSHI KHED PUNE', 9920933607, 'FRIEND', 1, '2021-10-26 07:53:13', '2021-11-12 07:04:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ptx_user_roles`
--

CREATE TABLE `ptx_user_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `designations_id` int(10) UNSIGNED NOT NULL,
  `menu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `edited` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ptx_user_roles`
--

INSERT INTO `ptx_user_roles` (`id`, `designations_id`, `menu`, `view`, `created`, `edited`, `deleted`, `created_at`, `updated_at`) VALUES
(74, 19, 'Dashboard', 1, 1, 1, 1, '2021-02-25 12:38:51', '2021-02-25 07:08:51'),
(75, 19, 'System_User', 1, 1, 1, 1, '2021-02-25 12:38:51', '2021-02-25 07:08:51'),
(76, 19, 'Variable_User', 1, 1, 0, 1, '2021-02-25 12:38:51', '2021-02-25 07:08:51'),
(77, 19, 'Department', 1, 1, 1, 1, '2021-02-24 17:00:27', '2021-02-24 11:30:27'),
(78, 19, 'Settings', 1, 1, 0, 1, '2021-02-25 12:38:51', '2021-02-25 07:08:51'),
(79, 19, 'Backup_Database', 1, 1, 1, 0, '2021-02-25 12:38:51', '2021-02-25 07:08:51'),
(104, 24, 'Dashboard', 1, 1, 1, 1, '2021-03-16 09:09:50', '2021-03-16 03:39:50'),
(105, 24, 'Master', 1, 0, 0, 0, '2021-03-16 09:09:50', '2021-03-16 03:39:50'),
(106, 24, 'Daily_Entry', 1, 0, 0, 0, '2021-03-16 09:09:50', '2021-03-16 03:39:50'),
(107, 24, 'Uploads', 1, 0, 0, 0, '2021-03-16 09:09:50', '2021-03-16 03:39:50'),
(108, 24, 'HRMs', 1, 0, 0, 0, '2021-03-16 09:09:50', '2021-03-16 03:39:50'),
(109, 24, 'Account', 1, 0, 0, 0, '2021-03-16 09:09:50', '2021-03-16 03:39:50'),
(110, 24, 'Roles_&_Permission', 1, 1, 1, 1, '2021-03-16 09:09:50', '2021-03-16 03:39:50'),
(111, 24, 'Settings', 1, 1, 1, 1, '2021-03-16 09:09:50', '2021-03-16 03:39:50'),
(112, 24, 'Company/Branch', 1, 0, 0, 0, '2021-03-16 09:09:50', '2021-03-16 03:39:50'),
(113, 24, 'Agent/Telecaller', 1, 0, 0, 0, '2021-03-16 09:09:50', '2021-03-16 03:39:50'),
(114, 24, 'Bank', 1, 0, 0, 0, '2021-03-16 09:09:50', '2021-03-16 03:39:50'),
(115, 24, 'Backet', 1, 0, 0, 0, '2021-03-16 09:09:50', '2021-03-16 03:39:50'),
(116, 24, 'Recollection_Roles', 1, 0, 0, 0, '2021-03-16 09:09:51', '2021-03-16 03:39:51'),
(117, 24, 'Employee', 1, 1, 1, 1, '2021-03-16 09:09:51', '2021-03-16 03:39:51'),
(118, 24, 'Cash_Collection', 1, 0, 0, 0, '2021-03-16 09:09:51', '2021-03-16 03:39:51'),
(119, 24, 'Backup_Database', 1, 1, 1, 1, '2021-03-16 09:09:51', '2021-03-16 03:39:51'),
(120, 24, 'Agent/Telecaller_Master', 1, 1, 1, 1, '2021-03-16 09:09:51', '2021-03-16 03:39:51'),
(121, 24, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-03-16 09:09:51', '2021-03-16 03:39:51'),
(122, 24, 'Company_Master_Upload', 1, 0, 0, 0, '2021-03-16 09:09:51', '2021-03-16 03:39:51'),
(123, 24, 'Manage_Salary', 1, 0, 0, 0, '2021-03-16 09:09:51', '2021-03-16 03:39:51'),
(124, 24, 'Make_Payment', 1, 0, 0, 0, '2021-03-16 09:09:51', '2021-03-16 03:39:51'),
(125, 24, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-03-16 09:09:51', '2021-03-16 03:39:51'),
(126, 24, 'Generate_Invoice', 1, 0, 0, 0, '2021-03-16 09:09:51', '2021-03-16 03:39:51'),
(127, 24, 'Payment_Received', 1, 0, 0, 0, '2021-03-16 09:09:51', '2021-03-16 03:39:51'),
(128, 25, 'Dashboard', 1, 0, 0, 0, '2021-08-07 07:02:07', '2021-08-07 02:02:07'),
(129, 25, 'Master', 1, 0, 0, 0, '2021-08-07 07:02:07', '2021-08-07 02:02:07'),
(130, 25, 'Daily_Entry', 1, 0, 0, 0, '2021-08-07 07:02:07', '2021-08-07 02:02:07'),
(131, 25, 'Uploads', 1, 0, 0, 0, '2021-08-07 07:02:07', '2021-08-07 02:02:07'),
(132, 25, 'HRMs', 1, 0, 0, 0, '2021-08-07 07:02:07', '2021-08-07 02:02:07'),
(133, 25, 'Account', 1, 0, 0, 0, '2021-08-07 07:02:07', '2021-08-07 02:02:07'),
(134, 25, 'Roles_&_Permission', 1, 0, 0, 0, '2021-08-07 07:02:07', '2021-08-07 02:02:07'),
(135, 25, 'Settings', 1, 0, 0, 0, '2021-08-07 07:02:07', '2021-08-07 02:02:07'),
(136, 25, 'Company/Branch', 1, 0, 0, 0, '2021-08-07 07:02:07', '2021-08-07 02:02:07'),
(137, 25, 'Agent/Telecaller', 1, 0, 0, 0, '2021-08-07 07:02:07', '2021-08-07 02:02:07'),
(138, 25, 'Bank', 1, 0, 0, 0, '2021-08-07 07:02:07', '2021-08-07 02:02:07'),
(139, 25, 'Backet', 1, 0, 0, 0, '2021-08-07 07:02:07', '2021-08-07 02:02:07'),
(140, 25, 'Recollection_Roles', 1, 0, 0, 0, '2021-08-07 07:02:07', '2021-08-07 02:02:07'),
(141, 25, 'Employee', 1, 0, 0, 0, '2021-08-07 07:02:07', '2021-08-07 02:02:07'),
(142, 25, 'Cash_Collection', 1, 0, 0, 0, '2021-08-07 07:02:07', '2021-08-07 02:02:07'),
(143, 25, 'Backup_Database', 1, 0, 0, 0, '2021-08-07 07:02:07', '2021-08-07 02:02:07'),
(144, 25, 'Agent/Telecaller_Master', 1, 0, 0, 0, '2021-08-07 07:02:07', '2021-08-07 02:02:07'),
(145, 25, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-08-07 07:02:07', '2021-08-07 02:02:07'),
(146, 25, 'Company_Master_Upload', 1, 0, 0, 0, '2021-08-07 07:02:07', '2021-08-07 02:02:07'),
(147, 25, 'Manage_Salary', 1, 0, 0, 0, '2021-08-07 07:02:07', '2021-08-07 02:02:07'),
(148, 25, 'Make_Payment', 1, 0, 0, 0, '2021-08-07 07:02:07', '2021-08-07 02:02:07'),
(149, 25, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-08-07 07:02:07', '2021-08-07 02:02:07'),
(150, 25, 'Generate_Invoice', 1, 0, 0, 0, '2021-08-07 07:02:07', '2021-08-07 02:02:07'),
(151, 25, 'Payment_Received', 1, 1, 0, 0, '2021-08-07 07:02:07', '2021-08-07 02:02:07'),
(152, 26, 'Dashboard', 1, 1, 1, 1, '2021-04-05 06:30:05', '2021-04-05 01:30:05'),
(153, 26, 'Master', 1, 1, 1, 1, '2021-04-05 06:30:05', '2021-04-05 01:30:05'),
(154, 26, 'Daily_Entry', 1, 1, 1, 1, '2021-04-05 06:30:05', '2021-04-05 01:30:05'),
(155, 26, 'Uploads', 1, 1, 1, 1, '2021-04-05 06:30:05', '2021-04-05 01:30:05'),
(156, 26, 'HRMs', 1, 1, 1, 1, '2021-04-05 06:30:05', '2021-04-05 01:30:05'),
(157, 26, 'Account', 1, 1, 1, 1, '2021-04-05 06:30:05', '2021-04-05 01:30:05'),
(158, 26, 'Roles_&_Permission', 1, 1, 1, 1, '2021-04-05 06:30:05', '2021-04-05 01:30:05'),
(159, 26, 'Settings', 1, 1, 1, 1, '2021-04-05 06:30:05', '2021-04-05 01:30:05'),
(160, 26, 'Company/Branch', 1, 1, 1, 1, '2021-04-05 06:30:05', '2021-04-05 01:30:05'),
(161, 26, 'Agent/Telecaller', 1, 1, 1, 1, '2021-04-05 06:30:05', '2021-04-05 01:30:05'),
(162, 26, 'Bank', 1, 1, 1, 1, '2021-04-05 06:30:05', '2021-04-05 01:30:05'),
(163, 26, 'Backet', 1, 1, 1, 1, '2021-04-05 06:30:05', '2021-04-05 01:30:05'),
(164, 26, 'Recollection_Roles', 1, 1, 1, 1, '2021-04-05 06:30:05', '2021-04-05 01:30:05'),
(165, 26, 'Employee', 1, 1, 1, 1, '2021-04-05 06:30:05', '2021-04-05 01:30:05'),
(166, 26, 'Cash_Collection', 1, 1, 1, 1, '2021-04-05 06:30:05', '2021-04-05 01:30:05'),
(167, 26, 'Backup_Database', 1, 1, 1, 1, '2021-04-05 06:30:05', '2021-04-05 01:30:05'),
(168, 26, 'Agent/Telecaller_Master', 1, 1, 1, 1, '2021-04-05 06:30:05', '2021-04-05 01:30:05'),
(169, 26, 'Bank_Master_Upload', 1, 1, 1, 1, '2021-04-05 06:30:05', '2021-04-05 01:30:05'),
(170, 26, 'Company_Master_Upload', 1, 1, 1, 1, '2021-04-05 06:30:05', '2021-04-05 01:30:05'),
(171, 26, 'Manage_Salary', 1, 1, 1, 1, '2021-04-05 06:30:05', '2021-04-05 01:30:05'),
(172, 26, 'Make_Payment', 1, 1, 1, 1, '2021-04-05 06:30:05', '2021-04-05 01:30:05'),
(173, 26, 'Generate_Page_Slip', 1, 1, 1, 1, '2021-04-05 06:30:05', '2021-04-05 01:30:05'),
(174, 26, 'Generate_Invoice', 1, 1, 1, 1, '2021-04-05 06:30:05', '2021-04-05 01:30:05'),
(175, 26, 'Payment_Received', 1, 1, 1, 1, '2021-04-05 06:30:05', '2021-04-05 01:30:05'),
(176, 27, 'Account', 1, 0, 0, 0, '2021-04-05 06:24:12', '2021-04-05 01:24:12'),
(177, 27, 'Company/Branch', 1, 0, 0, 0, '2021-04-05 06:24:12', '2021-04-05 01:24:12'),
(178, 27, 'Agent/Telecaller', 1, 0, 0, 0, '2021-04-05 06:24:12', '2021-04-05 01:24:12'),
(179, 27, 'Bank', 1, 0, 0, 0, '2021-04-05 06:24:12', '2021-04-05 01:24:12'),
(180, 27, 'Backet', 1, 0, 0, 0, '2021-04-05 06:24:12', '2021-04-05 01:24:12'),
(181, 27, 'Recollection_Roles', 1, 0, 0, 0, '2021-04-05 06:24:12', '2021-04-05 01:24:12'),
(182, 27, 'Employee', 1, 0, 0, 0, '2021-04-05 06:24:12', '2021-04-05 01:24:12'),
(183, 27, 'Cash_Collection', 1, 0, 0, 0, '2021-04-05 06:24:12', '2021-04-05 01:24:12'),
(184, 27, 'Backup_Database', 1, 0, 0, 0, '2021-04-05 06:24:12', '2021-04-05 01:24:12'),
(185, 27, 'Agent/Telecaller_Master', 1, 0, 0, 0, '2021-04-05 06:24:12', '2021-04-05 01:24:12'),
(186, 27, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:24:12', '2021-04-05 01:24:12'),
(187, 27, 'Make_Payment', 1, 0, 0, 0, '2021-04-05 06:24:12', '2021-04-05 01:24:12'),
(188, 27, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-04-05 06:24:12', '2021-04-05 01:24:12'),
(189, 27, 'Generate_Invoice', 1, 0, 0, 0, '2021-04-05 06:24:12', '2021-04-05 01:24:12'),
(190, 27, 'Payment_Received', 1, 0, 0, 0, '2021-04-05 06:24:12', '2021-04-05 01:24:12'),
(191, 28, 'Dashboard', 1, 0, 0, 0, '2021-10-20 09:49:21', '2021-10-20 04:49:21'),
(192, 28, 'Master', 1, 0, 0, 0, '2021-10-20 09:49:21', '2021-10-20 04:49:21'),
(193, 28, 'Daily_Entry', 1, 0, 0, 0, '2021-10-20 09:49:21', '2021-10-20 04:49:21'),
(194, 28, 'Uploads', 1, 0, 0, 0, '2021-10-20 09:49:21', '2021-10-20 04:49:21'),
(195, 28, 'HRMs', 1, 0, 0, 0, '2021-10-20 09:49:21', '2021-10-20 04:49:21'),
(196, 28, 'Account', 1, 0, 0, 0, '2021-10-20 09:49:21', '2021-10-20 04:49:21'),
(197, 28, 'Roles_&_Permission', 1, 0, 0, 0, '2021-10-20 09:49:21', '2021-10-20 04:49:21'),
(198, 28, 'Settings', 1, 0, 0, 0, '2021-10-20 09:49:21', '2021-10-20 04:49:21'),
(199, 28, 'Company/Branch', 1, 0, 0, 0, '2021-10-20 09:49:21', '2021-10-20 04:49:21'),
(200, 28, 'Agent/Telecaller', 1, 0, 0, 0, '2021-10-20 09:49:21', '2021-10-20 04:49:21'),
(201, 28, 'Bank', 1, 0, 0, 0, '2021-10-20 09:49:21', '2021-10-20 04:49:21'),
(202, 28, 'Backet', 1, 0, 0, 0, '2021-10-20 09:49:21', '2021-10-20 04:49:21'),
(203, 28, 'Recollection_Roles', 1, 0, 0, 0, '2021-10-20 09:49:21', '2021-10-20 04:49:21'),
(204, 28, 'Employee', 1, 0, 0, 0, '2021-10-20 09:49:21', '2021-10-20 04:49:21'),
(205, 28, 'Cash_Collection', 1, 0, 0, 0, '2021-10-20 09:49:21', '2021-10-20 04:49:21'),
(206, 28, 'Backup_Database', 1, 0, 0, 0, '2021-10-20 09:49:21', '2021-10-20 04:49:21'),
(207, 28, 'Agent/Telecaller_Master', 1, 0, 0, 0, '2021-10-20 09:49:21', '2021-10-20 04:49:21'),
(208, 28, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-10-20 09:49:21', '2021-10-20 04:49:21'),
(209, 28, 'Company_Master_Upload', 1, 0, 0, 0, '2021-10-20 09:49:21', '2021-10-20 04:49:21'),
(210, 28, 'Manage_Salary', 1, 0, 0, 0, '2021-10-20 09:49:21', '2021-10-20 04:49:21'),
(211, 28, 'Make_Payment', 1, 0, 0, 0, '2021-10-20 09:49:21', '2021-10-20 04:49:21'),
(212, 28, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-10-20 09:49:21', '2021-10-20 04:49:21'),
(213, 28, 'Generate_Invoice', 1, 0, 0, 0, '2021-10-20 09:49:21', '2021-10-20 04:49:21'),
(214, 28, 'Payment_Received', 1, 0, 0, 0, '2021-10-20 09:49:21', '2021-10-20 04:49:21'),
(215, 29, 'Dashboard', 1, 0, 0, 0, '2021-04-05 06:23:04', '2021-04-05 06:23:04'),
(216, 29, 'Master', 1, 0, 0, 0, '2021-04-05 06:23:04', '2021-04-05 06:23:04'),
(217, 29, 'Daily_Entry', 1, 0, 0, 0, '2021-04-05 06:23:04', '2021-04-05 06:23:04'),
(218, 29, 'Uploads', 1, 0, 0, 0, '2021-04-05 06:23:04', '2021-04-05 06:23:04'),
(219, 29, 'HRMs', 1, 0, 0, 0, '2021-04-05 06:23:04', '2021-04-05 06:23:04'),
(220, 29, 'Account', 1, 0, 0, 0, '2021-04-05 06:24:41', '2021-04-05 01:24:41'),
(221, 29, 'Roles_&_Permission', 1, 0, 0, 0, '2021-04-05 06:24:41', '2021-04-05 01:24:41'),
(222, 29, 'Settings', 1, 0, 0, 0, '2021-04-05 06:24:41', '2021-04-05 01:24:41'),
(223, 29, 'Company/Branch', 1, 0, 0, 0, '2021-04-05 06:24:41', '2021-04-05 01:24:41'),
(224, 29, 'Agent/Telecaller', 1, 0, 0, 0, '2021-04-05 06:24:41', '2021-04-05 01:24:41'),
(225, 29, 'Bank', 1, 0, 0, 0, '2021-04-05 06:24:41', '2021-04-05 01:24:41'),
(226, 29, 'Backet', 1, 0, 0, 0, '2021-04-05 06:24:41', '2021-04-05 01:24:41'),
(227, 29, 'Recollection_Roles', 1, 0, 0, 0, '2021-04-05 06:24:41', '2021-04-05 01:24:41'),
(228, 29, 'Employee', 1, 0, 0, 0, '2021-04-05 06:24:41', '2021-04-05 01:24:41'),
(229, 29, 'Cash_Collection', 1, 0, 0, 0, '2021-04-05 06:24:41', '2021-04-05 01:24:41'),
(230, 29, 'Backup_Database', 1, 0, 0, 0, '2021-04-05 06:24:41', '2021-04-05 01:24:41'),
(231, 29, 'Agent/Telecaller_Master', 1, 0, 0, 0, '2021-04-05 06:24:41', '2021-04-05 01:24:41'),
(232, 29, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:24:41', '2021-04-05 01:24:41'),
(233, 29, 'Company_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:24:41', '2021-04-05 01:24:41'),
(234, 29, 'Manage_Salary', 1, 0, 0, 0, '2021-04-05 06:24:41', '2021-04-05 01:24:41'),
(235, 29, 'Make_Payment', 1, 0, 0, 0, '2021-04-05 06:24:41', '2021-04-05 01:24:41'),
(236, 29, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-04-05 06:24:41', '2021-04-05 01:24:41'),
(237, 29, 'Generate_Invoice', 1, 0, 0, 0, '2021-04-05 06:24:41', '2021-04-05 01:24:41'),
(238, 29, 'Payment_Received', 1, 0, 0, 0, '2021-04-05 06:24:41', '2021-04-05 01:24:41'),
(239, 30, 'Account', 1, 0, 0, 0, '2021-04-05 06:23:59', '2021-04-05 06:23:59'),
(240, 30, 'Settings', 1, 0, 0, 0, '2021-04-05 06:23:59', '2021-04-05 06:23:59'),
(241, 30, 'Company/Branch', 1, 0, 0, 0, '2021-04-05 06:23:59', '2021-04-05 06:23:59'),
(242, 30, 'Agent/Telecaller', 1, 0, 0, 0, '2021-04-05 06:23:59', '2021-04-05 06:23:59'),
(243, 30, 'Bank', 1, 0, 0, 0, '2021-04-05 06:23:59', '2021-04-05 06:23:59'),
(244, 30, 'Backet', 1, 0, 0, 0, '2021-04-05 06:23:59', '2021-04-05 06:23:59'),
(245, 30, 'Recollection_Roles', 1, 0, 0, 0, '2021-04-05 06:23:59', '2021-04-05 06:23:59'),
(246, 30, 'Employee', 1, 0, 0, 0, '2021-04-05 06:23:59', '2021-04-05 06:23:59'),
(247, 30, 'Cash_Collection', 1, 0, 0, 0, '2021-04-05 06:23:59', '2021-04-05 06:23:59'),
(248, 30, 'Backup_Database', 1, 0, 0, 0, '2021-04-05 06:23:59', '2021-04-05 06:23:59'),
(249, 30, 'Agent/Telecaller_Master', 1, 0, 0, 0, '2021-04-05 06:23:59', '2021-04-05 06:23:59'),
(250, 30, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:23:59', '2021-04-05 06:23:59'),
(251, 30, 'Company_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:23:59', '2021-04-05 06:23:59'),
(252, 30, 'Manage_Salary', 1, 0, 0, 0, '2021-04-05 06:23:59', '2021-04-05 06:23:59'),
(253, 30, 'Make_Payment', 1, 0, 0, 0, '2021-04-05 06:23:59', '2021-04-05 06:23:59'),
(254, 30, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-04-05 06:23:59', '2021-04-05 06:23:59'),
(255, 30, 'Generate_Invoice', 1, 0, 0, 0, '2021-04-05 06:23:59', '2021-04-05 06:23:59'),
(256, 30, 'Payment_Received', 1, 0, 0, 0, '2021-04-05 06:23:59', '2021-04-05 06:23:59'),
(257, 31, 'Dashboard', 1, 0, 0, 0, '2021-04-05 06:25:35', '2021-04-05 06:25:35'),
(258, 31, 'Master', 1, 0, 0, 0, '2021-04-05 06:25:35', '2021-04-05 06:25:35'),
(259, 31, 'Daily_Entry', 1, 0, 0, 0, '2021-04-05 06:25:35', '2021-04-05 06:25:35'),
(260, 31, 'Uploads', 1, 0, 0, 0, '2021-04-05 06:25:35', '2021-04-05 06:25:35'),
(261, 31, 'HRMs', 1, 0, 0, 0, '2021-04-05 06:25:35', '2021-04-05 06:25:35'),
(262, 31, 'Account', 1, 0, 0, 0, '2021-04-05 06:25:35', '2021-04-05 06:25:35'),
(263, 31, 'Roles_&_Permission', 1, 0, 0, 0, '2021-04-05 06:25:35', '2021-04-05 06:25:35'),
(264, 31, 'Settings', 1, 0, 0, 0, '2021-04-05 06:25:35', '2021-04-05 06:25:35'),
(265, 31, 'Company/Branch', 1, 0, 0, 0, '2021-04-05 06:25:35', '2021-04-05 06:25:35'),
(266, 31, 'Agent/Telecaller', 1, 0, 0, 0, '2021-04-05 06:25:35', '2021-04-05 06:25:35'),
(267, 31, 'Bank', 1, 0, 0, 0, '2021-04-05 06:25:35', '2021-04-05 06:25:35'),
(268, 31, 'Backet', 1, 0, 0, 0, '2021-04-05 06:25:35', '2021-04-05 06:25:35'),
(269, 31, 'Recollection_Roles', 1, 0, 0, 0, '2021-04-05 06:25:35', '2021-04-05 06:25:35'),
(270, 31, 'Employee', 1, 0, 0, 0, '2021-04-05 06:25:35', '2021-04-05 06:25:35'),
(271, 31, 'Cash_Collection', 1, 0, 0, 0, '2021-04-05 06:25:35', '2021-04-05 06:25:35'),
(272, 31, 'Backup_Database', 1, 0, 0, 0, '2021-04-05 06:25:35', '2021-04-05 06:25:35'),
(273, 31, 'Agent/Telecaller_Master', 1, 0, 0, 0, '2021-04-05 06:25:35', '2021-04-05 06:25:35'),
(274, 31, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:25:35', '2021-04-05 06:25:35'),
(275, 31, 'Company_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:25:35', '2021-04-05 06:25:35'),
(276, 31, 'Manage_Salary', 1, 0, 0, 0, '2021-04-05 06:25:35', '2021-04-05 06:25:35'),
(277, 31, 'Make_Payment', 1, 0, 0, 0, '2021-04-05 06:25:35', '2021-04-05 06:25:35'),
(278, 31, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-04-05 06:25:35', '2021-04-05 06:25:35'),
(279, 31, 'Generate_Invoice', 1, 0, 0, 0, '2021-04-05 06:25:35', '2021-04-05 06:25:35'),
(280, 31, 'Payment_Received', 1, 0, 0, 0, '2021-04-05 06:25:35', '2021-04-05 06:25:35'),
(281, 32, 'Dashboard', 1, 0, 0, 0, '2021-04-05 06:25:58', '2021-04-05 06:25:58'),
(282, 32, 'Master', 1, 0, 0, 0, '2021-04-05 06:25:58', '2021-04-05 06:25:58'),
(283, 32, 'Daily_Entry', 1, 0, 0, 0, '2021-04-05 06:25:58', '2021-04-05 06:25:58'),
(284, 32, 'Uploads', 1, 0, 0, 0, '2021-04-05 06:25:58', '2021-04-05 06:25:58'),
(285, 32, 'HRMs', 1, 0, 0, 0, '2021-04-05 06:25:58', '2021-04-05 06:25:58'),
(286, 32, 'Account', 1, 0, 0, 0, '2021-04-05 06:25:58', '2021-04-05 06:25:58'),
(287, 32, 'Roles_&_Permission', 1, 0, 0, 0, '2021-04-05 06:25:58', '2021-04-05 06:25:58'),
(288, 32, 'Settings', 1, 0, 0, 0, '2021-04-05 06:25:58', '2021-04-05 06:25:58'),
(289, 32, 'Company/Branch', 1, 0, 0, 0, '2021-04-05 06:25:58', '2021-04-05 06:25:58'),
(290, 32, 'Agent/Telecaller', 1, 0, 0, 0, '2021-04-05 06:25:58', '2021-04-05 06:25:58'),
(291, 32, 'Bank', 1, 0, 0, 0, '2021-04-05 06:25:58', '2021-04-05 06:25:58'),
(292, 32, 'Backet', 1, 0, 0, 0, '2021-04-05 06:25:58', '2021-04-05 06:25:58'),
(293, 32, 'Recollection_Roles', 1, 0, 0, 0, '2021-04-05 06:25:58', '2021-04-05 06:25:58'),
(294, 32, 'Employee', 1, 0, 0, 0, '2021-04-05 06:25:58', '2021-04-05 06:25:58'),
(295, 32, 'Cash_Collection', 1, 0, 0, 0, '2021-04-05 06:25:58', '2021-04-05 06:25:58'),
(296, 32, 'Backup_Database', 1, 0, 0, 0, '2021-04-05 06:25:58', '2021-04-05 06:25:58'),
(297, 32, 'Agent/Telecaller_Master', 1, 0, 0, 0, '2021-04-05 06:25:58', '2021-04-05 06:25:58'),
(298, 32, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:25:58', '2021-04-05 06:25:58'),
(299, 32, 'Company_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:25:58', '2021-04-05 06:25:58'),
(300, 32, 'Manage_Salary', 1, 0, 0, 0, '2021-04-05 06:25:58', '2021-04-05 06:25:58'),
(301, 32, 'Make_Payment', 1, 0, 0, 0, '2021-04-05 06:25:58', '2021-04-05 06:25:58'),
(302, 32, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-04-05 06:25:58', '2021-04-05 06:25:58'),
(303, 32, 'Generate_Invoice', 1, 0, 0, 0, '2021-04-05 06:25:58', '2021-04-05 06:25:58'),
(304, 32, 'Payment_Received', 1, 0, 0, 0, '2021-04-05 06:25:58', '2021-04-05 06:25:58'),
(305, 33, 'Dashboard', 1, 0, 0, 0, '2021-04-05 06:28:30', '2021-04-05 06:28:30'),
(306, 33, 'Master', 1, 0, 0, 0, '2021-04-05 06:28:30', '2021-04-05 06:28:30'),
(307, 33, 'Daily_Entry', 1, 0, 0, 0, '2021-04-05 06:28:30', '2021-04-05 06:28:30'),
(308, 33, 'Uploads', 1, 0, 0, 0, '2021-04-05 06:28:30', '2021-04-05 06:28:30'),
(309, 33, 'HRMs', 1, 0, 0, 0, '2021-04-05 06:28:30', '2021-04-05 06:28:30'),
(310, 33, 'Account', 1, 0, 0, 0, '2021-04-05 06:28:30', '2021-04-05 06:28:30'),
(311, 33, 'Roles_&_Permission', 1, 0, 0, 0, '2021-04-05 06:28:30', '2021-04-05 06:28:30'),
(312, 33, 'Settings', 1, 0, 0, 0, '2021-04-05 06:28:30', '2021-04-05 06:28:30'),
(313, 33, 'Company/Branch', 1, 0, 0, 0, '2021-04-05 06:28:30', '2021-04-05 06:28:30'),
(314, 33, 'Agent/Telecaller', 1, 0, 0, 0, '2021-04-05 06:28:30', '2021-04-05 06:28:30'),
(315, 33, 'Bank', 1, 0, 0, 0, '2021-04-05 06:28:30', '2021-04-05 06:28:30'),
(316, 33, 'Backet', 1, 0, 0, 0, '2021-04-05 06:28:30', '2021-04-05 06:28:30'),
(317, 33, 'Recollection_Roles', 1, 0, 0, 0, '2021-04-05 06:28:30', '2021-04-05 06:28:30'),
(318, 33, 'Employee', 1, 0, 0, 0, '2021-04-05 06:28:30', '2021-04-05 06:28:30'),
(319, 33, 'Cash_Collection', 1, 0, 0, 0, '2021-04-05 06:28:30', '2021-04-05 06:28:30'),
(320, 33, 'Backup_Database', 1, 0, 0, 0, '2021-04-05 06:28:30', '2021-04-05 06:28:30'),
(321, 33, 'Agent/Telecaller_Master', 1, 0, 0, 0, '2021-04-05 06:28:30', '2021-04-05 06:28:30'),
(322, 33, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:28:30', '2021-04-05 06:28:30'),
(323, 33, 'Company_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:28:30', '2021-04-05 06:28:30'),
(324, 33, 'Manage_Salary', 1, 0, 0, 0, '2021-04-05 06:28:30', '2021-04-05 06:28:30'),
(325, 33, 'Make_Payment', 1, 0, 0, 0, '2021-04-05 06:28:30', '2021-04-05 06:28:30'),
(326, 33, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-04-05 06:28:30', '2021-04-05 06:28:30'),
(327, 33, 'Generate_Invoice', 1, 0, 0, 0, '2021-04-05 06:28:30', '2021-04-05 06:28:30'),
(328, 33, 'Payment_Received', 1, 0, 0, 0, '2021-04-05 06:28:30', '2021-04-05 06:28:30'),
(329, 34, 'Dashboard', 1, 0, 0, 0, '2021-04-05 06:28:51', '2021-04-05 06:28:51'),
(330, 34, 'Master', 1, 0, 0, 0, '2021-04-05 06:28:51', '2021-04-05 06:28:51'),
(331, 34, 'Daily_Entry', 1, 0, 0, 0, '2021-04-05 06:28:51', '2021-04-05 06:28:51'),
(332, 34, 'Uploads', 1, 0, 0, 0, '2021-04-05 06:28:51', '2021-04-05 06:28:51'),
(333, 34, 'HRMs', 1, 0, 0, 0, '2021-04-05 06:28:51', '2021-04-05 06:28:51'),
(334, 34, 'Account', 1, 0, 0, 0, '2021-04-05 06:28:51', '2021-04-05 06:28:51'),
(335, 34, 'Roles_&_Permission', 1, 0, 0, 0, '2021-04-05 06:28:51', '2021-04-05 06:28:51'),
(336, 34, 'Settings', 1, 0, 0, 0, '2021-04-05 06:28:51', '2021-04-05 06:28:51'),
(337, 34, 'Company/Branch', 1, 0, 0, 0, '2021-04-05 06:28:51', '2021-04-05 06:28:51'),
(338, 34, 'Agent/Telecaller', 1, 0, 0, 0, '2021-04-05 06:28:51', '2021-04-05 06:28:51'),
(339, 34, 'Bank', 1, 0, 0, 0, '2021-04-05 06:28:51', '2021-04-05 06:28:51'),
(340, 34, 'Backet', 1, 0, 0, 0, '2021-04-05 06:28:51', '2021-04-05 06:28:51'),
(341, 34, 'Recollection_Roles', 1, 0, 0, 0, '2021-04-05 06:28:51', '2021-04-05 06:28:51'),
(342, 34, 'Employee', 1, 0, 0, 0, '2021-04-05 06:28:51', '2021-04-05 06:28:51'),
(343, 34, 'Cash_Collection', 1, 0, 0, 0, '2021-04-05 06:28:51', '2021-04-05 06:28:51'),
(344, 34, 'Backup_Database', 1, 0, 0, 0, '2021-04-05 06:28:51', '2021-04-05 06:28:51'),
(345, 34, 'Agent/Telecaller_Master', 1, 0, 0, 0, '2021-04-05 06:28:51', '2021-04-05 06:28:51'),
(346, 34, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:28:51', '2021-04-05 06:28:51'),
(347, 34, 'Company_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:28:51', '2021-04-05 06:28:51'),
(348, 34, 'Manage_Salary', 1, 0, 0, 0, '2021-04-05 06:28:51', '2021-04-05 06:28:51'),
(349, 34, 'Make_Payment', 1, 0, 0, 0, '2021-04-05 06:28:51', '2021-04-05 06:28:51'),
(350, 34, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-04-05 06:28:51', '2021-04-05 06:28:51'),
(351, 34, 'Generate_Invoice', 1, 0, 0, 0, '2021-04-05 06:28:51', '2021-04-05 06:28:51'),
(352, 34, 'Payment_Received', 1, 0, 0, 0, '2021-04-05 06:28:51', '2021-04-05 06:28:51'),
(353, 35, 'Dashboard', 1, 0, 0, 0, '2021-04-05 06:29:28', '2021-04-05 06:29:28'),
(354, 35, 'Master', 1, 0, 0, 0, '2021-04-05 06:29:28', '2021-04-05 06:29:28'),
(355, 35, 'Daily_Entry', 1, 0, 0, 0, '2021-04-05 06:29:28', '2021-04-05 06:29:28'),
(356, 35, 'Uploads', 1, 0, 0, 0, '2021-04-05 06:29:28', '2021-04-05 06:29:28'),
(357, 35, 'HRMs', 1, 0, 0, 0, '2021-04-05 06:29:28', '2021-04-05 06:29:28'),
(358, 35, 'Account', 1, 0, 0, 0, '2021-04-05 06:29:28', '2021-04-05 06:29:28'),
(359, 35, 'Roles_&_Permission', 1, 0, 0, 0, '2021-04-05 06:29:28', '2021-04-05 06:29:28'),
(360, 35, 'Settings', 1, 0, 0, 0, '2021-04-05 06:29:28', '2021-04-05 06:29:28'),
(361, 35, 'Company/Branch', 1, 0, 0, 0, '2021-04-05 06:29:28', '2021-04-05 06:29:28'),
(362, 35, 'Agent/Telecaller', 1, 0, 0, 0, '2021-04-05 06:29:28', '2021-04-05 06:29:28'),
(363, 35, 'Bank', 1, 0, 0, 0, '2021-04-05 06:29:28', '2021-04-05 06:29:28'),
(364, 35, 'Backet', 1, 0, 0, 0, '2021-04-05 06:29:28', '2021-04-05 06:29:28'),
(365, 35, 'Recollection_Roles', 1, 0, 0, 0, '2021-04-05 06:29:28', '2021-04-05 06:29:28'),
(366, 35, 'Employee', 1, 0, 0, 0, '2021-04-05 06:29:28', '2021-04-05 06:29:28'),
(367, 35, 'Cash_Collection', 1, 0, 0, 0, '2021-04-05 06:29:28', '2021-04-05 06:29:28'),
(368, 35, 'Backup_Database', 1, 0, 0, 0, '2021-04-05 06:29:28', '2021-04-05 06:29:28'),
(369, 35, 'Agent/Telecaller_Master', 1, 0, 0, 0, '2021-04-05 06:29:28', '2021-04-05 06:29:28'),
(370, 35, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:29:28', '2021-04-05 06:29:28'),
(371, 35, 'Company_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:29:28', '2021-04-05 06:29:28'),
(372, 35, 'Manage_Salary', 1, 0, 0, 0, '2021-04-05 06:29:28', '2021-04-05 06:29:28'),
(373, 35, 'Make_Payment', 1, 0, 0, 0, '2021-04-05 06:29:28', '2021-04-05 06:29:28'),
(374, 35, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-04-05 06:29:28', '2021-04-05 06:29:28'),
(375, 35, 'Generate_Invoice', 1, 0, 0, 0, '2021-04-05 06:29:28', '2021-04-05 06:29:28'),
(376, 35, 'Payment_Received', 1, 0, 0, 0, '2021-04-05 06:29:28', '2021-04-05 06:29:28'),
(377, 36, 'Dashboard', 1, 0, 0, 0, '2021-10-20 09:55:40', '2021-10-20 04:55:40'),
(378, 36, 'Master', 1, 0, 0, 0, '2021-10-20 09:55:40', '2021-10-20 04:55:40'),
(379, 36, 'Daily_Entry', 1, 0, 0, 0, '2021-10-20 09:55:40', '2021-10-20 04:55:40'),
(380, 36, 'Uploads', 1, 0, 0, 0, '2021-10-20 09:55:40', '2021-10-20 04:55:40'),
(381, 36, 'HRMs', 1, 0, 0, 0, '2021-10-20 09:55:40', '2021-10-20 04:55:40'),
(382, 36, 'Account', 1, 0, 0, 0, '2021-10-20 09:55:40', '2021-10-20 04:55:40'),
(383, 36, 'Roles_&_Permission', 1, 0, 0, 0, '2021-10-20 09:55:40', '2021-10-20 04:55:40'),
(384, 36, 'Settings', 1, 0, 0, 0, '2021-10-20 09:55:40', '2021-10-20 04:55:40'),
(385, 36, 'Company/Branch', 1, 0, 0, 0, '2021-10-20 09:55:40', '2021-10-20 04:55:40'),
(386, 36, 'Agent/Telecaller', 1, 0, 0, 0, '2021-10-20 09:55:40', '2021-10-20 04:55:40'),
(387, 36, 'Bank', 1, 0, 0, 0, '2021-10-20 09:55:40', '2021-10-20 04:55:40'),
(388, 36, 'Backet', 1, 0, 0, 0, '2021-10-20 09:55:40', '2021-10-20 04:55:40'),
(389, 36, 'Recollection_Roles', 1, 0, 0, 0, '2021-10-20 09:55:40', '2021-10-20 04:55:40'),
(390, 36, 'Employee', 1, 0, 0, 0, '2021-10-20 09:55:40', '2021-10-20 04:55:40'),
(391, 36, 'Cash_Collection', 1, 0, 0, 0, '2021-10-20 09:55:40', '2021-10-20 04:55:40'),
(392, 36, 'Backup_Database', 1, 0, 0, 0, '2021-10-20 09:55:40', '2021-10-20 04:55:40'),
(393, 36, 'Agent/Telecaller_Master', 1, 0, 0, 0, '2021-10-20 09:55:40', '2021-10-20 04:55:40'),
(394, 36, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-10-20 09:55:40', '2021-10-20 04:55:40'),
(395, 36, 'Company_Master_Upload', 1, 0, 0, 0, '2021-10-20 09:55:40', '2021-10-20 04:55:40'),
(396, 36, 'Manage_Salary', 1, 0, 0, 0, '2021-10-20 09:55:40', '2021-10-20 04:55:40'),
(397, 36, 'Make_Payment', 1, 0, 0, 0, '2021-10-20 09:55:40', '2021-10-20 04:55:40'),
(398, 36, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-10-20 09:55:40', '2021-10-20 04:55:40'),
(399, 36, 'Generate_Invoice', 1, 0, 0, 0, '2021-10-20 09:55:40', '2021-10-20 04:55:40'),
(400, 36, 'Payment_Received', 1, 0, 0, 0, '2021-10-20 09:55:40', '2021-10-20 04:55:40'),
(401, 37, 'Dashboard', 1, 0, 0, 0, '2021-04-05 06:30:25', '2021-04-05 06:30:25'),
(402, 37, 'Master', 1, 0, 0, 0, '2021-04-05 06:30:25', '2021-04-05 06:30:25'),
(403, 37, 'Daily_Entry', 1, 0, 0, 0, '2021-04-05 06:30:25', '2021-04-05 06:30:25'),
(404, 37, 'Uploads', 1, 0, 0, 0, '2021-04-05 06:30:25', '2021-04-05 06:30:25'),
(405, 37, 'HRMs', 1, 0, 0, 0, '2021-04-05 06:30:25', '2021-04-05 06:30:25'),
(406, 37, 'Account', 1, 0, 0, 0, '2021-04-05 06:30:25', '2021-04-05 06:30:25'),
(407, 37, 'Roles_&_Permission', 1, 0, 0, 0, '2021-04-05 06:30:25', '2021-04-05 06:30:25'),
(408, 37, 'Settings', 1, 0, 0, 0, '2021-04-05 06:30:25', '2021-04-05 06:30:25'),
(409, 37, 'Company/Branch', 1, 0, 0, 0, '2021-04-05 06:30:25', '2021-04-05 06:30:25'),
(410, 37, 'Agent/Telecaller', 1, 0, 0, 0, '2021-04-05 06:30:25', '2021-04-05 06:30:25'),
(411, 37, 'Bank', 1, 0, 0, 0, '2021-04-05 06:30:25', '2021-04-05 06:30:25'),
(412, 37, 'Backet', 1, 0, 0, 0, '2021-04-05 06:30:25', '2021-04-05 06:30:25'),
(413, 37, 'Recollection_Roles', 1, 0, 0, 0, '2021-04-05 06:30:25', '2021-04-05 06:30:25'),
(414, 37, 'Employee', 1, 0, 0, 0, '2021-04-05 06:30:25', '2021-04-05 06:30:25'),
(415, 37, 'Cash_Collection', 1, 0, 0, 0, '2021-04-05 06:30:25', '2021-04-05 06:30:25'),
(416, 37, 'Backup_Database', 1, 0, 0, 0, '2021-04-05 06:30:25', '2021-04-05 06:30:25'),
(417, 37, 'Agent/Telecaller_Master', 1, 0, 0, 0, '2021-04-05 06:30:25', '2021-04-05 06:30:25'),
(418, 37, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:30:25', '2021-04-05 06:30:25'),
(419, 37, 'Company_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:30:25', '2021-04-05 06:30:25'),
(420, 37, 'Manage_Salary', 1, 0, 0, 0, '2021-04-05 06:30:25', '2021-04-05 06:30:25'),
(421, 37, 'Make_Payment', 1, 0, 0, 0, '2021-04-05 06:30:25', '2021-04-05 06:30:25'),
(422, 37, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-04-05 06:30:25', '2021-04-05 06:30:25'),
(423, 37, 'Generate_Invoice', 1, 0, 0, 0, '2021-04-05 06:30:25', '2021-04-05 06:30:25'),
(424, 37, 'Payment_Received', 1, 0, 0, 0, '2021-04-05 06:30:25', '2021-04-05 06:30:25'),
(425, 38, 'Dashboard', 1, 0, 0, 0, '2021-04-05 06:33:45', '2021-04-05 06:33:45'),
(426, 38, 'Master', 1, 0, 0, 0, '2021-04-05 06:33:45', '2021-04-05 06:33:45'),
(427, 38, 'Daily_Entry', 1, 0, 0, 0, '2021-04-05 06:33:45', '2021-04-05 06:33:45'),
(428, 38, 'Uploads', 1, 0, 0, 0, '2021-04-05 06:33:45', '2021-04-05 06:33:45'),
(429, 38, 'HRMs', 1, 0, 0, 0, '2021-04-05 06:33:45', '2021-04-05 06:33:45'),
(430, 38, 'Account', 1, 0, 0, 0, '2021-04-05 06:33:45', '2021-04-05 06:33:45'),
(431, 38, 'Roles_&_Permission', 1, 0, 0, 0, '2021-04-05 06:33:45', '2021-04-05 06:33:45'),
(432, 38, 'Settings', 1, 0, 0, 0, '2021-04-05 06:33:45', '2021-04-05 06:33:45'),
(433, 38, 'Company/Branch', 1, 0, 0, 0, '2021-04-05 06:33:45', '2021-04-05 06:33:45'),
(434, 38, 'Agent/Telecaller', 1, 0, 0, 0, '2021-04-05 06:33:45', '2021-04-05 06:33:45'),
(435, 38, 'Bank', 1, 0, 0, 0, '2021-04-05 06:33:45', '2021-04-05 06:33:45'),
(436, 38, 'Backet', 1, 0, 0, 0, '2021-04-05 06:33:45', '2021-04-05 06:33:45'),
(437, 38, 'Recollection_Roles', 1, 0, 0, 0, '2021-04-05 06:33:45', '2021-04-05 06:33:45'),
(438, 38, 'Employee', 1, 0, 0, 0, '2021-04-05 06:33:45', '2021-04-05 06:33:45'),
(439, 38, 'Cash_Collection', 1, 0, 0, 0, '2021-04-05 06:33:45', '2021-04-05 06:33:45'),
(440, 38, 'Backup_Database', 1, 0, 0, 0, '2021-04-05 06:33:45', '2021-04-05 06:33:45'),
(441, 38, 'Agent/Telecaller_Master', 1, 0, 0, 0, '2021-04-05 06:33:45', '2021-04-05 06:33:45'),
(442, 38, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:33:45', '2021-04-05 06:33:45'),
(443, 38, 'Company_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:33:45', '2021-04-05 06:33:45'),
(444, 38, 'Manage_Salary', 1, 0, 0, 0, '2021-04-05 06:33:45', '2021-04-05 06:33:45'),
(445, 38, 'Make_Payment', 1, 0, 0, 0, '2021-04-05 06:33:45', '2021-04-05 06:33:45'),
(446, 38, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-04-05 06:33:45', '2021-04-05 06:33:45'),
(447, 38, 'Generate_Invoice', 1, 0, 0, 0, '2021-04-05 06:33:45', '2021-04-05 06:33:45'),
(448, 38, 'Payment_Received', 1, 0, 0, 0, '2021-04-05 06:33:45', '2021-04-05 06:33:45'),
(449, 39, 'Dashboard', 1, 0, 0, 0, '2021-04-05 06:34:49', '2021-04-05 06:34:49'),
(450, 39, 'Master', 1, 0, 0, 0, '2021-04-05 06:34:49', '2021-04-05 06:34:49'),
(451, 39, 'Daily_Entry', 1, 0, 0, 0, '2021-04-05 06:34:49', '2021-04-05 06:34:49'),
(452, 39, 'Uploads', 1, 0, 0, 0, '2021-04-05 06:34:49', '2021-04-05 06:34:49'),
(453, 39, 'HRMs', 1, 0, 0, 0, '2021-04-05 06:34:49', '2021-04-05 06:34:49'),
(454, 39, 'Account', 1, 0, 0, 0, '2021-04-05 06:34:49', '2021-04-05 06:34:49'),
(455, 39, 'Roles_&_Permission', 1, 0, 0, 0, '2021-04-05 06:34:49', '2021-04-05 06:34:49'),
(456, 39, 'Settings', 1, 0, 0, 0, '2021-04-05 06:34:49', '2021-04-05 06:34:49'),
(457, 39, 'Company/Branch', 1, 0, 0, 0, '2021-04-05 06:34:49', '2021-04-05 06:34:49'),
(458, 39, 'Agent/Telecaller', 1, 0, 0, 0, '2021-04-05 06:34:49', '2021-04-05 06:34:49'),
(459, 39, 'Bank', 1, 0, 0, 0, '2021-04-05 06:34:49', '2021-04-05 06:34:49'),
(460, 39, 'Backet', 1, 0, 0, 0, '2021-04-05 06:34:49', '2021-04-05 06:34:49'),
(461, 39, 'Recollection_Roles', 1, 0, 0, 0, '2021-04-05 06:34:49', '2021-04-05 06:34:49'),
(462, 39, 'Employee', 1, 0, 0, 0, '2021-04-05 06:34:49', '2021-04-05 06:34:49'),
(463, 39, 'Cash_Collection', 1, 0, 0, 0, '2021-04-05 06:34:49', '2021-04-05 06:34:49'),
(464, 39, 'Backup_Database', 1, 0, 0, 0, '2021-04-05 06:34:49', '2021-04-05 06:34:49'),
(465, 39, 'Agent/Telecaller_Master', 1, 0, 0, 0, '2021-04-05 06:34:49', '2021-04-05 06:34:49'),
(466, 39, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:34:49', '2021-04-05 06:34:49'),
(467, 39, 'Company_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:34:49', '2021-04-05 06:34:49'),
(468, 39, 'Manage_Salary', 1, 0, 0, 0, '2021-04-05 06:34:49', '2021-04-05 06:34:49'),
(469, 39, 'Make_Payment', 1, 0, 0, 0, '2021-04-05 06:34:49', '2021-04-05 06:34:49'),
(470, 39, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-04-05 06:34:49', '2021-04-05 06:34:49'),
(471, 39, 'Generate_Invoice', 1, 0, 0, 0, '2021-04-05 06:34:49', '2021-04-05 06:34:49'),
(472, 39, 'Payment_Received', 1, 0, 0, 0, '2021-04-05 06:34:49', '2021-04-05 06:34:49'),
(473, 40, 'Dashboard', 1, 1, 1, 1, '2021-04-19 11:42:13', '2021-04-19 06:42:13'),
(474, 40, 'Master', 1, 1, 1, 1, '2021-04-19 11:42:13', '2021-04-19 06:42:13'),
(475, 40, 'Daily_Entry', 1, 1, 0, 1, '2021-04-19 11:42:13', '2021-04-19 06:42:13'),
(476, 40, 'Uploads', 1, 1, 1, 1, '2021-04-19 11:42:13', '2021-04-19 06:42:13'),
(477, 40, 'HRMs', 1, 1, 0, 1, '2021-04-19 11:42:13', '2021-04-19 06:42:13'),
(478, 40, 'Account', 1, 1, 1, 0, '2021-04-19 11:42:13', '2021-04-19 06:42:13'),
(479, 40, 'Roles_&_Permission', 1, 0, 0, 0, '2021-04-19 11:42:13', '2021-04-19 06:42:13'),
(480, 40, 'Settings', 1, 0, 0, 0, '2021-04-19 11:42:13', '2021-04-19 06:42:13'),
(481, 40, 'Company/Branch', 1, 0, 0, 0, '2021-04-19 11:42:13', '2021-04-19 06:42:13'),
(482, 40, 'Agent/Telecaller', 1, 0, 0, 0, '2021-04-19 11:42:13', '2021-04-19 06:42:13'),
(483, 40, 'Bank', 1, 0, 0, 0, '2021-04-19 11:42:13', '2021-04-19 06:42:13'),
(484, 40, 'Backet', 1, 0, 0, 0, '2021-04-19 11:42:13', '2021-04-19 06:42:13'),
(485, 40, 'Recollection_Roles', 1, 0, 0, 0, '2021-04-19 11:42:13', '2021-04-19 06:42:13'),
(486, 40, 'Employee', 1, 0, 0, 0, '2021-04-19 11:42:13', '2021-04-19 06:42:13'),
(487, 40, 'Cash_Collection', 1, 0, 0, 0, '2021-04-19 11:42:13', '2021-04-19 06:42:13'),
(488, 40, 'Backup_Database', 1, 0, 0, 0, '2021-04-19 11:42:13', '2021-04-19 06:42:13'),
(489, 40, 'Agent/Telecaller_Master', 1, 0, 0, 0, '2021-04-19 11:42:13', '2021-04-19 06:42:13'),
(490, 40, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-04-19 11:42:13', '2021-04-19 06:42:13'),
(491, 40, 'Company_Master_Upload', 1, 0, 0, 0, '2021-04-19 11:42:13', '2021-04-19 06:42:13'),
(492, 40, 'Manage_Salary', 1, 0, 0, 0, '2021-04-19 11:42:13', '2021-04-19 06:42:13'),
(493, 40, 'Make_Payment', 1, 0, 0, 0, '2021-04-19 11:42:13', '2021-04-19 06:42:13'),
(494, 40, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-04-19 11:42:13', '2021-04-19 06:42:13'),
(495, 40, 'Generate_Invoice', 1, 0, 0, 0, '2021-04-19 11:42:13', '2021-04-19 06:42:13'),
(496, 40, 'Payment_Received', 1, 0, 0, 0, '2021-04-19 11:42:13', '2021-04-19 06:42:13'),
(497, 41, 'Dashboard', 1, 0, 0, 0, '2021-04-05 06:36:51', '2021-04-05 06:36:51'),
(498, 41, 'Master', 1, 0, 0, 0, '2021-04-05 06:36:51', '2021-04-05 06:36:51'),
(499, 41, 'Daily_Entry', 1, 0, 0, 0, '2021-04-05 06:36:51', '2021-04-05 06:36:51'),
(500, 41, 'Uploads', 1, 0, 0, 0, '2021-04-05 06:36:51', '2021-04-05 06:36:51'),
(501, 41, 'HRMs', 1, 0, 0, 0, '2021-04-05 06:36:51', '2021-04-05 06:36:51'),
(502, 41, 'Account', 1, 0, 0, 0, '2021-04-05 06:36:51', '2021-04-05 06:36:51'),
(503, 41, 'Roles_&_Permission', 1, 0, 0, 0, '2021-04-05 06:36:51', '2021-04-05 06:36:51'),
(504, 41, 'Settings', 1, 0, 0, 0, '2021-04-05 06:36:51', '2021-04-05 06:36:51'),
(505, 41, 'Company/Branch', 1, 0, 0, 0, '2021-04-05 06:36:51', '2021-04-05 06:36:51'),
(506, 41, 'Agent/Telecaller', 1, 0, 0, 0, '2021-04-05 06:36:51', '2021-04-05 06:36:51'),
(507, 41, 'Bank', 1, 0, 0, 0, '2021-04-05 06:36:51', '2021-04-05 06:36:51'),
(508, 41, 'Backet', 1, 0, 0, 0, '2021-04-05 06:36:51', '2021-04-05 06:36:51'),
(509, 41, 'Recollection_Roles', 1, 0, 0, 0, '2021-04-05 06:36:51', '2021-04-05 06:36:51'),
(510, 41, 'Employee', 1, 0, 0, 0, '2021-04-05 06:36:51', '2021-04-05 06:36:51'),
(511, 41, 'Cash_Collection', 1, 0, 0, 0, '2021-04-05 06:36:51', '2021-04-05 06:36:51'),
(512, 41, 'Backup_Database', 1, 0, 0, 0, '2021-04-05 06:36:51', '2021-04-05 06:36:51'),
(513, 41, 'Agent/Telecaller_Master', 1, 0, 0, 0, '2021-04-05 06:36:51', '2021-04-05 06:36:51'),
(514, 41, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:36:51', '2021-04-05 06:36:51'),
(515, 41, 'Company_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:36:51', '2021-04-05 06:36:51'),
(516, 41, 'Manage_Salary', 1, 0, 0, 0, '2021-04-05 06:36:51', '2021-04-05 06:36:51'),
(517, 41, 'Make_Payment', 1, 0, 0, 0, '2021-04-05 06:36:51', '2021-04-05 06:36:51'),
(518, 41, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-04-05 06:36:51', '2021-04-05 06:36:51'),
(519, 41, 'Generate_Invoice', 1, 0, 0, 0, '2021-04-05 06:36:51', '2021-04-05 06:36:51'),
(520, 41, 'Payment_Received', 1, 0, 0, 0, '2021-04-05 06:36:51', '2021-04-05 06:36:51'),
(521, 42, 'Dashboard', 1, 0, 0, 0, '2021-04-05 06:37:01', '2021-04-05 06:37:01'),
(522, 42, 'Master', 1, 0, 0, 0, '2021-04-05 06:37:01', '2021-04-05 06:37:01'),
(523, 42, 'Daily_Entry', 1, 0, 0, 0, '2021-04-05 06:37:01', '2021-04-05 06:37:01'),
(524, 42, 'Uploads', 1, 0, 0, 0, '2021-04-05 06:37:01', '2021-04-05 06:37:01'),
(525, 42, 'HRMs', 1, 0, 0, 0, '2021-04-05 06:37:01', '2021-04-05 06:37:01'),
(526, 42, 'Account', 1, 0, 0, 0, '2021-04-05 06:37:01', '2021-04-05 06:37:01'),
(527, 42, 'Roles_&_Permission', 1, 0, 0, 0, '2021-04-05 06:37:01', '2021-04-05 06:37:01'),
(528, 42, 'Settings', 1, 0, 0, 0, '2021-04-05 06:37:01', '2021-04-05 06:37:01'),
(529, 42, 'Company/Branch', 1, 0, 0, 0, '2021-04-05 06:37:01', '2021-04-05 06:37:01'),
(530, 42, 'Agent/Telecaller', 1, 0, 0, 0, '2021-04-05 06:37:01', '2021-04-05 06:37:01'),
(531, 42, 'Bank', 1, 0, 0, 0, '2021-04-05 06:37:01', '2021-04-05 06:37:01'),
(532, 42, 'Backet', 1, 0, 0, 0, '2021-04-05 06:37:01', '2021-04-05 06:37:01'),
(533, 42, 'Recollection_Roles', 1, 0, 0, 0, '2021-04-05 06:37:01', '2021-04-05 06:37:01'),
(534, 42, 'Employee', 1, 0, 0, 0, '2021-04-05 06:37:01', '2021-04-05 06:37:01'),
(535, 42, 'Cash_Collection', 1, 0, 0, 0, '2021-04-05 06:37:01', '2021-04-05 06:37:01'),
(536, 42, 'Backup_Database', 1, 0, 0, 0, '2021-04-05 06:37:01', '2021-04-05 06:37:01'),
(537, 42, 'Agent/Telecaller_Master', 1, 0, 0, 0, '2021-04-05 06:37:01', '2021-04-05 06:37:01'),
(538, 42, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:37:01', '2021-04-05 06:37:01'),
(539, 42, 'Company_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:37:01', '2021-04-05 06:37:01'),
(540, 42, 'Manage_Salary', 1, 0, 0, 0, '2021-04-05 06:37:01', '2021-04-05 06:37:01'),
(541, 42, 'Make_Payment', 1, 0, 0, 0, '2021-04-05 06:37:01', '2021-04-05 06:37:01'),
(542, 42, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-04-05 06:37:01', '2021-04-05 06:37:01'),
(543, 42, 'Generate_Invoice', 1, 0, 0, 0, '2021-04-05 06:37:01', '2021-04-05 06:37:01'),
(544, 42, 'Payment_Received', 1, 0, 0, 0, '2021-04-05 06:37:01', '2021-04-05 06:37:01'),
(545, 43, 'Dashboard', 1, 0, 0, 0, '2021-04-05 06:37:57', '2021-04-05 06:37:57'),
(546, 43, 'Master', 1, 0, 0, 0, '2021-04-05 06:37:57', '2021-04-05 06:37:57'),
(547, 43, 'Daily_Entry', 1, 0, 0, 0, '2021-04-05 06:37:57', '2021-04-05 06:37:57'),
(548, 43, 'Uploads', 1, 0, 0, 0, '2021-04-05 06:37:57', '2021-04-05 06:37:57'),
(549, 43, 'HRMs', 1, 0, 0, 0, '2021-04-05 06:37:57', '2021-04-05 06:37:57'),
(550, 43, 'Account', 1, 0, 0, 0, '2021-04-05 06:37:57', '2021-04-05 06:37:57'),
(551, 43, 'Roles_&_Permission', 1, 0, 0, 0, '2021-04-05 06:37:57', '2021-04-05 06:37:57'),
(552, 43, 'Settings', 1, 0, 0, 0, '2021-04-05 06:37:57', '2021-04-05 06:37:57'),
(553, 43, 'Company/Branch', 1, 0, 0, 0, '2021-04-05 06:37:57', '2021-04-05 06:37:57'),
(554, 43, 'Agent/Telecaller', 1, 0, 0, 0, '2021-04-05 06:37:57', '2021-04-05 06:37:57'),
(555, 43, 'Bank', 1, 0, 0, 0, '2021-04-05 06:37:57', '2021-04-05 06:37:57'),
(556, 43, 'Backet', 1, 0, 0, 0, '2021-04-05 06:37:57', '2021-04-05 06:37:57'),
(557, 43, 'Recollection_Roles', 1, 0, 0, 0, '2021-04-05 06:37:57', '2021-04-05 06:37:57'),
(558, 43, 'Employee', 1, 0, 0, 0, '2021-04-05 06:37:57', '2021-04-05 06:37:57'),
(559, 43, 'Cash_Collection', 1, 0, 0, 0, '2021-04-05 06:37:57', '2021-04-05 06:37:57'),
(560, 43, 'Backup_Database', 1, 0, 0, 0, '2021-04-05 06:37:57', '2021-04-05 06:37:57'),
(561, 43, 'Agent/Telecaller_Master', 1, 0, 0, 0, '2021-04-05 06:37:57', '2021-04-05 06:37:57'),
(562, 43, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:37:57', '2021-04-05 06:37:57'),
(563, 43, 'Company_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:37:57', '2021-04-05 06:37:57'),
(564, 43, 'Manage_Salary', 1, 0, 0, 0, '2021-04-05 06:37:57', '2021-04-05 06:37:57'),
(565, 43, 'Make_Payment', 1, 0, 0, 0, '2021-04-05 06:37:57', '2021-04-05 06:37:57'),
(566, 43, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-04-05 06:37:57', '2021-04-05 06:37:57'),
(567, 43, 'Generate_Invoice', 1, 0, 0, 0, '2021-04-05 06:37:57', '2021-04-05 06:37:57'),
(568, 43, 'Payment_Received', 1, 0, 0, 0, '2021-04-05 06:37:57', '2021-04-05 06:37:57'),
(569, 44, 'Dashboard', 1, 0, 0, 0, '2021-04-05 06:40:16', '2021-04-05 06:40:16'),
(570, 44, 'Master', 1, 0, 0, 0, '2021-04-05 06:40:16', '2021-04-05 06:40:16'),
(571, 44, 'Daily_Entry', 1, 0, 0, 0, '2021-04-05 06:40:16', '2021-04-05 06:40:16'),
(572, 44, 'Uploads', 1, 0, 0, 0, '2021-04-05 06:40:16', '2021-04-05 06:40:16'),
(573, 44, 'HRMs', 1, 0, 0, 0, '2021-04-05 06:40:16', '2021-04-05 06:40:16'),
(574, 44, 'Account', 1, 0, 0, 0, '2021-04-05 06:40:16', '2021-04-05 06:40:16'),
(575, 44, 'Roles_&_Permission', 1, 0, 0, 0, '2021-04-05 06:40:16', '2021-04-05 06:40:16'),
(576, 44, 'Settings', 1, 0, 0, 0, '2021-04-05 06:40:16', '2021-04-05 06:40:16'),
(577, 44, 'Company/Branch', 1, 0, 0, 0, '2021-04-05 06:40:16', '2021-04-05 06:40:16'),
(578, 44, 'Agent/Telecaller', 1, 0, 0, 0, '2021-04-05 06:40:16', '2021-04-05 06:40:16'),
(579, 44, 'Bank', 1, 0, 0, 0, '2021-04-05 06:40:16', '2021-04-05 06:40:16'),
(580, 44, 'Backet', 1, 0, 0, 0, '2021-04-05 06:40:16', '2021-04-05 06:40:16'),
(581, 44, 'Recollection_Roles', 1, 0, 0, 0, '2021-04-05 06:40:16', '2021-04-05 06:40:16'),
(582, 44, 'Employee', 1, 0, 0, 0, '2021-04-05 06:40:16', '2021-04-05 06:40:16'),
(583, 44, 'Cash_Collection', 1, 0, 0, 0, '2021-04-05 06:40:16', '2021-04-05 06:40:16'),
(584, 44, 'Backup_Database', 1, 0, 0, 0, '2021-04-05 06:40:16', '2021-04-05 06:40:16'),
(585, 44, 'Agent/Telecaller_Master', 1, 0, 0, 0, '2021-04-05 06:40:16', '2021-04-05 06:40:16'),
(586, 44, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:40:16', '2021-04-05 06:40:16'),
(587, 44, 'Company_Master_Upload', 1, 0, 0, 0, '2021-04-05 06:40:16', '2021-04-05 06:40:16'),
(588, 44, 'Manage_Salary', 1, 0, 0, 0, '2021-04-05 06:40:16', '2021-04-05 06:40:16'),
(589, 44, 'Make_Payment', 1, 0, 0, 0, '2021-04-05 06:40:16', '2021-04-05 06:40:16'),
(590, 44, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-04-05 06:40:16', '2021-04-05 06:40:16'),
(591, 44, 'Generate_Invoice', 1, 0, 0, 0, '2021-04-05 06:40:16', '2021-04-05 06:40:16'),
(592, 44, 'Payment_Received', 1, 0, 0, 0, '2021-04-05 06:40:16', '2021-04-05 06:40:16'),
(593, 45, 'Dashboard', 1, 0, 0, 0, '2021-04-09 07:32:28', '2021-04-09 07:32:28'),
(594, 45, 'Master', 1, 0, 0, 0, '2021-04-09 07:32:28', '2021-04-09 07:32:28'),
(595, 45, 'Daily_Entry', 1, 0, 0, 0, '2021-04-09 07:32:28', '2021-04-09 07:32:28'),
(596, 45, 'Uploads', 1, 0, 0, 0, '2021-04-09 07:32:28', '2021-04-09 07:32:28'),
(597, 45, 'HRMs', 1, 0, 0, 0, '2021-04-09 07:32:28', '2021-04-09 07:32:28'),
(598, 45, 'Account', 1, 0, 0, 0, '2021-04-09 07:32:28', '2021-04-09 07:32:28'),
(599, 45, 'Roles_&_Permission', 1, 0, 0, 0, '2021-04-09 07:32:28', '2021-04-09 07:32:28'),
(600, 45, 'Settings', 1, 0, 0, 0, '2021-04-09 07:32:28', '2021-04-09 07:32:28'),
(601, 45, 'Company/Branch', 1, 0, 0, 0, '2021-04-09 07:32:28', '2021-04-09 07:32:28'),
(602, 45, 'Agent/Telecaller', 1, 0, 0, 0, '2021-04-09 07:32:28', '2021-04-09 07:32:28'),
(603, 45, 'Bank', 1, 0, 0, 0, '2021-04-09 07:32:28', '2021-04-09 07:32:28'),
(604, 45, 'Backet', 1, 0, 0, 0, '2021-04-09 07:32:28', '2021-04-09 07:32:28'),
(605, 45, 'Recollection_Roles', 1, 0, 0, 0, '2021-04-09 07:32:28', '2021-04-09 07:32:28'),
(606, 45, 'Employee', 1, 0, 0, 0, '2021-04-09 07:32:28', '2021-04-09 07:32:28'),
(607, 45, 'Cash_Collection', 1, 0, 0, 0, '2021-04-09 07:32:28', '2021-04-09 07:32:28'),
(608, 45, 'Backup_Database', 1, 0, 0, 0, '2021-04-09 07:32:28', '2021-04-09 07:32:28'),
(609, 45, 'Agent/Telecaller_Master', 1, 0, 0, 0, '2021-04-09 07:32:28', '2021-04-09 07:32:28'),
(610, 45, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-04-09 07:32:28', '2021-04-09 07:32:28'),
(611, 45, 'Company_Master_Upload', 1, 0, 0, 0, '2021-04-09 07:32:28', '2021-04-09 07:32:28'),
(612, 45, 'Manage_Salary', 1, 0, 0, 0, '2021-04-09 07:32:28', '2021-04-09 07:32:28'),
(613, 45, 'Make_Payment', 1, 0, 0, 0, '2021-04-09 07:32:28', '2021-04-09 07:32:28'),
(614, 45, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-04-09 07:32:28', '2021-04-09 07:32:28'),
(615, 45, 'Generate_Invoice', 1, 0, 0, 0, '2021-04-09 07:32:28', '2021-04-09 07:32:28'),
(616, 45, 'Payment_Received', 1, 0, 0, 0, '2021-04-09 07:32:28', '2021-04-09 07:32:28'),
(617, 46, 'Dashboard', 1, 0, 0, 0, '2021-04-09 07:32:47', '2021-04-09 07:32:47'),
(618, 46, 'Master', 1, 0, 0, 0, '2021-04-09 07:32:47', '2021-04-09 07:32:47'),
(619, 46, 'Daily_Entry', 1, 0, 0, 0, '2021-04-09 07:32:47', '2021-04-09 07:32:47'),
(620, 46, 'Uploads', 1, 0, 0, 0, '2021-04-09 07:32:47', '2021-04-09 07:32:47'),
(621, 46, 'HRMs', 1, 0, 0, 0, '2021-04-09 07:32:47', '2021-04-09 07:32:47'),
(622, 46, 'Account', 1, 0, 0, 0, '2021-04-09 07:32:47', '2021-04-09 07:32:47'),
(623, 46, 'Roles_&_Permission', 1, 0, 0, 0, '2021-04-09 07:32:47', '2021-04-09 07:32:47'),
(624, 46, 'Settings', 1, 0, 0, 0, '2021-04-09 07:32:47', '2021-04-09 07:32:47'),
(625, 46, 'Company/Branch', 1, 0, 0, 0, '2021-04-09 07:32:47', '2021-04-09 07:32:47'),
(626, 46, 'Agent/Telecaller', 1, 0, 0, 0, '2021-04-09 07:32:47', '2021-04-09 07:32:47'),
(627, 46, 'Bank', 1, 0, 0, 0, '2021-04-09 07:32:47', '2021-04-09 07:32:47'),
(628, 46, 'Backet', 1, 0, 0, 0, '2021-04-09 07:32:47', '2021-04-09 07:32:47'),
(629, 46, 'Recollection_Roles', 1, 0, 0, 0, '2021-04-09 07:32:47', '2021-04-09 07:32:47'),
(630, 46, 'Employee', 1, 0, 0, 0, '2021-04-09 07:32:47', '2021-04-09 07:32:47'),
(631, 46, 'Cash_Collection', 1, 0, 0, 0, '2021-04-09 07:32:47', '2021-04-09 07:32:47'),
(632, 46, 'Backup_Database', 1, 0, 0, 0, '2021-04-09 07:32:47', '2021-04-09 07:32:47'),
(633, 46, 'Agent/Telecaller_Master', 1, 0, 0, 0, '2021-04-09 07:32:47', '2021-04-09 07:32:47'),
(634, 46, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-04-09 07:32:47', '2021-04-09 07:32:47'),
(635, 46, 'Company_Master_Upload', 1, 0, 0, 0, '2021-04-09 07:32:47', '2021-04-09 07:32:47'),
(636, 46, 'Manage_Salary', 1, 0, 0, 0, '2021-04-09 07:32:47', '2021-04-09 07:32:47'),
(637, 46, 'Make_Payment', 1, 0, 0, 0, '2021-04-09 07:32:47', '2021-04-09 07:32:47'),
(638, 46, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-04-09 07:32:47', '2021-04-09 07:32:47'),
(639, 46, 'Generate_Invoice', 1, 0, 0, 0, '2021-04-09 07:32:47', '2021-04-09 07:32:47'),
(640, 46, 'Payment_Received', 1, 0, 0, 0, '2021-04-09 07:32:47', '2021-04-09 07:32:47'),
(665, 48, 'Dashboard', 1, 0, 0, 0, '2021-04-19 11:42:52', '2021-04-19 06:42:52'),
(666, 48, 'Master', 1, 0, 0, 0, '2021-04-19 11:42:52', '2021-04-19 06:42:52'),
(667, 48, 'Daily_Entry', 1, 0, 0, 0, '2021-04-19 11:42:52', '2021-04-19 06:42:52'),
(668, 48, 'Uploads', 1, 0, 0, 0, '2021-04-19 11:42:52', '2021-04-19 06:42:52'),
(669, 48, 'HRMs', 1, 0, 0, 0, '2021-04-19 11:42:52', '2021-04-19 06:42:52'),
(670, 48, 'Account', 1, 0, 0, 0, '2021-04-19 11:42:52', '2021-04-19 06:42:52'),
(671, 48, 'Roles_&_Permission', 1, 0, 0, 0, '2021-04-19 11:42:52', '2021-04-19 06:42:52'),
(672, 48, 'Settings', 1, 0, 0, 0, '2021-04-19 11:42:52', '2021-04-19 06:42:52'),
(673, 48, 'Company/Branch', 1, 0, 0, 0, '2021-04-19 11:42:52', '2021-04-19 06:42:52'),
(674, 48, 'Agent/Telecaller', 1, 0, 0, 0, '2021-04-19 11:42:52', '2021-04-19 06:42:52'),
(675, 48, 'Bank', 1, 0, 0, 0, '2021-04-19 11:42:52', '2021-04-19 06:42:52'),
(676, 48, 'Backet', 1, 0, 0, 0, '2021-04-19 11:42:52', '2021-04-19 06:42:52'),
(677, 48, 'Recollection_Roles', 1, 0, 0, 0, '2021-04-19 11:42:52', '2021-04-19 06:42:52'),
(678, 48, 'Employee', 1, 0, 0, 0, '2021-04-19 11:42:52', '2021-04-19 06:42:52'),
(679, 48, 'Cash_Collection', 1, 0, 0, 0, '2021-04-19 11:42:52', '2021-04-19 06:42:52'),
(680, 48, 'Backup_Database', 1, 0, 0, 0, '2021-04-19 11:42:52', '2021-04-19 06:42:52'),
(681, 48, 'Agent/Telecaller_Master', 1, 0, 0, 0, '2021-04-19 11:42:52', '2021-04-19 06:42:52'),
(682, 48, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-04-19 11:42:52', '2021-04-19 06:42:52'),
(683, 48, 'Company_Master_Upload', 1, 0, 0, 0, '2021-04-19 11:42:52', '2021-04-19 06:42:52'),
(684, 48, 'Manage_Salary', 1, 0, 0, 0, '2021-04-19 11:42:52', '2021-04-19 06:42:52'),
(685, 48, 'Make_Payment', 1, 0, 0, 0, '2021-04-19 11:42:52', '2021-04-19 06:42:52'),
(686, 48, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-04-19 11:42:52', '2021-04-19 06:42:52'),
(687, 48, 'Generate_Invoice', 1, 0, 0, 0, '2021-04-19 11:42:52', '2021-04-19 06:42:52'),
(688, 48, 'Payment_Received', 1, 0, 0, 0, '2021-04-19 11:42:52', '2021-04-19 06:42:52'),
(713, 50, 'Dashboard', 1, 0, 0, 0, '2021-04-19 11:43:54', '2021-04-19 11:43:54'),
(714, 50, 'Master', 1, 0, 0, 0, '2021-04-19 11:43:54', '2021-04-19 11:43:54'),
(715, 50, 'Daily_Entry', 1, 0, 0, 0, '2021-04-19 11:43:54', '2021-04-19 11:43:54'),
(716, 50, 'Uploads', 1, 0, 0, 0, '2021-04-19 11:43:54', '2021-04-19 11:43:54'),
(717, 50, 'HRMs', 1, 0, 0, 0, '2021-04-19 11:43:54', '2021-04-19 11:43:54'),
(718, 50, 'Account', 1, 0, 0, 0, '2021-04-19 11:43:54', '2021-04-19 11:43:54'),
(719, 50, 'Roles_&_Permission', 1, 0, 0, 0, '2021-04-19 11:43:54', '2021-04-19 11:43:54'),
(720, 50, 'Settings', 1, 0, 0, 0, '2021-04-19 11:43:54', '2021-04-19 11:43:54'),
(721, 50, 'Company/Branch', 1, 0, 0, 0, '2021-04-19 11:43:54', '2021-04-19 11:43:54'),
(722, 50, 'Agent/Telecaller', 1, 0, 0, 0, '2021-04-19 11:43:54', '2021-04-19 11:43:54'),
(723, 50, 'Bank', 1, 0, 0, 0, '2021-04-19 11:43:54', '2021-04-19 11:43:54'),
(724, 50, 'Backet', 1, 0, 0, 0, '2021-04-19 11:43:54', '2021-04-19 11:43:54'),
(725, 50, 'Recollection_Roles', 1, 0, 0, 0, '2021-04-19 11:43:54', '2021-04-19 11:43:54'),
(726, 50, 'Employee', 1, 0, 0, 0, '2021-04-19 11:43:54', '2021-04-19 11:43:54'),
(727, 50, 'Cash_Collection', 1, 0, 0, 0, '2021-04-19 11:43:54', '2021-04-19 11:43:54'),
(728, 50, 'Backup_Database', 1, 0, 0, 0, '2021-04-19 11:43:54', '2021-04-19 11:43:54'),
(729, 50, 'Agent/Telecaller_Master', 1, 0, 0, 0, '2021-04-19 11:43:54', '2021-04-19 11:43:54'),
(730, 50, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-04-19 11:43:54', '2021-04-19 11:43:54'),
(731, 50, 'Company_Master_Upload', 1, 0, 0, 0, '2021-04-19 11:43:54', '2021-04-19 11:43:54'),
(732, 50, 'Manage_Salary', 1, 0, 0, 0, '2021-04-19 11:43:54', '2021-04-19 11:43:54'),
(733, 50, 'Make_Payment', 1, 0, 0, 0, '2021-04-19 11:43:54', '2021-04-19 11:43:54'),
(734, 50, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-04-19 11:43:54', '2021-04-19 11:43:54'),
(735, 50, 'Generate_Invoice', 1, 0, 0, 0, '2021-04-19 11:43:54', '2021-04-19 11:43:54'),
(736, 50, 'Payment_Received', 1, 0, 0, 0, '2021-04-19 11:43:54', '2021-04-19 11:43:54'),
(761, 52, 'Dashboard', 1, 0, 0, 0, '2021-10-20 09:49:41', '2021-10-20 09:49:41'),
(762, 52, 'Master', 1, 0, 0, 0, '2021-10-20 09:49:41', '2021-10-20 09:49:41'),
(763, 52, 'Daily_Entry', 1, 0, 0, 0, '2021-10-20 09:49:42', '2021-10-20 09:49:42'),
(764, 52, 'Uploads', 1, 0, 0, 0, '2021-10-20 09:49:42', '2021-10-20 09:49:42'),
(765, 52, 'HRMs', 1, 0, 0, 0, '2021-10-20 09:49:42', '2021-10-20 09:49:42');
INSERT INTO `ptx_user_roles` (`id`, `designations_id`, `menu`, `view`, `created`, `edited`, `deleted`, `created_at`, `updated_at`) VALUES
(766, 52, 'Account', 1, 0, 0, 0, '2021-10-20 09:49:42', '2021-10-20 09:49:42'),
(767, 52, 'Roles_&_Permission', 1, 0, 0, 0, '2021-10-20 09:49:42', '2021-10-20 09:49:42'),
(768, 52, 'Settings', 1, 0, 0, 0, '2021-10-20 09:49:42', '2021-10-20 09:49:42'),
(769, 52, 'Company/Branch', 1, 0, 0, 0, '2021-10-20 09:49:42', '2021-10-20 09:49:42'),
(770, 52, 'Agent/Telecaller', 1, 0, 0, 0, '2021-10-20 09:49:42', '2021-10-20 09:49:42'),
(771, 52, 'Bank', 1, 0, 0, 0, '2021-10-20 09:49:42', '2021-10-20 09:49:42'),
(772, 52, 'Backet', 1, 0, 0, 0, '2021-10-20 09:49:42', '2021-10-20 09:49:42'),
(773, 52, 'Recollection_Roles', 1, 0, 0, 0, '2021-10-20 09:49:42', '2021-10-20 09:49:42'),
(774, 52, 'Employee', 1, 0, 0, 0, '2021-10-20 09:49:42', '2021-10-20 09:49:42'),
(775, 52, 'Cash_Collection', 1, 0, 0, 0, '2021-10-20 09:49:42', '2021-10-20 09:49:42'),
(776, 52, 'Backup_Database', 1, 0, 0, 0, '2021-10-20 09:49:42', '2021-10-20 09:49:42'),
(777, 52, 'Agent/Telecaller_Master', 1, 0, 0, 0, '2021-10-20 09:49:42', '2021-10-20 09:49:42'),
(778, 52, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-10-20 09:49:42', '2021-10-20 09:49:42'),
(779, 52, 'Company_Master_Upload', 1, 0, 0, 0, '2021-10-20 09:49:42', '2021-10-20 09:49:42'),
(780, 52, 'Manage_Salary', 1, 0, 0, 0, '2021-10-20 09:49:42', '2021-10-20 09:49:42'),
(781, 52, 'Make_Payment', 1, 0, 0, 0, '2021-10-20 09:49:42', '2021-10-20 09:49:42'),
(782, 52, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-10-20 09:49:42', '2021-10-20 09:49:42'),
(783, 52, 'Generate_Invoice', 1, 0, 0, 0, '2021-10-20 09:49:42', '2021-10-20 09:49:42'),
(784, 52, 'Payment_Received', 1, 0, 0, 0, '2021-10-20 09:49:42', '2021-10-20 09:49:42'),
(785, 53, 'Dashboard', 1, 0, 0, 0, '2021-10-20 09:51:20', '2021-10-20 09:51:20'),
(786, 53, 'Master', 1, 0, 0, 0, '2021-10-20 09:51:20', '2021-10-20 09:51:20'),
(787, 53, 'Daily_Entry', 1, 0, 0, 0, '2021-10-20 09:51:20', '2021-10-20 09:51:20'),
(788, 53, 'Uploads', 1, 0, 0, 0, '2021-10-20 09:51:20', '2021-10-20 09:51:20'),
(789, 53, 'HRMs', 1, 0, 0, 0, '2021-10-20 09:51:20', '2021-10-20 09:51:20'),
(790, 53, 'Account', 1, 0, 0, 0, '2021-10-20 09:51:20', '2021-10-20 09:51:20'),
(791, 53, 'Roles_&_Permission', 1, 0, 0, 0, '2021-10-20 09:51:20', '2021-10-20 09:51:20'),
(792, 53, 'Settings', 1, 0, 0, 0, '2021-10-20 09:51:20', '2021-10-20 09:51:20'),
(793, 53, 'Company/Branch', 1, 0, 0, 0, '2021-10-20 09:51:20', '2021-10-20 09:51:20'),
(794, 53, 'Agent/Telecaller', 1, 0, 0, 0, '2021-10-20 09:51:20', '2021-10-20 09:51:20'),
(795, 53, 'Bank', 1, 0, 0, 0, '2021-10-20 09:51:20', '2021-10-20 09:51:20'),
(796, 53, 'Backet', 1, 0, 0, 0, '2021-10-20 09:51:20', '2021-10-20 09:51:20'),
(797, 53, 'Recollection_Roles', 1, 0, 0, 0, '2021-10-20 09:51:20', '2021-10-20 09:51:20'),
(798, 53, 'Employee', 1, 0, 0, 0, '2021-10-20 09:51:20', '2021-10-20 09:51:20'),
(799, 53, 'Cash_Collection', 1, 0, 0, 0, '2021-10-20 09:51:20', '2021-10-20 09:51:20'),
(800, 53, 'Backup_Database', 1, 0, 0, 0, '2021-10-20 09:51:20', '2021-10-20 09:51:20'),
(801, 53, 'Agent/Telecaller_Master', 1, 0, 0, 0, '2021-10-20 09:51:20', '2021-10-20 09:51:20'),
(802, 53, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-10-20 09:51:20', '2021-10-20 09:51:20'),
(803, 53, 'Company_Master_Upload', 1, 0, 0, 0, '2021-10-20 09:51:20', '2021-10-20 09:51:20'),
(804, 53, 'Manage_Salary', 1, 0, 0, 0, '2021-10-20 09:51:20', '2021-10-20 09:51:20'),
(805, 53, 'Make_Payment', 1, 0, 0, 0, '2021-10-20 09:51:20', '2021-10-20 09:51:20'),
(806, 53, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-10-20 09:51:20', '2021-10-20 09:51:20'),
(807, 53, 'Generate_Invoice', 1, 0, 0, 0, '2021-10-20 09:51:20', '2021-10-20 09:51:20'),
(808, 53, 'Payment_Received', 1, 0, 0, 0, '2021-10-20 09:51:20', '2021-10-20 09:51:20'),
(809, 54, 'Dashboard', 1, 0, 0, 0, '2021-10-20 09:51:54', '2021-10-20 09:51:54'),
(810, 54, 'Master', 1, 0, 0, 0, '2021-10-20 09:51:54', '2021-10-20 09:51:54'),
(811, 54, 'Daily_Entry', 1, 0, 0, 0, '2021-10-20 09:51:54', '2021-10-20 09:51:54'),
(812, 54, 'Uploads', 1, 0, 0, 0, '2021-10-20 09:51:54', '2021-10-20 09:51:54'),
(813, 54, 'HRMs', 1, 0, 0, 0, '2021-10-20 09:51:54', '2021-10-20 09:51:54'),
(814, 54, 'Account', 1, 0, 0, 0, '2021-10-20 09:51:54', '2021-10-20 09:51:54'),
(815, 54, 'Roles_&_Permission', 1, 0, 0, 0, '2021-10-20 09:51:54', '2021-10-20 09:51:54'),
(816, 54, 'Settings', 1, 0, 0, 0, '2021-10-20 09:51:54', '2021-10-20 09:51:54'),
(817, 54, 'Company/Branch', 1, 0, 0, 0, '2021-10-20 09:51:54', '2021-10-20 09:51:54'),
(818, 54, 'Agent/Telecaller', 1, 0, 0, 0, '2021-10-20 09:51:54', '2021-10-20 09:51:54'),
(819, 54, 'Bank', 1, 0, 0, 0, '2021-10-20 09:51:54', '2021-10-20 09:51:54'),
(820, 54, 'Backet', 1, 0, 0, 0, '2021-10-20 09:51:54', '2021-10-20 09:51:54'),
(821, 54, 'Recollection_Roles', 1, 0, 0, 0, '2021-10-20 09:51:54', '2021-10-20 09:51:54'),
(822, 54, 'Employee', 1, 0, 0, 0, '2021-10-20 09:51:54', '2021-10-20 09:51:54'),
(823, 54, 'Cash_Collection', 1, 0, 0, 0, '2021-10-20 09:51:54', '2021-10-20 09:51:54'),
(824, 54, 'Backup_Database', 1, 0, 0, 0, '2021-10-20 09:51:54', '2021-10-20 09:51:54'),
(825, 54, 'Agent/Telecaller_Master', 1, 0, 0, 0, '2021-10-20 09:51:54', '2021-10-20 09:51:54'),
(826, 54, 'Bank_Master_Upload', 1, 0, 0, 0, '2021-10-20 09:51:54', '2021-10-20 09:51:54'),
(827, 54, 'Company_Master_Upload', 1, 0, 0, 0, '2021-10-20 09:51:54', '2021-10-20 09:51:54'),
(828, 54, 'Manage_Salary', 1, 0, 0, 0, '2021-10-20 09:51:54', '2021-10-20 09:51:54'),
(829, 54, 'Make_Payment', 1, 0, 0, 0, '2021-10-20 09:51:54', '2021-10-20 09:51:54'),
(830, 54, 'Generate_Page_Slip', 1, 0, 0, 0, '2021-10-20 09:51:54', '2021-10-20 09:51:54'),
(831, 54, 'Generate_Invoice', 1, 0, 0, 0, '2021-10-20 09:51:54', '2021-10-20 09:51:54'),
(832, 54, 'Payment_Received', 1, 0, 0, 0, '2021-10-20 09:51:54', '2021-10-20 09:51:54');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation_id` int(11) NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT '0' COMMENT '1 = Deleted, 0 = Not Deleted',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `employee_id`, `name`, `email`, `mobile`, `designation_id`, `password`, `remember_token`, `status`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, '1', 'Aakash Kothule', 'aakash@puretechnology.in', '8830705455', 19, '$2y$10$dCQdHJG5BthuEAMtdZGqXOkh5PJ0zOXR33cw4DzT/tXb.OeJOlcZK', NULL, NULL, 0, '2021-01-28 03:16:28', '2021-08-31 07:24:40'),
(32, '52101', 'NILESH ANANTRAO HIRALKAR', 'pahiralkar@gmail.com', '8010904077', 26, '$2y$10$BzViIBbu2l0ZxnHbWAxuhulR9PWWhK3JFaLr8T/0fjVrJAZLBrkQi', 'ead69da938fef521741e14393b04f501', NULL, 0, NULL, '2021-09-13 23:42:54'),
(33, '52090', 'SHEETAL AMIT  LADGOANKAR', 'ladgaonkar.sheetal1985@gmail.com', '7058120730', 33, '$2y$10$.FQUnCFcJdBBduwpeqIuPujkQ1GKU0WPjFfTEvfmCKKuRM2hG64yu', NULL, NULL, 0, NULL, '2021-09-14 23:43:08'),
(35, '52072', 'PRAVIN LAXMAN BADHE', 'pravin@omsaicredit.com', '7373563563', 41, '$2y$10$6wezR0YTfEzZTr4DDyvwKOsfjktwH3dkzLZ.36.0qELelLsWIgLsm', NULL, NULL, 0, NULL, '2021-06-25 07:23:08'),
(36, '51009', 'ANANT NETAJI YADAV', 'anant@omsaicredit.com', '9881000356', 28, '$2y$10$g9IfORJEpkL9tvumBwxLoee20nc9MWQvp/5dhLytl30aVmb0GK1zO', NULL, NULL, 0, NULL, '2021-10-18 06:10:13'),
(37, '51048', 'RAVINDRA SHRIRAM MAHADIK', 'ravi@omsaicredit.com', '9850904141', 37, '$2y$10$ziabENeurWY.pzYIPGMw0eNud9jPwKApfWlWf9zel6mEwpZweuEoy', NULL, NULL, 0, NULL, '2021-10-18 06:11:08'),
(38, '51681', 'RUPALI MANOJ KULDHARNE', 'rupali.omsaikrupa@gmail.com', '7620151554', 39, '$2y$10$ZxvyyQ2DcyxfBgBeThM65.Ho0/ddCNHF.02FzW9xAGuZ2rTMjMUAi', NULL, NULL, 0, NULL, '2021-09-14 23:45:47'),
(39, '51960', 'VIRENDRAKUMAR BABUSINGH RAJPUT', 'virendrarajput22@gmail.com', '9850841216', 40, '$2y$10$HNWBBD175ln.RLgOkn5kJuQiZTCOQHjjByDaGEEQdWFa7DkT50kOO', NULL, NULL, 1, NULL, '2021-08-27 08:38:53'),
(41, '51252', 'VIJAY SHAMRAO KHOLE', 'vijay.khole@gmail.com', '8007889889', 28, '$2y$10$Ihm2SXgotIkTsaG4e.IUq.1NBOvgKV6gcUiOxSEyqwqhfQr/Si0ju', NULL, NULL, 0, NULL, '2021-10-18 06:12:02'),
(42, '51273', 'SURENDRA DHANPATRAJ KOTHARI', 'surendra@omsaicredit.com', '9637471760', 32, '$2y$10$qmK2eYZ9PwbqsPNRkPXoKeMEHluqXRmBPFE.23LfiNZ3IYuNMBdXK', NULL, NULL, 1, NULL, '2021-09-11 07:22:37'),
(43, '51633', 'ASHWINI KUNAL KOLTE', 'ashwinikkolteomsai@gmail.com', '8888333876', 29, '$2y$10$T2pkttBw.2XhqinCDz86J.j9A6eL0vkCgYtodJKFOLMvDG1bGkBGa', NULL, NULL, 0, NULL, '2021-10-18 06:12:29'),
(45, '51961', 'RASHMI RUPESH TIKONE', 'rashmi.omsaicredit@gmail.com', '9226134909', 44, '$2y$10$/jyGVANnhcZeThGutzsF0elRuLDmZ0Vk/iQNmbj9TtZL5UtV3tXd.', NULL, NULL, 0, NULL, '2021-10-09 01:12:13'),
(48, '51658', 'SATISH SHANKARRAO MANE', 'asm20071993@gmail.com', '8446661519', 27, '$2y$10$Q3GPbsNJNOzLQzNYFJhM4.xEZWrmGNU1RGD6yMisIohuORONM6bYm', NULL, NULL, 0, NULL, '2021-09-11 08:18:07'),
(49, '51067', 'RAKESH RAMDAS BHAJANE', 'rakesh.bhajane@gmail.com', '9860809659', 33, '$2y$10$xFKMdFRHKh/LNEa7SCurrOlOesDuo4KFLLRe2N4zicjF7pd9zwFW2', NULL, NULL, 0, NULL, '2021-09-14 23:45:11'),
(50, '51808', 'SANDIP PRAKASH KULE', 'sandipkule0373@gmail.com', '7066482278', 27, '$2y$10$NabVgdh4Rl4hShjDem5yy.X9mvmjzvysgZNvjY1JSfUP/nLrHd9uu', NULL, NULL, 0, NULL, '2021-09-15 00:07:17'),
(51, '51963', 'ASHWINI BALBHIM RASAL', NULL, '7058200134', 43, '$2y$10$j.R65AwrPBP9UH54CjdKEuVF1ZToX7RQ21rTVv657PeMnVh9WYeM2', NULL, NULL, 0, NULL, '2021-05-07 08:08:11'),
(52, '51962', 'SANGITA SHANKAR WAGHMARE', NULL, '9970574304', 43, '$2y$10$M6dUA2vQ0XsvuxtJgu33QOCitSGLoGr9xUzhiNJ/U/FuniZHQ0Ow6', NULL, NULL, 0, NULL, '2021-10-18 06:34:15'),
(53, '52004', 'PRIYANKA NILESH BARKE', NULL, '7058496133', 24, '$2y$10$IgHKbK9hlFeQvhSWeieUNOTiRxAreFzhYx5QIyF/SIsErpm92EKeG', NULL, NULL, 1, NULL, '2021-09-11 07:30:36'),
(54, '52069', 'PRASAD DHURARDAR SHETTY', NULL, '7066626038', 46, '$2y$10$EBj/KpJSOdpk6/6efAD/kuq/8uMZaHgkBNNtbRoonWdcTLXTiF7w.', NULL, NULL, 0, NULL, '2021-06-26 00:06:12'),
(55, '52089', 'RAMCHANDRA ANDAPPA DUDHABHATE', 'dramchandra181@gmail.com', '9022686474', 45, '$2y$10$SjEHWdujHwbQPwMSgFTBEuhBo1c/KIGNK.SUtJVISCPJml4O1x7Nm', NULL, NULL, 0, NULL, '2021-09-11 08:06:21'),
(56, '51589', 'RAMESH RAJARAM SHINDE', NULL, '9028779145', 37, '$2y$10$S8n9dCn5YIqzmNLApO9B4uPEvWWeryOoT64OvCUJvIwOMnS4drGBu', NULL, NULL, 1, NULL, '2021-09-11 07:22:14'),
(58, 'EMP0016', 'Kumar Ingale', NULL, '8888200682', 37, '$2y$10$O0.at7.h3uvB9m2j9YDMDOw4M8n1MYgDdBWbyRf0xy5Vvij8RvGZS', NULL, 'Agent', 1, NULL, '2021-07-03 12:44:07'),
(59, '52058', 'NILAM PRABHAKAR BHOGALE', NULL, '8668712292', 30, '$2y$10$OEDTdKeMXYP1gTGNvrcBPO5obuG7yANwxIjxoc8vjVajrsDvsbDKO', NULL, NULL, 0, NULL, '2021-06-26 00:27:49'),
(60, 'EMP0049', 'Sudhir Kumar', NULL, '7877664545', 38, '$2y$10$3fOU.jrjz.S2QdKq5k8QeenVUz2m79yCKPLuuR/lplkXQ04UYSNFK', NULL, 'Agent', 1, NULL, '2021-07-22 08:26:48'),
(61, '51008', 'ANAND VINAYAK BADHE', 'andybadhe@gmail.com', '9922581111', 38, '$2y$10$ItCSrFIedng4nDEgbTii8.03TcqQl0cO5kgpKE0iH40LYs2MMRnYm', NULL, 'Agent', 0, NULL, '2021-09-14 01:13:07'),
(62, '51122', 'NILESH VISHVANATH  BARKE', NULL, '7745895566', 36, '$2y$10$4D2CGKzukIk7hu3drN0Vk.eN5jpL2cVX/J26EhAmOtOYzzc/UJOja', NULL, 'Agent', 1, NULL, '2021-07-07 03:46:08'),
(63, '51139', 'NAVNATH DATTATRAY  KUDALE', 'navnathkudale250@gmail.com', '9850903212', 36, '$2y$10$FjGp118772DMXGBQZVJfnObDBOOw2BYQBfMuXplAIm7uQ8tKJofH.', NULL, 'Agent', 0, NULL, '2021-10-20 04:46:26'),
(64, '51152', 'PRADEEP ASHOK  MANE', NULL, '7350717106', 36, '$2y$10$7mNP4WllocjodjESmrD9We7HqTi7Jm52HgIISCtf0U6SOc6UjOgAu', NULL, 'Agent', 0, NULL, '2021-10-20 04:47:07'),
(65, '51182', 'RAKESH RAMESH SHINDE', 'rs9733890@gmail.com', '8983448076', 36, '$2y$10$TEkvyOHMcF0SBIFDZW27Q.n1k1tjzGXP0ulhKbSu1Ns0riOJbPS4a', NULL, 'Agent', 0, NULL, '2021-10-20 04:44:47'),
(66, '51218', 'SHEETAL MAYURESH PAWAR', NULL, '7620600947', 28, '$2y$10$mg9lpYN31jXxUJS0a96YEurH42VjrZOPGUp2SnvJ7l5MBJbP.AGtS', NULL, 'Agent', 0, NULL, '2021-10-20 04:59:52'),
(67, '51254', 'GANESH BALASAHEB KONDE', NULL, '9922593259', 36, '$2y$10$fEwyZ/dKLPC2odUlj95TuuKYk.ZDyAGF17.AaTSyRdqb59MC/19ZO', NULL, 'Agent', 1, NULL, '2021-10-08 05:46:52'),
(68, '51458', 'NIKHIL SURESH  TARU', NULL, '9850907528', 54, '$2y$10$fYRr6HpZanxLlH8/P9v7auXRv8HKz0TxFWfmDifqUWfmkl8wxHq7W', NULL, 'Agent', 0, NULL, '2021-10-20 05:02:17'),
(69, 'OMSAI 51254', 'SUNIL BABASAHEB KONDE', 'sunilkone8@gmail.com', '8308837198', 36, '$2y$10$CYxVdiInP1aP88O.ImS8MeviSs5o1wL6FhQrbHxOOB0zptCKv1oDC', NULL, 'Agent', 0, NULL, '2021-10-14 01:38:46'),
(70, '51460', 'AJIT MANOHAR  BARKE', 'ajitbarke1@gmail.com', '9075097248', 36, '$2y$10$nKuxzoXGhRGMfX8kjoe3ZOL6V1IcU9EmwJYJmDEugJM9bnSlnF2rG', NULL, 'Agent', 1, NULL, '2021-08-19 05:35:24'),
(71, '51468', 'PRITAM VIKRAM KALE', NULL, '9922369052', 38, '$2y$10$NVRvqHhsgbFfcm76QgPfF.2PWJB4GH3wknImOG5aURK6EwhFe/V9.', NULL, 'Agent', 0, NULL, '2021-07-01 00:34:28'),
(72, '51489', 'AKASH SITARAM SHINGE', NULL, '7744944949', 49, '$2y$10$K1xeIt2RvprzhpahV2krKOn.VwzwcQdATubmVtUvHbi.KcMcBLbVG', NULL, 'Agent', 0, NULL, '2021-06-30 02:19:24'),
(73, '51492', 'SUDHAKAR  LAXMAN THOSAR', NULL, '9370527504', 36, '$2y$10$iGz4T3O2WZU8StiZgnbCQuxAVkBmKvF8FKGqVyPWNrYv4zmgIFKX6', NULL, 'Agent', 0, NULL, '2021-10-20 04:45:52'),
(74, '51504', 'POOJA ROHIT GUJAR', 'poojagaikwad66@gmail.com', '9604577151', 38, '$2y$10$jG7D7jJ6hOE54dUy9b8p4OzsMiQEomRRngBzZD064B3fHqbvBdRQC', NULL, 'Agent', 0, NULL, '2021-10-20 05:05:36'),
(75, '51555', 'ATUL NARENDRA PATEL', 'atulpatel12389@gmail.com', '7020936807', 52, '$2y$10$kk9A6DnNH56HoUjalM9I8OE06P.BNc0XBsd38IU0GoEDkYXzzPl2e', NULL, 'Agent', 0, NULL, '2021-09-16 00:29:50'),
(76, '51580', 'NEHA SURESH RANPISE', NULL, '9022691418', 35, '$2y$10$1lQnPfhL66MuPDCHiCtuIeaE0FZLC5SF5Vk70Hhv1bb.onuF0m1a2', NULL, 'Agent', 0, NULL, '2021-08-28 05:02:23'),
(77, '51622', 'DEEPIKA PRITAM KALE', 'deepikadhumal94@gmail.com', '9822677965', 38, '$2y$10$FHTocVQndyyIrk255GoS8epXsQQ0YCuxzSRDGGFqi/n6zSeWuCduG', NULL, 'Agent', 0, NULL, '2021-09-01 02:40:37'),
(78, '51636', 'NEHA SURAJ JOSHI', 'neha.cutipao06@gmail.com', '8999737368', 31, '$2y$10$M8WyLHFHDch/u2hlXp/OJu6PBtVz4aKAphkB.JuTU4wyw9lVvxe8m', NULL, 'Agent', 0, NULL, '2021-10-20 05:06:38'),
(79, '51637', 'SALMAN  ABBAS  HUNDEKARI', 'hundekarisalman070@gmail.com', '8180838114', 36, '$2y$10$j/FHa8liaKTQLmz79usKPOBkXmZo5XVo9b07v0ekQpH1vBckbr3tK', NULL, 'Agent', 0, NULL, '2021-09-03 03:13:26'),
(80, '51640', 'TUSHAR SURESH MISAL', 'tmisal41@gmail.com', '8007766761', 36, '$2y$10$cAlz3QcMaC.IPenczqB4HOLjheJjcoECLhQroEBGFgQN3TpBrh4p.', NULL, 'Agent', 0, NULL, '2021-10-20 05:07:05'),
(81, '51652', 'VRUSHAB MANOJ KOMKAR', NULL, '7841000725', 36, '$2y$10$Witwtrvz3GHHatHcAEDF..1SpHgyYcDLdIQgVK9INuZSMn5W6RJb.', NULL, 'Agent', 0, NULL, '2021-10-20 05:07:57'),
(82, '51686', 'DATTATRAY DASHRATH  KANDHARE', 'dattakandhare14@gmail.com', '8888834938', 49, '$2y$10$D9dH5GkDtDJOmfRwOrLiTeXW0RjFQwtKCicPynWH4GnYFdC8lyRTe', NULL, 'Agent', 0, NULL, '2021-09-15 00:10:04'),
(83, '51687', 'AKSHAY SUDHIR GAIKWAD', 'agaikwad2115@gmail.com', '8788086796', 49, '$2y$10$LfS.2177.8mizHEYutGbrO5eLBuPFIF2BGNGtEQbH6Frn625BMF9u', NULL, 'Agent', 0, NULL, '2021-09-13 01:35:49'),
(84, '51751', 'AMIT KASHINATHE FALE', NULL, '9623002651', 36, '$2y$10$JsHcXFHd4XkEwiIUHvaGVOqwZD.2KGpmqnVDLW.oQ5nboEQ0ZFhSi', NULL, 'Agent', 0, NULL, '2021-09-15 00:08:19'),
(85, '51852', 'DINESH BABURAO BESALE', NULL, '9096364080', 36, '$2y$10$7bkeoNEuj/RGksyyGUSdYevuvF5OsLG2fqqPzfcwf4nqnoA1yyhRC', NULL, 'Agent', 0, NULL, '2021-06-26 01:47:10'),
(86, '51899', 'SANGRAM RAJARAM JADHAV', 'sangent1990@gmail.com', '7770077714', 36, '$2y$10$HR.s6KxliQFHWEevG6bo4.xS1uYue.mikigbgUiECWb1Y6K5mVOpy', NULL, 'Agent', 0, NULL, '2021-10-20 07:47:39'),
(87, '51928', 'RAJENDRA  NARAYAN PANIBATE', NULL, '7149345669', 36, '$2y$10$ZTK3LK.4gOVCO8zfpYhKnuRPonDQxKcHLYG22ioiB1eXUVDyizHlm', NULL, 'Agent', 0, NULL, '2021-06-30 02:29:48'),
(88, '51935', 'PRAJAKTA NARENDRA BULE', 'handart5656@gmail.com', '9579045163', 38, '$2y$10$fdMTtakttfY7UGQ9SbSiCORtICdJ1iAxjSTP2wnwugovf3W0Jd3I2', NULL, 'Agent', 0, NULL, '2021-10-20 05:08:54'),
(89, '51939', 'SHAFIK SALIM  SHAIKH', 'shafikshaikj336@gmail.com', '9860029943', 54, '$2y$10$IDb778wZ59Wpfjt2CijxBOGi/F/tkGj7zw2PePlgL9XVVNzAZORhe', NULL, 'Agent', 0, NULL, '2021-10-20 06:55:28'),
(90, '51954', 'POOJA SHYAMNANDAN TIWARI', NULL, '7385513593', 35, '$2y$10$3nSb5cs7.NR5z3.Xz8dq/eVNjrZxVzFfd/f4EhMCuKGUWSQhIZAQ.', NULL, 'Agent', 1, NULL, '2021-08-25 07:50:07'),
(91, '51959', 'ASMITA VISHAL KAMBLE', NULL, '9172768890', 35, '$2y$10$26xcaV.PtnvUzaHCD9hKeuOEfDR6tsCTmRDPuCjGnTDJCL0Nn5NYu', NULL, 'Agent', 1, NULL, '2021-08-21 08:06:04'),
(92, '51965', 'MITHUN GOVIND KADAM', NULL, '9325465967', 36, '$2y$10$APBufTRCIKKi9WaKNM.QqOVEX9o3YZrBLcwxQMd8M8TayZQ10eD0a', NULL, 'Agent', 0, NULL, '2021-10-20 06:55:50'),
(93, '51970', 'VIKRAM BALU JADHAV', NULL, '8007947631', 36, '$2y$10$UsLhJWePlRUI3SSn3Jz4M.003htRxDn6GAW6SZioqAc5.GYhVyfT2', NULL, 'Agent', 0, NULL, '2021-10-20 06:58:25'),
(94, '51974', 'SAURABH SANJAY JADHAV', NULL, '9850380103', 36, '$2y$10$oGhTh0V3Kxt5NrXFmrdjA.0fm8EjEhEHO2h9CbaeoF4/wkZCQHhxO', NULL, 'Agent', 0, NULL, '2021-10-20 06:53:30'),
(95, '51980', 'VIKAS ASHOK SAKHARE', 'vikassakhare9@gmail.com', '9823657860', 36, '$2y$10$HSkYCcGDm6zZuhDA2UeLtujSzo0fohCFg9ONuVdJShWGfb/AJIogu', NULL, 'Agent', 0, NULL, '2021-10-20 07:03:00'),
(96, '51982', 'MAYURESH VIJAY SONAWANE', NULL, '8983781302', 36, '$2y$10$S9/388d37b8XI7xK/Uy.quIe5l7OO99ll7SsktgQqjroA3huHwpAW', NULL, 'Agent', 0, NULL, '2021-08-24 07:21:06'),
(97, '51995', 'PARAG PURSHOTTAM PINGALE', NULL, '9881998283', 36, '$2y$10$PPHXMCJJ/T519nPfs69vyOS/bptMLvSd6h0x38Xx35QizYjaZzhry', NULL, 'Agent', 0, NULL, '2021-10-20 07:08:17'),
(98, '51996', 'PRATIK CHANDRASHEKHAR JADHAV', NULL, '9049568625', 36, '$2y$10$1ZtoECCIt1WSSU/7C533T.Nl7IUztUoSr2cNjPA.yL71QBGA2krlK', NULL, 'Agent', 0, NULL, '2021-08-19 04:41:26'),
(99, '52003', 'GIRIJA PRASHANT TARAL', 'girijaptaral10@gmail.com', '8698353434', 35, '$2y$10$/yz9g8d7rlETVtsTiBGSJeqQWibS8BNV8J/YjQ1vq0vP3ignAHy8S', NULL, 'Agent', 0, NULL, '2021-10-20 07:08:46'),
(100, '52012', 'KEDAR CHANDRAKANT MULE', NULL, '9657645710', 36, '$2y$10$tuM71J0P1gtJ4Ro269YDQ.IX2bxxF9b/RKCIo1eTU.QpQva9Q9SXa', NULL, 'Agent', 1, NULL, '2021-08-23 00:37:50'),
(101, '52013', 'NILESH SURESH GAIKWAD', 'nilesh9370767111@gmail.com', '8767585901', 36, '$2y$10$AYdMLV1pkvUHyEnDrgVGseSS2qPdFnIcesTpBAhaeoJf1QNimTJ42', NULL, 'Agent', 0, NULL, '2021-10-01 02:47:54'),
(102, '52024', 'DHANANJAY RAJARAM JAGTAP', NULL, '9904534660', 36, '$2y$10$bGFeQLr8miZsP4cGtE/pkOusIG5PzQCJC/0ZmyxrnkXpPB4L5GKA2', NULL, 'Agent', 0, NULL, '2021-06-30 04:39:19'),
(103, '52030', 'AFSANA AJAAZ SHAIKH', NULL, '9767904844', 35, '$2y$10$PYewxm.dsM0SE.aVUE53ruKk9alz613v5VyczHvVSY.W/m37Gs31q', NULL, 'Agent', 1, NULL, '2021-08-25 04:48:57'),
(104, '52031', 'SAYALI AJAY DHAMDHERE', 'sayaliphadtare28@gmail.com', '9766838225', 35, '$2y$10$S8Fs0lbvDeOS9Or6TWfInuS9.ELtc8Ut8HEv.HvvmJBj4N6tITZVO', NULL, 'Agent', 0, NULL, '2021-08-31 06:57:43'),
(105, '52035', 'ARUN SHUBHRAO GAWARE', NULL, '9763840339', 36, '$2y$10$BWW3dPI7K45eULZpbb3OBefWVz8K4itzE3Hv/SrymA2AKwVoG62Q.', NULL, 'Agent', 0, NULL, '2021-10-20 04:43:41'),
(106, '52036', 'SIDDHARTH LALCHAND TRIVEDI', 'siddharth1trivedi@gmail.com', '8788367069', 36, '$2y$10$AzIVZ1eWFAq4R1aYAH..0e4p5td6XvnknKL2ZDVXa6ymBtAWHzjtK', NULL, 'Agent', 0, NULL, '2021-10-20 04:44:09'),
(107, '52041', 'AMIT ASHOK LONDHE', NULL, '9423447339', 36, '$2y$10$I.RHQYPey7iDVqW9y2zUyeIxWnmpOzq5IJcSLW1bIat0uSepEFu2W', NULL, 'Agent', 1, NULL, '2021-09-14 09:00:35'),
(108, '52043', 'KRUSHNA VISHWANATH NATKAR', 'krushnanatkar41@gmail.com', '8390203030', 36, '$2y$10$sjdYdS8S.U5/.kKV2rb.Ce.jalman2mxyWbpp4YAPWdbpMnS8Tv.G', NULL, 'Agent', 0, NULL, '2021-10-01 01:07:48'),
(109, '52044', 'RAHUL BASAVRAJ HANDRALMATH', NULL, '9763633653', 36, '$2y$10$/v0P/fkf54LglO6UJixzkuAzopV.0MlcwQkWCjfcTzZ6oSwTNJtma', NULL, 'Agent', 0, NULL, '2021-10-20 07:10:06'),
(110, '52046', 'SURAJ VISHWAS GAIKWAD', 'surajgaikwadsura@gmail.com', '7775090669', 36, '$2y$10$uhzFdxwdJQf7g0CxEeT/O.zj1UuJPvWEQnzQf0Mbs2dzi4hXqrLKS', NULL, 'Agent', 0, NULL, '2021-10-20 07:10:48'),
(111, '52059', 'AMANULLA MEHABUB SAYYAD', NULL, '9763771567', 36, '$2y$10$ZTvAx21DMKgKDQjJXO2vfekssCgVMw4FcIdMzt97WbwTYjoOfqQLi', NULL, 'Agent', 1, NULL, '2021-08-27 06:06:04'),
(112, '52064', 'NILESH MURLIDHAR BANDAL', NULL, '9923971363', 36, '$2y$10$OF75OLMHXGd19/0TPYtdouAH7yVnfQldrxnjFGhX3O.qH9ypsKgSW', NULL, 'Agent', 1, NULL, '2021-10-08 03:13:23'),
(113, '52067', 'URMILA VIKAS DALVI', NULL, '8149128268', 35, '$2y$10$3h3AQiq3oytBwq7u1MBYX.Ovb5q6iN5Z3ri5EaGm56EiQJL45MpM2', NULL, 'Agent', 1, NULL, '2021-08-21 08:10:20'),
(114, '52070', 'SURAJ ROHIDAS KHANDAGALE', NULL, '8379804595', 36, '$2y$10$47drNNrvpk9MS0vO.Nw7qOADrwAsozpzb8fCnfWyVOz4oPc/UK16y', NULL, 'Agent', 1, NULL, '2021-10-08 03:13:37'),
(115, '52081', 'MASIRA NISAR ATTAR', 'masiraattar16@gmail.com', '8007304909', 35, '$2y$10$oo3bx6rUdeTaOI0EuSgK3.YRNMNQrd2rDzt6o8pYeTmST/JSNkEFG', NULL, 'Agent', 0, NULL, '2021-10-20 07:14:43'),
(116, '52092', 'ARUN KASHINATH JETITHOR', 'arunjetithorgs@gmail.com', '9527473196', 36, '$2y$10$DefB60AqpyIAqwh0rw/35.5xnNv7C3ulZibFpDA/kAeTKW04dHx7.', NULL, 'Agent', 0, NULL, '2021-10-20 07:15:45'),
(117, '52098', 'UJMA HASAN BANDAR', 'buzma644@gmail.com', '8830201863', 35, '$2y$10$9e5vydD/vPnnw9ZKMoogOO1TrwfV0otjK40f2ivTmiVngcI7KI6Bi', NULL, 'Agent', 1, NULL, '2021-10-20 04:42:10'),
(118, '52099', 'SHUBHAM AVINASH GULHANE', 'shumhamgulhane096@gmail.com', '8605449366', 35, '$2y$10$il2tukZVtG52WkpAlAxZ8eOp3PVOzbn192NoMOqeYFxzamwWtSgv2', NULL, 'Agent', 1, NULL, '2021-09-11 07:49:01'),
(119, '52103', 'GAURAV GAJANAN RANE', 'gauravrane0511@gmail.com', '7057359668', 35, '$2y$10$qeDuyaqlDoAFGp.HlPqmVe9kGDGBHcMizop.ScmcSVBc.zPT1iF16', NULL, 'Agent', 0, NULL, '2021-09-01 05:52:34'),
(120, '52106', 'PRASHANT CHANDRAKANT KADAM', 'prashantkadam341@gmail.com', '7276651972', 36, '$2y$10$F0z6leqD.DuJMKho/AJhcuOG0zlUBMmR7feg1hpdg.jO7HAeX.rxm', NULL, 'Agent', 0, NULL, '2021-09-01 01:14:52'),
(121, '52109', 'Tushar Daulat Mohite', NULL, '9552085529', 36, '$2y$10$ZkAFSn.WLOQmEMqiIee8auxvl1jNrOlASE4OEvnwQCplOrUGnN64C', NULL, 'Agent', 1, NULL, '2021-10-08 03:14:14'),
(122, '52111', 'PURAV SANTOSH NAKATE', 'puravnakate@gmail.com', '8857029826', 36, '$2y$10$pdIIP6ZrHrRtIHvflNifc.vMWQh7IwuGQNOt/cDDei0fU7OlmpttC', NULL, 'Agent', 0, NULL, '2021-10-20 07:13:42'),
(123, '52033', 'GAUTAMI  AMBRISH RACCHA', 'gautamiraccha09@gmail.com', '7083562618', 38, '$2y$10$OlJ/vlhp1Okdw.ZxI58M.OYiqUJAkz9M030EiWMA/kkKgyBqtzBiC', NULL, 'Agent', 0, NULL, '2021-10-20 07:11:42'),
(124, '52034', 'PRIYANKA SHAM AARDE', 'priyankaaarde@gmail.com', '9604223094', 35, '$2y$10$DPQPejRDCw7pbQtOwRiwruZ3drJOPegk9RI4pO8TcHG/.rh13gr2a', NULL, 'Agent', 0, NULL, '2021-10-20 07:15:21'),
(125, 'OMSAI 52037', 'PANJABRAO PANDIT GAIKWAD', NULL, '9822632824', 36, '$2y$10$DukWje0ElsSwohCAUa8CCeY64VVdpZ3Un2ea8QyrVVUglt32fHYOe', NULL, 'Agent', 1, NULL, '2021-08-21 08:14:19'),
(126, '52038', 'PRAFULKUMAR PRAKASH DOLAS', NULL, '9028501984', 36, '$2y$10$UCDW4b28LpviGka0pXCQxuD3j4bfS2Mart0/wo6BblC0EoxtPTHh6', NULL, 'Agent', 0, NULL, '2021-10-20 07:17:38'),
(127, '52039', 'PRASAD RAJENDRA BAGAL', 'prasadbagal7796@gmail.com', '7796492382', 36, '$2y$10$TyLOjhrJF15iMo.szHbm5.XbN7UQ2jdt9pGDiX54IkFbywYQaSw9W', NULL, 'Agent', 0, NULL, '2021-10-20 07:19:07'),
(128, NULL, 'SAURABH ASHOK DALAVE', NULL, '8482932427', 36, '$2y$10$xTt42xUaywEganhR3II81.7d3TWHQdDhelptD0f9wEjsHTcPmhtLi', NULL, 'Agent', 0, NULL, NULL),
(129, '52051', 'SAURABH ASHOK DALAVE', 'saurabhdalvi97@gmail.com', '9765722168', 36, '$2y$10$LEkbEE7GkQzXetDO7Ngf1O4fJKSZwao8VhTu.i0qGTZLAFGbZ9tb2', NULL, 'Agent', 0, NULL, '2021-10-20 07:18:27'),
(130, '52052', 'SUNNY SANDEEP SHENDGE', 'sunny.shendge00@gmail.com', '9011323948', 36, '$2y$10$EmcnAMukXImJRu3lM9CD2OzSbxzABuD72/Ni3nn3X9c557uqpp0em', NULL, 'Agent', 0, NULL, '2021-10-20 07:19:48'),
(131, '52074', 'AKSHAY ULHAS INGALE', NULL, '8600216215', 36, '$2y$10$ra3CW7F.QTPBgi9ZhtOV3OQNE0Vxi9.XIUHQ/sELxfYmsS1l1j.Hy', NULL, 'Agent', 0, NULL, '2021-08-19 05:06:29'),
(132, '52075', 'ANIKET NITIN LIMBORE', 'aniketlimbore0809@gmail.com', '8181098282', 36, '$2y$10$BjaKw9KgJLRDnjEkOabhMu1vHBWieVBSv7WYDf5ey7sCIPRsvX/kS', NULL, 'Agent', 0, NULL, '2021-10-01 02:54:59'),
(133, '52097', 'AYESHA ALI SAYYED', 'ayeshasay@gmail.com', '9765403814', 35, '$2y$10$EVvH67p5CybRAcUcCH6Ls.MBKAPqy0bf0/VEKxux4H/GpO8IW3jgK', NULL, 'Agent', 0, NULL, '2021-10-08 07:16:06'),
(134, '51984', 'DEEPAK SHRIKANT DONGARE', NULL, '9860646874', 36, '$2y$10$n.E5wyfMwwgcyXIWRL6ameQSjCbA2V8mUWtbzl/sYiVXnbh3pWao.', NULL, 'Agent', 0, NULL, '2021-10-20 07:27:19'),
(135, '51004', 'AMIT SURESH DOLE', NULL, '9673471212', 28, '$2y$10$hhqEb6ZpXRRWw0k79QFU0.N6o8e9HJlm9sYr8ER4acTPV6BgQr.5e', NULL, 'Agent', 0, NULL, '2021-08-19 05:08:36'),
(136, '51020', 'HARESH SUKHADEV  DHAVALE', NULL, '9922912750', 28, '$2y$10$CzWprF43EJf8xkUCzlX7yOJIKou59Zt86ttz//a3dIj0/ZW4EDAFW', NULL, 'Agent', 0, NULL, '2021-06-26 02:12:31'),
(137, '51033', 'IQBAL SAKIR MOHAMMED SAYYAD', NULL, '9922131699', 38, '$2y$10$BLKc0zSgNTi3VvDUvW6ELuQULhVJh4DjPtNzoLzhGWgAfx3jXwp6i', NULL, 'Agent', 0, NULL, '2021-10-20 04:43:09'),
(138, '51038', 'KIRAN RAMDAS DHAYGUDE', 'kirandhaygude90@gmail.com', '9922269009', 54, '$2y$10$syBBtGrfS.eSQfRw.PIxyOJKCRVbClIuuP340ulbl2AxcuIwtYfP6', NULL, 'Agent', 0, NULL, '2021-10-20 07:31:41'),
(139, '51041', 'NITIN BABURAO GARAD', 'nitingaraf.6001@gamil.com', '8007448448', 48, '$2y$10$Ihyue6E6KtCbiEduP.kEEOXYkSeEBht0.vKheTUzyNkeOlVMl2AmO', NULL, 'Agent', 0, NULL, '2021-10-20 07:32:38'),
(140, '51064', 'RAJESH  BHAGWAN DHIWAR', NULL, '9850951525', 36, '$2y$10$PxOCNh3BEY5ASvDoTCKiSOLCFP/OS4ytiHIuiGyOz8/bjSwdSC.iy', NULL, 'Agent', 0, NULL, '2021-06-26 02:04:22'),
(141, '51075', 'VINAYAK MOHAN AHIR', NULL, '9552500372', 38, '$2y$10$.KP1LE0dBphM94aQ2URQXeAoXC32kHQZ9B/7upVfh4B/6Bd4taZhS', NULL, 'Agent', 0, NULL, '2021-06-26 02:01:57'),
(142, '51078', 'AKASH VINAYAK BADHE', NULL, '8329922557', 54, '$2y$10$AW/D50ydRAMOoYAIlqdcEOr5EA8kihrUcwZ0DegvQbWjiS20DJmJS', NULL, 'Agent', 0, NULL, '2021-10-20 07:49:36'),
(143, '51120', 'KIRAN SURYAKANT NIMBALKAR', NULL, '9923352525', 36, '$2y$10$BRmEbqxKru/OXb0gMnE/kezwbo8HHjd72ZlsMHdPRtIHpN1Bi5utC', NULL, 'Agent', 0, NULL, '2021-09-08 04:30:46'),
(144, '51149', 'POONAM MADAN PARDESHI', 'pardeshi.poonam28@gmail.com', '8484046740', 44, '$2y$10$9k1sKtCuSkusZF.g3DSyX.on7pHWUUHML1dzBf/BDof3gCUSnz0zq', NULL, 'Agent', 0, NULL, '2021-10-20 07:50:51'),
(145, '51161', 'NILESH LAXMAN SATAV', NULL, '9850951511', 36, '$2y$10$TtP8bqwxd2tFxM9CYcKIneBSQe.Jl.mCUl6n4yJ/BKMgvsamcJWGi', NULL, 'Agent', 1, NULL, '2021-08-25 04:58:56'),
(146, '51181', 'PRASHANT DATTATRY KUMBHARE', NULL, '7066216269', 36, '$2y$10$015j.NSK8H97QAbIF8RtOuC9nXPFj/9UOWtUuNVE2Pd7kLL7XKz/C', NULL, 'Agent', 0, NULL, '2021-08-19 05:17:50'),
(147, '51197', 'RAJU DADU DANANE', NULL, '9325128976', 36, '$2y$10$WMra1roNvH.83pQuzE9S0O2XQQhPHnzh1fdMmXuJ0AbhdhDzEuXla', NULL, 'Agent', 0, NULL, '2021-06-26 02:00:20'),
(148, '51209', 'SAGAR ASHOK BHANDEKAR', 'sagarbhandekar5112@gmail.com', '9146466606', 28, '$2y$10$LR3Q8cHPO3Kduf9B8/ZxDOid7f/oNJD26PncFcaOAsCw9H6kHGRYS', NULL, 'Agent', 0, NULL, '2021-09-03 06:23:37'),
(149, '51244', 'TRUNALI SANTOSH SANGAM', NULL, '9822089623', 38, '$2y$10$pzQewy2QJD6806QQLEYk9uWeHrWd4h8yH75spHQMwHEm85cfwZ3xK', NULL, 'Agent', 0, NULL, '2021-07-06 23:22:10'),
(150, '51258', 'SUBHASH DIGAMBAR MOHALE', 'subhashdmohale@gmail.com', '8805499191', 48, '$2y$10$hVl2ADZbB4Ym47lCxcC0WucAQzaf6L729Ljvr0arth/0w.lQbvSbm', NULL, 'Agent', 0, NULL, '2021-09-03 06:12:07'),
(151, '51304', 'NITIN BABANRAO SATAV', NULL, '9822835285', 36, '$2y$10$0B6skR7dy7XdBAYeEysSiegY9ChGUxuk/Wcj52Mnepk0ds.Vip47G', NULL, 'Agent', 0, NULL, '2021-06-26 02:00:47'),
(152, '51314', 'NAVIN RAGHAV HEGDE', NULL, '9922112824', 36, '$2y$10$Bj55sHJlW0QpbAtXGVQ8fumg7U.0JtsWxmnkZy1HrXLR.LdprDFh6', NULL, 'Agent', 0, NULL, '2021-06-26 02:07:59'),
(153, '51318', 'NITIN DADASAHEB PAWAR', NULL, '8308828842', 36, '$2y$10$GQxqZrrrNa.V8m2LpACGIe04cMOBBsSXkgIoYsyK7Xoa6zcZ9I.UW', NULL, 'Agent', 0, NULL, '2021-05-14 02:17:00'),
(154, '51320', 'SABA ASLAM SHAIKH', 'shaikhsaba297@gmail.com', '7721940781', 38, '$2y$10$5CYQqepbmeDVy3NjfXaOQOksB/wKep4DchVqbbsAb3CyN8YfCPwV2', NULL, 'Agent', 0, NULL, '2021-09-13 09:18:43'),
(155, '51360', 'DURGESH RAMESH TELGU', NULL, '8380804446', 36, '$2y$10$FDdGeWr4vfRjZDQUfo4ckeliSN3xUv8jCqNXbsu9XyyRmMQEdcRoG', NULL, 'Agent', 0, NULL, '2021-08-19 05:23:27'),
(156, '51361', 'OMKAR SANTOSH PATIL', NULL, '9028749314', 36, '$2y$10$GiPomVzcfMyNQs2gxPDEAePFil/f5SNEYSplKRhK.2R4ZaMRZBaia', NULL, 'Agent', 0, NULL, '2021-06-26 04:14:46'),
(157, '51454', 'RAJESHREE RAJENDRA SHINDE', 'shinderajeshri2178@gmail.com', '9145279048', 38, '$2y$10$u5S6TCPGis2I7KfFhzcRiO55vME94kfASLDYQpcg64DlfAYAvQHWS', NULL, 'Agent', 0, NULL, '2021-09-13 09:14:21'),
(158, '51478', 'RUSHABH TUKARAM KAMBLE', NULL, '8766521306', 36, '$2y$10$h/b0AgPfMUYzFhKwwGsOLekM88BBilmynH.HOZ7I1q8OmNIHjR9MO', NULL, 'Agent', 0, NULL, '2021-06-26 04:01:34'),
(159, '51481', 'DEEPALI PRATIK BANKAR', NULL, '7620600954', 38, '$2y$10$6y5nHfUCoCmEUE8h4OAmuuzNN53tJCikcel4qilocJbYuR6D6RTEy', NULL, 'Agent', 0, NULL, '2021-09-13 09:36:56'),
(160, '51483', 'SANDEEP SHANKAR SALEKAR', NULL, '9604304376', 36, '$2y$10$iEo2tAtyamWKPjOj3XesOul6cp7Fot6jRIhujP8hIQzvtw3ZzQyma', NULL, 'Agent', 1, NULL, '2021-10-21 01:12:04'),
(161, '51518', 'VICKY DELIP HOLE', NULL, '8605955140', 36, '$2y$10$XscaKjiJc61GJuX/i7VyaemFRlF97win5Hlqmt2tihenPQ4TRU8.6', NULL, 'Agent', 1, NULL, '2021-09-15 04:42:11'),
(162, '51527', 'JAGDISH LAXMAN DATKHILE', NULL, '7721907722', 36, '$2y$10$wg3PsxcEDXvaHkNMpHnixeoPZ4rjIZuJ9mdUn88lpYA12eg3OmkfW', NULL, 'Agent', 0, NULL, '2021-06-26 02:20:10'),
(163, '51547', 'MAHESH BHARAT BALWAR', 'maheshbalwar01@gmail.com', '7447475008', 36, '$2y$10$X0pFtpFBGmJd.sqShiQSp.Z/IYHtNOVb/pZWZTqKfryhVgZhtARlm', NULL, 'Agent', 0, NULL, '2021-09-03 06:11:39'),
(164, '51550', 'VINOD MAHADEV KARDE', NULL, '8788171704', 38, '$2y$10$NiemZ/7atlrRInXA9FoPPOji/itXg7xyJmjCisMGGE4wyS3S6W0My', NULL, 'Agent', 0, NULL, '2021-06-26 03:09:02'),
(165, '51553', 'PRASAD RAJARAM  ZUTING', NULL, '9922355229', 36, '$2y$10$4zlTejfzD1bgGO16aTZPBuDUvs8C4ipDmg6ySM2gr9eM9dHKXY93O', NULL, 'Agent', 0, NULL, '2021-08-19 05:33:02'),
(166, '51574', 'SHAILENDRA MOHANSINGH RAJPUT', NULL, '9822602713', 38, '$2y$10$4zlOLwwDdoa/eSBhJJYv.uH6gqCN3Qyxpo0uUqnoTotI2gbHMXqQm', NULL, 'Agent', 1, NULL, '2021-08-21 07:37:42'),
(167, '51581', 'KADAMBARI SUHAS BHILARE', NULL, '9623520841', 38, '$2y$10$10VIDhh4PGQGmcNExDhUuOP5U5hvXCdDBYFzikLGcyXOeSwm3UmBC', NULL, 'Agent', 1, NULL, '2021-08-23 05:21:55'),
(168, '51617', 'LAKHAN DEVDAS CHAVAN', NULL, '8888883898', 36, '$2y$10$ftpKi4YIL8m0cMbEEY.b2.sI0uWcLaG1os8GoWlSbnnn5eyG2ABqG', NULL, 'Agent', 0, NULL, '2021-10-20 07:57:02'),
(169, '51619', 'SHRAVAN VIKAS JADHAV', NULL, '8668746993', 36, '$2y$10$lGSSw/.fXSYIMgLwkCZdPuvu6w1eQXPc.yWlSBJB7a31HB6l6UivW', NULL, 'Agent', 0, NULL, '2021-06-26 03:58:06'),
(170, '51627', 'PAPESH RAGHU KATURDE', NULL, '9960228824', 36, '$2y$10$uaQYO9MncILCoYk3JTk3ZOiUkZ4NkwWyVYYKF70H3xZfzAh49RvIa', NULL, 'Agent', 1, NULL, '2021-08-18 07:44:23'),
(171, '51704', 'PALLAVI NAGESH BANSODE', 'pallavibansod1996@gmail.com', '9307902950', 38, '$2y$10$i/cm0jksvtQ7DKQuWFwf5udpv8KP4rUgbAxXzmkNUFNPgG2lHeO0m', NULL, 'Agent', 0, NULL, '2021-09-09 07:27:13'),
(172, '51707', 'PRASHANT ANIL TIKHE', NULL, '9561262661', 36, '$2y$10$pPUm3zPloOzi.KWJl0oAM.WGKYZs7I/lqUDQCkl1KEUu0/4MA1iRq', NULL, 'Agent', 1, NULL, '2021-10-08 03:13:10'),
(173, '51844', 'SAVITA MAHENDRA SURYAWANSHI', NULL, '9623392696', 35, '$2y$10$SzpQun6BynXq181USJAoAeOBCfoFl3qlWznt.pMJbpgQ3UrBMQgHu', NULL, 'Agent', 1, NULL, '2021-08-18 07:44:29'),
(174, '51846', 'SHRINATH  ASHOK MEMANE', 'shrinathmemane1271@gmail.com', '9359686179', 35, '$2y$10$FVwOM9O5tBbgTFyG1BebEO9gwDQovMVIKVU8zjOSajbABFT60waSm', NULL, 'Agent', 0, NULL, '2021-10-20 07:55:41'),
(175, '51871', 'PRATIK SACHIN KADEKAR', 'kedekarpratik@gmail.com', '7261985454', 36, '$2y$10$rLhodPtWQKxbaxhjdh9J9uMz7Ne0cZIfYJYjL762WvJNKmx/AlO3q', NULL, 'Agent', 0, NULL, '2021-10-20 07:57:51'),
(176, '51874', 'JALINDAR LAXMAN KAKADE', NULL, '8830625976', 48, '$2y$10$Ia0vU8D6CmsMdINLRoQYSuML05H8ccY5EMcZCYhK1/drd.xJalYzm', NULL, 'Agent', 0, NULL, '2021-10-20 08:02:43'),
(177, '51926', 'YOGESH SAMBHAJI LOKHANDE', NULL, '9850062481', 36, '$2y$10$VAZfDdiw.GL.7YdS1m8oleuzRTArAqupSuS5X95cH7RwjL5Q9QdrS', NULL, 'Agent', 0, NULL, '2021-10-20 08:01:50'),
(178, '51955', 'POOJA BHARAT BHUSHAN TIWARI', NULL, '9307909964', 35, '$2y$10$8J0LwHrRIToAMV4YIHiCSu4WpTP9JDhpfBKl8OShDcP7jhbAyuREW', NULL, 'Agent', 1, NULL, '2021-09-09 07:55:46'),
(179, '51986', 'MADHURI KISHOR OSWAL', NULL, '8208402479', 35, '$2y$10$VHQDemuMuQ1X8UEiT2HYA.VTfjLy0HWp7O0Bn8NPQssE3XpkGqdRC', NULL, 'Agent', 0, NULL, '2021-10-20 08:15:00'),
(180, '51997', 'SAHIL SURYAKANT LONDE', NULL, '7447774414', 36, '$2y$10$NhlMxn.amrnBd34VLJNdcOGtAgCKcFWT1Zbf7F8v637kKkSmfaiAq', NULL, 'Agent', 0, NULL, '2021-10-08 05:49:23'),
(181, '51998', 'RUSHIKESH  RAM PHULE', NULL, '8956660143', 36, '$2y$10$lyuN/oOK0phkSSfrNZJKXeNGTFC6wVzFtVkuUCPwAeoJeSOcSVaza', NULL, 'Agent', 0, NULL, '2021-10-20 08:20:36'),
(182, '51999', 'MAYUR PRAMOD AHIR', NULL, '9011372855', 36, '$2y$10$B4leYGUAU3XICh4DRX6Dd.EF2hCJW23st0xkFTFhL8SmE/3Y1ZLZa', NULL, 'Agent', 1, NULL, '2021-10-21 01:12:24'),
(183, '52077', 'SUMIT DEVAJI VICHARE', NULL, '8149382112', 45, '$2y$10$dlz.3ZlDyB8/AdmFrD9JoeRrCKTR4fwDxubRDeh4R4K./2D.LJE6a', NULL, 'Agent', 0, NULL, '2021-06-26 03:56:07'),
(184, '52080', 'MANISH AKASH KATARIA', NULL, '9321522707', 35, '$2y$10$EEJgPdl1fPfR.jwdupzk/OQLNOKisU4nncBGlLrYgAEvIHAUNcMv.', NULL, 'Agent', 1, NULL, '2021-08-18 07:45:06'),
(185, '52083', 'ABHAY BAPUSAHEB KALAMKAR', NULL, '7620390520', 36, '$2y$10$S71ITm2K7ZoWRZUHhJZsaOdWAk3TBy5XipH2z7jYBqNErjKCbfxSO', NULL, 'Agent', 1, NULL, '2021-08-19 05:18:52'),
(186, '52087', 'ADESH KHANDU JADHAV', NULL, '9890930924', 36, '$2y$10$SFgQCf7DQkZ0YXTHPr1tsO/9MGvf2NJPffxsmuW8i0UQ5QtfeFUQ2', NULL, 'Agent', 1, NULL, '2021-10-08 03:13:54'),
(187, '52093', 'PALLAVI KAILAS SALUNKE', 'pallaviksalunkhe2611@gmail.com', '7798159322', 35, '$2y$10$Xudnb9Du2ZcrtWDdSjXEm.33hxMneNiNIt0.4C4.zc3jLWuWEoAru', NULL, 'Agent', 1, NULL, '2021-08-18 07:45:19'),
(188, '52102', 'SAGAR PRAKASH SHINDE', 'sagar.shinde5432@gmail.com', '8408066867', 36, '$2y$10$7GmCzF7FO/0MkkbmenRsUuooJlml9ts9mfL6TnIZ6.BiMkrtUwSX.', NULL, 'Agent', 1, NULL, '2021-10-08 03:14:04'),
(189, '52105', 'HARISH DATTATRY OTARI', NULL, '9373708922', 36, '$2y$10$QJEV8frpsX4hRQnVDlgFm.f/rWSPcpcWEYH/0wg/qqRcAkGOLCMvy', NULL, 'Agent', 1, NULL, '2021-08-18 07:45:26'),
(190, '52108', 'YOGESH YADAV HARGUDE', 'yogeshhargude905@gmail.com', '9970116767', 36, '$2y$10$ZAJY1zskVTd5uUJVqSLlB.aU6i06SA/btcFztImvqt1xjwLpIiTH6', NULL, 'Agent', 0, NULL, '2021-08-19 05:52:28'),
(191, '1003', 'GANESH  AATMARAM BHOIR', 'ganeshbhoir11@gmail.com', '9137585122', 31, '$2y$10$tbPQpVaZPQjOMTPuRq57fOGpGIcLZ9J0hBB0AgudRpZSKooHXM4xa', NULL, 'Agent', 0, NULL, '2021-09-11 04:42:24'),
(192, '1002', 'NEELWANT SHIVAJI BANSODE', NULL, '8180982248', 36, '$2y$10$p2q2/yw1t6S7effhqni88.EILLW2GgDtFzwOwaSv1GS.CnUi3Iv/i', NULL, 'Agent', 0, NULL, '2021-06-26 04:49:56'),
(193, '1008', 'ROSHAN SKARAM PATIL', NULL, '8928933199', 36, '$2y$10$YVa4kKQ8/p/RSN2cu6WMiuZKgxWt28tv1bL94RDCTaDj9tx5hdLPO', NULL, 'Agent', 0, NULL, '2021-05-17 03:28:45'),
(194, '1017', 'MAMTA PRAKASH MESTRE', NULL, '9137247481', 35, '$2y$10$b8YxB7o9bVDmpCNDv4f9BuBefdtfIAPiW4amsJPxYC37aBftDiVnC', NULL, 'Agent', 1, NULL, '2021-10-27 04:44:23'),
(195, '1037', 'LAKHAN BHIMRAO PAWAR', NULL, '8828958808', 35, '$2y$10$Dn.bFa0OMhFjRjt28IrzwujsSJOhBytNQLXqe8d9npWIx8FTrrdCe', NULL, 'Agent', 0, NULL, '2021-06-26 04:57:29'),
(196, '1038', 'RAVIKUMAR ANJAIAH MANGILIPELLI', NULL, '8530039362', 36, '$2y$10$iAlcy9bMXEPfa274XoSjHOPgZ.IQfGApBhaysT8OJc4b35aCMtcJK', NULL, 'Agent', 1, NULL, '2021-09-11 06:38:15'),
(197, '1041', 'RESHMA RAMCHANDRA THOMBRE', NULL, '9321700716', 35, '$2y$10$lmSs3RDkI8AxTtWT9NIb8uIXGoCHhY.AKM90j.pSs/rh6z0ZAtrCu', NULL, 'Agent', 0, NULL, '2021-05-17 03:26:12'),
(198, '1042', 'CHANCHAL PRAKASH KEDARE', NULL, '7304066640', 38, '$2y$10$8DlkptwJHa7LyUedfH/km.EiZauq/9VYhMQsPz1PSECozIGAQPlDm', NULL, 'Agent', 1, NULL, '2021-10-20 01:22:26'),
(199, '1043', 'LAHU SITARAM APARAJ', NULL, '8424846685', 36, '$2y$10$fbT9j/Rn6osjwGzBDNS92OjS697QmIovDXetEuJ1lZtPaYoPkLg5O', NULL, 'Agent', 0, NULL, '2021-09-11 05:54:01'),
(200, '1046', 'KOMAL BHARAT DHOKALE', NULL, '9867270107', 35, '$2y$10$ndEvddflqS52KVTttM32M.V2j1NatkMjf1UupAw/PLtxsDSDNIi5y', NULL, 'Agent', 0, NULL, '2021-05-17 03:36:55'),
(201, '51894', 'AMOL  ATMARAM SHINDE', NULL, '9076067250', 36, '$2y$10$xkjnAPlHUcRynddhu88Zq.wZjbhMy6ipXP7yBW6VIVWCX084vi9JO', NULL, 'Agent', 0, NULL, '2021-10-20 08:05:27'),
(202, '52011', 'RUPAM GAJANAN BHOIR', NULL, '9321817977', 36, '$2y$10$hzbGYMGswYVom2xFSbOOV.2YERbpWu7wSXlNuRbxVrXhour2MH9su', NULL, 'Agent', 0, NULL, '2021-08-20 06:48:36'),
(203, '51323', 'FIROZ HASAN SHAIKH', NULL, '8180080037', 36, '$2y$10$udmj5Eue9Ch8y9re/p8lxOnXQ3HdIS5XHpJWTk5n7P1MYSdKWbowW', NULL, 'Agent', 0, NULL, '2021-08-19 05:59:06'),
(204, '51516', 'KOMAL MACHINDRA JAGTAP', 'komaljagtap78@gmail.com', '8308615582', 38, '$2y$10$NoaRR/XdXnD.gQiojJSwYOHhT6IwjiGlVZwKsuac.F9bg0553efwO', NULL, 'Agent', 0, NULL, '2021-08-31 07:17:13'),
(205, '51618', 'SANTOSH SHAM  KENGAR', 'santkunal93@gmail.com', '9923888148', 36, '$2y$10$aDirMBroDka5emUBjyCl7ult2dLsWXI9RZV8qbEjVcDW7gVFtScEG', NULL, 'Agent', 0, NULL, '2021-09-01 02:21:24'),
(206, '51683', 'VIKRANT SANJAY KUDALE', NULL, '8793733349', 36, '$2y$10$98EEo2lCK4KHqMRRGH7hyOUnDtdD1b02JNIEIg5Ydka.NMYdjZxhK', NULL, 'Agent', 0, NULL, '2021-06-26 03:55:39'),
(207, '51734', 'RAVINDRA  SUBHAM PHADTARE', NULL, '9011313555', 36, '$2y$10$NZnnoW/Q/O6ROjbdUbZWFuHkcXWGwL/8Nf/DR8Q5LVef1m3CZNaM2', NULL, 'Agent', 1, NULL, '2021-10-20 08:00:09'),
(208, '51822', 'HARISH PANDURANG GHOSALKAR', NULL, '9767021632', 36, '$2y$10$H11IpqfjBCpYAh.J9jMiYOxhYSKM.1nZWH7IbzLOoo9FFx9aaqn5u', NULL, 'Agent', 0, NULL, '2021-06-26 02:04:53'),
(209, '51842', 'MAHESH VASANT RATHOD', NULL, '9158606249', 35, '$2y$10$kn4O8eY.68v7ng4Dz7FZIesginY7hSp5hjeDFsv0By1YTjpiKVbOm', NULL, 'Agent', 1, NULL, '2021-08-21 07:39:01'),
(210, '51942', 'SHRADDHA SUHAS GAIKWAD', NULL, '9834873061', 35, '$2y$10$OQeS9wyKkbHVdWV0yKTGDeQyIFY6U/DIMrsYqS0hZO4VIi.6VojEa', NULL, 'Agent', 0, NULL, '2021-10-20 08:13:12'),
(211, '51947', 'HARSHAD BAPUSAHEB TAPKIR', 'harshtapkir6001@gmail.com', '9273601601', 36, '$2y$10$C5BPQ2bxXsLzlkeWn.J0ouFhcMxn6WtlBcFtyIk6Bt6aRPqS/OMhm', NULL, 'Agent', 0, NULL, '2021-09-01 04:29:49'),
(212, '51948', 'BALAJI MADHUKAR ALHAT', 'balajialhat42@gmail.com', '9021871832', 36, '$2y$10$CuIBrCp5vIpAqlokbMqq0e//sUDa72LG1tHk/jCfOo69GxdXA6l6e', NULL, 'Agent', 0, NULL, '2021-09-01 05:09:15'),
(213, '51922', 'AKASH ANKUSH SHILIMKAR', 'akashshilimkar27@gmail.com', '8208575949', 36, '$2y$10$F9LOEfnUYP4PUECqehplB.B/Ny148DYnj0nzSmkvGY//misQUn5yu', NULL, 'Agent', 0, NULL, '2021-08-31 07:02:05'),
(214, '51923', 'HEMANT ROHIDAS DHEDE', NULL, '7757822775', 36, '$2y$10$zDg3QnnVB6iTOxGQBhbsReYPrVeExSrY52ziL.Y0exbmpNv1a01RG', NULL, 'Agent', 1, NULL, '2021-08-18 07:48:08'),
(215, '51944', 'POOJA LAXMAN KALPANDE', 'poojabari751998@gmail.com', '9503200267', 38, '$2y$10$B/PAPOr7DAxKU9bCRRoo.OLQRMBuN.fkdXt66RBtBnC/2rpnw8LPu', NULL, 'Agent', 0, NULL, '2021-10-20 08:13:46'),
(216, '52008', 'JAYESH SURYAKANT NIMBALKAR', NULL, '9850951523', 36, '$2y$10$MMkBDHTbTjLcM8ji09clG.1uk3xudHrQWIXI5cD2cS38Luud22j8e', NULL, 'Agent', 0, NULL, '2021-08-19 06:07:11'),
(217, '52009', 'ABHIJEET DATTATRAY JATHAR', NULL, '9921104036', 36, '$2y$10$grVThPlzSoAhY/Sgtza/B..UohSikdCOsxuj58IyvIQpZhcQfoKpu', NULL, 'Agent', 1, NULL, '2021-08-21 08:06:41'),
(218, '52027', 'MUJAMMIL ABDUL SHAIKH', NULL, '9730836043', 36, '$2y$10$cSNmm751gPchj/bNj4zvCOniKSJiYJU3iPqOStjFzORkIt.kq552m', NULL, 'Agent', 0, NULL, '2021-09-14 09:01:45'),
(219, '52028', 'SANDEEP PANDURANG PASALKAR', NULL, '8605052929', 36, '$2y$10$JOWy2spBscKJDD1TkijSGuRrUjj261TrECBxsRPN.apqzitMI6e0a', NULL, 'Agent', 0, NULL, '2021-06-26 02:10:50'),
(220, '52029', 'SHAMRAO VASANT FASALE', NULL, '9822099464', 36, '$2y$10$aKclYWCKGVAvvMGxxbF45OxY3cb3uF7hBPshdkL2.LtHoynOBRobO', NULL, 'Agent', 1, NULL, '2021-08-18 07:48:27'),
(221, '52048', 'PRASHANT PRAKASH DOLAS', NULL, '9881750999', 36, '$2y$10$QHgY.xEPc1y.GQz3hwKUuOxm4./bUp5B2jbwUJ5eQBxEiLrpN4jOS', NULL, 'Agent', 0, NULL, '2021-06-26 02:32:33'),
(222, '52049', 'PRAVIN YAMANAPPA GAIKWAD', NULL, '9890375639', 36, '$2y$10$mkROTwK3SowC956ci0C6Qu3wZbtB4zId5cMl73XGQh5r9jYpwsqQm', NULL, 'Agent', 1, NULL, '2021-10-21 01:11:28'),
(223, '52100', 'ASHOK DNYOBA PASALKAR', NULL, '9075221010', 36, '$2y$10$zGRq2XHCJmh5w61E06yEPeq83dR4q1NjkbkuzlR/j/GshqD9Jo10e', NULL, 'Agent', 1, NULL, '2021-10-21 01:12:39'),
(224, '52113', 'RAKESH CHETAN SUNGAT', 'sungatrakesh2@gmail.com', '9309994614', 36, '$2y$10$ThmfipyzYpbOeWVZXVf1xuTX0OEYQ.SdgFDp.CU.DGV4a3wLzPf.2', NULL, 'Agent', 1, NULL, '2021-10-08 03:14:45'),
(225, '52114', 'GOPAL GURU PANDE', NULL, '9021607007', 36, '$2y$10$ozfLWWLzJAiuODEcsfV/cuf/LDUAJeGCrBKqT7coaVnPfn6btoQuC', NULL, 'Agent', 0, NULL, '2021-08-19 06:13:29'),
(230, '52073', 'AMIT BALKRUSHNA SATPUTE', 'amit@omsaicredit.com', '9850900800', 42, '$2y$10$jFsGkBNJ7OyzTNNDR061V.7OLy1NRIQzl86p91aj2ir/fKYaoDzhG', NULL, NULL, 0, NULL, '2021-09-18 02:56:36'),
(231, '52002', 'MANASI JAGDISH BHANAGE', NULL, '8855817186', 35, '$2y$10$ZwkcZoW8AFoqRkH7T8a/deFQ5o3mlQFPAxulnwlzZyZwusJpdqx46', NULL, 'Agent', 1, NULL, '2021-08-18 07:48:41'),
(232, '51841', 'ATUL RAGHUNATH BHALERAO', NULL, '7350668395', 35, '$2y$10$f.Xx.lgDh3Z1mlGcJf.WSeATLHK.8d57uWB6uWNwfr3mXfJ8vgLpK', NULL, 'Agent', 1, NULL, '2021-08-21 07:38:44'),
(233, '51703', 'PRAJAKTA ABHIJEET JATHAR', NULL, '9307902957', 35, '$2y$10$2AquYLfus2P7alYiOujRuuxXlj5CDfd.5X/lvDHUc35RCN3I.rgSK', NULL, 'Agent', 1, NULL, '2021-08-18 07:49:03'),
(234, '52118', 'DEEPAK DEVRAM KHEDKAR', NULL, '9762850860', 36, '$2y$10$0pQ2wuptFhlQFn1UffQ4COq9ud/MsbtZxT1or7aytKGTmM3/nkobi', NULL, 'Agent', 0, NULL, '2021-10-08 02:07:00'),
(235, '52124', 'SUSHANT RANGRAO PAWAR', 'procees..omsai@gmail.com', '9975555999', 44, '$2y$10$EHaibhL8nIFWHYVssGf21eGzmEG2G4su0H/stYq0Xf6PoH7imEV1a', NULL, 'Agent', 1, NULL, '2021-08-18 07:49:18'),
(236, '52123', 'GAURAV NAVNEETLAL BORA', NULL, '9822233100', 36, '$2y$10$szdY1B9rehFRF4K/ftxdFeXt6tjNT6ImnRGYan/yv89WxZrSmbJcq', NULL, 'Agent', 0, NULL, '2021-10-08 02:09:43'),
(237, '52117', 'SUSHIL KASHINATH JADHAV', NULL, '9021010746', 36, '$2y$10$CSwQO31oTSEFsXsT2wz0ge8iUwiRwgBTt6zmaJKmFvyDlPbahLa5K', NULL, 'Agent', 0, NULL, '2021-10-08 02:05:54'),
(238, '52110', 'SAURABH BHIMRAO KALBHOR', 'saurabhkalbhor1997@gmail.com', '9145316466', 36, '$2y$10$DniL.WNBjmr/gUlOWpfkDOU4e81EeUMWSKkOWtNDeH6t3o/XnRd0a', NULL, 'Agent', 0, NULL, '2021-10-01 01:43:32'),
(239, '52134', 'ISMAIL IQBAL SHAIKH', 'ismailshaikh512@gmail.com', '9960183243', 36, '$2y$10$o/qA6PuDZII0teUXe3GCxeGfbfJQjv4/liWUlNJwt508K0FFrukI2', NULL, 'Agent', 0, NULL, '2021-09-23 03:13:14'),
(240, '52128', 'Ajaykumar Dinkar Dhamdhere', 'ajaykumardhamdhere@gmail.com', '9604604004', 36, '$2y$10$HznmpYXeloq4y3KYitTeNOV1JwKkDwkcFxb.jCi66L3O4pfy26ESG', NULL, 'Agent', 0, NULL, '2021-08-31 07:26:48'),
(241, '52135', 'SOHAM ANIL KHAWALE', NULL, '9307698515', 35, '$2y$10$1R7CpkSA87oUrWxKrR5gdekzbcKyX7ZwyrmqJi.GML03qppb0nFmi', NULL, 'Agent', 0, NULL, '2021-08-21 08:11:26'),
(242, '51921', 'PRATIBHA PARSHURAM UGALE', NULL, '9970711532', 35, '$2y$10$KcFdJS9xTrkX2K5i1cI5Mes2cjDiwVs7MN0T3nP0kLQlN9AEjsf5i', NULL, 'Agent', 1, NULL, '2021-08-28 04:28:17'),
(243, '51850', 'MAMTA KISHOR LONARE', NULL, '8482910080', 35, '$2y$10$zbKfPIj77RLuwZdW5mF7cuZE8LLgtOKmO11onghM9d0cPFZUB8iHm', NULL, 'Agent', 0, NULL, '2021-09-14 03:58:57'),
(244, '51936', 'TEJASVI RAMAKANT SHINDE', NULL, '9028412282', 35, '$2y$10$9Rdsa/NNaKQYzZleexvEmOyvhuZB7laKqRx39ZxHdEWv5LYVIcPUW', NULL, 'Agent', 0, NULL, '2021-10-20 08:11:05'),
(246, '51964', 'SUNITA PANDURANG DHEBE', NULL, '9373324874', 43, '$2y$10$dMgdZLGKf4A7DQ/nCc2/Jesg6QlfM4tgHZ.1T4YO9zzSg5Wbp6lgy', NULL, NULL, 0, NULL, '2021-09-08 04:16:15'),
(247, '52116', 'SAMEER RAJARAM GHOPANE', NULL, '9850555757', 36, '$2y$10$blWO3zoEn.zjiVoxcOjuEe35ubXBd8SUmGhaHUK7cjS41sjMeI3JS', NULL, 'Agent', 0, NULL, NULL),
(248, '52137', 'PRIYA  SUDHIR AHIR', 'nair.priya0404@gmail.com', '9823801967', 40, '$2y$10$w9pEQI8SQ6q0fKZNOzHMveQWkkyVdP26/WBPrycdns/4DyoHLW4Qq', NULL, NULL, 1, NULL, '2021-09-11 07:32:32'),
(250, '53139', 'VARSHA SUBHAV HIWRALE', NULL, '7448030195', 35, '$2y$10$qKKcCAS3lbDKDAKUQaEYte1Nj9mrkVc.8h456vEOnOYwW5jO1qs0G', NULL, 'Agent', 1, NULL, NULL),
(251, '52140', 'SMITALI  RAMESH BIBWE', 'smitalibibwe09@gmail.com', '8007763669', 35, '$2y$10$XsQcUut5mS0ffuRCaVygYOTrsYqO.sDo5NCNuxq.dTPvu9jcsKeIi', NULL, 'Agent', 0, NULL, '2021-09-23 03:55:07'),
(252, '52141', 'SHRUTIKA RAJESH SABLE', NULL, '9356124707', 35, '$2y$10$AzDD5jzJBTYfz5W2cM8hK.EcnrlyXLrPLEYmjGO/CkkhH1CipGb7K', NULL, 'Agent', 1, NULL, '2021-08-21 08:13:08'),
(253, '52142', 'RUPALI GULABRAO MANE', 'rupali92008@rediffmail.com', '9834772897', 35, '$2y$10$f0AUo6M3ZIP4I/Qq8J81Qebqc4vTDAC7Cj6cFFV8C0RW70i2ATl86', NULL, 'Agent', 0, NULL, '2021-09-23 04:01:27'),
(254, '52143', 'DIPTI SUDHIR BHAMBURE', 'dipti.bhambure85@gmail.com', '7248915529', 35, '$2y$10$eHCucOZDB0sqsBQMpedjAOS1XJWhrKMKs9hfgp2ABxTIXwOMGdp0q', NULL, 'Agent', 0, NULL, '2021-09-23 03:37:54'),
(255, '52144', 'ANKITA SATISH NAGARE', NULL, '7410540091', 35, '$2y$10$RCTbcsjTJmYP5XDKsyRsPO8szm5tKslMXTfrFVnYQdVScEUpuwive', NULL, 'Agent', 0, NULL, NULL),
(256, '52132', 'YOGESH ASHOK SORATE', NULL, '9922950155', 36, '$2y$10$wgpoM.Lk4zUka4WwyXLlEOrLC17Um9Afs/2yUIohS0BN4krQ6AIre', NULL, 'Agent', 1, NULL, '2021-08-18 07:49:54'),
(259, '52133', 'AMIT SUHAS SALVE', 'salve.amit123@gmail.com', '9158158128', 36, '$2y$10$Eo69AwyLGg4hkEhJjWRmROzp0x89n6PfPU/BjCvuKr5Yxpm//eTG2', NULL, 'Agent', 0, NULL, '2021-09-23 03:22:19'),
(260, '52139', 'VARSHA HIWRALE', NULL, '9156536660', 35, '$2y$10$OZ035cCmr1xaJWclJ7cSxuF4CN6U.LZDBUwzacN3PJ1YlcoNmjYQS', NULL, 'Agent', 1, NULL, '2021-08-09 08:53:52'),
(261, '52162', 'OMKAR GANESH GADE', 'omigade3888@gmail.com', '7057291274', 36, '$2y$10$ymA6Lm44cqJriWPPMFMzS.B7QKS6GQ.FPXkZkhNncZGlrai9oGlMu', NULL, 'Agent', 0, NULL, '2021-09-23 09:02:56'),
(262, '52129', 'MARUTI VITHOBA HATEKAR', NULL, '9022033436', 35, '$2y$10$zQmt5wmEkoq3rCCdMTspwOwVZYO96DY/CAwTrjfYLFy0apk41b6vu', NULL, 'Agent', 0, NULL, NULL),
(263, '52131', 'LEENA MANISH PUNJANI', 'leenapunjani1234@gmail.com', '7517355602', 35, '$2y$10$vWBJhBWe.mZKbwlPyu153uInRW5wDiFnx0VASOp58FdJSy0r54Uom', NULL, 'Agent', 0, NULL, '2021-10-08 02:11:32'),
(265, '52171', 'MOHIT SANJAY BHORKAR', NULL, '8983352440', 34, '$2y$10$Qrc60Du01vHF8fgR5GRQz.Lk6tlCIJaMNgdN3qKerW/S4oOVwTS.a', NULL, 'Agent', 0, NULL, '2021-09-11 07:54:22'),
(266, '52170', 'NAGAESH BABRUVAN HUMBE', NULL, '9657011286', 36, '$2y$10$Q5jzA6EW1ty62ZqNWPV8GuiV0ugUt4/aXa91RgLO.Yd1XpenEqb4u', NULL, 'Agent', 0, NULL, '2021-10-08 02:23:14'),
(267, '52173', 'POURNIMA MAHADIK', NULL, '8668410184', 34, '$2y$10$ycjQqzOATqEjMd8zaLepNOI9UWpA3Rc29GKpcKqmGU7zNZwTxAAia', NULL, 'Agent', 0, NULL, '2021-09-01 07:41:26'),
(268, '52176', 'PRASHANT SURESH WAVHAL', NULL, '9309969614', 36, '$2y$10$TQIVa0Kt7rcuRzojgkxbeOLNEpsb72xyg2fIZdGIJ.qr5RZDclIb.', NULL, 'Agent', 0, NULL, NULL),
(269, '52146', 'GURUNATH JAYRAM BANE', NULL, '8446203515', 36, '$2y$10$dMLN3ua2HWiiEcuXlUf0XuC.8pUVBZsgzz21L8TeZU5FqrZZ5KdeG', NULL, 'Agent', 1, NULL, '2021-08-21 08:13:30'),
(270, '52147', 'YOGENDRA PRAKASH PARDESHI', NULL, '9890351469', 36, '$2y$10$TrZezIwwTYq4ZQVOdILzae0yBCo61.fD.tCVgMNqr5lXfW8Ju..GO', NULL, 'Agent', 1, NULL, '2021-09-14 03:58:41'),
(271, '52148', 'AMOL SUDHAKAR KADAM', NULL, '9890999880', 36, '$2y$10$xYTygTtQ1xtstFnoXTaRMu8CwXJZpx7XTEvhP0hhxya4WEdHUREea', NULL, 'Agent', 0, NULL, '2021-08-19 06:34:07'),
(272, '52149', 'MAHENDRA DINKAR KHUDE', NULL, '9765936044', 36, '$2y$10$yjG2I7qIuXYG56CL0rKNT.5.svdWIRLeaUYX1HVrPe.D8/FnHQ/OG', NULL, 'Agent', 0, NULL, '2021-09-23 03:49:26'),
(273, '52151', 'GOPAL CHANDEAKANT KIRAD', 'gopalkirad23@gmail.com', '8208614248', 36, '$2y$10$fE2sl7VTggGIVfdM48GMDOuGGc1YUScDTYlqMwV5xNBO.je7lijYK', NULL, 'Agent', 1, NULL, '2021-09-15 04:30:03'),
(274, '52160', 'MUKESH SUNIL MORE', NULL, '9703791151', 36, '$2y$10$zU78jOPPNFw1pS0Au187jOFD5mxy34/3W18pe35hV0r3zsEc1evIe', NULL, 'Agent', 1, NULL, '2021-10-08 03:15:21'),
(276, '52163', 'KOMAL SANTOSH KAMBLE', NULL, '7821847813', 35, '$2y$10$4sD3MjMDhKbUaMjULNZg.eys..ZIb.kTrpVnllJdNEyuQujGFkSA2', NULL, 'Agent', 0, NULL, '2021-08-20 02:14:43'),
(277, '52164', 'AKSHAY SHANTARAM KURADE', NULL, '7620384174', 36, '$2y$10$Zja3dAYjzlmxa72.ySSRvembywX6dUdY6Fwk68T8GZOmQr09plvNC', NULL, 'Agent', 0, NULL, '2021-08-19 06:27:56'),
(278, '52165', 'PRATHAMESH DASHRATH BELIDKAR', NULL, '8237140203', 36, '$2y$10$WemRc3ULz13Lfn5GNuZ5gOrOXd1sWtxmedDeW8g8iTvMZQDLZ8QXm', NULL, 'Agent', 0, NULL, '2021-10-08 02:21:34'),
(279, '52168', 'PUSHPA RAJESH ZAREKAR', NULL, '8329832266', 35, '$2y$10$u3qSLyJmi3kPirM3TqQHcuHrYTjvGG9zcDeFjQApVU3Z5GP5b5tz.', NULL, 'Agent', 1, NULL, NULL),
(280, '52169', 'GAURAV RAVINDRA SALVEKAR', 'salvekarg1@gmail.com', '7972471063', 35, '$2y$10$jpLyDXJy8rOzY5Yg6gP/7.dEKUp5vpjJBMfT7OuDG3ppbPjwIF76K', NULL, 'Agent', 0, NULL, '2021-09-01 05:54:31'),
(281, '52158', 'RESHMA ARJUN KHARMATE', NULL, '9766240518', 35, '$2y$10$3atOH..N0R1xfQRzHTlfn.mEoVq5e1yssGMeitDaJwu13ZsEPvbMK', NULL, 'Agent', 0, NULL, NULL),
(282, '52174', 'ANKITA ANIL KUMBHAR', 'ankikumbhar@gmail.com', '9970871173', 35, '$2y$10$KyQGveGm0V/ahSHbqLiUvurFPlw/9gxx0XvuQBLiQGA1L0hGiHPzm', NULL, 'Agent', 1, NULL, '2021-10-20 03:11:04'),
(283, '12345686', 'Naveen K', 'balu.khengat@gmail.com', '9637430435', 43, '$2y$10$FcmQErkFYy5RyzTuxOS6KOkP4HxD6Pn2jEXtrO4y8hF/XBBW1tfMW', 'B955kxjXy6rwLxGhz3qmNrHp73ekoluXpeOpk96fEZPpnfbu6ZPjsQ2OW0gA', NULL, 1, NULL, '2021-08-12 12:54:14'),
(285, '52175', 'POOJA UMESH SHELAR', NULL, '9022216902', 35, '$2y$10$1tc.e6y2xgkRK9wYGq8luO8Kv4xCFSL0MY5g11AHG65NqN.U5GlX.', NULL, 'Agent', 1, NULL, '2021-09-08 04:26:42'),
(286, '52167', 'RUSHIKESH HANUMANT PHAD', NULL, '7057053857', 36, '$2y$10$b1K3sK4EjHIaxJKXASSGvujhHoep100F1FywaWUNmFd/kHvMPg2iG', NULL, 'Agent', 0, NULL, '2021-09-04 07:28:07'),
(287, '52150', 'ADITYA SATISH SHINDE', NULL, '7276055459', 36, '$2y$10$BIID1yjUV2358kkBuKGpa.vtMtIrWKM0mHAGWTC2YS54mBnZDz4Sa', NULL, 'Agent', 0, NULL, '2021-09-08 04:24:09'),
(288, '1048', 'RAMESH BIKU SHIRODKAR', NULL, '8369983572', 43, '$2y$10$oIq8U4mGexaWMpOAvABfWevLES/LexUpWfsghqFGUDwSa76J6wC6.', NULL, 'Agent', 0, NULL, NULL),
(292, '52042', 'GANESH SURESH PANCHRAS', NULL, '9011091067', 36, '$2y$10$2r.3T2iF/Lj/kZci/4bQDOADx/FZvBjBgfv3iCscgrI2nrb89bEUm', NULL, 'Agent', 1, NULL, '2021-10-20 01:24:27'),
(293, '52125', 'GAYATRI PANDURANG NIKAM', 'gayatrinikam112217@gmail.com', '9307902942', 35, '$2y$10$7rrKTFP.a40W.QLXEAJtL.eyhijXrZf35kIl3dbfCnEWx90.qXQlO', NULL, 'Agent', 0, NULL, '2021-09-09 07:59:49'),
(294, '52156', 'LALCHAND NATHU MACHEWAL', 'lalchandmanchewal88@gmail.com', '9860395846', 36, '$2y$10$N1vdbfAqce/NwJ6GVpNPyOmEIJLqrAhXWhB/8jlcXpgKzYXvLV6bW', NULL, 'Agent', 0, NULL, '2021-09-11 04:53:38'),
(295, '52155', 'PRADEEP ONKAR BADGE', 'pradeepbadge59340@gmail.com', '9767833701', 36, '$2y$10$ZI1Y01hmnGgtoIBgnUUeQuOCPbxMVA1v.isgTqJo3IcHTPIdWGbtK', NULL, 'Agent', 0, NULL, '2021-09-11 06:39:34'),
(296, '52153', 'SANGITA SATISH BUDHKAR', 'sangitabudhkar@gmail.com', '9823321726', 35, '$2y$10$1LIjI8yz1OoFl/MZDxndFOAQhGue96pMyJEXPzy19ZjSaFDJtxfhe', NULL, 'Agent', 0, NULL, '2021-09-11 05:55:46'),
(297, '52159', 'IMRAN MAJJID SHIKALGAR', NULL, '9673040020', 36, '$2y$10$pHGRdlbJQEFF3LSXssA8JOCKQFffcFx8b78oSrhtP1ohGdK8YPK8G', NULL, 'Agent', 0, NULL, NULL),
(298, '52178', 'SATYAJEET SANTOSHSINGH RAJPUT', NULL, '7393909494', 44, '$2y$10$BusdtOVcu7TnfPhl.Ofzv.C6nUKUebiaRamWXetS2tKWULFS455Ju', NULL, 'Agent', 1, NULL, '2021-10-21 01:11:46'),
(299, 'TEST123', 'Gaurav', 'gaurav@dmstechno.com', '9673826622', 24, '$2y$10$ofGArYa8pvfzh3kHj6vz1umTttGeAk34Np63UXGjug.Qktop6OI0K', '922a59de6e6860215c33d32099002ee4', NULL, 1, NULL, '2021-09-11 07:19:49'),
(300, '52145', 'SOHEB ABDULRAHIM SHAIKH', NULL, '9834958327', 36, '$2y$10$cR.f/6kre/BToNzXG.Z3DOydg3UpeiZi6GV/8N5u7te5K01.ElPcO', NULL, 'Agent', 1, NULL, '2021-10-08 03:15:13'),
(302, '52200', 'SHRAVAN PRAVIN BADHE', NULL, '9697635635', 44, '$2y$10$XL70oSsgpquEmN0Z0Nsb1OhFbe9rqJrrmu4VKKfn4WIc/.SvcapTq', NULL, 'Agent', 1, NULL, '2021-10-04 08:25:55'),
(303, '52188', 'SHUBHADA DYANESHWAR PATIL', 'shubhadapatil_229@rediffmail.com', '8087580229', 35, '$2y$10$C5e7CSkOGmVdRkVVErgVvOrTetsJJE8J5./MGp1a/LHIl1lGaBvja', NULL, 'Agent', 0, NULL, '2021-10-08 02:35:00'),
(304, '52189', 'ASHVINI S NAIK', NULL, '9970622783', 35, '$2y$10$eRyCsvk7mfLQvRuC8tFYyexTCZ.IHdSoEB11F5slKwNSJyDfdpr66', NULL, 'Agent', 1, NULL, '2021-10-08 03:15:32'),
(305, '52157', 'DATTATRAY MANSING AHIREKAR', NULL, '9545249724', 36, '$2y$10$.cCGuQ8BZ0d.EBQR4h1LoeRzVwtlBM4qIBuIAlcbaPG6nteaMgUkO', NULL, 'Agent', 0, NULL, '2021-09-23 05:20:24'),
(306, '52184', 'AKASH SURESH CHAVAN', NULL, '7276879593', 34, '$2y$10$e68BCpclNUePEk77LDXzwu0cU/tOb8RIL0guzdZoSjFpaxtZEqBNO', NULL, 'Agent', 0, NULL, NULL),
(307, '52177', 'TEJAS AMIT SAWANT', NULL, '9373736425', 38, '$2y$10$r3R3tQc5vUk/HPaZXv44FuRWT/2spvIWQ3BmLg15oq/iFuGWCqOfy', NULL, 'Agent', 0, NULL, '2021-09-28 03:05:56'),
(308, '52186', 'VAIBHAV VISHNU KADAM', NULL, '9822668001', 36, '$2y$10$gQ./IwiyVcW0z9clCA6GIuGaJ6VaZEeN5rXNtT/uQkX7OtmKhWAA6', NULL, 'Agent', 0, NULL, '2021-09-06 07:28:24'),
(309, '52183', 'RAVI RANGAPPA TALWAR', NULL, '9049051004', 36, '$2y$10$bkR3qoZON.R0fEYKzhaGqOORy.Z8xar2f1u/E5L4hJu441FewoQ5W', NULL, 'Agent', 0, NULL, '2021-10-02 04:11:28'),
(310, '52191', 'KETAL BAPU PATIL', 'ketanpatil1115@gmail.com', '9637795679', 36, '$2y$10$NzwveSYQCojUx2QomXB4we/xY8yWW/kYupXrz.zokR77D37RJlvGy', NULL, 'Agent', 0, NULL, NULL),
(311, '52193', 'SUJIT SAMBHAJI KENGAR', 'kengarsujit71@gmail.com', '8888889045', 36, '$2y$10$6/hCFaYJ59GrzCMJhnhK9.vNe7hGBlrNX0QuduujJ1GUYTTTGJ4ru', NULL, 'Agent', 1, NULL, '2021-10-12 09:07:27'),
(312, '52196', 'SANA ASLAM SHAIKH', 'shaikhsana762@yahoo.com', '9657066590', 35, '$2y$10$D5WyA/2aL.6aCvhRguW4Xexiq.EO5OlrYzOd/3v/f.RBWwyEsVmji', NULL, 'Agent', 0, NULL, '2021-10-05 09:42:13'),
(313, '52138', 'VAISHNAVI SANTOSH FALKE', 'vaishnavifalke217@gmail.com', '9011962058', 35, '$2y$10$ga6E7DbCQ6PSrBwmDumwLOZbyqwg8DruB7wpEK9CjbZw1ABzwkJl.', NULL, 'Agent', 0, NULL, '2021-09-23 03:17:05'),
(314, '52195', 'SHAMA KUTBUDDIN SIDDIQUI', 'siddiquishama1234@gmail.com', '8390294906', 35, '$2y$10$Yhf00LkeSg1/sZHMTOGk2.OFDG5lS3W8orVFWnH4CykteWY0t1JDy', NULL, 'Agent', 0, NULL, '2021-10-05 09:36:58'),
(315, '52161', 'PRASHANT LIMBRAJ SHEDOLKAR', NULL, '7397976167', 36, '$2y$10$LFOVrLX2xUoJXE6Y9c9jM.nQKv9I3okSj8nCuOh2CUkXHpjv987JS', NULL, 'Agent', 1, NULL, '2021-09-11 08:43:42'),
(316, '52204', 'RAJ CHANDRAKANT RASKAR', 'raj.c.raskar@gmail.com', '8329912908', 36, '$2y$10$BqrFpA/1WbOd9tM5UsUqVODZhmf4lXEX0wiUY6i4e3lXsHgLRrwuu', NULL, 'Agent', 0, NULL, '2021-09-28 02:53:46'),
(317, '52205', 'VIDYA GOVARDHAN PATHARE', 'patharevidya26@gmail.com', '9325670238', 35, '$2y$10$AiWkUVH1KwWiSfPZpwHIr.4ODiG8ZtT6gKsRI90oPwjPJ76SJ1L5i', NULL, 'Agent', 0, NULL, NULL),
(318, '52185', 'AKASH MAHADU SHINDE', 'akashinde122@gmail.com', '9112847533', 36, '$2y$10$Fn6tiYocI3hQa0B/YEtQjeKTbb/y1DZHcX8ez.ea7Dbc6OhI1K94y', NULL, 'Agent', 0, NULL, '2021-10-26 07:10:06'),
(319, '52203', 'RIYAZZ ABDUL GAFFAR SHAIKH', 'shaireyaz333@gmail.com', '9595829318', 36, '$2y$10$DkI/.DN6OvCtKucWV8FF1elY3KqFscbP9eZX1O2L5j2QN49fDneQq', NULL, 'Agent', 1, NULL, '2021-11-12 06:50:29'),
(320, '52199', 'MEGHANA AMOL  RAUT', 'meghanaraut62@gmail.com', '7499845374', 35, '$2y$10$iK2rI6OeMrCxdrw2XPrRu.NeKcqOdlcaL.kKr39wxayJYdP9BpCNG', NULL, 'Agent', 0, NULL, NULL),
(321, '1047', 'POONAM NAMDEV KHANDEKAR', 'poonamkhandekar02@gmail.com', '9326947594', 35, '$2y$10$4jQzPVoMZatDsbbUbemWl.95q2foKv72E/e9G6y3o..c4i5VjE7Ya', NULL, 'Agent', 0, NULL, '2021-10-08 04:15:00'),
(322, '52094', 'HAUSRAO NAVNATH MAHAJAN', 'hnmahajan@yahoo.com', '9730049057', 36, '$2y$10$uPVNlrTxuVRLBepoU2RlK.MrK4u8PRgMvIqj1tx1if8TYOUXBAD9a', NULL, 'Agent', 0, NULL, '2021-10-08 07:05:41'),
(323, '52190', 'RAHUL BALKRUSHNA NIKAM', 'rahulnikam75930@gmail.com', '9730508003', 40, '$2y$10$JrRJvhaPKdhl6vOK4mIwOexXF.NuvjMEiLrmzBAlfkFBd96qlu4jS', NULL, NULL, 0, NULL, NULL),
(324, '52209', 'SHEKHAR DATTARAM GADANKUSH', 'shekhargadankush5@gmail.com', '9766676979', 36, '$2y$10$lpLh01Vp0Wkmq4uM8SZGieiCvemoCgvvDg3tg0jB0VrPkIz3SSDfe', NULL, 'Agent', 0, NULL, NULL);
INSERT INTO `users` (`id`, `employee_id`, `name`, `email`, `mobile`, `designation_id`, `password`, `remember_token`, `status`, `isDeleted`, `created_at`, `updated_at`) VALUES
(325, '51646', 'SRINIWAS RAMCHANDRA BALGAR', 'sriniwasbaglar7174@gmail.com', '7517799819', 36, '$2y$10$yrD5oPt2a9NNcFlW8mdMTOesjJ6jaBTbfjiT8NHb2Eyi195Iwt75S', NULL, 'Agent', 0, NULL, '2021-10-09 06:48:08'),
(326, '52206', 'ANKITA SANTOSH BHOLE', NULL, '8956596613', 35, '$2y$10$yeX1TqpRKDTC9PACXag.ruRcisNPZH6CkT9rnkWx1ztYY5hHhLSDq', NULL, 'Agent', 0, NULL, '2021-11-12 06:49:34'),
(327, '52198', 'PRASAD ASHOK SONAWANE', 'sprasad0386@gmail.com', '8485823891', 36, '$2y$10$ksOntAOdxYMduNR6TfGe..Xmuaan0Gr6JR38AiVslsBYZ1HgiXFeG', NULL, 'Agent', 0, NULL, NULL),
(328, '52212', 'PRASAD ASHOK SONAWANE', 'anushree311113@gmail.com', '8329750062', 36, '$2y$10$PYHJPFcV78XprhvKXwqtWuYbrebAuBKXjQUkbkHAkuZhylyJYaR..', NULL, 'Agent', 1, NULL, '2021-11-12 07:04:08');

-- --------------------------------------------------------

--
-- Table structure for table `variable_users`
--

CREATE TABLE `variable_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bank_uploads`
--
ALTER TABLE `bank_uploads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `ptx_areas`
--
ALTER TABLE `ptx_areas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ptx_assign_banks`
--
ALTER TABLE `ptx_assign_banks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bank_id` (`bank_id`),
  ADD KEY `area_id` (`area_id`);

--
-- Indexes for table `ptx_attendance`
--
ALTER TABLE `ptx_attendance`
  ADD PRIMARY KEY (`attendance_id`);

--
-- Indexes for table `ptx_banks`
--
ALTER TABLE `ptx_banks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ptx_branches`
--
ALTER TABLE `ptx_branches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK` (`company_id`);

--
-- Indexes for table `ptx_bucket`
--
ALTER TABLE `ptx_bucket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ptx_cash_collections`
--
ALTER TABLE `ptx_cash_collections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bank_id` (`bank_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `telecaller` (`telecaller`),
  ADD KEY `agent` (`agent`),
  ADD KEY `field_executive_id` (`field_executive_id`);

--
-- Indexes for table `ptx_cash_collection_details`
--
ALTER TABLE `ptx_cash_collection_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ptx_companies`
--
ALTER TABLE `ptx_companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ptx_companyids`
--
ALTER TABLE `ptx_companyids`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ptx_departments`
--
ALTER TABLE `ptx_departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ptx_designations`
--
ALTER TABLE `ptx_designations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `designations_dept_id_foreign` (`dept_id`);

--
-- Indexes for table `ptx_documents`
--
ALTER TABLE `ptx_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `documents_user_id_foreign` (`user_id`);

--
-- Indexes for table `ptx_dras`
--
ALTER TABLE `ptx_dras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `ptx_employee_salary`
--
ALTER TABLE `ptx_employee_salary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ptx_failed_jobs`
--
ALTER TABLE `ptx_failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ptx_import_data_files`
--
ALTER TABLE `ptx_import_data_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `loan_no` (`loan_no`),
  ADD KEY `product_id` (`product_id`) USING BTREE,
  ADD KEY `assignee` (`agent_id`),
  ADD KEY `telecaller_id` (`telecaller_id`),
  ADD KEY `field_executive_id` (`field_executive_id`),
  ADD KEY `bank_id` (`bank_id`);

--
-- Indexes for table `ptx_import_data_files_temp`
--
ALTER TABLE `ptx_import_data_files_temp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `loan_no` (`loan_no`),
  ADD KEY `product_id` (`product_id`) USING BTREE;

--
-- Indexes for table `ptx_lead_history`
--
ALTER TABLE `ptx_lead_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `loan_no` (`loan_no`),
  ADD KEY `agent_id` (`agent_id`),
  ADD KEY `telecaller_id` (`telecaller_id`),
  ADD KEY `field_executive_id` (`field_executive_id`);

--
-- Indexes for table `ptx_loan_comment`
--
ALTER TABLE `ptx_loan_comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `loan_no` (`loan_no`);

--
-- Indexes for table `ptx_menu`
--
ALTER TABLE `ptx_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ptx_menus`
--
ALTER TABLE `ptx_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ptx_migrations`
--
ALTER TABLE `ptx_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ptx_otp_verification`
--
ALTER TABLE `ptx_otp_verification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ptx_products`
--
ALTER TABLE `ptx_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ptx_product_categories`
--
ALTER TABLE `ptx_product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ptx_product_short_names`
--
ALTER TABLE `ptx_product_short_names`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ptx_pvrs`
--
ALTER TABLE `ptx_pvrs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pvrs_user_id_foreign` (`user_id`);

--
-- Indexes for table `ptx_user_details`
--
ALTER TABLE `ptx_user_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_details_user_id_foreign` (`user_id`),
  ADD KEY `user_details_area_id_foreign` (`area_id`);

--
-- Indexes for table `ptx_user_roles`
--
ALTER TABLE `ptx_user_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_role_designations_id_foreign` (`designations_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank_uploads`
--
ALTER TABLE `bank_uploads`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `ptx_areas`
--
ALTER TABLE `ptx_areas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `ptx_assign_banks`
--
ALTER TABLE `ptx_assign_banks`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;

--
-- AUTO_INCREMENT for table `ptx_attendance`
--
ALTER TABLE `ptx_attendance`
  MODIFY `attendance_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=572;

--
-- AUTO_INCREMENT for table `ptx_banks`
--
ALTER TABLE `ptx_banks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `ptx_branches`
--
ALTER TABLE `ptx_branches`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `ptx_bucket`
--
ALTER TABLE `ptx_bucket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `ptx_cash_collections`
--
ALTER TABLE `ptx_cash_collections`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ptx_cash_collection_details`
--
ALTER TABLE `ptx_cash_collection_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ptx_companies`
--
ALTER TABLE `ptx_companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ptx_companyids`
--
ALTER TABLE `ptx_companyids`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `ptx_departments`
--
ALTER TABLE `ptx_departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `ptx_designations`
--
ALTER TABLE `ptx_designations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `ptx_documents`
--
ALTER TABLE `ptx_documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT for table `ptx_dras`
--
ALTER TABLE `ptx_dras`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=187;

--
-- AUTO_INCREMENT for table `ptx_employee_salary`
--
ALTER TABLE `ptx_employee_salary`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `ptx_failed_jobs`
--
ALTER TABLE `ptx_failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ptx_import_data_files`
--
ALTER TABLE `ptx_import_data_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `ptx_import_data_files_temp`
--
ALTER TABLE `ptx_import_data_files_temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ptx_lead_history`
--
ALTER TABLE `ptx_lead_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ptx_loan_comment`
--
ALTER TABLE `ptx_loan_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ptx_menu`
--
ALTER TABLE `ptx_menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `ptx_migrations`
--
ALTER TABLE `ptx_migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `ptx_otp_verification`
--
ALTER TABLE `ptx_otp_verification`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `ptx_products`
--
ALTER TABLE `ptx_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `ptx_product_categories`
--
ALTER TABLE `ptx_product_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `ptx_product_short_names`
--
ALTER TABLE `ptx_product_short_names`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=439;

--
-- AUTO_INCREMENT for table `ptx_pvrs`
--
ALTER TABLE `ptx_pvrs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=217;

--
-- AUTO_INCREMENT for table `ptx_user_details`
--
ALTER TABLE `ptx_user_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=310;

--
-- AUTO_INCREMENT for table `ptx_user_roles`
--
ALTER TABLE `ptx_user_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=833;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=329;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ptx_designations`
--
ALTER TABLE `ptx_designations`
  ADD CONSTRAINT `designations_dept_id_foreign` FOREIGN KEY (`dept_id`) REFERENCES `ptx_departments` (`id`);

--
-- Constraints for table `ptx_documents`
--
ALTER TABLE `ptx_documents`
  ADD CONSTRAINT `documents_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `ptx_pvrs`
--
ALTER TABLE `ptx_pvrs`
  ADD CONSTRAINT `pvrs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `ptx_user_roles`
--
ALTER TABLE `ptx_user_roles`
  ADD CONSTRAINT `user_role_designations_id_foreign` FOREIGN KEY (`designations_id`) REFERENCES `ptx_designations` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
